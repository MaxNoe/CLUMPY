// CLUMPY includes
#include "../include/clumps.h"
#include "../include/janalysis.h"
#include "../include/jeans_analysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"

// ROOT includes
#include <TCanvas.h>
#include <TGraph.h>
#include <TH1.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TMultiGraph.h>
#include <TRandom3.h>

// C++ std libraries
using namespace std;
#include <math.h>

double f_point(double x, double R)
{
   return 4.*3.1415926 * pow(R, 3) / (3.*pow(x, 2.));
}
double b(double f)
{
   return 1. - 2.*f + 3.*pow(f, 2.);
}
double f_true(double D, double R)
{
   double B = 1. - (pow(R, 2.) / pow(D, 2.));
   double alpha_max = atan(R / D);
   return 2.*3.1415926 * D * (1.*sqrt(1 * 1 - B) - B * log(2.*(sqrt(1.*1. - B) + 1.))
                              - (cos(alpha_max) * sqrt(cos(alpha_max) * cos(alpha_max) - B)
                                 - B * log(2.*(sqrt(cos(alpha_max) * cos(alpha_max) - B) + cos(alpha_max)))));
}

//______________________________________________________________________________
void test_load_kindata()
{
   //--- Loads kinetic data in structure.

   string file = "data/dataLight.txt";

   struct gStructJeansData data_halo;
   halo_load_data4jeans(file, data_halo, 1);

   for (int n = 0; n < int(data_halo.Radius.size()); n++) {
      cout << " radius " << data_halo.Radius[n] << "   errradius " << data_halo.ErrRadius[n];
      if (data_halo.TypeData == "SB")
         cout << " SB " << data_halo.LightData[n] << "   dSB " << data_halo.ErrLightData[n] << endl;
      else
         cout << " veloc " << data_halo.VelocData[n] << "   errveloc " << data_halo.ErrVelocData[n] << endl;
      if (data_halo.TypeData == "Vel") {
         //cout << " ra " << data_halo.RA[n] << endl;
         //cout << " dec " << data_halo.Dec[n] << endl;
         cout << " membership " << data_halo.MembershipProb[n] << endl;
      }

   }

   cout << "  => TYPEDATA = " << data_halo.TypeData << endl;
}

//______________________________________________________________________________
void test_mcmc_file()
{
   //--- Loads MCMC analysis file in structure.

   string file = "stat_Fornax.txt";
   struct gStructHalo data_stat;

   stat_load_list(file, data_stat, 1);

   cout << "name=" << data_stat.Name << "  distance=" << data_stat.l;
   cout << "psi=" << data_stat.PsiDeg << "  theta=" << data_stat.ThetaDeg;
   cout << "ndata=" << data_stat.Ndata << "  npar=" << data_stat.Npar << endl << endl;
   cout << "IsRhos= " << data_stat.StatIsFreeParam[0] << endl;
   cout << "IsRscale = " << data_stat.StatIsFreeParam[1] << endl;
   cout << "IsShapeParam1=" << data_stat.StatIsFreeParam[3] << endl;
   cout << "IsShapeParam2=" << data_stat.StatIsFreeParam[4] << endl;
   cout << "IsShapeParam3=" << data_stat.StatIsFreeParam[5] << endl;
   cout << "IsL=" << data_stat.StatIsFreeParam[6] << endl;
   cout << "IsRLight=" << data_stat.StatIsFreeParam[7] << endl;
   cout << "IsLightShapeParam1=" << data_stat.StatIsFreeParam[8] << endl;
   cout << "IsLightShapeParam2=" << data_stat.StatIsFreeParam[9] << endl;
   cout << "IsLightShapeParam3=" << data_stat.StatIsFreeParam[10] << endl;
   cout << "IsBeta_Aniso_0=" << data_stat.StatIsFreeParam[11] << endl;
   cout << "IsBeta_Aniso_Inf=" << data_stat.StatIsFreeParam[12] << endl;
   cout << "IsRAniso=" << data_stat.StatIsFreeParam[13] << endl;
   cout << "IsAnisoShapeParam=" << data_stat.StatIsFreeParam[14] << endl;
   cout << "IsHyper=" << data_stat.StatIsFreeParam[15] << endl;

   for (int i = 0; i < int(data_stat.StatChi2.size()); i++) {
      cout << "  i=" << i <<  "     (chi2 = " << data_stat.StatChi2[i] << ")" << endl;
      // DM params
      cout << "     rhos=" << data_stat.StatParam[0][i]
           << " rscale=" << data_stat.StatParam[1][i]
           << " rvir=" << data_stat.StatParam[2][i]
           << " profile=" << gNAMES_PROFILE[data_stat.StatHaloProfile[i]]
           << " shape1=" << data_stat.StatParam[3][i]
           << " shape2=" << data_stat.StatParam[4][i]
           << " shape3=" << data_stat.StatParam[5][i] << endl;
      // Light params
      cout << "     L0=" << data_stat.StatParam[6][i]
           << " rL=" << data_stat.StatParam[7][i]
           << " lightprofile=" << gNAMES_LIGHTPROFILE[data_stat.StatLightProfile[i]]
           << " lightshape1=" << data_stat.StatParam[8][i]
           << " lightshape2=" << data_stat.StatParam[9][i]
           << " lightshape3=" << data_stat.StatParam[10][i] << endl;
      // Anisotrotpy
      cout << "     beta0 = " << data_stat.StatParam[11][i]
           << " betainf=" << data_stat.StatParam[12][i]
           << " anisoprofile=" << gNAMES_ANISOTROPYPROFILE[data_stat.StatAnisoProfile[i]]
           << " rani=" << data_stat.StatParam[13][i]
           << " eta=" << data_stat.StatParam[14][i] << endl;
      // Other
      cout << "     hyper=" << data_stat.StatParam[15][i] << endl;
   }
}

//______________________________________________________________________________
void test_random_integer(TApplication *my_app)
{
   //--- Test of random integer generation.

   TCanvas *canvas = new TCanvas("Resultats", "", 200, 10, 700, 500);
   int seed = 7;
   TRandom3 *ran = new TRandom3(seed);

   double nmax = 3220.;
   TH1F *h1 = new TH1F("h1", "", 100, 0, nmax - 1.);
   double dx = (nmax - 1. - 0.) / (double)100.;
   int N = 1.e8;
   double scale = 1. / ((double)N * dx);
   double y = 0.;
   int z = 0;
   for (int i = 0; i < N; i++) {
      y = ran->Uniform(0., 1.*nmax);
      //      double intpart = 0.;
      //      double fracpart = modf (y*nmax , &intpart);
      //      printf ("%f = %f + %f \n", y*nmax, intpart, fracpart);
      z = int(y);
      //      cout << z << endl;
      h1->Fill(z);
   }
   h1->Scale(scale);
   h1->Draw();

   canvas->Update();
   my_app->Run(kTRUE);
}

//______________________________________________________________________________
void test_new_zhao_mass_calculation()
{
   //--- Test for new mass calculation (hypergeom function for Zhao).
   //  par[0]     Dark matter density normalisation: density [Msol/kpc^3]
   //  par[1]     Dark matter density Scale radius [kpc]
   //  par[2]     Dark matter Shape parameter #1
   //  par[3]     Dark matter Shape parameter #2
   //  par[4]     Dark matter Shape parameter #3
   //  par[5]     Dark matter card_profile [gENUM_PROFILE]
   //  par[6]     Max radius of integration

   const int n_par = 7;
   double par[n_par];
   par[0] = 1.040334e+10;
   par[1] = 5.038083e-01;
   par[2] = 6.938644e-01;
   par[3] = 6.753814e+00;
   par[4] = 7.188156e-01;
   par[5] = kZHAO;
   double eps = 0.01;

   // Grid of mass values
   const int n = 30;
   double r_min = 0.1;
   double r_max = 4.;
   double pas_lin_r = (r_max - r_min) / double(n - 1.);
   double r[n];

   cout << "   NB: relative precision for integration is " << eps << endl;
   for (int i = 0; i < n; i++) {
      r[i] = r_min + pas_lin_r * i;
      double rmax = par[6]; // we save the value of maximun radius of integration
      par[6] = r[i]; // radius where to calculate the mass
      double M_int = mass_singlehalo(par, eps, true);
      double M_hypergeom = mass_singlehalo(par, eps, false);
      par[6] = rmax; // we re-affect its initial value at par[6]
      cout << "   Mass(<" << r[i] << ") = "
           << M_int << " (integration)  vs  "
           << M_hypergeom << " (using hypergeom func) " << endl;
   }
}

//______________________________________________________________________________
void test_for_matt_code()
{
   //--- Check for Matt FORTRAN code.

   // Set here jeans parameters (DM, light, anisotropy) used in test functions
   const int n_par = 20;
   double par[n_par];
   par[0] = 4.080350e+07;
   par[1] =  0.01;
   par[2] = 9.605012e-01;
   par[3] = 3.;
   par[4] = 0.5;
   par[5] = kEINASTO;
   par[6] = 100.;
   par[7] = 1e+06;
   par[8] = 0.1;
   par[9] = 1.18288;
   par[10] = 4.30536;
   par[11] = 0.304093;
   par[12] = kZHAO3D;
   par[18] = 1.;
   par[19] = 0.005;
   par[15] = 0.;
   par[16] = 1.;
   par[17] = 1.e-2;
   par[18] = 2.;
   par[19] = kCONSTANT;

   const int n = 30;
   double r_min = 0.1;
   double r_max = 4.;
   double pas_lin_r = (r_max - r_min) / (double)n;
   double r[n];
   double I = 0.;
   double nu_vr2[n];
   double sigmap2[n];

   double R_temp = par[13];
   for (int i = 0; i < n; i++) {
      r[i] = r_min + pas_lin_r * i;
      I = light_i(r[i], &par[7]);
      nu_vr2[i] = jeans_nuvr2(r[i], par);
      par[19] = r[i];
      sigmap2[i] = jeans_sigmap2(r[i], par);
      sigmap2[i] *= 6.67703e-29 * 9.52136e32;
      par[19] = R_temp;
      cout << "r = " << r[i] << "     I = " << I << "     nuvr2 = " << nu_vr2[i] << "     sigmap2 = " << sigmap2[i] <<  endl;
   }
}

//______________________________________________________________________________
void test_beta_anisotropy(TApplication *my_app)
{
   //--- Plots of beta_anisotropy(r).

   double eps = 0.01;

   // Grid in r
   const int n = 100;
   double rmin = 1.e-2;
   double rmax = 5.;
   double pas_log_r = pow(rmax / rmin, 1. / double(n - 1));
   double r[n];
   for (int i = 0; i < n; i++)
      r[i] = rmin * pow(pas_log_r, i);

   // Anisotropy parameters
   const int n_par = 5;
   double par[n_par];
   par[0] = -1.; // Anisotropy at r=0
   par[1] = 1.;  // Anisotropy at r=+infinity
   par[2] = 0.1; // Anisotropy scale radius ra [kpc]
   par[3] = 3.;  // Anisotropy shape parameter eta
   par[4] = 1;   // Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)

   const int n_plots = 3;
   TCanvas *cbeta[n_plots];
   TMultiGraph *mg[n_plots];
   TLegend *leg[n_plots];
   double qty[n];
   string name_canvas[n_plots] = {"beta_anisotropy_profile", "beta_anisotropy_profile_fr", "beta_anisotropy_profile_kernel"};
   string xtitle[n_plots] = {"r [kpc]", "r [kpc]", "r [kpc]"};
   string ytitle[n_plots] = {"#beta_{anisotropy}(r)", "f(r)#propto exp[#int^{r} 2#beta_{ani}(t)/t dt]", "K(r/R,r_{a}/R)"};
   string gr_name_pre[n_plots] = {"beta_anis", "beta_anis_fr", "beta_anis_kernel"};

   for (int c = 0; c < 2; ++c) {
      leg[c] = new TLegend(0.15, 0.7, 0.4, 0.95);
      leg[c]->SetFillColor(0);
      leg[c]->SetBorderSize(0);
      leg[c]->SetTextFont(132);
      leg[c]->SetTextSize(0.04);

      cbeta[c] = new TCanvas(name_canvas[c].c_str(), name_canvas[c].c_str(), 5 + 650 * c, 5, 650, 500);
      cbeta[c]->SetLogx(1);
      cbeta[c]->SetLogy(0);
      if (c == 1)
         cbeta[c]->SetLogy(1);

      mg[c] = new TMultiGraph();

      for (int k = 0; k < gN_ANISOTROPYPROFILE; ++k) {
         // No kernel for BAES
         if (c == 2 && k == kBAES)
            continue;

         if (k == kCONSTANT)
            par[0] = -0.5;
         else
            par[0] = -1.3;
         par[4] = k;
         for (int i = 0; i < n; i++) {
            if (c == 0)
               qty[i] = beta_anisotropy(r[i], par);
            else if (c == 1)
               qty[i] = beta_anisotropy_fr(r[i], par, eps);
            else if (c == 2)
               qty[i] = beta_anisotropy_kernel(r[i], par, 3.);
         }

         TGraph *gr_beta = new TGraph(n, r, qty);
         string name = "gr_" + gr_name_pre[c] + "_" + gNAMES_ANISOTROPYPROFILE[k];
         //cout << name << endl;
         gr_beta->SetTitle("");
         gr_beta->SetName(name.c_str());
         gr_beta->SetFillColor(0);
         gr_beta->SetLineColor(rootcolor(k));
         gr_beta->SetLineWidth(3);
         gr_beta->SetLineStyle(k + 1);
         mg[c]->Add(gr_beta, "L");
         string legend = beta_anisotropy_legend(par, true);
         TLegendEntry *entry = leg[c]->AddEntry(name.c_str(), legend.c_str(), "L");
         entry->SetTextColor(rootcolor(k));
         entry->SetLineColor(rootcolor(k));
         entry->SetLineWidth(3);
         entry->SetLineStyle(k + 1);
      }
      mg[c]->Draw("AL");
      mg[c]->GetXaxis()->SetTitle(xtitle[c].c_str());
      mg[c]->GetYaxis()->SetTitle(ytitle[c].c_str());
      mg[c]->GetXaxis()->CenterTitle(true);
      mg[c]->GetYaxis()->CenterTitle(true);
      leg[c]->Draw();

      cbeta[c]->Update();
   }
   my_app->Run(kTRUE);
}

//______________________________________________________________________________
void test_light_profile(TApplication *my_app)
{
   //--- Plots of 2D and 2D light profiles.

   // Grid in r
   const int n = 50;
   double rmin = 1.e-3;
   double rmax = 100.;
   double pas_log_r = pow(rmax / rmin, 1. / double(n - 1));
   double r[n];
   for (int i = 0; i < n; i++)
      r[i] = rmin * pow(pas_log_r, i);
   double eps = 0.05;

   // Light profile parameters
   const int n_par = 8;
   double par[n_par];

   par[0] = 2.600000e+05;       // Light profile 1st free par (usually normalisation)
   par[1] = 0.026184;       // Light profile 2nd free par (usually scale radius [kpc])
   par[2] = 2.21935;       // Light profile 3rd free par (if used)
   par[3] =  12.6069;      // Light profile 4th free par (if used)
   par[4] =  0.746626;     // Light profile 5th free par (if used)
   par[5] =  kZHAO3D;  // Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   par[6] =  2000.; // If not analytical, maximum radius for integration [kpc]
   par[7] =  eps;  // If not analytical, relative precision for the integration

   const int n_plots = 2;
   TCanvas *clight[n_plots];
   TMultiGraph *mg[n_plots];
   TLegend *leg[n_plots];
   double qty[n];
   string name_canvas[n_plots] = {"light_profile_IR_2D", "light_profile_nur_3D"};
   string xtitle[n_plots] = {"R [kpc]", "r [kpc]"};
   string ytitle[n_plots] = {"Projected:  I(R) / I(r_{c})", "De-projected:  #nu(r) / #nu(r_{c})"};
   string gr_name_pre[n_plots] = {"IR", "nur"};

   for (int c = 0; c < n_plots; ++c) {
      leg[c] = new TLegend(0.15, 0.15, 0.4, 0.7);
      leg[c]->SetFillColor(0);
      leg[c]->SetBorderSize(0);
      leg[c]->SetTextFont(132);
      leg[c]->SetTextSize(0.04);

      clight[c] = new TCanvas(name_canvas[c].c_str(), name_canvas[c].c_str(), 5 + 650 * c, 5, 650, 500);
      clight[c]->SetLogx(1);
      clight[c]->SetLogy(1);

      mg[c] = new TMultiGraph();

      for (int k = 0; k < gN_LIGHTPROFILE; ++k) {
         if (k != kZHAO3D)
            continue;
         if (k == 2)
            par[2] = rmax;
         else
            par[2] = 1.;
         par[5] = k;
         double ref = par[1];
         double norm = 1.;

         // Loop on point and calculate time
         clock_t begin = clock();
         for (int i = 0; i < n; i++) {
            if (c == 0) {
               if (i == 0)
                  norm = light_nu(ref, par);
               qty[i] = light_nu(r[i], par) / norm;
            } else if (c == 1) {
               if (i == 0)
                  norm = light_i(ref, par);
               qty[i] = light_i(r[i], par) / norm;
//               cout << r[i] << "    " << light_i(r[i], par) << endl;
            }
            if (qty[i] < 1.e-40)
               qty[i] = 1.e-40;
            //cout << "  " << r[i] << "  " <<  qty[i] << endl;
         }
         clock_t end = clock();
         double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
         cout << " Time spent = " << time_spent << endl;

         TGraph *gr = new TGraph(n, r, qty);
         string name = "gr_" + gr_name_pre[c] + "_" + gNAMES_LIGHTPROFILE[k];
         //cout << name << endl;
         gr->SetTitle("");
         gr->SetName(name.c_str());
         gr->SetFillColor(0);
         gr->SetLineColor(rootcolor(k));
         gr->SetLineWidth(3);
         gr->SetLineStyle(k + 1);
         mg[c]->Add(gr, "L");
         string legend = light_profile_legend(par, true);
         TLegendEntry *entry = leg[c]->AddEntry(name.c_str(), legend.c_str(), "L");
         entry->SetTextColor(rootcolor(k));
         entry->SetLineColor(rootcolor(k));
         entry->SetLineWidth(3);
         entry->SetLineStyle(k + 1);
      }
      mg[c]->Draw("AL");
      mg[c]->GetXaxis()->SetTitle(xtitle[c].c_str());
      mg[c]->GetYaxis()->SetTitle(ytitle[c].c_str());
      mg[c]->GetXaxis()->CenterTitle(true);
      mg[c]->GetYaxis()->CenterTitle(true);
      mg[c]->SetMinimum(1.e-5);
      mg[c]->SetMaximum(20.);

      leg[c]->Draw();
      clight[c]->Update();
   }
   my_app->Run(kTRUE);
}

//______________________________________________________________________________
void test_nuvr2_sigmap(TApplication *my_app)
{
   //--- Plots of nuvr2 and sigmap2 (both the value and integrand).

   // Grid in r
   const int n = 100;
   double r_min = 1.e-3;
   double r_max = 10.;
   double pas_log_r = pow(r_max / r_min, 1. / double(n - 1));
   double r[n];
   for (int j = 0; j < n; ++j)
      r[j] = r_min * pow(pas_log_r, j);

   double rmax = 2.000000e+03;
   double eps = 0.01;

   // Parameters for the Jeans analysis
   const int n_par = 20;
   double par[n_par];
   // DM parameters
   //   par[0]= 1.040334e+10;
   //   par[1] = 1.038083e-03;
   //   par[2] = 6.938644e-01;
   //   par[3] = 6.753814e+00;
   //   par[4] = 7.188156e-01;
   //   par[5] = kZHAO;

   par[0] = 1.017115e+09; // par[0] Dark matter profile normalisation [Msol/kpc^3]
   par[1] =  7.862192e-02;        // par[1] Dark matter profile scale radius [kpc]
   par[2] = 1.469225e-01; // par[2] Dark matter shape parameter #1
   par[3] = 4.000000e+00;           // par[3] Dark matter shape parameter #2
   par[4] = 1.000000e+00;          // par[4] Dark matter shape parameter #3
   par[5] = kEINASTO;     // par[5] Dark matter card_profile [gENUM_PROFILE] (see params.h)
   par[6] = rmax;         // par[6] Max radius of integration
   // Light profile parameters
   par[7] = 1.000000e+03;           // Light profile 1st free par (usually normalisation)
   par[8] = 6.352768e-02;           // Light profile 2nd free par (usually scale radius [kpc])
   par[9] =  2.346510e+00;           // Light profile 3rd free par (if used)
   par[10] = 1.179549e+01;          // Light profile 4th free par (if used)
   par[11] = 8.966846e-01;         // Light profile 5th free par (if used)
   par[12] = kZHAO3D;           // Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   par[13] = rmax;        // R: projected radius considered for calculations of projected quantities
   par[14] = eps;         // Relative precision sought for integrations
   // Anisotropy parameters
   par[15] = -3.206018e+00;         // Anisotropy at r=0
   par[16] = 0.3;          // Anisotropy at r=+infinity
   par[17] = 0.001;         // Anisotropy scale radius ra [kpc]
   par[18] = 2.;          // Anisotropy shape parameter eta
   par[19] = kCONSTANT;           // Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)

   /*
      par[0] = 4.080350e+07;
      par[1] =  0.01;
      par[2] = 9.605012e-01;
      par[3] = 3.;
      par[4] = 0.5;
      par[5] = kZHAO;
      par[6] = 100.;
      par[7] = 1.e6;
      par[8] = 0.1;
      par[9] = 1.18288;
      par[10] = 4.30536;
      par[11] = 0.304093;
      par[12] = kZHAO3D;
      par[13] = 1.;
      par[14] = 0.01;
      par[15] = 0.;
      par[16] = 1.;
      par[17] = 1.e-2;
      par[18] = 2.;
      par[19] = kCONSTANT;
   */
   // Calculate all quantities (nuvr2, etc.)
   const int n_plots = 6;
   double qty[n];
   string name_canvas[n_plots] = {"nuvr2_integrand_log",
                                  "sigmap2_integrandkernel_log",
                                  "sigmap2_integrandnum_log",
                                  "nuvr2",
                                  "sigmap2 (kernel)",
                                  "sigmap2 (num)"
                                 };
   string xtitle[n_plots] = {"s [kpc]",
                             "r [kpc]",
                             "y [kpc]",
                             "r [kpc]",
                             "R [kpc]",
                             "R [kpc]"
                            };
   string ytitle[n_plots] = {"s #times [f(s) #nu(s) M(s)/s^{2}]",
                             "r #times 2 [K(r/R,r_{a}/R) #nu(r) M(r)/r] / G",
                             "y #times 2 [1-#beta_{ani}(r)R^{2}/r^{2}] #times #nu(r) #bar{v_{r}^{2}}(r) / G",
                             "#nu(r) #bar{v_{r}^{2}}(r) / G",
                             "#sigma_{p}(R)",
                             "#sigma_{p}(R)"
                            };
   string gr_title[n_plots] = {"nuvr2_integrand_log (s)",
                               "sigmap2_integrandkernel_log (s)",
                               "sigmap2_integrandnum_log (s)",
                               "nuvr2 (r)",
                               "sigmap_ker (r)",
                               "sigmap_num (r)"
                              };


   TCanvas *cjeans[n_plots];
   TMultiGraph *mg[n_plots];
   TLegend *leg_light[n_plots];
   TLegend *leg_anis[n_plots];


   // Check mass
   if (1 == 0) {
      clock_t begin = clock();
      double ttt = par[6];
      for (double rr = 1.e-3; rr < 0.1; rr *= 1.1) {
         par[6] = rr;
         cout << "   " << rr << " " << mass_singlehalo(par, 1.e-2) << endl;
      }
      par[6] = ttt;
      clock_t end = clock();
      double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
      cout << "          => Time spent for mass calculation" << " = " << time_spent << endl;

      abort();
   }

   // Check kernel
   if (1 == 0) {
      for (double rr = 1.e-2; rr < 10.; rr *= 1.5) {
         double R = 1.e-3;
         double ker = beta_anisotropy_kernel(rr, &par[15], R);
         cout << "   " << rr << " " << R << " " << ker << endl;
      }
      abort();
   }

   // Loop on 4 canvas
   cout << "  DM=" << gNAMES_PROFILE[(int)par[5]] << "  [kPROFILE=" << (int)par[5] << "]" << endl;
   for (int c = 0; c < 6; ++c) {
      cout << "    - Quantity="  << name_canvas[c] << endl;
      leg_light[c] = new TLegend(0.12, 0.12, 0.7, 0.6);
      leg_light[c]->SetFillColor(0);
      leg_light[c]->SetBorderSize(0);
      leg_light[c]->SetTextFont(132);
      leg_light[c]->SetTextSize(0.035);

      leg_anis[c] = new TLegend(0.6, 0.45, 0.9, 0.95);
      leg_anis[c]->SetFillColor(0);
      leg_anis[c]->SetBorderSize(0);
      leg_anis[c]->SetTextFont(132);
      leg_anis[c]->SetTextSize(0.035);

      cjeans[c] = new TCanvas(name_canvas[c].c_str(), name_canvas[c].c_str(), 5 + 550 * (c % 3), 5 + 450 * (c / 3), 550, 450);
      cjeans[c]->SetLogx(1);
      cjeans[c]->SetLogy(1);
      if (c == 4 || c == 5)
         cjeans[c]->SetLogy(0);

      mg[c] = new TMultiGraph();

      // Loop on light profiles
      for (int l = 0; l < gN_LIGHTPROFILE; ++l) {
         if (l != (int)par[12]) continue;
         cout << "       - Light="  << gNAMES_LIGHTPROFILE[l] << "  [kLIGHTPROFILE=" << l << "]" << endl;
         par[12] = l;

         // Loop on anisotropy profiles
         for (int k = 0; k < gN_ANISOTROPYPROFILE; ++k) {
            if (k != (int)par[19]) continue;
            cout << "          - Anis="  << gNAMES_ANISOTROPYPROFILE[k] << "  [kANISOTROPYPROFILE=" << k << "]" << endl;
            //if (k==kCONSTANT) par[15] = -0.5;
            //else par[15] = -1.3;
            par[19] = k;

            // Calculate quantities
            clock_t begin = clock();
            if (c == 0) {
               for (int j = 0; j < n; j++)
                  jeans_nuvr2_integrand_log(r[j], par, qty[j]);

            } else if (c == 1) {
               // N.B.: par[13] here is the projected radius for
               // which to calculate isigmap2 integrand (using kernel)
               double par_ref = par[13];
               par[13] = 5.;
               for (int j = 0; j < n; j++) {
                  if (k == 1) {
                     qty[j] = 0.;
                  } else {
                     jeans_isigmap2_integrand_withkernel(r[j], par, qty[j]);
                     qty[j] *= r[j]; // because we want the integrand in 'log'
                     //cout << r[j] << " " << qty[j] << endl;
                  }
               }
               par[13] = par_ref;

            } else if (c == 2) {
               // N.B.: par[13] here is the projected radius for
               // which to calculate isigmap2 integrand (using numerical integration)
               double par_ref = par[13];
               par[13] = 5.;
               for (int j = 0; j < n; j++) {
                  jeans_isigmap2_integrand_numerical(r[j], par, qty[j]);
                  qty[j] *= r[j]; // because we want the integrand in 'log'
               }
               par[13] = par_ref;

            } else if (c == 3) {
               for (int j = 0; j < n; j++)
                  qty[j] = jeans_nuvr2(r[j], par);


            } else if (c == 4) {
               double par_ref = par[13];
               for (int j = 0; j < n; j++) {
                  par[13] = r[j];
                  qty[j] = sqrt(jeans_sigmap2(r[j], par, true));
               }
               par[13] = par_ref;

            } else if (c == 5) {
               double par_ref = par[13];
               for (int j = 0; j < n; j++) {
                  par[13] = r[j];
                  qty[j] = sqrt(jeans_sigmap2(r[j], par, false));
               }
               par[13] = par_ref;
            }

            clock_t end = clock();
            double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
            cout << "             => Time spent for " << name_canvas[c] << "(" << n << " points) = " << time_spent << endl;


            // Fill graph and plot
            TGraph *gr = new TGraph(n, r, qty);
            string name = "gr_" + name_canvas[c] + "_" + gNAMES_LIGHTPROFILE[l] + "_" + gNAMES_ANISOTROPYPROFILE[k];
            gr->SetTitle(gr_title[c].c_str());
            gr->SetName(name.c_str());
            gr->SetFillColor(0);
            gr->SetLineColor(rootcolor(k));
            gr->SetLineWidth(2);
            gr->SetLineStyle(l + 1);
            mg[c]->Add(gr, "L");
            if (k == 0) {
               string legend = light_profile_legend(&par[7], false);
               TLegendEntry *entry = leg_light[c]->AddEntry(name.c_str(), legend.c_str(), "L");
               entry->SetTextColor(kBlack);
               entry->SetLineColor(kBlack);
               entry->SetLineWidth(2);
               entry->SetLineStyle(l + 1);
            }
            if (l == 0) {
               string legend = beta_anisotropy_legend(&par[15], false);
               TLegendEntry *entry = leg_anis[c]->AddEntry(name.c_str(), legend.c_str(), "L");
               entry->SetTextColor(rootcolor(k));
               entry->SetLineColor(rootcolor(k));
               entry->SetLineWidth(2);
               entry->SetLineStyle(1);
            }
         }
      }
      mg[c]->Draw("AL");
      mg[c]->GetXaxis()->SetTitle(xtitle[c].c_str());
      mg[c]->GetYaxis()->SetTitle(ytitle[c].c_str());
      mg[c]->GetXaxis()->CenterTitle(true);
      mg[c]->GetYaxis()->CenterTitle(true);
      leg_light[c]->Draw();
      leg_anis[c]->Draw();

      cjeans[c]->Update();
   }
   my_app->Run(kTRUE);
}
//______________________________________________________________________________
void test_other2(TApplication *my_app)
{


   //--- Test for new mass calculation (hypergeom function for Zhao).
   //  par[0]     Dark matter density normalisation: density [Msol/kpc^3]
   //  par[1]     Dark matter density Scale radius [kpc]
   //  par[2]     Dark matter Shape parameter #1
   //  par[3]     Dark matter Shape parameter #2
   //  par[4]     Dark matter Shape parameter #3
   //  par[5]     Dark matter card_profile [gENUM_PROFILE]
   //  par[6]     Max radius of integration

   const int n_par = 6;
   double par[n_par];
   par[0] = 1.040334e+7;
   par[1] = 5.038083e-01;
   par[2] = 2.;
   par[3] = 6.753814e+00;
   par[4] = -0.7;
   par[5] = kZHAO;

   const int n = 100;
   double r_min = 1.e-3;
   double r_max = 100.;
   double pas_log_r = pow(r_max / r_min, 1. / double(n - 1));
   double r[n];
   double res[n];
   for (int j = 0; j < n; ++j) {
      r[j] = r_min * pow(pas_log_r, j);
      rho(r[j], par, res[j]);
   }

   TGraph *gr = new TGraph(n, r, res);

   gr->Draw("AL");

   my_app->Run(kTRUE);


   /*
      double eps = 0.01;
      struct gStructHalo stat_halo;
   //   string datafile = "/afs/in2p3.fr/home/b/bonnivar/Program/CLUMPY_dev2/MockData/Unbinned/gs010_bs050_rcrs025_rarc100_core_xy_large.dat" ;
   //   string datafile = "/afs/in2p3.fr/home/b/bonnivar/Program/CLUMPY_dev2/MockData/Unbinned/gs010_bs050_rcrs010_rarcinf_cusp_xy_large.dat" ;
      string datafile = "/afs/in2p3.fr/home/b/bonnivar/Program/CLUMPY_dev2/MockData/Unbinned/Spherical_Charb/A_xy_large_vel.dat";

      halo_load_data4jeans(datafile, stat_halo, 0);

      TH1F *h = new TH1F("", "", 100, -100, 100);

      for(int k=0; k<int(stat_halo.KinData.Radius.size()); k++ )
            h->Fill(stat_halo.KinData.VelocData[k]);

      h->Draw();

      my_app->Run(kTRUE);




         // !!!! Plot de sigmap2 avec utilisation d'interpolation superpos� au true sigmap2 !!!!


         const int ni = 15;
         double sigmap2_inter[ni];
         double xmin = r_min, xmax = r_max+0.01;
         double pas_log_x=pow(xmax / xmin, 1. / double(ni-1));
         double xi[ni];
         double sigmap2_test_inter[n];

         double Rtemp=par[13];

         clock_t begin2, end2;
         double time_spent2;

         begin2 = clock();


         // You can choose among the following methods:
         // CSPLINE, LINEAR, POLYNOMIAL,
         // CSPLINE_PERIODIC, AKIMA, AKIMA_PERIODIC
         ROOT::Math::Interpolator inter(ni, ROOT::Math::Interpolation::kLINEAR);
         for ( int j = 0; j < ni; j++ )
         {
            xi[j] = xmin * pow(pas_log_x, j);
      //    cout << "xi[" << j << "] = " << xi[j] << endl;
            par[13]=xi[j];
            jeans_sigmap2(xi[j],param, sigmap2_inter[j]);
      //    cout << "sigmap2_inter[" << j << "] = " << sigmap2_inter[j] << endl;
            par[13]=Rtemp;
         }
         inter.SetData(ni, xi, sigmap2_inter);

         for (int i=0; i< n; i++){
         sigmap2_test_inter[i]=inter.Eval(r[i]);
         }



         end2 = clock();
         time_spent2 = (double)(end2 - begin2) / CLOCKS_PER_SEC;
         cout << " Time spent with interpolation  = " << time_spent2 << endl;



         TCanvas *c12 = new TCanvas("c12", "c12",5,505,650,500);
         c12->SetFillColor(0);
         c12->SetBorderMode(0);
         c12->SetBorderSize(0);
         c12->SetTickx(1);
         c12->SetTicky(1);
         c12->SetFrameLineColor(0);
         c12->SetFrameBorderMode(0);
         c12->SetFrameLineColor(0);
         c12->SetFrameBorderMode(0);

         TGraph *sigmap_inter_graph = new TGraph(n, r, sigmap2_test_inter);
         sigmap_inter_graph->GetXaxis()->SetTitle("R [kpc]");
         sigmap_inter_graph->GetYaxis()->SetTitle("sigmap2 interpolated");
         sigmap_inter_graph->SetFillColor(0);
         sigmap_inter_graph->SetLineColor(kBlue);
         sigmap_inter_graph->SetLineWidth(2);
         sigmap_inter_graph->SetLineStyle(0);
         sigmap_inter_graph->GetYaxis()->SetTitleOffset(1.3);
         sigmap_inter_graph->GetXaxis()->CenterTitle(true);
         sigmap_inter_graph->GetYaxis()->CenterTitle(true);
         sigmap_inter_graph->SetTitle("sigmap2(R) interpolated ");

         TMultiGraph *g2 = new TMultiGraph();
         g2->Add(sigmap_inter_graph, "c");
         g2->Add(sigmap2_graph, "c");

         g2->Draw("a");
         g2->GetXaxis()->SetLabelFont(132);
         g2->GetXaxis()->SetLabelSize(0.045);
         g2->GetXaxis()->SetTitleSize(0.04);
         g2->GetXaxis()->SetTickLength(0.05);
         g2->GetXaxis()->SetTitleOffset(1.05);
         g2->GetXaxis()->SetTitleFont(132);
         g2->GetYaxis()->SetLabelFont(132);
         g2->GetYaxis()->SetLabelSize(0.045);
         g2->GetYaxis()->SetTitleSize(0.04);
         g2->GetYaxis()->SetTickLength(0.05);
         g2->GetYaxis()->SetTitleOffset(0.9);
         g2->GetYaxis()->SetTitleFont(132);
         g2->GetXaxis()->SetTitle("R [kpc]");
         g2->GetXaxis()->CenterTitle(true);
         g2->GetYaxis()->CenterTitle(true);
         g2->GetYaxis()->SetTitle("sigmap2");

         gPad->SetLogx();
      //   gPad->SetLogy();



      */

   /*

   // !!!! Plot de I_integrand(y) !!!!
   double I_integrand[n];
   double par_light_integrand[7];
   for (int i = 0; i < 6; i++)
      par_light_integrand[i] = par[7 + i];

   par_light_integrand[6] = par[13];


   for (int i = 0; i < n; i++) {
      light_i_integrand(r[i], par_light_integrand, I_integrand[i]);

   }

   TCanvas *c8 = new TCanvas("c8", "c8", 5, 5, 650, 500);
   c8->SetFillColor(0);
   c8->SetBorderMode(0);
   c8->SetBorderSize(0);
   c8->SetTickx(1);
   c8->SetTicky(1);
   c8->SetFrameLineColor(0);
   c8->SetFrameBorderMode(0);
   c8->SetFrameLineColor(0);
   c8->SetFrameBorderMode(0);

   TGraph *I_integrand_graph = new TGraph(n, r, I_integrand);
   I_integrand_graph->GetXaxis()->SetTitle("y [kpc]");
   I_integrand_graph->GetYaxis()->SetTitle("I integrand");
   I_integrand_graph->SetFillColor(0);
   I_integrand_graph->SetLineColor(kBlack);
   I_integrand_graph->SetLineWidth(2);
   I_integrand_graph->SetLineStyle(0);
   I_integrand_graph->GetYaxis()->SetTitleOffset(1.3);
   I_integrand_graph->GetXaxis()->CenterTitle(true);
   I_integrand_graph->GetYaxis()->CenterTitle(true);
   I_integrand_graph->SetTitle("I integrand (y)");

   I_integrand_graph->Draw("AL");

   //   leg->Draw();

   gPad->SetLogx();
   gPad->SetLogy();



   double norm = light_i_norm(&par[7]);
   cout << "norm = " << norm << endl;
   */

}


//______________________________________________________________________________
//______________________________________________________________________________
int main(int argc, char *argv [])
{
   if (argc <= 1) {
      printf("  usage:\n");
      printf("          ------------ base functions ---------\n");
      printf("     %s -a  => load kinetic data file\n", argv[0]);
      printf("     %s -b  => load MCMC analysis file\n", argv[0]);
      printf("     %s -c  => random integer generation\n", argv[0]);
      printf("     %s -d  => Zhao mass calculation (using hypergeom function)\n", argv[0]);
      printf("     %s -e  => Test for Matt FORTRAN code [TO CHECK]\n", argv[0]);
      printf("     %s -f  => Plots for beta anisotropy profiles\n", argv[0]);
      printf("     %s -g  => Plots for light profiles\n", argv[0]);
      printf("     %s -h  => Plots for nuvr2 and sigmap2 (integrand and value)\n", argv[0]);
      printf("     %s -r  => test for cross product: all clumps in the loi\n", argv[0]);
      printf("\n");
      return 1;
   }

   // --- OPEN root application
   TApplication *my_app = new TApplication("App", NULL, NULL);
   // Set CLUMPY style
   rootstyle_set2CLUMPY();

   string File = "clumpy_params.txt";
   //load_parameters(File);

   //--- Test functions
   size_t l_arg = strlen(argv[1]);
   if (memchr(argv[1], 'a', l_arg)) test_load_kindata();
   if (memchr(argv[1], 'b', l_arg)) test_mcmc_file();
   if (memchr(argv[1], 'c', l_arg)) test_random_integer(my_app);
   if (memchr(argv[1], 'd', l_arg)) test_new_zhao_mass_calculation();
   if (memchr(argv[1], 'e', l_arg)) test_for_matt_code();
   if (memchr(argv[1], 'f', l_arg)) test_beta_anisotropy(my_app);
   if (memchr(argv[1], 'g', l_arg)) test_light_profile(my_app);
   if (memchr(argv[1], 'h', l_arg)) test_nuvr2_sigmap(my_app);

   delete my_app;
   my_app = NULL;
   return 0;
}


/*! \file clumpy_jeans_test.cc
  \brief \c CLUMPY test functions and plots (<i>Jeans module</i>)

   <CENTER>________________________________________</CENTER>\n

 Just type \c ./bin/clumpy_jeans_tests to get the executable usage, or look into the code.
 This is not meant to be part of the public version.

*/
