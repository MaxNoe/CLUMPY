#include "../include/misc.h"

#include <TApplication.h>
#include <TCanvas.h>
#include <TMath.h>
#include <TAxis.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TLegend.h>
#include <TLegendEntry.h>

#include <iostream>
#include <istream>
#include <fstream>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
using namespace std;

const int n_stat = 4;
string stat_names[4] = {"pdf_median", "pdf_mean", "mostlikely", "chi2_best"};
string xunit_ref = "", yunit_ref = "", xname_ref = "", yname_ref = "";
vector<double> cls_ref;

//______________________________________________________________________________
void load_clumpy_output(string file, vector<double> &x, vector<vector<double> > &y)
{
   //--- Reads a CL stat CLUMPY output file (.output), formatted as follows
   //       x= xaxis_name  [xaxis_unit]    y=  yaxis_name  [yaxis_unit]
   //       x       y_pdf_median  y_pdf_mean  y_mostlikely  y_chi2_best |  CL_lo_PDF CL_up_PDF
   //                                                                   |      cl_val1% CL     |    cl_val2% CL
   cout << ">>>>> Read and load " << file << endl;

   // Open file and check if exists
   ifstream f(file.c_str());
   if (!f) {
      printf("\n====> ERROR: load_clumpy_output() in clumpy_statoutputs_plot.cc");
      printf("\n             File %s not found", file.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   x.clear();
   y.clear();

   // Read file line-by-line
   string line;

   vector<double> *vecy_tmp = NULL;
   int i_read = 0;
   while (std::getline(f, line)) {
      removeblanks_from_startstop(line);
      if (line.size() == 0)
         continue;

      // First line must be "x= xaxis_name  [xaxis_unit]    y=  yaxis_name  [yaxis_unit]"
      if (i_read == 0) {
         // N.B.: axis names may contain spaces!!!
         vector<string> units;
         string2list(line, "[]", units);
         if (xunit_ref == "")
            xunit_ref = units[1];
         else if (xunit_ref != units[1]) {
            printf("\n====> ERROR: load_clumpy_output() in clumpy_statoutputs_plot.cc");
            printf("\n             xunit found [%s] differs from previous files [%s]", units[1].c_str(), xunit_ref.c_str());
            printf("\n             => abort()\n\n");
            abort();
         }
         if (yunit_ref == "")
            yunit_ref = units[3];
         else if (yunit_ref != units[3]) {
            printf("\n====> ERROR: load_clumpy_output() in clumpy_statoutputs_plot.cc");
            printf("\n             yunit found [%s] differs from previous files [%s]", units[3].c_str(), yunit_ref.c_str());
            printf("\n             => abort()\n\n");
            abort();
         }

         vector<string> names;
         string2list(units[0], "=", names);
         if (xname_ref == "")
            xname_ref = names[1];
         else if (xname_ref != names[1]) {
            printf("\n====> ERROR: load_clumpy_output() in clumpy_statoutputs_plot.cc");
            printf("\n             xname found [%s] differs from previous files [%s]", names[1].c_str(), xname_ref.c_str());
            printf("\n             => abort()\n\n");
            abort();
         }
         string2list(units[2], "=", names);
         if (yname_ref == "")
            yname_ref = names[3];
         else if (yname_ref != names[3]) {
            printf("\n====> ERROR: load_clumpy_output() in clumpy_statoutputs_plot.cc");
            printf("\n             yname found [%s] differs from previous files [%s]", names[3].c_str(), yname_ref.c_str());
            printf("\n             => abort()\n\n");
            abort();
         }
         ++i_read;
         continue;
      }

      // Second line is    x     y_pdf_median  y_pdf_mean  y_mostlikely  y_chi2_best |  CL_lo_PDF CL_up_PDF
      if (i_read == 1) {
         ++i_read;
         continue;
      }

      // Third line is  //    |      cl_val1% CL     |    cl_val2% CL
      if (i_read == 2) {
         vector<string> cls;
         string2list(line, "|", cls);
         for (int i = 1; i < (int)cls.size(); ++i) {
            vector<string> tmp;
            string2list(cls[i], "|", tmp);

            double cl_tmp = atof(tmp[0].c_str());
            if ((int)cls_ref.size() < i)
               cls_ref.push_back(cl_tmp);
            else if (fabs(cls_ref[i - 1] - cl_tmp) / cls_ref[i - 1] > 0.001) {
               printf("\n====> ERROR: load_clumpy_output() in clumpy_statoutputs_plot.cc");
               printf("\n             CL found (%f) differs from previous files (%f)", cl_tmp, cls_ref[i - 1]);
               printf("\n             => abort()\n\n");
               abort();
            }
         }
         ++i_read;

         // Reserve size for y_tmp
         vecy_tmp = new vector<double>[n_stat + 2 * cls_ref.size()];
         continue;
      }


      // All other lines are values
      istringstream iss(line);
      double x_tmp, y_tmp;
      // Read x
      iss >> x_tmp;
      x.push_back(x_tmp);
      // Read y_stat
      for (int i = 0; i < n_stat; ++i) {
         iss >> y_tmp;
         vecy_tmp[i].push_back(y_tmp);
      }
      // Read y_cls
      for (int i = 0; i < (int)cls_ref.size(); ++i) {
         int j = n_stat + 2 * i;
         string dummy;
         iss >> dummy >> y_tmp;
         vecy_tmp[j].push_back(y_tmp);
         iss >> y_tmp;
         vecy_tmp[j + 1].push_back(y_tmp);
      }
   }

   // Copy in y
   for (int i = 0; i < n_stat + int(2 * cls_ref.size()); ++i)
      y.push_back(vecy_tmp[i]);

   delete[] vecy_tmp;
   vecy_tmp = NULL;
   cout << "    => file has " <<  x.size() << " x_axis entries" << endl << endl;
   f.close();
}

//______________________________________________________________________________
void Plot(vector<string> &file_names, vector<string> &leg_names, string &leg_title, vector<bool> &is_stat, int ncls)
{
   //--- Plot files for comparison.
   //  file_names   vector of clumpy output files to read
   //  leg_names    legend of names (associated to the content of the file plotted)
   //  leg_title    global name of the legend
   //  switch_stat  size-4 string of 0 and 1 (median, mean, most likely, best)
   //  ncls         number of CLs to draw [default = 1]


   rootstyle_set2CLUMPY();

   TCanvas *c_plotcompare = new TCanvas("c_plotcompare", "c_plotcompare", 650, 500);
   c_plotcompare->SetLogx();
   c_plotcompare->SetLogy();
   c_plotcompare->SetTickx(1);
   c_plotcompare->SetTicky(1);

   TMultiGraph *mg = new TMultiGraph();
   mg->SetName("mg_comparisons");
   mg->SetTitle("");

   TLegend *leg = new TLegend(0.15, 0.5, 0.6, 0.95, NULL, "brNDC");
   leg->SetBorderSize(0);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   leg->SetTextSize(0.035);
   if (leg_title != "")
      leg->AddEntry("NULL", leg_title.c_str(), "h");

   vector<double> x;
   vector<vector<double> > y;
   string x_title, y_title;
   for (int i = (int)file_names.size() - 1; i >= 0; --i) {
      load_clumpy_output(file_names[i], x, y);


      // Loop on stats to plot
      int i_stat = 0;
      for (int k = 0; k < (int)is_stat.size(); ++k) {
         if (!is_stat[k])
            continue;

         char stat_name[200];
         sprintf(stat_name, "gr_input%d_%s", i, stat_names[k].c_str());
         TGraph *gr = new TGraph(x.size(), &x[0], &y[k][0]);
         gr->SetName(stat_name);
         gr->SetTitle("");
         gr->SetLineWidth(3);
         gr->SetLineColor(rootcolor(i));
         gr->SetLineStyle(1);

         if (i_stat == 0) {
            mg->Add(gr, "L");
            char ttt[200];
            if (leg_names[i] == "")
               sprintf(ttt, "File %d  (%s)", i + 1, stat_names[k].c_str());
            else
               sprintf(ttt, "%s  (%s)", leg_names[i].c_str(), stat_names[k].c_str());
            TLegendEntry *entry = leg->AddEntry(gr, ttt, "L");
            entry->SetTextColor(rootcolor(i));
         } else {
            gr->SetMarkerStyle(rootmarker(k));
            gr->SetMarkerColor(rootcolor(i));
            gr->SetMarkerSize(0.9);
            mg->Add(gr, "PL");
            if (i == 0)
               leg->AddEntry(gr, stat_names[k].c_str(), "PL");
         }
         ++i_stat;
      }

      // Loop on CLs to plot
      for (int k = 0; k < (int)cls_ref.size(); ++k) {
         int j = n_stat + 2 * k;
         if (k >= ncls)
            continue;
         char cl_name[200];
         // CL lo
         sprintf(cl_name, "gr_input%d_CLlo%d", i, (int)cls_ref[k]);
         TGraph *gr = new TGraph(x.size(), &x[0], &y[j][0]);
         gr->SetName(cl_name);
         gr->SetTitle("");
         gr->SetLineWidth(2);
         gr->SetLineColor(rootcolor(i));
         gr->SetLineStyle(k + 2);
         mg->Add(gr, "L");

         // CL up
         sprintf(cl_name, "gr_input%d_CLup%d", i, (int)cls_ref[k]);
         gr = new TGraph(x.size(), &x[0], &y[j + 1][0]);
         gr->SetName(cl_name);
         gr->SetTitle("");
         gr->SetLineWidth(3);
         gr->SetLineColor(rootcolor(i));
         gr->SetLineStyle(k + 2);
         mg->Add(gr, "L");

         if (i == 0) {
            char cl[200];
            sprintf(cl, "%.2f %% CLs", cls_ref[k]);
            leg->AddEntry(gr, cl, "L");
         }
      }

   }

   mg->Draw("A");
   string x_tt = xname_ref + "  [" + xunit_ref + "]";
   string y_tt = yname_ref + "  [" + yunit_ref + "]";
   mg->GetXaxis()->SetTitle(x_tt.c_str());
   mg->GetYaxis()->SetTitle(y_tt.c_str());

   leg->Draw();
   c_plotcompare->Modified();
   c_plotcompare->cd();
   c_plotcompare->SetSelected(c_plotcompare);
}

//______________________________________________________________________________
int main(int argc, char **argv)
{

   cout <<  argc << " " <<  argv[1] << endl;
   if (argc <= 1) {
      printf("  usage:\n");
      printf("   %s file1,file2,...,filen   leg_name1,leg_name2,...leg_namen  leg_title  switch_stat  ncls\n", argv[0]);
      printf("     - file              clumpy output file (extension .output)\n");
      printf("     - [opt: leg_name]   legend name (associated to the content of the file plotted)\n");
      printf("     - [opt:leg_title]   global name of the legend\n");
      printf("     - [opt:switch_stat] size-4 string of 0 and 1 (median, mean, most likely, best) [default = 1000]\n");
      printf("     - [opt:ncls]        number of CLs to draw [default = 1]\n");
      printf("  exemples:\n");
      printf("     %s  file1.outout,file2.output\n", argv[0]);
      printf("     %s  file1.outout,file2.output  leg1,leg2  dsph  1000 2\n", argv[0]);
      return 0;
   }

   // File names
   string tmp = argv[1];
   vector<string> file_names;
   string2list(tmp, ",", file_names);

   // Legend names
   vector<string> leg_names(file_names.size(), "");
   if (argc > 2) {
      tmp = argv[2];
      vector<string> tmp_names;
      string2list(tmp, ",", tmp_names);
      for (int i = 0; i < (int)min(file_names.size(), tmp_names.size()); ++i)
         leg_names[i] = tmp_names[i];
   }

   // Legend title
   string leg_title = "";
   if (argc > 3)
      leg_title = argv[3];

   // Statistics to display
   vector<bool> is_stat(n_stat, false);
   is_stat[0] = true;
   if (argc > 4) {
      tmp = argv[4];
      for (int i = 0; i < min(n_stat, (int)tmp.size()); ++i) {
         if (tmp[i] == '1')
            is_stat[i] = true;
      }
   }

   // CLs to display
   int ncls = 1;
   if (argc > 5)
      ncls = atoi(argv[5]);

   // Display
   TApplication *my_app = new TApplication("myapp", 0, 0);
   Plot(file_names, leg_names, leg_title, is_stat, ncls);
   my_app->Run(0);
}
