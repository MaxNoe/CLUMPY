// C++ std libraries
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <iostream>
#include <vector>
using namespace std;

// CLUMPY includes
#include "../include/params.h"
#include "../include/misc.h"
#include "../src/cosmo.cc"


// GSL includes
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_deriv.h>
#include <gsl/gsl_chebyshev.h>

// ROOT includes
#include <TH2D.h>
#include <TMath.h>
#include <TRandom.h>



//==================================================================================================

int main(void)
{

   //load_parameters("clumpy_params.txt", false);

   double zmin = gSIM_EXTRAGAL_ZMIN;
   double zmax = gSIM_EXTRAGAL_ZMAX;
   double nz = gSIM_EXTRAGAL_DELTAZ_PRECOMP;
   double dz = (zmax - zmin) / nz;
   double Mmin = gSIM_EXTRAGAL_MMIN; // in units of h^-1 M_solar
   double Mmax = gSIM_EXTRAGAL_MMAX; // in units of h^-1 M_solar
   double nm = gSIM_EXTRAGAL_NM;
   double dlnM = (log(Mmax) - log(Mmin)) / nm;
   vector<double> lnM_grid;

   for (int i = 0; i <= nz; i++) {
      gCOSMO_Z_GRID.push_back(zmin + i * dz);
   }

   for (int i = 0; i <= nm; i++) {
      lnM_grid.push_back(log(Mmin) + i * dlnM);
   }


//  get_pk();

//  // Tinker
// load_mf_tab(kTINKER08);
// for(int j=0;j<=nm;j++){
//    double Mh=exp(lnM_grid[j]);
//    cout << log10(exp(lnM_grid[j])*gCOSMO_OMEGA0_M) << " " <<log10(g_dndlnMh[0][j])<< " " <<log10(g_dndlnMh[1][j])<< " " <<log10(g_dndlnMh[2][j])<< " " <<log10(g_dndlnMh[3][j]) << " " <<log10(g_dndlnMh[4][j])<< " " <<log10(g_dndlnMh[5][j])<<endl;
//  }

// // Sheth-Tormen
// load_mf_tab(kSHETHTORMEN);
//  for(int j=0;j<=nm;j++){
//    double Mh=exp(lnM_grid[j]);
//    cout << log10(exp(lnM_grid[j])*gCOSMO_OMEGA0_M) << " " <<log10(g_dndlnMh[0][j])<< " " <<log10(g_dndlnMh[1][j])<< " " <<log10(g_dndlnMh[2][j])<< " " <<log10(g_dndlnMh[3][j]) << " " <<log10(g_dndlnMh[4][j])<< " " <<log10(g_dndlnMh[5][j])<<endl;
//  }

//  // Jenkins
//  load_mf_tab(kJENKINS);
//  for(int j=0;j<=nm;j++){
//    double Mh=exp(lnM_grid[j]);
//    cout << log10(exp(lnM_grid[j])*gCOSMO_OMEGA0_M) << " " <<log10(g_dndlnMh[0][j])<< " " <<log10(g_dndlnMh[1][j])<< " " <<log10(g_dndlnMh[2][j])<< " " <<log10(g_dndlnMh[3][j]) << " " <<log10(g_dndlnMh[4][j])<< " " <<log10(g_dndlnMh[5][j])<<endl;
//  }

//  // Press-Schechter
//  load_mf_tab(kPRESSSCHECHTER);
//  for(int j=0;j<=nm;j++){
//    double Mh=exp(lnM_grid[j]);
//    cout << log10(exp(lnM_grid[j])*gCOSMO_OMEGA0_M) << " " <<log10(g_dndlnMh[0][j])<< " " <<log10(g_dndlnMh[1][j])<< " " <<log10(g_dndlnMh[2][j])<< " " <<log10(g_dndlnMh[3][j]) << " " <<log10(g_dndlnMh[4][j])<< " " <<log10(g_dndlnMh[5][j])<<endl;
//  }

   //struct my_f_params {int i; double r; vector<double> vec_lnk; vector<vector<double> > linear_lnpkz;};


   // cout << vec_lnk.size() << "  " << vec_z.size() << "  "<< linear_lnpkz.size() << endl;
   // cout << vec_lnk[0] << "  " << vec_z[0] << "  "<< linear_lnpkz[0][0] << endl;
   // cout << vec_lnk[9] << "  " << vec_z[4] << "  "<< linear_lnpkz[4][9] << endl;

   // for(int i=0;i<=nz;i++){
   // get_linear_pk_at_zi(i,linear_pk_at_z,vec_lnk,linear_lnpkz);
   // cout << linear_lnpkz[i][0]<<endl;
   // double res=linear_pk(i, 2.3, vec_lnk, linear_lnpkz);
   //   cout << vec_z[i] << " 2.3 " << res << endl;

   // double lnR=log(7.);
   // struct my_f_params params={i, exp(lnR), vec_lnk, linear_lnpkz};

   // gsl_function F;
   // F.function = &lnsigma2;
   // F.params = &params;

   // //cout << exp(lnR) << " " << vec_lnk[0]<<endl;
   // double lnsigma2_tmp=GSL_FN_EVAL(&F,lnR);
   // double tmp=lnsigma2_tmp;
   // cout << vec_z[i] << " " <<exp(lnR) << " " << exp(0.5*GSL_FN_EVAL(&F,lnR))<< endl;
   //cout << vec_z[i] << " " <<lnR << " " << tmp<< endl;

   //  for(int j=0;j<=nm;j++){

   //   }
   // }

   // load_mf_tab(vec_z, vec_lnk, vec_lnM, linear_lnpkz, g_dndlnMh);

   // for(int j=0;j<=nm;j++){
   //   double Mh=exp(vec_lnM[j]);
   //   cout << log10(exp(vec_lnM[j])) << " " <<log10(Mh*g_dndlnMh[0][j]) << endl;

   // }


   //cout << exp(vec_lnM[j]) << " " << vec_z[i] << endl;

   // int npts=1000;
   //  for(int i=0;i<npts;i++){
   //    double lnR=3.;
   //    double lnk=log(vec_lnk[0])+i*(log(vec_lnk[vec_lnk.size()-1])-log(vec_lnk[0]))/npts;
   //    struct my_f_params params={0, exp(lnR), vec_lnk, linear_lnpkz};

   //      gsl_function F;
   //      F.function = &ds2dk;
   //      F.params = &params;

   //      cout << lnk <<  " " << GSL_FN_EVAL(&F,lnk)<< endl;
   //  }

   // TH2D *h_lnpkz = new TH2D("lnpkz", "title", vec_z.size(),zmin, zmax, vec_lnk.size(), ln(vec_lnk[0]),ln(vec_lnk[vec_lnk.size()-1]));

   // for (int i=0;i<vec_lnk.size();i++){
   //   for (int j=0; j<vec_z.size();j++){
   //     cout <<  ln(vec_lnk[i]) << " " << vec_z[j] << " " <<ln(linear_lnpkz[j][i])<< endl;
   //     h_logpkz->Fill(vec_z[j], ln(vec_lnk[i]), ln(linear_lnpkz[j][i]));
   //   }
   // }

   // // check interpolation at a random location
   // double res=h_logpkz->Interpolate(2.43,ln(1));
   // cout << "res interpolation = " << res << endl;

   for (int i = 0; i < (int)gCOSMO_Z_GRID.size(); i++) {
      gCOSMO_D_TRANS_GRID.push_back(dh_trans(gCOSMO_Z_GRID[i]));
      gCOSMO_D_LUM_GRID.push_back(dh_l(gCOSMO_Z_GRID[i]));
      gCOSMO_D_ANG_GRID.push_back(dh_a(gCOSMO_Z_GRID[i]));
   }


   for (double z = 0; z < 2; z += 0.01) {
      //     cout << z <<" " <<"  "<< dh_a(z)/2998.<<"  "<< dh_l(z)/2998.<< "  "<< dVhdzdOmega(z)/pow(2998.,3.)<< endl;
      cout << z << " " << "  " << spline_interp(z, gCOSMO_Z_GRID, gCOSMO_D_ANG_GRID) << "  " << spline_interp(z, gCOSMO_Z_GRID, gCOSMO_D_ANG_GRID) * gCOSMO_HUBBLE << endl;
   }

   // checks interpolation at a random redshift
   // double z=1.e-1;
   // cout << dh_a(z)<< "  " <<dh_l(z)<< endl;

   return 0;
}
