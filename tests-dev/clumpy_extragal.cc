
// C++ std libraries
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <iostream>
#include <vector>
using namespace std;

// ROOT includes
#include <TF3.h>
#include <TH2D.h>
#include <TMath.h>
#include <TRandom.h>

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/geometry.h"
#include "../include/healpix_fits.h"
#include "../include/inlines.h"
#include "../include/janalysis.h"
#include "../include/jeans_analysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/spectra.h"
#include "../include/stat.h"
#include "../include/cosmo.h"
#include "../include/extragal.h"


//int main(int argc, char *argv []){
int main(int argc, char *argv [])
{
    argc = 1.;
   // get location of executable:
   gPATH_TO_CLUMPY_EXE = argv[0];
   gPATH_TO_CLUMPY_EXE = gPATH_TO_CLUMPY_EXE.substr(0, gPATH_TO_CLUMPY_EXE.find_last_of("\\/"));
   // assume clumpy home directory one level above executable:
   string exe_dir = string(getenv("PWD"));
   if (gPATH_TO_CLUMPY_EXE.substr(0, 1) != "/") {
      if (gPATH_TO_CLUMPY_EXE.substr(0, 2) == "./") gPATH_TO_CLUMPY_EXE = exe_dir + "/" + gPATH_TO_CLUMPY_EXE.substr(2, gPATH_TO_CLUMPY_EXE.size());
      else gPATH_TO_CLUMPY_EXE =  exe_dir + "/" + gPATH_TO_CLUMPY_EXE;
   }
   gPATH_TO_CLUMPY_HOME = gPATH_TO_CLUMPY_EXE.substr(0, gPATH_TO_CLUMPY_EXE.find_last_of("\\/"));
   gPATH_TO_CLUMPY_EXE += "/";
   gPATH_TO_CLUMPY_HOME += "/";

   if (gDM_LOGCDELTA_STDDEV > 1.e-5)
      gDM_FLAG_CDELTA_DIST = kLOGNORM;
   else
      gDM_FLAG_CDELTA_DIST = kDIRAC;


   double zmin = 0.;
   double zmax = 2.;
   double nz = 12;
   double Mmin = 1e6; // in units of M_solar
   double Mmax = 1e16; // in units of M_solar
   gDM_SUBS_NUMBEROFLEVELS = 2;


   gCOSMO_OMEGA0_M = 0.308;
   gCOSMO_HUBBLE = 0.678;
   gCOSMO_OMEGA0_LAMBDA = 0.692;
   gCOSMO_OMEGA0_B = 0.0486;

   //gHALO_SUBS_DPDV_RSCALE_TO_RS_HOST[kEXTRAGAL]  = 2;
   //gHALO_SUBS_DPDM_SLOPE[kEXTRAGAL] = 2;
   //gHALO_SUBS_MASSFRACTION [kEXTRAGAL] =  0.2;


   double nm = gSIM_EXTRAGAL_NM;
   double dlnM = (log(Mmax) - log(Mmin)) / nm;
   double dz = (zmax - zmin) / nz;
   vector<double> M_grid;

   for (int i = 0; i <= nm; i++) {
      M_grid.push_back(Mmin * exp(i * dlnM));
   }
   vector<double> z_grid;
   vector<double> d_lum_grid;
   vector<double> d_ang_grid;
   for (int i = 0; i <= nz; i++) {
      z_grid.push_back(zmin + i * dz);
      d_lum_grid.push_back(dh_l(zmin + i * dz));
      d_ang_grid.push_back(dh_a(zmin + i * dz));
   }

   cout << endl;
   cout << "Do you want to:" << endl;
   cout << "  0. Cosmology, distances, volumes, etc." << endl;
   cout << "  1. Test the mass function" << endl;
   cout << "  2. Compute the differential flux dPhi/dE and E^2dPhi/dE" << endl;
   cout << "  3. Check contributions to dPhi/dE as function of mass and redshift ranges" << endl;
   cout << "  4. Compute the integrated flux above 3 GeV as function of mass and redshift ranges" << endl;
   cout << "  5. Compute the cumulative flux above 3 GeV with increasing mass and redshift" << endl;
   cout << "  6. Draw mass, redshift and directions for SZ clusters" << endl;
   cout << "  7. Check mean number of clusters" << endl;
   cout << "Your choice: ";

   int test;
   cin >> test;
   cout << endl;

   if (test == 0) { // Test 0: test comology. Output has been compared to Hogg (2000).
      string file = gSIM_OUTPUT_DIR + "check_cosmo_distances.dat";
      ofstream myfile;
      myfile.open(file.c_str());
      for (double z = 1.e-5; z < zmax; z += zmax / 1000.) {
         double dist_lum = dh_l(z) / HUBBLE_LENGTHxh_Mpc;
         double dist_lum_interpol = spline_interp(z, z_grid, d_lum_grid) / HUBBLE_LENGTHxh_Mpc;
         double dist_ang = dh_a(z) / HUBBLE_LENGTHxh_Mpc;
         double dist_ang_interpol = spline_interp(z, z_grid, d_ang_grid) / HUBBLE_LENGTHxh_Mpc;
         double diff_vol = dVhdzdOmega(z) / pow(HUBBLE_LENGTHxh_Mpc, 3.);
         myfile << z << " " << dist_lum << " " << dist_lum_interpol << " " << dist_ang << " " << dist_ang_interpol << " " << diff_vol << endl;
      }
      myfile.close();
      cout << "Test 0a done. Output written in " + file << endl;

      double *params = NULL;
      file = gSIM_OUTPUT_DIR + "check_cosmo_Omega.dat";
      myfile.open(file.c_str());
      for (double z = 1.e-5; z < zmax; z += 0.01) {
         double x = (1 + z);
         double OmegaR_z = gCOSMO_OMEGA0_R * pow(H0_over_H(z, params), 2.) * pow(x, 4.);
         double OmegaCDM_z = (gCOSMO_OMEGA0_M - gCOSMO_OMEGA0_B) * pow(H0_over_H(z, params), 2.) * pow(x, 3.);
         double Omegab_z = gCOSMO_OMEGA0_B * pow(H0_over_H(z, params), 2.) * pow(x, 3.);
         double OmegaL_z = gCOSMO_OMEGA0_LAMBDA * pow(H0_over_H(z, params), 2.);
         double sum = OmegaR_z + OmegaCDM_z + Omegab_z + OmegaL_z;
         myfile << z << " " << OmegaR_z << " " << OmegaCDM_z << " " << Omegab_z << " " << OmegaL_z << " " << sum << endl;
      }
      myfile.close();
      cout << "Test 0b done. Output written in " + file << endl;

      return 0;
   }

   // Initialize all extragalactic-related arrays to generate tabulated mass function gCOSMO_DNDVHDLNMH_Z
   int card_mf = kTINKER08;

   cout << endl << " Initialise mass function " << gNAMES_MASSFUNCTION[card_mf] << endl;
   init_extragal_manipulatedHMF(z_grid, M_grid, card_mf, kTOP_HAT);

   if (test == 1) { // Test 1: Check mass function

      double z = 0, lnMh = 0;
      int N_halo = 1e5;
      string file = gSIM_OUTPUT_DIR + "MF_draw_random.dat";
      ofstream myfile;
      myfile.open(file.c_str());

      for (int i = 0; i < N_halo; i++) {
         //gCOSMO_DNDVHDLNMH_Z->GetRandom2(z, lnMh);
         myfile << log10(exp(lnMh)) << " " << z << endl;
      }
      myfile.close();

      cout << "Test 1 done. Output written in " + file << endl;



   } else if (test == 2) { // Test 2: Check differential flux
      gPP_DM_MASS_GEV = 500;
      //gEXTRAGAL_FLAG_CDELTAMDELTA = kPRADA12_200;
      double par[38];
      // par[0-22] = par_subs
      // par[23-24]= Mmin, Mmax
      // par[25-29]= spectral parameters
      // par[31-32]= zmin, zmax
      // par[33]= ultiplication factor

      double res;

      //   par[25]=gPP_DM_MASS_GEV; //Mass of DM candidate [GeV]
      par[28] = gPP_DM_MASS_GEV;
      par[29] = gPP_FLAG_SPECTRUMMODEL; //   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
      par[30] = kGAMMA; //   Card for PP final state (gENUM_FINALSTATE)

      //double zmin_diff = 0.001;
      //double zmax_diff = 4.99;
      //double Mmin_diff = 1e12;
      //double Mmax_diff = 1e16;


      cout << "DM particle mass: " << par[28] << " GeV" << endl;


      //int nz_diff = 99;
      //int nm_diff = 10;
      vector <double> zdiff_vec;
      vector <double> mdiff_vec;

      //double log_nz_diff_Delta = (log(zmax_diff) - log(zmin_diff)) / (nz_diff);
      //double log_nm_diff_Delta = (log(Mmax_diff) - log(Mmin_diff)) / (nm_diff);

      string file = gSIM_OUTPUT_DIR + "diff_flux.dat"; // --> E, dPhi/dE, E^2 dPhi/dE
      string file_diff_z = gSIM_OUTPUT_DIR + "diff_flux_diff_z.dat"; // --> E, dPhi/dE, E^2 dPhi/dE
      string file_diff_m = gSIM_OUTPUT_DIR + "diff_flux_diff_m.dat"; // --> E, dPhi/dE, E^2 dPhi/dE
      ofstream myfile;
      ofstream myfile_diff_z;
      ofstream myfile_diff_m;

      myfile.open(file.c_str());

//      myfile_diff_z.open(file_diff_z.c_str());
//      myfile_diff_z << -1 << "\t";
//      for (int i = 0; i <= nz_diff; ++i) {
//         zdiff_vec.push_back(exp(log(zmin_diff) + i * log_nz_diff_Delta));
//         myfile_diff_z << zdiff_vec[i] << "\t";
//      }
//      myfile_diff_z << endl;
//      for (double e_gev = par[28] / 5.e3; e_gev < par[28]; e_gev *= 1.5) {
//         myfile_diff_z << e_gev << "\t";
//         for (int i = 1; i <= nz_diff; ++i) {
//            par[33] = e_gev; //   Energy at which to compute differential flux
//            par[34] = zdiff_vec[i - 1]; // redshift integration range - zmin
//            par[35] = zdiff_vec[i]; // redshift integration range - zmax
//            par[36] = 0.; // --> dPhi/dE
//            dPhidOmegadE(e_gev, par, res);
//
//            myfile_diff_z << res << "\t";
//         }
//         myfile_diff_z << endl;
//      }
//      myfile_diff_z.close();

//            myfile_diff_m.open(file_diff_m.c_str());
//            myfile_diff_m << -1 << "\t";
//            for (int i = 0; i <= nm_diff; ++i) {
//             mdiff_vec.push_back(exp(log(Mmin_diff) + i * log_nm_diff_Delta));
//               myfile_diff_m << mdiff_vec[i] << "\t";
//            }
//            myfile_diff_m << endl;
//            for (double e_gev = par[28] / 5.e3; e_gev < par[28]; e_gev *= 1.5) {
//               myfile_diff_m << e_gev << "\t";
//               for (int i = 1; i <= nm_diff; ++i) {
//                par[26] = mdiff_vec[i - 1]; // mass integration range - Mmin
//                par[27] = mdiff_vec[i]; // mass integration range - Max
//                  par[33] = e_gev; //   Energy at which to compute differential flux
//                  par[34] = 1e-3; // redshift integration range - zmin
//                  par[35] = 0.999*zmax; // redshift integration range - zmax
//                  par[36] = 0.; // --> dPhi/dE
//                  dPhidOmegadE(e_gev, par, res);
//
//                  myfile_diff_z << res << "\t";
//               }
//               myfile_diff_z << endl;
//            }
//            myfile_diff_m.close();
//            cout << "Test mdiff done. Output written in " + file_diff_m << endl; abort();

      par[26] = log(Mmin * gCOSMO_HUBBLE / gCOSMO_OMEGA0_M); // mass integration range - Mmin
      par[27] = log(Mmax * gCOSMO_HUBBLE / gCOSMO_OMEGA0_M); // mass integration range - Max

      par[35] = 1e-3; // redshift integration range - zmin
      par[36] = 0.999 * zmax; // redshift integration range - zmax
      for (double e_gev = par[28] / 5.e3; e_gev < par[28]; e_gev *= 1.3) {
         par[33] = e_gev; //   Energy at which to compute differential flux
         par[37] = 0.; // --> dPhi/dE
         dPhidOmegadE(e_gev, par, res);

         myfile << e_gev << " " << res << " " << res *pow(e_gev, 2.) << endl;
      }
      myfile.close();

      cout << "Test 2 done. Output written in " + file << endl;

   } else if (test == 3) { // Test 3: For a given energy e_gev, check which mass range and redshift contributes the most to dPhi/dE
      double e_gev = gPP_DM_MASS_GEV / 10.;
      double m_start = Mmin;
      double par[38];
      double res;
      par[28] = gPP_DM_MASS_GEV; //Mass of DM candidate [GeV]
      par[29] = gPP_FLAG_SPECTRUMMODEL; //   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
      par[30] = kGAMMA; //   Card for PP final state (gENUM_FINALSTATE)
      par[26] = log(Mmin); // mass integration range - Mmin
      par[27] = log(Mmax); // mass integration range - Max
      par[32] = false;
      //par[33] = gEXTRAGAL_FLAG_ABSORPTIONPROFILE;
      string file = gSIM_OUTPUT_DIR + "diff_flux_ranges.dat"; // -->  E^2 dPhi/dE
      ofstream myfile;
      myfile.open(file.c_str());

      for (double m_low = m_start; m_low < Mmax; m_low *= 1.1) {
         double m_up = 1.1 * m_low;
         cout << m_low << " " << m_up << endl;
         if (m_up >= Mmax) break;
         for (double z_low = zmin + 1.e-5; z_low < zmax; z_low += 0.5) {
            double z_up = z_low + 0.5;
            if (z_up >= zmax) break;
            //cout << z_low << " " << z_up << endl;
            par[26] = log(m_low); // mass integration range - Mmin
            par[27] = log(m_up); // mass integration range - Max
            par[35] = z_low; // redshift integration range - zmin
            par[36] = z_up; // redshift integration range - zmax
            dPhidOmegadE(e_gev, par, res);
            myfile << m_low << " " << z_low << " " << log10(res) << endl;
         }
      }
      myfile.close();
      cout << "Test 3 done. Output written in " + file << endl;

   } else if (test == 4) { // Test 4: integrated flux abouve 3 GeV
      double e_gev_min = 3;
      double e_gev_max = gPP_DM_MASS_GEV;
      double m_start = Mmin;
      double par[37];
      double res;
      par[25] = gPP_DM_MASS_GEV; //Mass of DM candidate [GeV]
      par[26] = gPP_FLAG_SPECTRUMMODEL; //   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
      par[27] = kGAMMA; //   Card for PP final state (gENUM_FINALSTATE)
      par[32] = false;

      string file = gSIM_OUTPUT_DIR + "int_flux_ranges.dat"; // -->  E^2 dPhi/dE
      ofstream myfile;
      myfile.open(file.c_str());

      for (double m_low = m_start; m_low < Mmax; m_low *= 10.) {
         double m_up = 10.*m_low;
         if (m_up >= Mmax) break;
         cout << m_low << " " << m_up << endl;
         for (double z_low = zmin + 1.e-5; z_low < zmax; z_low += 0.1) {
            double z_up = z_low + 0.1;
            if (z_up >= zmax) break;
            //cout << z_low << " " << z_up << endl;
            par[26] = log(m_low); // mass integration range - Mmin
            par[27] = log(m_up); // mass integration range - Max
            par[35] = z_low; // redshift integration range - zmin
            par[36] = z_up; // redshift integration range - zmax
            res = dPhidOmega(e_gev_min, e_gev_max, par);
            myfile << m_low << " " << z_low << " " << log10(res) << endl;
         }
      }
      myfile.close();
      cout << "Test 4 done. Output written in " + file << endl;

   } else if (test == 5) { // Test 5: cumulative integrated flux abouve 3 GeV
      double e_gev_min = 3;
      double e_gev_max = gPP_DM_MASS_GEV;
      double m_start = Mmin;
      double z_start = zmin + 1.e-5;
      double par[37];
      double res;
      par[25] = gPP_DM_MASS_GEV; //Mass of DM candidate [GeV]
      par[26] = gPP_FLAG_SPECTRUMMODEL; //   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
      par[27] = kGAMMA; //   Card for PP final state (gENUM_FINALSTATE)
      par[32] = false;

      string file = gSIM_OUTPUT_DIR + "int_flux_cumul.dat"; // -->  E^2 dPhi/dE
      ofstream myfile;
      myfile.open(file.c_str());

      for (double m_up = m_start * 10; m_up < Mmax; m_up *= 10.) {
         if (m_up >= Mmax) break;
         cout << m_start << " " << m_up << endl;
         for (double z_up = z_start + 0.2; z_up < zmax; z_up += 0.2) {
            if (z_up >= zmax) break;
            //cout << z_low << " " << z_up << endl;
            par[26] = log(m_start); // mass integration range - Mmin
            par[27] = log(m_up); // mass integration range - Max
            par[35] = z_start; // redshift integration range - zmin
            par[36] = z_up; // redshift integration range - zmax
            res = dPhidOmega(e_gev_min, e_gev_max, par);
            myfile << m_up << " " << z_up << " " << log10(res) << endl;
         }
      }
      myfile.close();
      cout << "Test 5 done. Output written in " + file << endl;
   } else if (test == 6) { // Test 6: Draw from mass function and random directions
      double z = 0., lnM = 0., theta, psi;
      int N_halo = 1e5;
      string file = gSIM_OUTPUT_DIR + "SZ_draw_random.dat";
      ofstream myfile;
      myfile.open(file.c_str());

      gRandom = new TRandom(); // create new random number generator
      gRandom->SetSeed(gSIM_SEED);

      for (int i = 0; i < N_halo; i++) {
         // Choosing random direction for cluster i
         double u = gRandom->Uniform(0., 1.);
         double v = gRandom->Uniform(0., 1.);
         psi = (-PI + 2 * PI * u) * 180. / PI; // degrees between -180 and 180
         theta = (-PI / 2. + acos(2.*v - 1.)) * 180. / PI; // degrees between -90 and 90 deg

         //Drawing mass and redshift of cluster i from mass function
         //gCOSMO_DNDVHDLNMH_Z->GetRandom2(z, lnM);
         myfile << theta << " " << psi << " " << z << " " <<  log10(exp(lnM)) << endl;
      }
      myfile.close();
      cout << "Test 6 done. Output written in " + file << endl;
   } else if (test == 7) { // Test 7: Mean number of halo in z and M range
      zmin = 0.05;
      zmax = 0.42;
      Mmin = 2 * Mmin * gCOSMO_HUBBLE;
      Mmax = Mmax * gCOSMO_HUBBLE;
      //double par_ref[4] = {zmin, zmax, Mmin, Mmax};
      double res = 0.;
      //dNdOmega(par_ref, res); // per steradian
      cout <<  "# halos/sr between z=[" << zmin << "," << zmax << "] and M=[" << Mmin << "," << Mmax << "] = " << res << endl;
      cout <<  "# halos (14000 deg^2) between z=[" << zmin << "," << zmax << "] and M=[" << Mmin << "," << Mmax << "] = " << (14000. / SR_to_DEG2)*res << endl;
      cout <<  "# halos (fullsky) between z=[" << zmin << "," << zmax << "] and M=[" << Mmin << "," << Mmax << "] = " << 4 * PI *res << endl;
   }
   return 0;
}

