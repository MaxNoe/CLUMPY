// CLUMPY includes
#include "../include/params.h"
#include "../include/spectra.h"

// ROOT includes
//#include <TCanvas.h>
//#include <TF3.h>
//#include <TFrame.h>
//#include <TGraph.h>
//#include <TH2D.h>
//#include <TLegend.h>
//#include <TMultiGraph.h>
//#include <TPaveText.h>
//#include <TRandom.h>
//#include <TStyle.h>

// C++ std libraries
using namespace std;
#include <iostream>
#include <math.h>
//#include <stdlib.h>
//#include <sys/stat.h>

int main(int argc, char *argv [])
{

   // double E[7]={3.47, 66.2,79.6,95.7,115,138,166};
   // double z=3.;

   // for(int i=0;i<7;i++){
   //   double res=exp_minus_opt_depth(E[i], z);
   //   printf("E=\%le tau=%le e^-tau=%le\n", E[i], -log(res), res);
   // }

   cout << argv[argc] << endl;

   double m_wimp = 1.; //Tev
   double log_E_min = -2;
   double log_E_max = log(m_wimp);
   double log_deltaE = (log_E_max - log_E_min) / 100.;
   for (int i = 0; i < 100; i++) {
      double E = pow(10., log_E_min + i * log_deltaE);
      double z = 0.;
      double par[3] = {1., 0, z};
      double res0 = dNdE(E, par);
      z = 0.1;
      par[2] = z;
      double res1 = dNdE(E, par);
      z = 2.;
      par[2] = z;
      double res2 = dNdE(E, par);
      z = 5.;
      par[2] = z;
      double res3 = dNdE(E, par);
      printf("%le %le %le %le %le\n", E / par[0], (E / par[0]) * (E / par[0])*res0, (E / par[0]) * (E / par[0])*res1,
             (E / par[0]) * (E / par[0])*res2, (E / par[0]) * (E / par[0])*res3);
   }
   return 0;

}
