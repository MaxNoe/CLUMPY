// CLUMPY includes
#include "../include/clumps.h"
#include "../include/geometry.h"
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/integr_los.h"
#include "../include/janalysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/stat.h"

// ROOT includes
#include <TCanvas.h>
#include <TF3.h>
#include <TFrame.h>
#include <TH2D.h>
#include <TLegend.h>
#include <TMultiGraph.h>
#include <TPaveText.h>
#include <TRandom.h>
#include <TStyle.h>

// C++ std libraries
#include <sys/stat.h>
#include <time.h>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <iostream>
#include <vector>
using namespace std;

// PARAMETERS VALUES FOR TEST
int istart_prof = 0;
int istop_prof = 9;
const int ngal = 9;
string gal_name_prof[ngal] = {"ISO", "NFW97", "M98", "DMS04", "EINASTO_M06", "EINASTO_N04", "DPDV_SPRINGEL08_ANTIBIASED", "DPDV_GAO04", "BURKERT"};
int    gal_card_prof[ngal] = { 0, 0, 0, 0, 3, 1, 2, 5, 4};
double gal_rs_smooth[ngal] = {4., 21.7, 34.5, 32.6, 250., 21.7, 1., 20., 9.26};
double gal_shape[ngal][4] = {{2., 2., 0., 0}, {1., 3., 1, 0}, {1.5, 3., 1.5, 0}, {1., 3., 1.2, 0}, {6., 0., 0., 3}, {0.17, 0., 0., 1}, {0.17, 0., 0., 2}, {19., 1.95, 2.9, 5}, {0., 0., 0., 4}};
double Delta = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, 0.);

//--- Dummy
void y_test(double &r, double par[1], double &res);
void y_test2(double &r, double par[1], double &res);

//--- Tests generic
void    test_goldenmin_and_find();
void    test_integr();
void    test_Dcosmo();
void    test_rhotot_and_sub();
void    test_profiles();
void    test_profiles_triaxiality();
void    test_profile2rhos();
void    test_mass_and_lum();

//--- los-integral related tests
void    test_los_int();
void    test_rho2_smooth();
void    test_jgal_smooth();
void    test_jgal_smooth_cumul();
void    test_jgal_smooth_skymap();

//---  Clump-related tests
void    test_R90_and_pointlikeness();
void    test_draw_positions_from_dpdv();
void    test_draw_masses_from_dpdm();
void    test_clump_distribs();
void    test_fNcl_clump();
void    test_cross_product_1cl();
void    test_cross_product_continuum();
void    test_mdelta1_to_mdelta2();


//______________________________________________________________________________
string legend_gal(int i)
{
   // To have halo name and its rs.

   char rs_name[500];
   if (gal_shape[i][3] == kZHAO)
      sprintf(rs_name, "%s=%s: r_{s}=%.2f kpc",
              gal_name_prof[i].c_str(), legend_for_profile(gal_shape[i], false).c_str(),
              gal_rs_smooth[i]);
   else
      sprintf(rs_name, "%s: r_{s}=%.2f kpc",
              legend_for_profile(gal_shape[i], false).c_str(),
              gal_rs_smooth[i]);

   return (string)rs_name;
}

//______________________________________________________________________________
void y_test(double &r, double par[1], double &res)
{
   //--- Dumy test function.

   res = par[0] * pow(r, -4.);

   //double x_gas = r / 50.;
   //double alpha_gas = 0.525;
   //double beta_gas = 0.768;
   //double xc_gas = 0.303;
   //double exp_gas = -3.*beta_gas / 2. + alpha_gas / 2.;
   //res = 4.*PI * r * r * pow(x_gas / xc_gas, -alpha_gas)
   //      * pow((1. + pow(x_gas / xc_gas, 2.)), exp_gas);
}

//______________________________________________________________________________
void y_test2(double &r, double par[1], double &res)
{
   //--- Another dummy test function.

   res = exp(-(r - par[0]) * (r - par[0]) / 0.1);
}

//______________________________________________________________________________
void y_testDcosmo(double &z, double par[1], double &res)
{
   //--- Test of Dcosmo.

   // Dummy
   par[0] += 0.;

   // [h] = 1
   // [h0] = km/s/kpc
   // [rho_c*omega_M] = Msol/kpc3
   // [c] = km/s
   // [h_z] = 1/s
   double h = 0.72;
   double h0 = h * 100 * 1.e-3;
   double OMEGA0_M = 0.26;
   double OMEGA0_LAMBDA = 0.74;
   double RHO0_C = 144;
   double c = 299792.458;
   double h_z = h0 * sqrt(OMEGA0_M * pow(1. + z, 3.) + OMEGA0_LAMBDA);
   //double tau = 0.038; // approx valid up do z = 6

   // [res] =  Msol/kpc3 * km/s / (km/s/kpc) = [Msol/kpc2]
   res = 4.*PI * RHO0_C * OMEGA0_M * c / h_z; //*exp(-tau);
}

//______________________________________________________________________________
//
//                         Tests generic
//______________________________________________________________________________
void test_goldenmin_and_find()
{
   //--- Tests find max of function and find point closest to a given value

   cout << endl;
   cout << "     ++++++++++++++++++++++++++++++++" << endl;
   cout << "     ++++ test_goldenmin_and_find +++" << endl;
   cout << "     ++++++++++++++++++++++++++++++++" << endl;

   double par = 0.5;
   double eps = 1.e-3;

   //double res = 0.;
   //for (double r=1.e-3; r<10.; r*=2.) {
   //   y_test2(r, &par, res);
   //   cout << r << " " << res << endl;
   //}

   for (par = 0.5; par < 2.; par += 0.25) {
      cout << "   - Test function is a Gaussian with peak at " << par << endl;
      double ax = par / 10., cx = par * 10., bx = sqrt(ax * cx);
      double rmax = 0.;
      double max = goldenmin_logstep(ax, bx, cx, y_test2, &par, eps, false, rmax);
      cout <<  "       goldenmin_logstep(): maximum=" << max << " reached for r=" << rmax << "  [tol=" << eps << "]" << endl;

      double y_ref = max / 10.;
      ax = par / 100.;
      double  r01left = 0.;
      find_x_logstep(y_ref, r01left, ax, rmax, y_test2, &par, eps);
      double max01 = 0.;
      y_test2(r01left, &par, max01);
      cout <<  "       find_x_logstep(): max/10 [left] reached for y(r=" << r01left << ")= " << max01 << "  [tol=" << eps << "]" << endl;


      double  r10right = 0.;
      find_x_logstep(y_ref, r10right, rmax, cx, y_test2, &par, eps);
      max01 = 0.;
      y_test2(r10right, &par, max01);
      cout <<  "       find_x_logstep(): max/10 [right] reached for y(r=" << r10right << ")=" << max01 << "  [tol=" << eps << "]" << endl;
      cout << endl;
   }
}

//______________________________________________________________________________
void test_integr()
{
   //--- Tests integrations routines from integr.h

   double xmin = 1., xmax = 2., par[1] = {1.};
   double res = 0.;
   int n = 25;
   bool is_verbose = true;
   double eps = 1.e-4;


   cout << endl;
   cout << "           ++++++++++++++++++++" << endl;
   cout << "           ++++ test_integr +++" << endl;
   cout << "           ++++++++++++++++++++" << endl;

   cout << "Integrate x^{-4} between 1. and 2." << endl;
   cout << "Reference (analytical) = 0.291667" << endl << endl;

   cout << "Simpson adaptive (precision=" << eps << "): " << endl;
   simpson_lin_adapt(y_test, xmin, xmax, par, res, eps, is_verbose);
   cout << "    => " << res << endl << endl;

   cout << "Simpson log-step adaptive (precision=" << eps << "): " << endl;
   simpson_log_adapt(y_test, xmin, xmax, par, res, eps, is_verbose);
   cout << "    => " << res << endl << endl;

   cout << "Simpson log-step (fixed number of steps=" << n << "): " << endl;
   simpson_log(y_test, xmin, xmax, par, res, n);
   cout << "    => " << res << endl << endl;
}

//______________________________________________________________________________
void test_Dcosmo()
{
   //--- Dummy test for Eq.(12) of Prada et al. (2012) for concentration.

   cout << endl;
   cout << "           ++++++++++++++++++++" << endl;
   cout << "           ++++ test_Dcosmo +++" << endl;
   cout << "           ++++++++++++++++++++" << endl;

   double zmin = 1., zmax = 20., par[1] = {1.};
   double res = 0.;
   int n = 25;
   bool is_verbose = true;
   double eps = 1.e-3;

   cout << "Simpson adaptive (precision=" << eps << "): " << endl;
   simpson_lin_adapt(y_testDcosmo, zmin, zmax, par, res, eps, is_verbose);
   cout << "    => " << res << endl << endl;

   cout << "Simpson log-step adaptive (precision=" << eps << "): " << endl;
   simpson_log_adapt(y_testDcosmo, zmin, zmax, par, res, eps, is_verbose);
   cout << "    => " << res << endl << endl;

   cout << "Simpson log-step (fixed number of steps=" << n << "): " << endl;
   simpson_log(y_testDcosmo, zmin, zmax, par, res, n);
   cout << "    => " << res << endl << endl;
}

//______________________________________________________________________________
void test_rhotot_and_sub()
{
   cout << endl;
   cout << "           ++++++++++++++++++++" << endl;
   cout << "           ++++ test_rhotot_and_sub +++" << endl;
   cout << "           ++++++++++++++++++++" << endl;

   //load_parameters("clumpy_params.txt", false);

   int prof_tot = 5;
   double par_tot[7];
   par_tot[0] = 1.;
   par_tot[1] = gal_rs_smooth[prof_tot];
   par_tot[2] = gal_shape[prof_tot][0];
   par_tot[3] = gal_shape[prof_tot][1];
   par_tot[4] = gal_shape[prof_tot][2];
   par_tot[5] = gal_card_prof[prof_tot];
   par_tot[6] = gMW_RMAX;
   set_par0_given_rhoref(par_tot, gMW_RSOL, gMW_RHOSOL);
   double M_tot = mass_singlehalo(par_tot, gSIM_EPS);

   int prof_cl = 5; // dpdv
   double par_cl[7];
   par_cl[0] = 1.;
   par_cl[1] = gal_rs_smooth[prof_cl];
   par_cl[2] = gal_shape[prof_cl][0];
   par_cl[3] = gal_shape[prof_cl][1];
   par_cl[4] = gal_shape[prof_cl][2];
   par_cl[5] = gal_card_prof[prof_cl];
   par_cl[6] = gMW_RMAX;
   dpdv_setnormprob(par_cl, gSIM_EPS); // set par_cl[0] to get a prob
   double frac;
   printf("Enter the clump mass fraction (0<f<1):  ");
   scanf("%le", &frac);
   par_cl[0] = M_tot * frac * par_cl[0]; // multiply by Mtot_cl to get "average" clump density

   int const n_par = 21;
   double par_mix[n_par];
   for (int i = 0; i < 6; i++) par_mix[i] = par_tot[i];
   for (int i = 11; i < 17; i++) par_mix[i] = par_cl[i - 11];
   double dummy = 0.;
   par_mix[6] = dummy;
   par_mix[7] = dummy;
   par_mix[8] = dummy;
   par_mix[9] = dummy;
   par_mix[10] = 1.; // to calculate rho1-rho2 in rho_mix()
   par_mix[17] = dummy;
   par_mix[18] = dummy;
   par_mix[19] = dummy;
   par_mix[20] = dummy;


   cout << "  - Total profile is " << gal_name_prof[prof_tot].c_str() << endl;
   cout << "  - Clump distribution is " << gal_name_prof[prof_cl].c_str() << endl;
   printf("  - rhos_tot=%le\n", par_tot[0]);

   int n = 1000;
   double pas = gMW_RMAX / double(n);
   vector<double> r;
   vector<double> rho_tot;
   vector<double> rho_cl;
   vector<double> rho_sm; // calculated using function rho_mix() - should equal rho_sm1

   double res1, res2, res3;
   for (int i = 0; i < n; ++i) {
      r.push_back(0.001 + i * pas);
      rho(r[i], par_tot, res1);
      rho(r[i], par_cl, res2);
      rho_mix(r[i], par_mix, res3);
      rho_tot.push_back(res1);
      rho_cl.push_back(res2);
      rho_sm.push_back(res3);
   }

   // Create directory "clumpy_test" to store output files
   string path = "clumpy_test/";
   mkdir(path.c_str(), S_IRWXU); // mode_t: read, write, execute/search by owner

   string fname = path + "test_rho_tot.dat";
   FILE *fp1;
   fp1 = fopen(fname.c_str(), "w");
   fprintf(fp1, "#     r         rho_tot       rho_cl     rho_sm     rho_tot=rho_cl+rho_sm\n\n");
   for (int i = 0; i < n; ++i)
      fprintf(fp1, "%le %le %le %le %le\n", r[i], rho_tot[i], rho_cl[i],
              rho_sm[i], rho_sm[i] + rho_cl[i]);
   fclose(fp1);
   cout << " => Test results printed in test_rho_tot.dat" << endl;



   //--- c_rhotot for halos
   TCanvas *c_rhotot = new TCanvas("c_rhotot", "c_rhotot", 650, 500);

   TMultiGraph *multi = new TMultiGraph();
   TLegend *leg = new TLegend(0.68, 0.58, 0.91, 0.92);

   TGraph *gr_tot = new TGraph(n, &r[0], &rho_tot[0]);
   gr_tot->SetLineColor(kBlack);
   gr_tot->SetLineWidth(3);
   gr_tot->SetLineStyle(1);
   multi->Add(gr_tot, "L");
   leg->AddEntry(gr_tot, "#rho_{tot}", "L");

   TGraph *gr_sm = new TGraph(n, &r[0], &rho_sm[0]);
   gr_sm->SetLineColor(kBlue);
   gr_sm->SetLineWidth(2);
   gr_sm->SetLineStyle(3);
   multi->Add(gr_sm, "L");
   leg->AddEntry(gr_sm, "#rho_{sm}", "L");

   TGraph *gr_cl = new TGraph(n, &r[0], &rho_cl[0]);
   gr_cl->SetLineColor(kRed);
   gr_cl->SetLineWidth(2);
   gr_cl->SetLineStyle(3);
   multi->Add(gr_cl, "L");
   leg->AddEntry(gr_cl, "<#rho_{cl}>", "L");

   TGraph *gr_tot2 = new TGraph(n);
   for (int i = 0; i < n; ++i)
      gr_tot2->SetPoint(i, r[i], rho_sm[i] + rho_cl[i]);
   gr_tot2->SetLineColor(kGreen + 1);
   gr_tot2->SetLineWidth(2);
   gr_tot2->SetLineStyle(2);
   multi->Add(gr_tot2, "L");
   leg->AddEntry(gr_tot2, "#rho_{sm}+<#rho_{cl}>", "L");

   multi->Draw("A");
   multi->GetXaxis()->SetTitle("r [kpc]");
   multi->GetYaxis()->SetTitle("#rho(r)  [M_{#odot} kpc^{-3}]");
   multi->SetMinimum(multi->GetYaxis()->GetXmin());
   multi->SetMaximum(multi->GetYaxis()->GetXmax());
   leg->Draw("SAME");
   c_rhotot->Update();
   c_rhotot->Modified();

   gSIM_ROOTAPP->Run(kTRUE);
}

//______________________________________________________________________________
void test_profiles()
{
   //--- Plots/Prints density profiles (rho^2, r^3*rho, r^2*rho^2) from profiles.h

   cout << endl;
   cout << "           ++++++++++++++++++++++" << endl;
   cout << "           ++++ test_profiles +++" << endl;
   cout << "           ++++++++++++++++++++++" << endl;

   //load_parameters("clumpy_params.txt", false);


   const int n = 4;
   FILE *fp[n] = {NULL, NULL, NULL, NULL};
   string names[n] = {"rho2_halo", "r2rho_halo", "r2rho2_cl", "r3rho_cl"};
   string x_leg[n] = {"r [kpc]", "r [kpc]", "r/r_{-2}", "r/r_{-2}"};
   string y_leg[n] = {"#rho^{2}(r)  [M_{#odot}/kpc^{3}]^{2}", "r^{2}#rho(r)  [M_{#odot}^{2}/kpc]",
                      "r^{2}#rho^{2}(r)  [M_{#odot}^{2}/kpc^{4}]" , "r^{3} #rho^{2}(r)  [M_{#odot}^{2}/kpc^{3}]"
                     };
   vector<vector<string> > f_names;
   vector<vector<bool> > is_draw;

   // Create directory "clumpy_test" to store output files
   string path = "clumpy_test/";
   mkdir(path.c_str(), S_IRWXU); // mode_t: read, write, execute/search by owner

   double m_cl = 1.e6;
   double rmin_halo = 1.e-1;
   double rmin_cl = 1.e-5;

   vector<int> i_std, i_cl;
   for (int i = istart_prof; i < istop_prof ; ++i) {
      cout << endl;
      cout << ">>>> Profile for " << gal_name_prof[i]
           << "  (rs= " << gal_rs_smooth[i] << " kpc)" << endl;

      vector<string> tmp_file;
      vector<bool> is_tmp;
      for (int k = 0; k < n; ++k) {
         tmp_file.push_back(path + names[k] + "_" + gal_name_prof[i] + ".dat");
         fp[k] = fopen(tmp_file[k].c_str(), "w");
         if ((k == 2 || k == 3) && gal_name_prof[i] == "ISO")
            is_tmp.push_back(false);
         else
            is_tmp.push_back(true);
      }
      f_names.push_back(tmp_file);
      is_draw.push_back(is_tmp);

      // For rho^2(r) smooth
      double par_sm[6] = {
         1., gal_rs_smooth[i], gal_shape[i][0], gal_shape[i][1],
         gal_shape[i][2], (double)gal_card_prof[i]
      };
      //  par[0]    UNUSED
      //  par[1]    Scale radius [kpc]
      //  par[2]    Shape parameter #1
      //  par[3]    Shape parameter #2
      //  par[4]    Shape parameter #3
      //  par[5]    card_profile [gENUM_PROFILE]
      set_par0_given_rhoref(par_sm, gMW_RSOL, gMW_RHOSOL);

      for (double r = rmin_halo; r < gMW_RMAX; r *= 1.1) {
         double res1 = 0., res2 = 0.;
         rho2(r, par_sm, res1);
         r2rho(r, par_sm, res2);
         fprintf(fp[0], "%le\t%le\n", r, res1);
         fprintf(fp[1], "%le\t%le\n", r, res2);
      }
      cout << "   => rho^2(r) stored in " << f_names[i][0] << endl;
      fclose(fp[0]);
      cout << "   => r^2rho(r) stored in " << f_names[i][1] << endl;
      fclose(fp[1]);


      // Check rsat found (at which rho(rsat)=rho_sat (not always reached)
      double rsat = get_rsat(par_sm);
      double rho_sat = 0.;
      rho(rsat, par_sm, rho_sat);
      if (rsat < 1.e-40)
         cout << "  rsat=" << rsat << "  =>  rho_sat not reached, rho(0)=" << rho_sat << endl;
      else
         cout << "  rsat=" << rsat << "  =>  rho(rsat)=" << rho_sat << endl;

      i_std.push_back(i);
      if (gal_name_prof[i] == "ISO")
         continue;

      // Check r_2, position at which slope of rho(r) is -2
      double r_m2 = get_r_2_from_rscale(par_sm[1], &par_sm[2]);
      if (r_m2 < 1.e-40)
         cout << "  r_2=" << r_m2 << "  =>  does not exist" << endl;
      else {
         // Calculate derivative at this point
         double h = 1.e-7 * r_m2;
         double lo = 0., up = 0.;
         double r_lo = r_m2 - h;
         double r_up = r_m2 + h;
         r2rho(r_lo, par_sm, lo);
         r2rho(r_up, par_sm, up);
         double slope = (up - lo) / (2.*h);
         cout << "  r_2=" << r_m2 << "  =>  d[r^2*rho(r)]_{r=r_m2]=" << slope << endl;
      }

      // For r^2*rho^2(r) for a clump of 10^6 Msol
      i_cl.push_back(i);
      double par_prof[4] = {gal_shape[i][0], gal_shape[i][1], gal_shape[i][2], (double)gal_card_prof[i]};
      //  par_prof[0]   DM profile shape parameter #1
      //  par_prof[1]   DM profile shape parameter #2
      //  par_prof[2]   DM profile shape parameter #3
      //  par_prof[3]   card_profile [gENUM_PROFILE]

      double par_cl[7];
      mdelta_to_par(par_cl, m_cl, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, par_prof, 1.e-2, 0.);

      double r_2 = par_cl[6] / mdelta_to_cdelta(m_cl, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, par_prof, 0.);
      for (double r = rmin_cl; r < gMW_RMAX; r *= 2.) {
         double res = 0.;
         r2rho2(r, par_cl, res);
         fprintf(fp[2], "%le\t%le\n", r / r_2, res);

         r3rho(r, par_cl, res);
         //fprintf(fp[3], "%le\t%le\n", r / r_2, res);
         fprintf(fp[3], "%le\t%le\n", r, res);
      }
      cout << "   => r^2*rho^2(r) stored in " << f_names[i][2] << endl;
      cout << "   => r^2*rho(r) stored in " << f_names[i][3] << endl;

      fclose(fp[2]);
      fclose(fp[3]);
   }
   cout << endl << endl;


   //----------//
   // DISPLAYS //
   //----------//
   TCanvas *plots[n];
   TMultiGraph *multi[n];
   TLegend *leg[n];

   for (int k = 0; k < n; ++k) {
      plots[k] = new TCanvas(names[k].c_str(), names[k].c_str());
      plots[k]->SetWindowPosition((k % 2) * 700, (k / 2) * 600);

      multi[k] = new TMultiGraph();
      string mg_name = "mg_" + names[k];
      multi[k]->SetName(mg_name.c_str());
      leg[k] = new TLegend(0.4, 0.45, 0.9, 0.95);
      TGraph **gr = new TGraph*[(int)i_std.size()];
      for (int i = 0; i < (int)i_std.size(); ++i) {
         if (!is_draw[i][k])
            continue;
         char gr_name[500];
         sprintf(gr_name, "gr_%s_%s_%d", names[k].c_str(), gal_name_prof[i].c_str(), k);
         gr[i] = new TGraph(f_names[i][k].c_str(), "%lg %lg");
         gr[i]->SetName(gr_name);
         gr[i]->SetLineColor(rootcolor(i));
         gr[i]->SetLineWidth(3);
         gr[i]->SetLineStyle(i);
         multi[k]->Add(gr[i], "L");
         string nam_leg = legend_gal(i);
         leg[k]->AddEntry(gr[i], nam_leg.c_str(), "L");
      }

      multi[k]->Draw("A");
      multi[k]->GetXaxis()->SetTitle(x_leg[k].c_str());
      multi[k]->GetYaxis()->SetTitle(y_leg[k].c_str());
      multi[k]->SetMinimum(multi[k]->GetYaxis()->GetXmin());
      multi[k]->SetMaximum(multi[k]->GetYaxis()->GetXmax());
      leg[k]->Draw("SAME");
      plots[k]->Update();
      plots[k]->Modified();
   }

   gSIM_ROOTAPP->Run(kTRUE);
}

//______________________________________________________________________________
void test_profiles_triaxiality()
{
   //--- Plots/Prints 2D density triaxial profiles

   cout << endl;
   cout << "           ++++++++++++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_profiles_triaxiality +++" << endl;
   cout << "           ++++++++++++++++++++++++++++++++++" << endl;

   //load_parameters("clumpy_params.txt", false);


   // Set Gal. profile parameters from loaded parameters
   double par_tot[10];
   gal_set_partot(par_tot);

   // Preliminary calculations
   // 1. Set steps
   int n = 50;
   double step = 2.*gMW_RMAX / double(2 * n + 1);

   // 2. Set contours (and range)
   const int n_res = 2;
   double log_res = 0., log_resmin[2] = {0., 0.}, log_resmax[2] = {0., 0.};
   double r = 0.;
   rho(r, par_tot, log_resmin[0]);
   log_resmin[0] = log(log_resmin[0]);
   r = 1.;
   r2rho2(r, par_tot, log_resmin[1]);
   log_resmin[1] = log(log_resmin[1]);

   r = gMW_RMAX / 1.1;
   rho(r, par_tot, log_resmax[0]);
   log_resmax[0] = log(log_resmax[0]);
   r2rho2(r, par_tot, log_resmax[1]);
   log_resmax[1] = log(log_resmax[1]);

   // determine contours
   int n_cont = 20;
   double contours[n_res][n_cont];
   for (int k = 0; k < n_res; ++k) {
      if (k == 0) cout << "   contours for rho" << endl;
      else if (k == 1) cout << "   contours for r2rho2" << endl;
      cout << log_resmin[k] << " " << log_resmax[k] << endl;

      for (int i = 0; i < n_cont; ++i) {
         double step_cont = (log_resmax[k] - log_resmin[k]) / (double)(n_cont - 1);
         contours[k][n_cont - i - 1] = log_resmin[k] + i * step_cont;
         cout << contours[k][n_cont - i - 1] << endl;
      }
   }

   // Allocate 3*3 pads: xy, xz, and yz view, for three values of the remaining coordidate (0,rmax/100, rmax/10)
   TCanvas *triax_rho = new TCanvas("triax_rho", "triax_rho", 0, 0, 800, 800);
   triax_rho->Divide(3, 3);
   TCanvas *triax_r2rho2 = new TCanvas("triax_integrang_j", "triax_integrang_j", 800, 0, 800, 800);
   triax_r2rho2->Divide(3, 3);

   const int n_hist_rho = 9;
   TH2D **hist_rho = new TH2D*[n_hist_rho];
   TH2D **hist_r2rho2 = new TH2D*[n_hist_rho];
   string names[n_hist_rho] = {"triaxial_xy_z0", "triaxial_xy_z1", "triaxial_xy_z2",
                               "triaxial_xz_y0", "triaxial_xz_y1", "triaxial_xz_y2",
                               "triaxial_yz_x0", "triaxial_yz_x1", "triaxial_yz_x2"
                              };
   double lastcoor[3] = {0., gMW_RMAX / 100., gMW_RMAX / 10.};
   string title[n_hist_rho] = {"triaxial_xy (z=0)", "triaxial_xy (z=rmax/100)", "triaxial_xy (z=rmax/10)",
                               "triaxial_xz (y=0)", "triaxial_xz (y=rmax/100)", "triaxial_xz (y=rmax/10)",
                               "triaxial_yz (x=0)", "triaxial_yz (x=rmax/100)", "triaxial_yz (x=rmax/10)"
                              };

   for (int ii = 0; ii < 3; ++ii) {
      for (int jj = 0; jj < 3; ++jj) {
         int k = 3 * ii + jj;
         string name = "rho_" + names[k];
         hist_rho[k] = new TH2D(name.c_str(), title[k].c_str(), 2 * n + 1, -gMW_RMAX, gMW_RMAX, 2 * n + 1, -gMW_RMAX, gMW_RMAX);
         name = "r2rho2_" + names[k];
         hist_r2rho2[k] = new TH2D(name.c_str(), title[k].c_str(), 2 * n + 1, -gMW_RMAX, gMW_RMAX, 2 * n + 1, -gMW_RMAX, gMW_RMAX);

         double x[3] = {0., 0., 0.};
         double *first = NULL, *second = NULL, *last = NULL;
         if (ii == 0) {
            first = &x[0];
            second = &x[1];
            last = &x[2];
         } else if (ii == 1) {
            first = &x[0];
            second = &x[2];
            last = &x[1];
         } else if (ii == 2) {
            first = &x[1];
            second = &x[2];
            last = &x[0];
         }
         *last = lastcoor[jj];

         // Load Histogram Data
         for (int i = 0; i < 2 * n + 1; i++) {
            *first = -gMW_RMAX + i * step;
            for (int j = 0; j < 2 * n + 1; j++) {
               *second = -gMW_RMAX + j * step;
               if (gMW_TRIAXIAL_IS)
                  r = get_riso_triaxial(x, gMW_TRIAXIAL_AXES, gMW_TRIAXIAL_ROTANGLES);
               else
                  r = sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);
               double res = 0.;
               rho(r, par_tot, res);
               log_res = log(res);
               hist_rho[k]->SetBinContent(i, j, log_res);
               r2rho2(r, par_tot, res);
               log_res = log(res);
               hist_r2rho2[k]->SetBinContent(i, j, log_res);
            }
         }
         first = NULL;
         second = NULL;
         last = NULL;

         // Draw rho
         triax_rho->cd(k + 1);
         hist_rho[k]->SetContour(n_cont, contours[0]);
         hist_rho[k]->Draw("CONT Z LIST");
         //hist_rho[k]->Draw("LEGO2Z");

         // Draw r2rho2
         triax_r2rho2->cd(k + 1);
         hist_r2rho2[k]->SetContour(n_cont, contours[1]);
         hist_r2rho2[k]->Draw("CONT Z LIST");
         //hist_r2rho2[k]->Draw("LEGO2Z");
      }
   }

   gStyle->SetPalette(0);
   gStyle->SetOptStat(0);
   gStyle->SetTitleW(0.99);
   gStyle->SetTitleH(0.08);

   triax_rho->GetFrame()->SetFillColor(0);
   triax_rho->GetFrame()->SetBorderSize(0);
   triax_rho->Modified();
   triax_rho->Update(); // Needed to force the plotting and retrieve the contours in TGraphs

   triax_r2rho2->GetFrame()->SetFillColor(0);
   triax_r2rho2->GetFrame()->SetBorderSize(0);
   triax_r2rho2->Modified();
   triax_r2rho2->Update(); // Needed to force the plotting and retrieve the contours in TGraphs

   gSIM_ROOTAPP->Run(kTRUE);

   delete[] hist_rho;
   delete triax_rho;

   delete[] hist_r2rho2;
   delete triax_r2rho2;
}

//______________________________________________________________________________
void test_profile2rhos()
{
   // Returns rhos for an input mass M(r<r_x)=m_x [Msol], with r_x [kpc]
   // (the profile parameters are set below)

   cout << endl;
   cout << "           ++++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_profile2rhos +++" << endl;
   cout << "           ++++++++++++++++++++++++++" << endl;
   cout << endl;

   double r_x   = 5.;    // kpc
   double m_x   = 1.e8;  // Msol
   double rs    = .1;    // kpc
   int card_prof = kZHAO;
   float  alpha = 1.;
   float  beta  = 3.;
   double eps = 1.e-3;

   const int n_g = 7;
   double gamma[n_g] = {0., 0.25, 0.5, 0.75, 1.0, 1.25, 1.5};
   //double triaxial_axes[3] = {1.0001, 1.0001, 1.0001};
   double triaxial_axes[3] = {1.47, 1.22, 0.74};
   //double triaxial_axes[3] = {0.74, 1.22, 1.47};
   //double triaxial_axes[3] = {1.5, 1., 1.};
   int indices_sel[3] = {0, 2, 4};
   int i_sel = 0;

   bool is_test_dehnen = true;
   if (is_test_dehnen) {
      printf("               (alpha,beta,gamma)  rs    rhos      Mtot\n");
      printf("dehnen cusped:      (1,4,1)        1.5   5.522e7   1.171e9\n");
      printf("dehnen cored:       (1,4,0.23)     1.5   1.177e8   1.802e9\n");
      double par_cusped[7] = {5.522e7, 1.5, 1., 4., 1., kZHAO, 10.};
      double par_cored[7] = {1.177e8, 1.5, 1., 4., 0.23, kZHAO, 10.};
      double triaxial_denhen[3] = {1.277182, 1.0217459, 0.766309};
      double m_cusped = mass_triaxialhalo(par_cusped, triaxial_denhen, eps);
      double m_cored = mass_triaxialhalo(par_cored, triaxial_denhen, eps);
      double m_cusped_sph = mass_singlehalo(par_cusped, eps);
      double m_cored_sph = mass_singlehalo(par_cored, eps);
      printf("  => triaxial mass:  m_clumpy(cusped) = %le,  m_clumpy(cored) = %le\n", m_cusped, m_cored);
      printf("  => spherical mass: m_clumpy(cusped) = %le,  m_clumpy(cored) = %le\n", m_cusped_sph, m_cored_sph);
      printf("\n");
   }

   printf("Constraint: M(r<%.2le kpc)=%.2le Msol\n", r_x, m_x);
   printf("%s  alpha=%f  beta=%f  rs=%.3f kpc\n\n",
          gNAMES_PROFILE[card_prof], alpha, beta, rs);
   printf("gamma  => rhos_spherical  rhos_triaxial    [Msol/kpc]\n");

   for (int i = 0; i < n_g; ++i) {
      if (indices_sel[i_sel] != i)
         continue;

      int i_current = indices_sel[i_sel];

      double par[7] = {1., rs, alpha, beta, gamma[i_current], (double)card_prof, r_x};
      //  par[0]    Dark matter density normalisation [Msol/kpc^3]
      //  par[1]    Scale radius [kpc]
      //  par[2]    Shape parameter #1
      //  par[3]    Shape parameter #2
      //  par[4]    Shape parameter #3
      //  par[5]    card_profile [gENUM_PROFILE]
      double m_unnorm = mass_singlehalo(par, eps);
      double rhos =  m_x / m_unnorm;
      par[0] = 1.;
      double m_unnorm_triaxial = mass_triaxialhalo(par, triaxial_axes, eps);
      double rhos_triaxial =  m_x / m_unnorm_triaxial;
      printf("%f  %le    %le\n", gamma[i_current], rhos, rhos_triaxial);

      ++ i_sel;
   }
   cout << endl;
}

//______________________________________________________________________________
void test_mass_and_lum()
{
   //--- Calculates mass and intrinsic luminosity for various profiles and find rsat
   // (based on mass_singlehalo() and lum_singlehalo_one() from profiles.h)
   // [also compares with analytical formulae when available]

   cout << endl;
   cout << "           +++++++++++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_mass_and_lum +++" << endl;
   cout << "           +++++++++++++++++++++++++++++++++" << endl;


   //load_parameters("clumpy_params.txt", false);
   bool is_verbose = false;
   double par[7];
   const int n_eps = 2;
   double eps[n_eps] = {1.e-3, 1.e-5};

   for (int j = 0; j < n_eps; ++j) {
      cout << endl;
      cout << "-------------------------------------" << endl;
      cout << "Relative precision: eps = " << eps[j] << endl;
      cout << "-------------------------------------" << endl;
      for (int i = istart_prof; i < istop_prof; ++i) {
         par[0] = 1.;
         par[1] = gal_rs_smooth[i];
         par[2] = gal_shape[i][0];
         par[3] = gal_shape[i][1];
         par[4] = gal_shape[i][2];
         par[5] = gal_card_prof[i];
         par[6] = gMW_RMAX;

         cout << endl;
         cout << "        *** " << gal_name_prof[i]
              << " ***" << endl;
         if (par[5] == kZHAO)
            cout << "      (alpha,beta,gamma)="
                 << gal_shape[i][0]
                 << "," << gal_shape[i][1]
                 << "," << gal_shape[i][2] << endl;
         cout << "   => [rsat = " << get_rsat(par) << " kpc]"
              << endl;

         // Calculate rho_s to have RHOSOL at Rsol
         set_par0_given_rhoref(par, gMW_RSOL, gMW_RHOSOL);

         double mass_num = mass_singlehalo(par, eps[j], true, is_verbose);
         double mass_ana = mass_singlehalo(par, eps[j], false);
         double axes[3] = {1., 1., 1.};
         double mass_triaxial = mass_triaxialhalo(par, axes, eps[j], is_verbose);
         const int n_par_tot = 21;
         double par_tmp[n_par_tot];
         for (int k = 0; k < 7; ++k)
            par_tmp[k] = par[k];
         for (int k = 7; k < 11; ++k)
            par_tmp[k] = 0.;
         for (int k = 11; k < n_par_tot; ++k)
            par_tmp[k] = 0.;


         double lum_num = lum_singlehalo_nosubs_mix(par_tmp, eps[j], true);
         double lum_ana = lum_singlehalo_nosubs_mix(par_tmp, eps[j], false);
         cout << "Mtot (num) = " << mass_num << " Msol    => (triaxial mass integration method) " << mass_triaxial << " Msol" << endl;
         if (strcmp(gal_name_prof[i].c_str(), "EINASTO_M06") == 0 || strcmp(gal_name_prof[i].c_str(), "NFW97") == 0)
            cout << "Mtot (ana) = " << mass_ana << " Msol" << endl;
         cout << "Ltot (num) = " << lum_num <<  " Msol^2/kpc^3" << endl;
         if (strcmp(gal_name_prof[i].c_str(), "NFW97") == 0)
            cout << "Ltot (ana) = " << lum_ana <<  " Msol^2/kpc^3" << endl;
      }
   }
   cout << endl << endl;
}



//______________________________________________________________________________
void test_substructures()
{
   //--- Calculates intrinsic luminosity for various levels of substructures
   // (based on lum_singlehalo() from clumps.h)

   cout << endl;
   cout << "           +++++++++++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_substructures +++" << endl;
   cout << "           +++++++++++++++++++++++++++++++++" << endl;


   //load_parameters("clumpy_params.txt", false);

   // Set Gal. parameters
   const int npar1 = 10;
   const int npar2 = 24;
   double par_galdpdv[npar1];
   double par_galsubs[npar2];
   double par_galtot[npar1];
   gal_set_partot(par_galtot);
   double m_gal = mass_singlehalo(par_galtot, gSIM_EPS);

   double f_dm = 0., ntot_subs = 0.;
   double mmin_subs = gDM_SUBS_MMIN;
   double mmax_subs = m_gal * gDM_SUBS_MMAXFRAC;

   gal_set_pardpdv(par_galdpdv);
   gal_set_parsubs(par_galsubs);

   ntot_subs = nsubtot_from_nsubm1m2(par_galsubs, gMW_SUBS_M1, gMW_SUBS_M2, gMW_SUBS_N_INM1M2, gSIM_EPS);
   f_dm =  ntot_subs * mean1cl_mass(par_galsubs, mmin_subs, mmax_subs, gSIM_EPS) / m_gal;
   cout << "  => f_dm=" << f_dm << endl;
   par_galsubs[9] = f_dm;

   double m = 1.e6;
   par_galsubs[7] = m;

   cout << "Ltot  = " << mean1cl_lumn_r_m(par_galsubs, 1) <<  " Msol^2/kpc^3" << endl;

}

//______________________________________________________________________________
//
//                         Test SMOOTH contribution
//______________________________________________________________________________
void test_rho2_smooth()
{
   //--- calculate rho2 smooth seen from Earth on various sky directions,

   cout << "           ++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_rho_smooth +++" << endl;
   cout << "           ++++++++++++++++++++++++" << endl;

   FILE *fp;
   double psi[6] = { -180., -45., -10., -1., -0.1, 0.};
   double par[7];
   for (int i = 0; i < 6; ++i) psi[i] *= (DEG_to_RAD);

   // Create directory "clumpy_test" to store output files
   string path = "clumpy_test/";
   mkdir(path.c_str(), S_IRWXU); // mode_t: read, write, execute/search by owner

   vector<string> f_names;
   for (int i = istart_prof; i < istop_prof ; ++i) {
      //  par[0]            Dark matter density normalisation [Msol/kpc^3]
      //  par[1]            Scale radius [kpc]
      //  par[2]            Shape parameter #1
      //  par[3]            Shape parameter #2
      //  par[4]            Shape parameter #3
      //  par[5]            card_profile [gENUM_PROFILE]
      //  par[6]            Effective radius where to stop integration [kpc]

      par[0] = 1.;
      par[1] = gal_rs_smooth[i];
      par[2] = gal_shape[i][0];
      par[3] = gal_shape[i][1];
      par[4] = gal_shape[i][2];
      par[5] = gal_card_prof[i];
      par[6] = gMW_RMAX;

      set_par0_given_rhoref(par, gMW_RSOL, gMW_RHOSOL);

      cout << ">>>> Profiles for " << gal_name_prof[i] << "  (rs= " << gal_rs_smooth[i] << " kpc)" << endl;

      string tmp = gal_name_prof[i];
      string f_name = path + "rho2_sm_psi_" + tmp + ".dat";
      f_names.push_back(f_name);
      fp = fopen(f_name.c_str(), "w");

      for (double l = 0; l < gMW_RMAX; l += 0.1) {
         fprintf(fp, "%le ", l);
         // Calculate for several psi
         for (int j = 0; j < 6; ++j) {
            double x[3] = {l, 0., 0.}; // l, alpha, beta
            double psi_theta[2] = {psi[j], 0.}; //psi,theta
            lalphabeta_to_xyzgal(x, psi_theta);
            double r = sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);
            double rho2_earth = 0.;
            rho2(r, par, rho2_earth);
            fprintf(fp, "%le ", rho2_earth);
            //cout << "l=" << l << "  rho2=" << rho_earth << endl;
         }
         fprintf(fp, "\n");
      }
      cout << "   => rho2(phi,theta=0) for l=[0,RvirGal] stored in " << f_name << endl;
      fclose(fp);
   }
   cout << endl << endl;


   //---------//
   // DISPLAY //
   //---------//
   for (int i = 0; i < 6; ++i) psi[i] /= (DEG_to_RAD);

   TCanvas *rho2_sm_psi = new TCanvas("rho2_sm_psi", "rho2_sm_psi", 19, 98, 650, 700);
   rho2_sm_psi->SetHighLightColor(0);
   rho2_sm_psi->Range(-0.5157081, -4.594267, 1.96531, -0.1544978);
   rho2_sm_psi->SetFillColor(0);
   rho2_sm_psi->SetBorderSize(0);
   rho2_sm_psi->SetLogx();
   rho2_sm_psi->SetLogy();
   rho2_sm_psi->SetGridx(0);
   rho2_sm_psi->SetGridy(0);
   rho2_sm_psi->SetFrameFillColor(0);
   rho2_sm_psi->SetLeftMargin(0.1);
   rho2_sm_psi->SetRightMargin(0.05);
   rho2_sm_psi->SetTopMargin(0.05);
   rho2_sm_psi->SetBottomMargin(0.1);
   rho2_sm_psi->SetFrameBorderMode(0);
   rho2_sm_psi->SetBorderMode(0);
   rho2_sm_psi->Divide(2, 3);

   const int n_prof = f_names.size();
   const int n_psi = 6;
   TGraph **gr = new TGraph*[n_prof * n_psi];
   TLegend *leg = new TLegend(0.6, 0.55, 0.89, 0.9);
   leg->SetFillColor(0);
   leg->SetTextSize(0.04);
   //leg->AddEntry("#rho_{sat} = 10^{19} Msol/kpc^{3}");

   for (int n = 1; n <= n_psi; ++n) {
      rho2_sm_psi->cd(n);
      rho2_sm_psi->GetPad(n)->SetFillColor(0);
      rho2_sm_psi->GetPad(n)->SetBorderSize(0);
      rho2_sm_psi->GetPad(n)->SetLogx();
      rho2_sm_psi->GetPad(n)->SetLogy();
      rho2_sm_psi->GetPad(n)->SetLeftMargin(0.12);
      rho2_sm_psi->GetPad(n)->SetRightMargin(0.01);
      rho2_sm_psi->GetPad(n)->SetTopMargin(0.01);
      rho2_sm_psi->GetPad(n)->SetBottomMargin(0.12);
      rho2_sm_psi->GetPad(n)->SetFrameBorderMode(0);
      rho2_sm_psi->GetPad(n)->SetBorderMode(0);
      rho2_sm_psi->GetPad(n)->SetFrameFillColor(0);

      string read = "%lg";
      for (int ii = 1; ii <= n - 1; ++ii) read.append(" %*lg");
      read += " %lg";
      //cout << n << "  " << read << endl;
      for (int i = 0; i < n_prof; ++i) {
         gr[(n - 1)*n_prof + i] = new TGraph(f_names[i].c_str(), read.c_str());
         //gr[(n-1)*n_prof + i]->Print();
         gr[(n - 1)*n_prof + i]->SetLineColor(rootcolor(i));
         gr[(n - 1)*n_prof + i]->SetLineWidth(3);
         gr[(n - 1)*n_prof + i]->SetLineStyle(i);
         //gr[(n-1)*n_prof + i]->SetMarkerStyle(20+i);

         if (i == 0) {
            gr[(n - 1)*n_prof + 0]->SetTitle("");
            gr[(n - 1)*n_prof + 0]->GetXaxis()->SetTitle("l [kpc]");
            gr[(n - 1)*n_prof + 0]->GetXaxis()->SetTitleOffset(0.8);
            gr[(n - 1)*n_prof + 0]->GetXaxis()->SetTitleFont(132);
            gr[(n - 1)*n_prof + 0]->GetXaxis()->SetTitleSize(0.06);
            gr[(n - 1)*n_prof + 0]->GetXaxis()->SetLabelFont(132);
            gr[(n - 1)*n_prof + 0]->GetXaxis()->SetLabelSize(0.06);
            gr[(n - 1)*n_prof + 0]->GetYaxis()->SetTitle("#rho^{2}(l,#theta,#psi)  [M_{#odot}^{2}/kpc^{6}]");
            gr[(n - 1)*n_prof + 0]->GetYaxis()->SetTitleOffset(1.);
            gr[(n - 1)*n_prof + 0]->GetYaxis()->SetTitleFont(132);
            gr[(n - 1)*n_prof + 0]->GetYaxis()->SetTitleSize(0.06);
            gr[(n - 1)*n_prof + 0]->GetYaxis()->SetLabelFont(132);
            gr[(n - 1)*n_prof + 0]->GetYaxis()->SetLabelSize(0.06);
            gr[(n - 1)*n_prof + 0]->Draw("AL");
         } else gr[(n - 1)*n_prof + i]->Draw("LSAME");
         if (n == 1) leg->AddEntry(gr[(n - 1)*n_prof + i], legend_gal(i).c_str(), "L");
      }
      char title[100];
      sprintf(title, "(#psi,#theta) = (%.1f^{o}, 0^{o})", psi[n - 1]);
      TPaveText *pt = new TPaveText(0.15, 0.15, 0.5, 0.3, "NDC");
      pt->SetBorderSize(0);
      pt->SetFillColor(0);
      pt->SetTextSize(0.055);
      pt->AddText(title);
      pt->Draw("SAME");
      leg->Draw("SAME");
      gPad->SetTickx(1);
      gPad->SetTicky(1);
   }
   // TCanvas::Update() draws the frame, after which one can change it
   rho2_sm_psi->Update();
   rho2_sm_psi->GetFrame()->SetFillColor(0);
   rho2_sm_psi->GetFrame()->SetBorderSize(0);
   rho2_sm_psi->Modified();

   gSIM_ROOTAPP->Run(kTRUE);
}

//______________________________________________________________________________
void test_jgal_smooth()
{
   //--- Test jgal_smooth for a given instrument resolution:
   //    Phi_astro = \int_{DOmega} dOmega \int_{l.o.s.} rho(l,psi, theta) dl

   cout << "           +++++++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_jgal_smooth +++" << endl;
   cout << "           +++++++++++++++++++++++++++++" << endl;

   //load_parameters("clumpy_params.txt", false);
   double frac;
   printf("Enter the clump mass fraction:\n");
   scanf("%le", &frac);

   // Create directory "clumpy_test" to store output files
   string path = "clumpy_test/";
   mkdir(path.c_str(), S_IRWXU); // mode_t: read, write, execute/search by owner

   double psi = 0., theta = 0.;
   vector<string> f_names;

   const int npar = 10;
   double par_tot[npar];
   double par_dpdv[npar];
   // Loop on various profiles
   for (int k = 0; k < 6 ; ++k) {
      // define total density properties
      //  par_tot[0]  rho_tot normalisation [kpc^{-3}]
      //  par_tot[1]  rho_tot scale radius [kpc]
      //  par_tot[2]  rho_tot shape parameter #1
      //  par_tot[3]  rho_tot shape parameter #2
      //  par_tot[4]  rho_tot shape parameter #3
      //  par_tot[5]  rho_tot card_profile [gENUM_PROFILE]
      //  par_tot[6]  Radius of rho_tot [kpc]
      //  par_tot[7]  Distance observer - host halo centre [kpc]
      //  par_tot[8]  Longitude (psi) of host halo [rad]
      //  par_tot[9]  Latitude (theta) of host halo [rad]
      par_tot[0] = 1.;
      par_tot[1] = gal_rs_smooth[k];
      par_tot[2] = gal_shape[k][0];
      par_tot[3] = gal_shape[k][1];
      par_tot[4] = gal_shape[k][2];
      par_tot[5] = gal_card_prof[k];
      par_tot[6] = gMW_RMAX;
      par_tot[7] = gMW_RSOL;
      par_tot[8] = 0.;
      par_tot[9] = 0.;
      set_par0_given_rhoref(par_tot, gMW_RSOL, gMW_RHOSOL);
      double mtot = mass_singlehalo(par_tot, gSIM_EPS);

      //define clump distribution properties
      //  par_dpdv[0]  dpdv normalisation [kpc^{-3}]
      //  par_dpdv[1]  dpdv scale radius [kpc]
      //  par_dpdv[2]  dpdv shape parameter #1
      //  par_dpdv[3]  dpdv shape parameter #2
      //  par_dpdv[4]  dpdv shape parameter #3
      //  par_dpdv[5]  dpdv card_profile [gENUM_PROFILE]
      //  par_dpdv[6]  Radius of dpdv [kpc]
      //  par_dpdv[7]  Distance observer - host halo centre [kpc]
      //  par_dpdv[8]  Longitude (psi) of host halo [rad]
      //  par_dpdv[9]  Latitude (theta) of host halo [rad]
      par_dpdv[0] = 1.;
      par_dpdv[1] = gal_rs_smooth[k];
      par_dpdv[2] = gal_shape[k][0];
      par_dpdv[3] = gal_shape[k][1];
      par_dpdv[4] = gal_shape[k][2];
      par_dpdv[5] = gal_card_prof[k];
      par_dpdv[6] = gMW_RMAX;
      par_dpdv[7] = gMW_RSOL;
      par_dpdv[8] = 0.;
      par_dpdv[9] = 0.;
      dpdv_setnormprob(par_dpdv, gSIM_EPS); // set par_dpdv[0] to get a prob [1/kpc3]

      FILE *fp;
      string tmp = gal_name_prof[k];
      string f_name = path + "jgal_sm_" + tmp + ".dat";
      f_names.push_back(f_name);
      fp = fopen(f_name.c_str(), "w");
      printf("============================\n");
      gMW_TOT_FLAG_PROFILE = k;
      cout << ">>>> Profiles for " << gal_name_prof[k] << "  (rs= " << gal_rs_smooth[k] << " kpc)";
      cout << " [precision = " << gSIM_EPS << "]" << endl;

      for (psi = -PI; psi <= PI ; psi += PI / 100.) {
         double res = jsmooth_mix(mtot, par_tot, psi, theta, gSIM_EPS, frac, par_dpdv);
         printf("(psi,theta) = (%le,%le) rad =>  jgal_sm = %le [Msol^2/kpc^5]\n", psi, theta, res);
         fprintf(fp, "%le %le %le\n", psi * 180. / PI, theta, res);
      }
      printf(">>>> Profile %d (%s) is printed in file\n", k, gal_name_prof[k].c_str());
      fclose(fp);
   }
   cout << endl << endl;


   //----------//
   // DISPLAYS //
   //----------//

   //--- rho2 for halos
   TCanvas *phi_astro_smooth = new TCanvas("phi_astro_smooth", "phi_astro_smooth", 650, 500);
   phi_astro_smooth->SetHighLightColor(0);
   phi_astro_smooth->Range(-0.5157081, -4.594267, 1.96531, -0.1544978);
   phi_astro_smooth->SetFillColor(0);
   phi_astro_smooth->SetBorderSize(0);
   phi_astro_smooth->SetLogx(0);
   phi_astro_smooth->SetLogy();
   phi_astro_smooth->SetGridx(0);
   phi_astro_smooth->SetGridy(0);
   phi_astro_smooth->SetFrameFillColor(0);
   phi_astro_smooth->SetFrameFillColor(0);
   phi_astro_smooth->SetFrameFillColor(0);
   phi_astro_smooth->SetLeftMargin(0.1);
   phi_astro_smooth->SetRightMargin(0.025);
   phi_astro_smooth->SetTopMargin(0.025);
   phi_astro_smooth->SetBottomMargin(0.1);
   phi_astro_smooth->SetFrameBorderMode(0);
   phi_astro_smooth->SetBorderMode(0);

   char alpha_exp[50];
   sprintf(alpha_exp, "Instrument aperture: #alpha_{\rm int} =%.2f^{o}", gSIM_ALPHAINT * 180. / PI);

   TMultiGraph *multi = new TMultiGraph();
   const int n = 6;
   TGraph **gr = new TGraph*[n];
   TLegend *leg = new TLegend(0.68, 0.58, 0.91, 0.92);
   leg->SetFillColor(0);
   leg->SetTextSize(0.028);
   for (int i = 0; i < (int)f_names.size(); ++i) {
      gr[i] = new TGraph(f_names[i].c_str(), "%lg %*s %lg");
      gr[i]->SetLineColor(rootcolor(i));
      gr[i]->SetLineWidth(3);
      gr[i]->SetLineStyle(i);
      multi->Add(gr[i], "L");
      leg->AddEntry(gr[i], legend_gal(i).c_str(), "L");
   }
   multi->Draw("A");
   multi->GetXaxis()->SetTitle("#psi [degree]");
   multi->GetXaxis()->SetTitleOffset(1.2);
   multi->GetXaxis()->SetTitleFont(132);
   multi->GetXaxis()->SetLabelFont(132);
   multi->GetXaxis()->SetLabelSize(0.04);
   multi->GetYaxis()->SetTitle("J_{sm}  [M_{#odot}^{2} kpc^{-5}]");
   multi->GetYaxis()->SetTitleOffset(1.075);
   multi->GetYaxis()->SetTitleFont(132);
   multi->GetYaxis()->SetTitleSize(0.045);
   multi->GetYaxis()->SetLabelFont(132);
   multi->GetYaxis()->SetLabelSize(0.04);
   leg->Draw("SAME");
   gPad->SetTickx(1);
   gPad->SetTicky(1);
   TPaveText pt(0.15, 0.85, 0.4, 0.9, "NDC");
   pt.SetBorderSize(0);
   pt.SetFillColor(0);
   pt.SetTextSize(0.03);
   pt.AddText(alpha_exp);
   pt.Draw();
   phi_astro_smooth->Update();
   phi_astro_smooth->Modified();

   gSIM_ROOTAPP->Run(kTRUE);
}

//______________________________________________________________________________
void test_jgal_smooth_cumul()
{
   //--- Cumulative Phi^{astro}_{sm}(psi, theta; lmin,lmax) for different lmax values
   // (various sky directions)

   cout << "           +++++++++++++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_jgal_smooth_cumul +++" << endl;
   cout << "           +++++++++++++++++++++++++++++++++++" << endl;

   //load_parameters("clumpy_params.txt", false);
   FILE *fp;
   const int n_psi = 4;
   double psi[n_psi] = { -180., -45., -10., -1.};
   for (int i = 0; i < n_psi; ++i) psi[i] *= (DEG_to_RAD);

   // Create directory "clumpy_test" to store output files
   string path = "clumpy_test/";
   mkdir(path.c_str(), S_IRWXU); // mode_t: read, write, execute/search by owner

   vector<string> f_names;
   double lmin = 0.;
   double lmax = gMW_RMAX + gMW_RSOL;
   double theta = 0;
   double par_tot[10];
   cout << " [precision = " << gSIM_EPS << "]" << endl;
   // Loop on profiles
   for (int i = istart_prof; i < istop_prof ; ++i) {
      //  par_tot[0]  rho_tot normalisation [kpc^{-3}]
      //  par_tot[1]  rho_tot scale radius [kpc]
      //  par_tot[2]  rho_tot shape parameter #1
      //  par_tot[3]  rho_tot shape parameter #2
      //  par_tot[4]  rho_tot shape parameter #3
      //  par_tot[5]  rho_tot card_profile [gENUM_PROFILE]
      //  par_tot[6]  Radius of rho_tot [kpc]
      //  par_tot[7]  Distance observer - host halo centre [kpc]
      //  par_tot[8]  Longitude (psi) of host halo [rad]
      //  par_tot[9]  Latitude (theta) of host halo [rad]
      par_tot[0] = 1.;
      par_tot[1] = gal_rs_smooth[i];
      par_tot[2] = gal_shape[i][0];
      par_tot[3] = gal_shape[i][1];
      par_tot[4] = gal_shape[i][2];
      par_tot[5] = gal_card_prof[i];
      par_tot[6] = gMW_RMAX;
      par_tot[7] = gMW_RSOL;
      par_tot[8] = 0.;
      par_tot[9] = 0.;
      set_par0_given_rhoref(par_tot, gMW_RSOL, gMW_RHOSOL);

      string tmp = gal_name_prof[i];
      string f_name = path + "cumul_smooth_psi_" + tmp + ".dat";
      f_names.push_back(f_name);
      fp = fopen(f_name.c_str(), "w");

      gMW_TOT_FLAG_PROFILE = gal_card_prof[i];
      cout << ">>>> Cumulative Phi_astro_sm for " << gal_name_prof[i] << "  (rs= " << gal_rs_smooth[i] << " kpc)"  << " [precision = " << gSIM_EPS << "]" << endl;

      // Calculate total flux in each psi directions
      double tot[n_psi];
      for (int j = 0; j < n_psi; ++j)
         tot[j] = jsmooth(par_tot, psi[j], theta, gSIM_EPS);

      // loop on various integration boundary lmax
      for (lmax = 0.1; lmax <= gMW_RMAX + gMW_RSOL; lmax *= 1.15) {
         fprintf(fp, "%le ", lmax);
         // Calculate for several psi
         for (int j = 0; j < n_psi; ++j) {
            double cumul = jsmooth(par_tot, psi[j], theta, lmin, lmax, gSIM_EPS) / tot[j];
            fprintf(fp, "%le ", cumul * 100.);
            //cout << "psi=" << psi[j] << "  l=" << lmax << "  cumul=" << cumul << endl;
         }
         fprintf(fp, "\n");
      }
      cout << "   => Cumul(J(phi,theta=0)) for l=[0,RvirGal] stored in " << f_name << endl << endl;
      fclose(fp);
   }
   cout << endl << endl;


   //---------//
   // DISPLAY //
   //---------//
   for (int i = 0; i < (int)f_names.size(); ++i) psi[i] /= (DEG_to_RAD);

   TCanvas *cumul_smooth_psi = new TCanvas("cumul_smooth_psi", "cumul_smooth_psi", 19, 98, 650, 600);
   cumul_smooth_psi->SetHighLightColor(0);
   cumul_smooth_psi->Range(-0.5157081, -4.594267, 1.96531, -0.1544978);
   cumul_smooth_psi->SetFillColor(0);
   cumul_smooth_psi->SetBorderSize(0);
   cumul_smooth_psi->SetFrameFillColor(0);
   cumul_smooth_psi->SetLeftMargin(0.1);
   cumul_smooth_psi->SetRightMargin(0.05);
   cumul_smooth_psi->SetTopMargin(0.05);
   cumul_smooth_psi->SetBottomMargin(0.1);
   cumul_smooth_psi->SetFrameBorderMode(0);
   cumul_smooth_psi->SetBorderMode(0);
   cumul_smooth_psi->Divide(2, 2);

   const int n_prof = f_names.size();
   TGraph **gr = new TGraph*[n_prof * n_psi];
   TLegend *leg = new TLegend(0.6, 0.15, 0.89, 0.6);
   leg->SetFillColor(0);
   leg->SetTextSize(0.04);
   //leg->AddEntry("#rho_{sat} = 10^{19} Msol/kpc^{3}");

   for (int n = 1; n <= n_psi; ++n) {
      cumul_smooth_psi->cd(n);
      cumul_smooth_psi->GetPad(n)->SetFillColor(0);
      cumul_smooth_psi->GetPad(n)->SetBorderSize(0);
      cumul_smooth_psi->GetPad(n)->SetLogx(1);
      cumul_smooth_psi->GetPad(n)->SetLogy(0);
      cumul_smooth_psi->GetPad(n)->SetLeftMargin(0.05);
      cumul_smooth_psi->GetPad(n)->SetRightMargin(0.01);
      cumul_smooth_psi->GetPad(n)->SetTopMargin(0.02);
      cumul_smooth_psi->GetPad(n)->SetBottomMargin(0.05);
      cumul_smooth_psi->GetPad(n)->SetFrameBorderMode(0);
      cumul_smooth_psi->GetPad(n)->SetBorderMode(0);
      cumul_smooth_psi->GetPad(n)->SetFrameFillColor(0);

      string read = "%lg";
      for (int ii = 1; ii <= n - 1; ++ii) read.append(" %*lg");
      read += " %lg";
      //cout << n << "  " << read << endl;
      for (int i = 0; i < n_prof; ++i) {
         gr[(n - 1)*n_prof + i] = new TGraph(f_names[i].c_str(), read.c_str());
         //gr[(n-1)*n_prof + i]->Print();
         gr[(n - 1)*n_prof + i]->SetLineColor(rootcolor(i));
         gr[(n - 1)*n_prof + i]->SetLineWidth(3);
         gr[(n - 1)*n_prof + i]->SetLineStyle(i);
         //gr[(n-1)*n_prof + i]->SetMarkerStyle(20+i);

         gr[(n - 1)*n_prof + i]->SetTitle("");
         gr[(n - 1)*n_prof + i]->GetXaxis()->SetTitle("l [kpc]");
         gr[(n - 1)*n_prof + i]->GetXaxis()->SetTitleOffset(0.8);
         gr[(n - 1)*n_prof + i]->GetXaxis()->SetTitleFont(132);
         gr[(n - 1)*n_prof + i]->GetXaxis()->SetTitleSize(0.06);
         gr[(n - 1)*n_prof + i]->GetXaxis()->SetLabelFont(132);
         gr[(n - 1)*n_prof + i]->GetXaxis()->SetLabelSize(0.06);
         gr[(n - 1)*n_prof + i]->GetYaxis()->SetTitle("Cumulative(#Phi^{astro}_{sm})");
         gr[(n - 1)*n_prof + i]->GetYaxis()->SetTitleOffset(1.);
         gr[(n - 1)*n_prof + i]->GetYaxis()->SetTitleFont(132);
         gr[(n - 1)*n_prof + i]->GetYaxis()->SetTitleSize(0.06);
         gr[(n - 1)*n_prof + i]->GetYaxis()->SetLabelFont(132);
         gr[(n - 1)*n_prof + i]->GetYaxis()->SetLabelSize(0.06);
         gr[(n - 1)*n_prof + i]->SetMinimum(0.);
         gr[(n - 1)*n_prof + i]->SetMaximum(100.);
         if (i == 0) gr[(n - 1)*n_prof + 0]->Draw("AL");
         else gr[(n - 1)*n_prof + i]->Draw("LSAME");
         if (n == 1) leg->AddEntry(gr[(n - 1)*n_prof + i], legend_gal(i).c_str(), "L");
      }
      char title[100];
      sprintf(title, "(#psi,#theta) = (%.1f^{o}, 0^{o})", psi[n - 1]);
      TPaveText *pt = new TPaveText(0.15, 0.85, 0.4, 0.95, "NDC");
      pt->SetBorderSize(0);
      pt->SetFillColor(0);
      pt->SetTextSize(0.055);
      pt->AddText(title);
      pt->Draw("SAME");
      leg->Draw("SAME");
      gPad->SetTickx(1);
      gPad->SetTicky(1);
   }

   cumul_smooth_psi->Update();
   cumul_smooth_psi->GetFrame()->SetFillColor(0);
   cumul_smooth_psi->GetFrame()->SetBorderSize(0);
   cumul_smooth_psi->Modified();

   gSIM_ROOTAPP->Run(kTRUE);
}

//______________________________________________________________________________
void test_jgal_smooth_skymap()
{
   //--- Skymap for the smooth profile

   cout << "           ++++++++++++++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_jgal_smooth_skymap +++" << endl;
   cout << "           ++++++++++++++++++++++++++++++++++++" << endl;

   //load_parameters("clumpy_params.txt", false);
   int prof_id = 1;

   cout << ">>>> Skymap Phi_astro_sm for " << gal_name_prof[prof_id] << "  (rs= " << gal_rs_smooth[prof_id] << " kpc)"  << " [precision = " << gSIM_EPS << "]" << endl;

   double par_tot[10];
   par_tot[0] = 1.;
   par_tot[1] = gal_rs_smooth[prof_id];
   par_tot[2] = gal_shape[prof_id][0];
   par_tot[3] = gal_shape[prof_id][1];
   par_tot[4] = gal_shape[prof_id][2];
   par_tot[5] = gal_card_prof[prof_id];
   par_tot[6] = gMW_RMAX;
   par_tot[7] = gMW_RSOL;
   par_tot[8] = 0.;
   par_tot[9] = 0.;
   set_par0_given_rhoref(par_tot, gMW_RSOL, gMW_RHOSOL);

   int n = 50;
   TH2D *h_smooth = new TH2D("h_smooth", "h_smooth", n, -180, 180, n, -90., 90.);

   double delta_psi = 2.*PI / (double)n;
   double delta_theta = PI / (double)n;
   for (double psi = -PI; psi < PI ; psi += delta_psi) {
      printf("psi = %le (loop also on theta)\n", psi);
      for (double theta = -PI / 2.; theta < PI / 2. ; theta += delta_theta) {
         double res = jsmooth(par_tot, psi, theta, gSIM_EPS);
         h_smooth->Fill((psi + delta_psi / 2.) * 180. / PI, (theta + delta_theta / 2.) * 180. / PI, res);
      }
   }

   char title[100];
   sprintf(title, "%s (#alpha_{\rm int} =%.2f^{o})", gal_name_prof[prof_id].c_str(), gSIM_ALPHAINT * 180. / PI);

   h_smooth->SetStats(0);
   h_smooth->SetTitle("");
   h_smooth->SetXTitle("#psi^{o}");
   h_smooth->SetYTitle("#theta^{o}");
   h_smooth->SetZTitle("#Phi^{astro}_{sm}");
   h_smooth->GetXaxis()->SetTitleOffset(0.8);
   h_smooth->GetXaxis()->SetTitleFont(132);
   h_smooth->GetXaxis()->SetTitleSize(0.06);
   h_smooth->GetXaxis()->SetLabelFont(132);
   h_smooth->GetXaxis()->SetLabelSize(0.04);
   h_smooth->GetYaxis()->SetTitleOffset(1.);
   h_smooth->GetYaxis()->SetTitleFont(132);
   h_smooth->GetYaxis()->SetTitleSize(0.06);
   h_smooth->GetYaxis()->SetLabelFont(132);
   h_smooth->GetYaxis()->SetLabelSize(0.04);
   h_smooth->GetZaxis()->SetTitleOffset(.8);
   h_smooth->GetZaxis()->SetTitleFont(132);
   h_smooth->GetZaxis()->SetTitleSize(0.06);
   h_smooth->GetZaxis()->SetLabelFont(132);
   h_smooth->GetZaxis()->SetLabelSize(0.05);

   cout << endl << endl;

   TCanvas *c_smooth_skymap = new TCanvas("c_smooth_skymap", "c_smooth_skymap", 500, 300);
   c_smooth_skymap->SetFillColor(0);
   c_smooth_skymap->SetBorderSize(0);
   c_smooth_skymap->GetFrame()->SetFillColor(0);
   c_smooth_skymap->GetFrame()->SetBorderSize(0);
   c_smooth_skymap->SetFrameBorderMode(0);
   c_smooth_skymap->SetFrameFillColor(0);
   c_smooth_skymap->SetLeftMargin(0.1);
   c_smooth_skymap->SetRightMargin(0.05);
   c_smooth_skymap->SetTopMargin(0.05);
   c_smooth_skymap->SetBottomMargin(0.1);
   c_smooth_skymap->SetFrameBorderMode(0);
   c_smooth_skymap->SetBorderMode(0);
   c_smooth_skymap->SetLogz(1);
   gStyle->SetPalette(1);
   //  hxy_xyz->Draw("contz");
   // hxy_xyz->Draw("lego1");
   h_smooth->Draw("surf3");
   TPaveText *pt = new TPaveText(0.01, 0.9208065, 0.3471141, 0.995, "blNDC");
   pt->SetBorderSize(2);
   pt->SetFillColor(0);
   pt->SetLineColor(1);
   pt->SetLineWidth(1);
   pt->AddText(title);
   pt->Draw();

   c_smooth_skymap->Modified();
   c_smooth_skymap->Update();

   gSIM_ROOTAPP->Run(kTRUE);
}

//______________________________________________________________________________
//
//                         Test others
//______________________________________________________________________________
void test_R90_and_pointlikeness()
{
   // Find radius where x% of the luminosity is emitted, and test r_cl/d_cl<<1

   cout << "           +++++++++++++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_R90_and_pointlikeness +++" << endl;
   cout << "           +++++++++++++++++++++++++++++++++++" << endl;

   //load_parameters("clumpy_params.txt", false);
   double m_cl = 1.e6;
   double par_tot[7];
   for (int i = 1; i < 6 ; ++i) {
      // Clump parameters
      //  par_tot[0]  rho_tot normalisation [kpc^{-3}]
      //  par_tot[1]  rho_tot scale radius [kpc]
      //  par_tot[2]  rho_tot shape parameter #1
      //  par_tot[3]  rho_tot shape parameter #2
      //  par_tot[4]  rho_tot shape parameter #3
      //  par_tot[5]  rho_tot card_profile [gENUM_PROFILE]
      //  par_tot[6]  Radius of rho_tot [kpc]
      par_tot[0] = 1.;
      par_tot[1] = 1.;
      par_tot[2] = gal_shape[i][0];
      par_tot[3] = gal_shape[i][1];
      par_tot[4] = gal_shape[i][2];
      par_tot[5] = gal_card_prof[i];
      par_tot[6] = mdelta_to_Rdelta(m_cl, Delta, 0.);
      par_tot[1] = mdelta_to_rscale(m_cl, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, &par_tot[2], 0);
      set_par0_given_mref(par_tot, par_tot[6], m_cl, gSIM_EPS); // setting par_tot[0]

      printf(">>>> Profile is %s:  rvir=%le (kpc)   rs=%le (kpc)   rhos=%le (Msol/kpc3)\n", gal_name_prof[i].c_str(), par_tot[6], par_tot[1], par_tot[0]);


      const int n_par_tot = 21;
      double par_tmp[n_par_tot];
      for (int j = 0; j < 7; ++j)
         par_tmp[j] = par_tot[j];
      for (int j = 7; j < 11; ++j)
         par_tmp[j] = 0.;
      for (int j = 11; j < n_par_tot; ++j)
         par_tmp[j] = 0.;

      double lum_cl = lum_singlehalo_nosubs_mix(par_tmp, gSIM_EPS);
      cout << "lum_cl=  " << lum_cl << endl;
      const int n_pc = 4;
      double x_pc[n_pc] = {0.5, 0.9, 0.95, 0.99};
      for (int n = 0; n < n_pc; ++n) {
         double radius = par_tot[6];
         find_fraclumref_radius(par_tot, par_tot[6], lum_cl, x_pc[n], radius);
         printf("   - r(x_percent_lum=%f) = %le (kpc)  [%.3f rvir]\n", x_pc[n], radius, radius / par_tot[6]);
      }
      printf("\n");

      printf("  (+ we consider point-like if radius(x percent luminosity)/d_cl<<1)\n");
      double criterion = 0.1;
      for (int n = 0; n < n_pc; ++n) {
         double radius = par_tot[6];
         find_fraclumref_radius(par_tot, par_tot[6], lum_cl, x_pc[n], radius);
         find_fracjpointlike_dist(par_tot, x_pc[n], criterion);
         printf("   - point-like(x_percent_lum=%f) if d_cl > %le kpc\n", x_pc[n], criterion);
      }
      printf("\n");
   }
   printf("\n\n");
}

//______________________________________________________________________________
void test_los_int()
{
   // Test integr_los for luminosity of a clump

   cout << "           +++++++++++++++++++++++++++++++++++" << endl;
   cout << "           ++ test integr_los on clump lum +++" << endl;
   cout << "           +++++++++++++++++++++++++++++++++++" << endl;

   //load_parameters("clumpy_params.txt", false);
   cout << "  [Integration precision is gSIM_EPS=" << gSIM_EPS << "]" << endl;
   double m_cl = 1.e6;
   double tmp;
   double par_tot[10];
   double dist = 100.;
   for (int i = 1; i < 6 ; ++i) {
      // Clump parameters
      //  par_tot[0]  rho_tot normalisation [kpc^{-3}]
      //  par_tot[1]  rho_tot scale radius [kpc]
      //  par_tot[2]  rho_tot shape parameter #1
      //  par_tot[3]  rho_tot shape parameter #2
      //  par_tot[4]  rho_tot shape parameter #3
      //  par_tot[5]  rho_tot card_profile [gENUM_PROFILE]
      //  par_tot[6]  Radius of rho_tot [kpc]
      //  par_tot[7]     l_cl: distance observer - clump centre [kpc]
      //  par_tot[8]     psi_cl: to clump centre [rad]
      //  par_tot[9]     theta_cl: to clump centre [rad]
      par_tot[0] = 1.;
      par_tot[1] = 1.;
      par_tot[2] = gal_shape[i][0];
      par_tot[3] = gal_shape[i][1];
      par_tot[4] = gal_shape[i][2];
      par_tot[5] = gal_card_prof[i];
      par_tot[6] = mdelta_to_Rdelta(m_cl, Delta, 0.);
      par_tot[7] = dist;
      par_tot[8] = 0.;
      par_tot[9] = 0.;
      par_tot[1] = mdelta_to_rscale(m_cl, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, &par_tot[2], 0.);
      set_par0_given_mref(par_tot, par_tot[6], m_cl, gSIM_EPS); // setting par_tot[0]

      printf(">>>> Profile is %s:  rvir=%le (kpc)   rs=%le (kpc)   rhos=%le (Msol/kpc3)\n", gal_name_prof[i].c_str(), par_tot[6], par_tot[1], par_tot[0]);


      const int n_par_tot = 21;
      double par_tmp[n_par_tot];
      for (int j = 0; j < 7; ++j)
         par_tmp[j] = par_tot[j];
      for (int j = 7; j < 11; ++j)
         par_tmp[j] = 0.;
      for (int j = 11; j < n_par_tot; ++j)
         par_tmp[j] = 0.;

      double lum_single = lum_singlehalo_nosubs_mix(par_tmp, gSIM_EPS);
      double mass_single = mass_singlehalo(par_tot, gSIM_EPS);

      double psi_los = 0, theta_los = 0;
      double lmin = max(0., par_tot[7] - par_tot[6]);
      double lmax = par_tot[7] + par_tot[6];
      double alpha_cl = atan(par_tot[6] / par_tot[7]); // angular size of clump

      int switch_f = 6; // integrates l^2*rho^2 along the los.
      tmp = gSIM_ALPHAINT;
      gSIM_ALPHAINT = alpha_cl;
      double lum_los = los_integral(par_tot, switch_f, psi_los, theta_los, lmin, lmax, gSIM_EPS, 0);

      switch_f = 2; // integrates l^2*rho along the los.
      double mass_los = los_integral(par_tot, switch_f, psi_los, theta_los, lmin, lmax, gSIM_EPS);
      printf("lum_single = %le  lum_los = %le\n", lum_single, lum_los);
      printf("mass_single = %le  mass_los = %le\n\n", mass_single, mass_los);
   }
   gSIM_ALPHAINT = tmp;
}

//______________________________________________________________________________
void test_draw_positions_from_dpdv()
{
   //--- Test dPdV from different frameworks

   cout << "           ++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_dpdv_clump +++" << endl;
   cout << "           ++++++++++++++++++++++++" << endl;


   cout << "We draw clumps from dPdV (spatial distribution of clumps in the Galaxy) "
        "calculated from different system of coordinates. These plots check that "
        "the formulae to change coordinates are correctly implemented" << endl << endl;

   int id_prof = 5;

   // Clump global distribution parameters
   double par[8] = {
      1., gal_rs_smooth[id_prof], gal_shape[id_prof][0], gal_shape[id_prof][1], gal_shape[id_prof][2],
      (double)gal_card_prof[id_prof], gMW_RMAX, gMW_RSOL
   };

   double eps = 1.e-2;
   dpdv_setnormprob(par, eps);

   const double Ncl = 1.e6;

   //----------------------------
   // Draw clumps (x,y,z) from GC (cartesian coordinates)
   //----------------------------
   clock_t start = clock();
   cout << ".... processing....\n dPdV from (x,y,z) [GC framework]" << endl;

   double dz = 0.5;
   TF3 *f_xyz = new TF3("f_xyz", dpdv_xyz, -gMW_RMAX, gMW_RMAX, -gMW_RMAX, gMW_RMAX, -dz, dz, 8);
   f_xyz->SetNpx(100);
   f_xyz->SetNpy(100);
   f_xyz->SetNpz(100);
   f_xyz->SetParameters(par);

   // Draw Ncl and store in hist
   TH2D *hxy_xyz = new TH2D("hxy_xyz", "hxy_xyz", 100, -gMW_RMAX, gMW_RMAX, 100, -gMW_RMAX, gMW_RMAX);
   gRandom->SetSeed(777);
   for (double n = 0.; n < Ncl; n += 1.) {
      double x1, x2, x3; // positions x,y,z of the clump
      f_xyz->GetRandom3(x1, x2, x3);
      hxy_xyz->Fill(x1, x2);
      //cout << "clump #" << n << "  x1=" << x1 << "  x2=" << x2 << "  x3=" << x3 << endl;
   }
   clock_t stop = clock();
   cout << "  => took " << (stop - start) / CLOCKS_PER_SEC << " s" << endl << endl;

   TCanvas *c_xyz = new TCanvas("c_xyz", "Test draw distrib.", 600, 400);
   c_xyz->SetFillColor(0);
   c_xyz->SetBorderSize(0);
   c_xyz->GetFrame()->SetFillColor(0);
   c_xyz->GetFrame()->SetBorderSize(0);
   c_xyz->SetFrameBorderMode(0);
   c_xyz->SetLogz(1);
   gStyle->SetPalette(1);
   hxy_xyz->Draw("surf3");
   c_xyz->Modified();
   c_xyz->Update();


   //----------------------------------
   // Draw clumps (r,theta,psi) from GC (spherical coordinates)
   //----------------------------------
   start = clock();
   cout << ".... processing....\n dPdV from (r,theta,phi) [GC framework]" << endl;

   TF3 *f_rthetapsi = new TF3("f_rthetapsi", dpdv_rthetapsi, 0., gMW_RMAX, 0., PI, 0., 2.*PI, 8);
   f_rthetapsi->SetNpx(100);
   f_rthetapsi->SetNpy(100);
   f_rthetapsi->SetNpz(100);
   f_rthetapsi->SetParameters(par);

   // Draw Ncl and store in hist
   TH2D *hxy_rthetapsi = new TH2D("hxy_rthetapsi", "hxy_rthetapsi", 100, -gMW_RMAX, gMW_RMAX, 100, -gMW_RMAX, gMW_RMAX);
   gRandom->SetSeed(777);
   for (double n = 0.; n < 5.e1 * Ncl; n += 1.) {
      double r, theta, psi;
      f_rthetapsi->GetRandom3(r, theta, psi);
      double x = r * cos(psi) * sin(theta);
      double y = r * sin(theta) * sin(psi);
      double z = r * cos(theta);
      if (fabs(z) < dz) hxy_rthetapsi->Fill(x, y);
      //   cout << "clump #" << n << "  x1=" <<  r*cos(psi)*sin(theta)<< "  x2=" << r*sin(theta)*sin(psi)<< endl;
   }
   stop = clock();
   cout << "  => took " << (stop - start) / CLOCKS_PER_SEC << " s" << endl << endl;

   TCanvas *c_rthetapsi = new TCanvas("c_rthetapsi", "Test draw distrib.", 600, 400);
   c_rthetapsi->SetFillColor(0);
   c_rthetapsi->SetBorderSize(0);
   c_rthetapsi->GetFrame()->SetFillColor(0);
   c_rthetapsi->GetFrame()->SetBorderSize(0);
   c_rthetapsi->SetFrameBorderMode(0);
   c_rthetapsi->SetLogz(1);
   hxy_rthetapsi->Draw("surf3");
   c_rthetapsi->Modified();
   c_rthetapsi->Update();



   //---------------------------------------
   // Draw clumps (l,theta0,phi0) from Earth (spherical coordinates centered on Earth position)
   //---------------------------------------
   start = clock();
   cout << ".... processing....\n dPdV from (l,theta,phi) [Earth framework]" << endl;

   TF3 *f_lthetapsi = new TF3("f_lthetapsi", dpdv_lthetapsi, 0, gMW_RMAX + gMW_RSOL, -PI / 2, PI / 2, -PI, PI, 8);
   f_lthetapsi->SetNpx(100);
   f_lthetapsi->SetNpy(100);
   f_lthetapsi->SetNpz(100);
   f_lthetapsi->SetParameters(par);

   // Draw Ncl and store in hist
   TH2D *hxy_lthetapsi = new TH2D("hxy_lthetapsi", "hxy_lthetapsi", 100, -gMW_RMAX, gMW_RMAX, 100, -gMW_RMAX, gMW_RMAX);
   gRandom->SetSeed(777);
   for (double n = 0.; n < 5.e1 * Ncl; n += 1.) {
      double l, theta0, psi0;
      f_lthetapsi->GetRandom3(l, theta0, psi0);
      double x = l * cos(psi0) * cos(theta0) - par[7];
      double y = l * cos(theta0) * sin(psi0);
      double z = l * sin(theta0);
      if (fabs(z) < dz) hxy_lthetapsi->Fill(x, y);
      //cout << "clump #" << n << "  x1=" << x1 << "  x2=" << x2 << "  x3=" << x3 << endl;
   }
   stop = clock();
   cout << "  => took " << (stop - start) / CLOCKS_PER_SEC << " s" << endl << endl;

   TCanvas *c_lthetapsi = new TCanvas("c_lthetapsi", "Test draw distrib.", 600, 400);
   c_lthetapsi->SetFillColor(0);
   c_lthetapsi->SetBorderSize(0);
   c_lthetapsi->GetFrame()->SetFillColor(0);
   c_lthetapsi->GetFrame()->SetBorderSize(0);
   c_lthetapsi->SetFrameBorderMode(0);
   c_lthetapsi->SetLogz(1);
   hxy_lthetapsi->Draw("surf3");
   c_lthetapsi->Modified();
   c_lthetapsi->Update();

   cout << "... finished..." << endl << endl;
   gSIM_ROOTAPP->Run(kTRUE);

   // Free memory
   delete f_xyz;
   delete hxy_xyz;
   delete c_xyz;
   delete f_rthetapsi;
   delete hxy_rthetapsi;
   delete c_rthetapsi;
   delete f_lthetapsi;
   delete hxy_lthetapsi;
   delete c_lthetapsi;
}

//______________________________________________________________________________
void test_draw_masses_from_dpdm()
{
   //--- Draw masses from dpdm

   cout << "           +++++++++++++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_draw_masses_from_dpdm +++" << endl;
   cout << "           +++++++++++++++++++++++++++++++++++" << endl;

   //load_parameters("clumpy_params.txt", false);
   double gMW_CLUMPS_MMIN = 1.e-6;
   double gMW_CLUMPS_MMAX = 1.e10;

   // 1. Total number of clumps in the Galaxy
   double par_dpdm[2] = {1., gMW_SUBS_DPDM_SLOPE};
   dpdm_setnormprob(par_dpdm,  gMW_CLUMPS_MMIN, gMW_CLUMPS_MMAX, gSIM_EPS);
   double Ntot_cl = nsubtot_from_nsubm1m2(par_dpdm, gMW_SUBS_M1, gMW_SUBS_M2, gMW_SUBS_N_INM1M2, gSIM_EPS);

   // 2. Number of clumps to draw in a given mass range
   double log_min_mass = -4.;
   double log_max_mass = 10.;
   const int nb_decades = (int)(log_max_mass - log_min_mass);
   double mmin_draw = pow(10., log_min_mass);
   double mmax_draw = pow(10., log_max_mass);
   double Ntot_cl_drawn = Ntot_cl * frac_nsubs_in_m1m2(par_dpdm, mmin_draw, mmax_draw, gSIM_EPS);


   // 3. Number of clumps in the l.o.s. to be drawn
   double theta = 0.;
   double psi = 0.;
   printf(">>>>> Calculate number of clumps (to be drawn) in the mass decades [%.1f,%.1f]\n", log_min_mass, log_max_mass);
   printf("    N.B.: l.o.s. direction is (%.1f,%.1f)\n", psi, theta);

   int prof_id1 = 1;// for clumps spatial distribution - NFW
   double par_dpdv[10] = {
      1., gal_rs_smooth[prof_id1], gal_shape[prof_id1][0], gal_shape[prof_id1][1], gal_shape[prof_id1][2],
      (double)gal_card_prof[prof_id1], gMW_RMAX, gMW_RSOL, 0., 0.
   };
   dpdv_setnormprob(par_dpdv, gSIM_EPS);
   double lmin = 0.;
   double lmax = gMW_RMAX + gMW_RSOL;
   double frac = frac_nsubs_in_foi(par_dpdv , psi, theta, lmin, lmax, gSIM_EPS);
   double N_cl_in_domega = frac * Ntot_cl;
   double N_cl_in_domega_drawn = frac * Ntot_cl_drawn;
   printf("N_tot (in Galaxy) = %le\nN_drawn (in mass range) = %le\nFraction (in l.o.s.) = %le\n", Ntot_cl, Ntot_cl_drawn, frac);
   printf("   => N_cl_in_domega = %le\n", N_cl_in_domega);
   printf("   => N_cl_in_domega_drawn = %le\n", N_cl_in_domega_drawn);
   cout << endl;

   // 4. Break into decades of mass
   double clumps_per_decade[nb_decades];
   double tot = 0;
   for (int i = 0; i < nb_decades; i++) {
      mmin_draw = pow(10, log_min_mass) * pow(10., i);
      mmax_draw = 10 * mmin_draw;
      Ntot_cl_drawn = Ntot_cl * frac_nsubs_in_m1m2(par_dpdm, mmin_draw, mmax_draw, gSIM_EPS);
      printf("%le %le %le\n", 5.*mmin_draw, Ntot_cl_drawn, frac * Ntot_cl_drawn);
      clumps_per_decade[i] = frac * Ntot_cl_drawn; // clumps in the mass decade, in the observation cone
      tot = tot + clumps_per_decade[i];
   }
   printf("Check that the numbers add up: tot_drawn_los=%le sum_over_decades=%le\n", N_cl_in_domega_drawn, tot);


   // 5. Do the drawing
   gRandom->SetSeed(778);
   TF1 *f_mass = new TF1("f_mass", dpdlogm, log10(gMW_CLUMPS_MMIN), log10(gMW_CLUMPS_MMAX), 2);
   f_mass->SetNpx(100);
   double norm = (1. - gMW_SUBS_DPDM_SLOPE) / (pow(gMW_CLUMPS_MMAX, 1. - gMW_SUBS_DPDM_SLOPE) - pow(gMW_CLUMPS_MMIN, 1. - gMW_SUBS_DPDM_SLOPE));
   f_mass->SetParameters(norm, gMW_SUBS_DPDM_SLOPE);

   double log_mass_cl;

   TH1D *h2 = new TH1D("h2", "h2", nb_decades * 20, log_min_mass, log_max_mass);
   TH1D *h2bis = new TH1D("h2bis", "h2bis", 100, log_min_mass, log_max_mass);

   int prof_id2 = 1;// for clumps inner density - NFW
   double par_prof[4] = {gal_shape[prof_id2][0], gal_shape[prof_id2][1], gal_shape[prof_id2][2], (double)gal_card_prof[prof_id2]};
   for (int i = 0; i < int(nb_decades); i++) {
      if (clumps_per_decade[i] > 1) {
         printf("drawing %d clumps \n", int(clumps_per_decade[i]));
         for (int n = 1; n <= int(clumps_per_decade[i]); n++) {
            log_mass_cl = f_mass->GetRandom(log_min_mass + i, log_min_mass + i + 1); // random in each mass decade
            h2->Fill(log_mass_cl);
            double mass = pow(10.,  log_mass_cl);
            double par_cl[7];
            mdelta_to_par(par_cl, mass, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, par_prof, gSIM_EPS, 0.);
            const int n_par_tot = 21;
            double par_tmp[n_par_tot];
            for (int j = 0; j < 7; ++j)
               par_tmp[j] = par_cl[j];
            for (int j = 7; j < 11; ++j)
               par_tmp[j] = 0.;
            for (int j = 11; j < n_par_tot; ++j)
               par_tmp[j] = 0.;

            double L_clump = lum_singlehalo_nosubs_mix(par_tmp, 1.e-4); // clump intrinsic luminosity [Msol^2/kpc^3]
            h2bis->Fill(log_mass_cl, L_clump);
         }
      } else printf("no good ");
      printf("%d done\n", i);
   }

   TH1D *h3 = new TH1D("h3", "h3", nb_decades * 20, log_min_mass, log_max_mass);
   TH1D *h3bis = new TH1D("h3bis", "h3bis", 100, log_min_mass, log_max_mass);
   for (int n = 0; n < int(N_cl_in_domega_drawn); n++) {
      log_mass_cl = f_mass->GetRandom(log_min_mass, log_max_mass);
      h3->Fill(log_mass_cl);

      double mass = pow(10., log_mass_cl);
      double par_cl[7];
      mdelta_to_par(par_cl, mass, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, par_prof, gSIM_EPS, 0.);

      const int n_par_tot = 21;
      double par_tmp[n_par_tot];
      for (int i = 0; i < 7; ++i)
         par_tmp[i] = par_cl[i];
      for (int i = 7; i < 11; ++i)
         par_tmp[i] = 0.;
      for (int i = 11; i < n_par_tot; ++i)
         par_tmp[i] = 0.;

      double L_clump = lum_singlehalo_nosubs_mix(par_tmp, 1.e-4); // clump intrinsic luminosity [Msol^2/kpc^3]
      h3bis->Fill(log_mass_cl, L_clump);
   }


   TCanvas *c_mass = new TCanvas("c_mass", "Test draw distrib mass.", 200, 10, 600, 400);
   c_mass->SetFillColor(0);
   c_mass->SetBorderSize(0);
   c_mass->GetFrame()->SetFillColor(0);
   c_mass->GetFrame()->SetBorderSize(0);
   c_mass->SetFrameBorderMode(0);
   //c_mass->SetLogx(1);
   c_mass->SetLogy(1);
   h2->SetLineColor(2);
   h2->Draw();
   h3->Draw("same");
   //f_mass->Draw();
   c_mass->Modified();
   c_mass->Update();


   TCanvas *c_lum = new TCanvas("c_lum", "Test draw distrib lum.", 200, 10, 600, 400);
   c_lum->SetFillColor(0);
   c_lum->SetBorderSize(0);
   c_lum->GetFrame()->SetFillColor(0);
   c_lum->GetFrame()->SetBorderSize(0);
   c_lum->SetFrameBorderMode(0);
   //c_lum->SetLogy(1);
   //c_lum->SetLogx(1);
   h2bis->SetLineColor(2);
   h2bis->Draw();
   h3bis->Draw("same");
   //f_mass->Draw();
   c_lum->Modified();
   c_lum->Update();

   gSIM_ROOTAPP->Run(kTRUE);

   delete f_mass;
   // delete f_mass2;
   delete h2;
   delete h2bis;
   delete h3;
   // delete h4;
}

//______________________________________________________________________________
void test_clump_distribs()
{
   cout << "           ++++++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_clump_distribs +++" << endl;
   cout << "           ++++++++++++++++++++++++++++" << endl;

   //load_parameters("clumpy_params.txt", false);

   double log_min_mass = -2;
   double Mmin = pow(10., log_min_mass);
   double log_max_mass = 4.;
   double Mmax = pow(10., log_max_mass);

   double M1 = 1.;
   double M2 = 100.;

   double par_dpdm[2] = {1., gMW_SUBS_DPDM_SLOPE };
   double par_dpdm2[2] = {1., gMW_SUBS_DPDM_SLOPE };
   dpdm_setnormprob(par_dpdm, Mmin, Mmax, gSIM_EPS);
   dpdm_setnormprob(par_dpdm2, M1, M2, gSIM_EPS);

   double Mbar = mean1cl_mass(par_dpdm, Mmin, Mmax, gSIM_EPS);

   int Nbins = 3;
   double dlogM = (log10(Mmax) - log10(Mmin)) / Nbins;
   double mass_array[Nbins];
   int mass_histo[Nbins];


   // tirage
   //gRandom->SetSeed(gSIM_SEED);
   gRandom->SetSeed(0);

   TF1 *f_logmass = new TF1("f_logmass", dpdlogm, log10(Mmin), log10(Mmax), 2);
   f_logmass->SetNpx(100);
   f_logmass->SetParameters(par_dpdm[0], gMW_SUBS_DPDM_SLOPE);


   // choice: - 0: calculates the average mass and variance on the mass of 1 clump
   //         - 1: calculates the average number of clumps and variance in a given mass decade, for many Ntot
   //         - 2: calculates the number of clumps histogram for 1 Ntot
   //         - 3: calculates the average and variance of <Mtot>=<Ntot>*<M_1cl>, for many Ntot
   //         - 4: calculates the average and variance of <Ltot>=<Ntot>*<L_1cl> + histo of Ltot
   //         - 5: calculates the average and variance of distance distribution
   //         - 6: checks average and variance (analytical vs drawn)
   //         - 7: calculates l_crit
   double Ntot_cl, N_tirage;
   double N_mean, sigma2, sum_for_average, sum_for_variance, Nbar;
   int choice;
   printf("Enter your choice [0]-->[7]\n");
   printf("     - 0: calculates the average mass and variance on the mass of 1 clump\n");
   printf("     - 1: calculates the average number of clumps and variance in a given mass decade, for many Ntot (total number of clumps)\n");
   printf("     - 2: calculates the number of clumps histogram for 1 Ntot\n");
   printf("     - 3: calculates the average and variance of <Mtot>=<Ntot>*<M_1cl>, for many Ntot\n");
   printf("     - 4: calculates the average and variance of <Ltot>=<Ntot>*<L_1cl> + histo of Ltot\n");
   printf("     - 5: calculates the average and variance of distance distribution\n");
   printf("     - 6: checks average and variance (analytical vs drawn)\n");
   printf("     - 7: calculates l_crit\n");
   printf("     - 8: test of the concentration distribution\n");
   scanf("%d", &choice);

   int prof_id1 = 1; // clump inner density profile
   int prof_id2 = 1; // clumps spatial distribution

   double par_prof[4] = {
      gal_shape[prof_id1][0], gal_shape[prof_id1][1], gal_shape[prof_id1][2],
      (double)gal_card_prof[prof_id1]
   };


   switch (choice) {
      //**************************************************************************//
      case 0: {
            cout << "Mass [M_sol] | Lum [M_sol^2 kpc^{-3}] " << endl;
            for (double m = Mmin; m <= Mmax; m *= 1.5) {
               double par_cl[7];
               mdelta_to_par(par_cl, m, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, par_prof, gSIM_EPS, 0.);
               const int n_par_tot = 21;
               double par_tmp[n_par_tot];
               for (int i = 0; i < 7; ++i)
                  par_tmp[i] = par_cl[i];
               for (int i = 7; i < 11; ++i)
                  par_tmp[i] = 0.;
               for (int i = 11; i < n_par_tot; ++i)
                  par_tmp[i] = 0.;

               double l = lum_singlehalo_nosubs_mix(par_tmp, gSIM_EPS);
               printf("%le %le\n", m, l);
            }

            const int npar = 10;
            double par_galdpdv[npar];
            gal_set_pardpdv(par_galdpdv);

            double par_subs[24];
            gal_set_parsubs(par_subs);

            // variance analytique sur M 1 clump
            double var_M = var1cl_mass(par_dpdm, Mmin, Mmax, gSIM_EPS);

            double Lbar = mean1cl_lumn_r(par_subs, Mmin, Mmax, 1);
            double var_L = var1cl_lum(par_subs, Mmin, Mmax);

            printf("# Nb clumps   <M>theo      <M>tire     Var(M)theo     Var(M)tire      <L>theo     <L>tire      var(L)tire    var(L)theo   RSE(L)/RSE(M)  RSE(L)/RSE(M)(tire) \n\n");

            for (Ntot_cl = 10; Ntot_cl <= 1.e7; Ntot_cl *= 1.5) {

               double mtot = 0.;
               double mtot2 = 0.; // for variance calculation
               double lumtot = 0.;
               double lumtot2 = 0.; // for variance calculation

               for (int i = 0; i < Ntot_cl; i++) {
                  double log_mass = f_logmass->GetRandom();
                  double m = pow(10., log_mass);
                  mtot += m;
                  mtot2 += m * m;

                  double par_cl[7];
                  mdelta_to_par(par_cl, m, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, par_prof, gSIM_EPS, 0.);
                  const int n_par_tot = 21;
                  double par_tmp[n_par_tot];
                  for (int j = 0; j < 7; ++j)
                     par_tmp[j] = par_cl[j];
                  for (int j = 7; j < 11; ++j)
                     par_tmp[j] = 0.;
                  for (int j = 11; j < n_par_tot; ++j)
                     par_tmp[j] = 0.;

                  double lum = lum_singlehalo_nosubs_mix(par_tmp, gSIM_EPS);
                  // double lum=pow(m,0.92);
                  lumtot += lum;
                  lumtot2 += lum * lum;
               }
               double Mbar_tirage = mtot / Ntot_cl;
               double Lbar_tirage = lumtot / Ntot_cl;
               double var_M_tirage = mtot2 / Ntot_cl - Mbar_tirage * Mbar_tirage;
               double var_L_tirage = lumtot2 / Ntot_cl - Lbar_tirage * Lbar_tirage;
               printf("%le  %le %le  %le  %le   %le %le %le %le  %le %le\n", Ntot_cl, Mbar, Mbar_tirage,
                      var_M, var_M_tirage, Lbar, Lbar_tirage, var_L_tirage, var_L,
                      sqrt(var_L) * (Mbar) / (Lbar * sqrt(var_M)), sqrt(var_L_tirage) * (Mbar_tirage) / (Lbar_tirage * sqrt(var_M_tirage)));
            }
         }
         break;
      //**************************************************************************//

      case 1: {
            N_tirage = 10000;
            printf("# Nb de tirages=%le\n\n", N_tirage);
            printf("# Nb clumps   <N>theo       <N>tire      var(N)tire    sigma    sigma/N\n");
            for (Ntot_cl = 1; Ntot_cl <= 10000; Ntot_cl *= 2) {
               // initialize mass_array and mass histogram
               for (int i = 0; i < Nbins; i++) {
                  double logm = log10(Mmin) + i * dlogM;
                  mass_array[i] = pow(10., logm);
                  mass_histo[i] = 0;
               }
               Nbar = Ntot_cl * frac_nsubs_in_m1m2(par_dpdm, M1, M2, gSIM_EPS);

               sum_for_average = 0;
               sum_for_variance = 0;


               for (int i_tirage = 0; i_tirage < N_tirage; i_tirage++) {
                  for (int k = 0; k <= Nbins; k++)
                     mass_histo[k] = 0;

                  for (int j = 0; j <= Ntot_cl; j++) {
                     double log_mass = f_logmass->GetRandom();
                     int i = 0;
                     while (pow(10., log_mass) > mass_array[i + 1]) {
                        i++;
                     }
                     mass_histo[i] += 1; // add one clump in bin i of the mass histogram
                  }
                  sum_for_average += mass_histo[1];
                  sum_for_variance += mass_histo[1] * mass_histo[1];
               }
               N_mean = sum_for_average / N_tirage;
               sigma2 = sum_for_variance / N_tirage - N_mean * N_mean;
               printf("%le %le  %le %le %le %le\n", Ntot_cl, Nbar, N_mean, sigma2, sqrt(sigma2), sqrt(sigma2) / Nbar);
            }
         }
         break;
      //**************************************************************************//

      case 2: {
            Ntot_cl = 100;
            N_tirage = 20000;
            printf("# Ntot=%le  N_tirage=%le\n\n", Ntot_cl, N_tirage);
            printf("# N          number of drawing giving N clumps       distrib/gaussian\n");
            // initialize mass_array and mass histogram
            for (int i = 0; i < Nbins; i++) {
               double logm = log10(Mmin) + i * dlogM;
               mass_array[i] = pow(10., logm);
               mass_histo[i] = 0;
            }

            Nbar = Ntot_cl * frac_nsubs_in_m1m2(par_dpdm, M1, M2, 1.e-3);

            // initialize Nb of clumps arrays and nb of clump histogram
            int N_nb = int(5 * sqrt(Nbar));
            int N_array[2 * N_nb + 1];
            int Nmin = int(Nbar) - N_nb;
            int N_histo[2 * N_nb + 1];

            for (int i = 0; i < 2 * N_nb + 1; i++) {
               N_array[i] = Nmin + i;
               N_histo[i] = 0;
            }

            sum_for_average = 0;
            sum_for_variance = 0;

            for (int i_tirage = 0; i_tirage < N_tirage; i_tirage++) {
               for (int k = 0; k < Nbins; k++)
                  mass_histo[k] = 0;

               for (int j = 0; j < Ntot_cl; j++) {
                  double log_mass = f_logmass->GetRandom();
                  int i = 0;
                  while (pow(10., log_mass) > mass_array[i + 1])
                     i++;
                  mass_histo[i] += 1; // add one clump in bin i of the mass histogram
               }
               sum_for_average += mass_histo[1];
               sum_for_variance += mass_histo[1] * mass_histo[1];

               int i = 0;
               //    printf("%i %i\n", mass_histo[1], N_array[i+1]);
               while (mass_histo[1] >= N_array[i + 1])
                  i++;
               N_histo[i] += 1; // add one event in bin i of nb of clump histogram
            }

            N_mean = sum_for_average / N_tirage;
            sigma2 = sum_for_variance / N_tirage - N_mean * N_mean;

            for (int j = 0; j < 2 * N_nb + 1; j++)
               printf("%-10i %18i %36le %36le\n", N_array[j], N_histo[j], N_tirage * exp(-pow(N_array[j] - Nbar, 2.) / (2 * Nbar)) / sqrt(2 * 3.1415926 * Nbar),
                      N_histo[j]*sqrt(2 * 3.1415926 * Nbar) / (N_tirage * exp(-pow(N_array[j] - Nbar, 2.) / (2 * Nbar))));
         }
         break;

      //**************************************************************************//
      case 3: {

            N_tirage = 50000;
            printf("# Nb de tirages=%le\n\n", N_tirage);
            double Mbar2 = mean1cl_mass(par_dpdm2, M1, M2, gSIM_EPS);
            cout << "Mbar2 = " << Mbar2 << endl;

            printf("# Ntot       <N>_theo    <N>_tire   Mtot_theo=<N><M>    <Mtot>_tire    var(Mtot)tire   sigma   RSE   term1 term2   tot\n\n");


            double var_M = var1cl_mass(par_dpdm2, M1, M2, gSIM_EPS);
            for (Ntot_cl = 1; Ntot_cl <= 1.e5; Ntot_cl *= 2) {

               Nbar = Ntot_cl * frac_nsubs_in_m1m2(par_dpdm, M1, M2, gSIM_EPS);
               double NMbar_theo = Mbar2 * Nbar;


               // initialize mass_array and mass histogram
               for (int i = 0; i < Nbins; i++) {
                  double logm = log10(Mmin) + i * dlogM;
                  mass_array[i] = pow(10., logm);
                  mass_histo[i] = 0;
               }

               double sum_for_N_average = 0.;
               double sum_for_N_variance = 0.;
               double sum_for_Mtot_average = 0.;
               double sum_for_Mtot_variance = 0.;

               for (int i_tirage = 0; i_tirage < N_tirage; i_tirage++) {
                  for (int k = 0; k <= Nbins; k++)
                     mass_histo[k] = 0;

                  double mass_tot = 0.;
                  for (int j = 0; j <= Ntot_cl; j++) {
                     double log_mass = f_logmass->GetRandom();
                     double m = pow(10., log_mass);
                     if (m >= mass_array[1] && m < mass_array[2])
                        mass_tot += pow(10., log_mass);
                     int i = 0;
                     while (m > mass_array[i + 1])
                        i++;
                     mass_histo[i] += 1; // add one clump in bin i of the mass histogram
                  }
                  sum_for_N_average += mass_histo[1];
                  sum_for_N_variance += mass_histo[1] * mass_histo[1];
                  sum_for_Mtot_average += mass_tot;
                  sum_for_Mtot_variance += mass_tot * mass_tot;
               }
               N_mean = sum_for_N_average / N_tirage;
               sigma2 = sum_for_N_variance / N_tirage - N_mean * N_mean;
               double NMbar_tire = sum_for_Mtot_average / N_tirage;
               double NMbar_variance_tire = sum_for_Mtot_variance / N_tirage - NMbar_tire * NMbar_tire;
               printf("%le  %5.3le     %5.3le   %5.3le          %5.3le       %5.3le  %5.3le  %5.3le  %5.3le  %5.3le  %5.3le \n", Ntot_cl, Nbar, N_mean, NMbar_theo, NMbar_tire,
                      NMbar_variance_tire, sqrt(NMbar_variance_tire), sqrt(NMbar_variance_tire) / NMbar_tire,
                      1 / sqrt(Nbar), sqrt(var_M) / Mbar2, sqrt(1 / Nbar + var_M / (Mbar2 * Mbar2)));
            }
            break;
         }

      case 4: {
            N_tirage = 10000;
            printf("# Nb de tirages=%le\n\n", N_tirage);
            double Mbar2 = mean1cl_mass(par_dpdm, M1, M2, gSIM_EPS);

            const int npar = 10;
            double par_galdpdv[npar];
            gal_set_pardpdv(par_galdpdv);

            double par_subs[24];
            gal_set_parsubs(par_subs);

            double Lbar = mean1cl_lumn_r(par_subs, M1, M2, 1);

            printf("M_mean=%le  L_mean=%le\n", Mbar2, Lbar);
            printf("# Ntot       <N>_theo    <N>_tire   Ltot_theo=<N><L>    <Ltot>_tire    var(Ltot)tire   sigma   RSE\n\n");

            for (Ntot_cl = 1; Ntot_cl <= 1.e5; Ntot_cl *= 2) {
               Nbar = Ntot_cl * frac_nsubs_in_m1m2(par_dpdm, M1, M2, 1.e-3);
               double NLbar_theo = Lbar * Nbar;


               // initialize mass_array and mass histogram
               for (int i = 0; i < Nbins; i++) {
                  double logm = log10(Mmin) + i * dlogM;
                  mass_array[i] = pow(10., logm);
                  mass_histo[i] = 0;
               }

               double sum_for_N_average = 0.;
               double sum_for_N_variance = 0.;
               double sum_for_Ltot_average = 0.;
               double sum_for_Ltot_variance = 0.;

               for (int i_tirage = 0; i_tirage < N_tirage; i_tirage++) {
                  for (int k = 0; k <= Nbins; k++)
                     mass_histo[k] = 0;

                  double lum_tot = 0.;
                  for (int j = 0; j <= Ntot_cl; j++) {
                     double log_mass = f_logmass->GetRandom();
                     double m = pow(10., log_mass);
                     if (m >= mass_array[1] && m < mass_array[2]) {
                        double par_cl[7];

                        double c = 0.;

                        if (gDM_FLAG_CDELTA_DIST != kDIRAC) { //we draw the concentration from its distribution if gMW_SUBS_FLAG_CDELTAMDELTA is not kDIRAC
                           double cvir_mean =  mdelta_to_cdelta(m, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, par_subs, 0); // par_subs maybe wrong!?
                           const int n_par_c = 3;
                           double par_conc[n_par_c] = {cvir_mean, gDM_LOGCDELTA_STDDEV, (double)gDM_FLAG_CDELTA_DIST};
                           TF1 *f_c = new TF1("f_c", dpdc, 0.01 * cvir_mean, 100.*cvir_mean, 3);
                           f_c->SetNpx(100);
                           f_c->SetParameters(par_conc);
                           c = f_c->GetRandom();
                           delete f_c;
                           f_c = NULL;
                        }

                        mdelta_to_par(par_cl, m, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, par_prof, gSIM_EPS, 0., c);
                        //const int n_par_tot = 21;
                        //double par_tmp[n_par_tot];
                        //for (int i = 0; i < 7; ++i)
                        //   par_tmp[i] = par_cl[i];
                        //for (int i = 7; i < 11; ++i)
                        //   par_tmp[i] = 0.;
                        //for (int i = 11; i < n_par_tot; ++i)
                        //   par_tmp[i] = 0.;

                        gal_set_pardpdv(par_galdpdv);
                        double par_subs_new[26];
                        for (int kk = 0; kk < 24; ++kk)
                           par_subs_new[kk + 2] = par_subs[kk];
                        par_subs_new[0] = par_cl[0];
                        par_subs_new[1] = par_cl[1];

                        //lum_tot += lum_singlehalo_nosubs_mix(par_tmp, gSIM_EPS);
                        lum_tot += lum_singlehalo_subs(par_subs_new);
                     }
                     int i = 0;
                     while (pow(10., log_mass) > mass_array[i + 1])
                        i++;
                     mass_histo[i] += 1; // add one clump in bin i of the mass histogram
                  }
                  sum_for_N_average += mass_histo[1];
                  sum_for_N_variance += mass_histo[1] * mass_histo[1];
                  sum_for_Ltot_average += lum_tot;
                  sum_for_Ltot_variance += lum_tot * lum_tot;
               }
               N_mean = sum_for_N_average / N_tirage;
               sigma2 = sum_for_N_variance / N_tirage - N_mean * N_mean;
               double NLbar_tire = sum_for_Ltot_average / N_tirage;
               double NLbar_variance_tire = sum_for_Ltot_variance / N_tirage - NLbar_tire * NLbar_tire;
               printf("%le  %5.3le     %5.3le   %5.3le          %5.3le       %5.3le  %5.3le  %5.3le \n", Ntot_cl,
                      Nbar, N_mean, NLbar_theo, NLbar_tire, NLbar_variance_tire, sqrt(NLbar_variance_tire),
                      sqrt(NLbar_variance_tire) / NLbar_tire);
            }
            break;
         }

      case 5: {
            double psi = -3.12;
            double theta = 0.;
            double lmin = 9.; //kpc
            double lmax = 280.;

            /*

                              double test = 0.;
                     double ltest = 40.;
                     double param[36];

                     param[0] = 1.;
                     param[1] = 21.7;
                     param[2] = 1.;
                     param[3] = 3.;
                     param[4] = 1.;
                     param[5] = kZHAO;
                     param[6] = 280.;
                     param[7] = 280.;
                     param[8] = 0.;
                     param[9] = 0.;
                     param[10] = 0.;
                     param[11] = 1.;
                     param[12] = 21.7;
                     param[13] = 1.;
                     param[14] = 3.;
                     param[15] = 1.;
                     param[16] = kZHAO;
                     param[17] = 280.;
                     param[18] = 280.;
                     param[19] = 0.;
                     param[20] = 0.;
                     param[21] = 0.;
                     param[22] = psi;
                     param[23] = theta;
                     param[24] = lmin;
                     param[25] = lmax;
                     param[26] = 0.001;
                     param[27] = 0;
                     param[28] = 6.28319;
                     param[29] = 0.00174533;
                     param[30] = 0.;
                     param[31] = 0.;
                     param[32] = 2.7;
                     param[33] = true;
                     param[34] = true;
                     param[35] = true;

                     integrand_l(ltest, param, test);
                     cout << "test = " << test << endl;
               //--- Value of the integrand (related to the DM density) at position (l,alpha,beta)
               //
               //  par[0]     rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
               //  par[1]     rho_1(r) scale radius [kpc]
               //  par[2]     rho_1(r) shape parameter #1
               //  par[3]     rho_1(r) shape parameter #2
               //  par[4]     rho_1(r) shape parameter #3
               //  par[5]     rho_1(r) card_profile [gENUM_PROFILE]
               //  par[6]     rho_1(r) radius [kpc]
               //  par[7]     rho_1(r) distance to observer [kpc]
               //  par[8]     rho_1(r) psi_center [rad]
               //  par[9]     rho_1(r) theta_center [rad]
               //  par[10]    switch_rho: selects which combination of rho_1 and rho_2 to use
               //                0 -> rho(r) = rho1(r)
               //                1 -> rho(r) = rho1(r)-rho2(r)
               //                2 -> rho(r) = rho1(r)*rho2(r)
               //  par[11]    rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
               //  par[12]    rho_2(r) scale radius [kpc]
               //  par[13]    rho_2(r) shape parameter #1
               //  par[14]    rho_2(r) shape parameter #2
               //  par[15]    rho_2(r) shape parameter #3
               //  par[16]    rho_2(r) card_profile [gENUM_PROFILE]
               //  par[17]    rho_2(r) radius [kpc]
               //  par[18]    rho_2(r) distance to observer [kpc]
               //  par[19]    rho_2(r) psi_center [rad]
               //  par[20]    rho_2(r) theta_center [rad]
               //  par[21]    switch_f: selects the function f to integrate
               //                0 -> rho:           for jsub_continuum (also proxy for dP/dV)
               //                1 -> rho^2[or rho]: annihilation[decay] for a single clump or DM halo
               //                2 -> l^2*rho:       for clump number/mass fraction estimates (proxy for dP/dV)
               //                3 -> l^3*rho:       to calculate <l>
               //                4 -> l^4*rho:       to calculate var(l)
               //                5 -> l^-2*rho:      to calculate var(J)
               //                6 -> l^2*rho^2:     to check with Lum direct calculation (integrand corresponds to d3Lum)
               //  par[22]    psi_los: longitude of the line of sight [rad]
               //  par[23]    theta_los: latitude of the line of sight [rad]
               //  par[24]    lmin: lower l.o.s. integration boundary [kpc]
               //  par[25]    lmax: upper l.o.s. integration boundary [kpc]
               //  par[26]    eps: relative precision sought for integrations
               //  par[27]    phi_los: direction between GC and l.o.s. (psi,theta) [rad]
               //  par[28]    beta: integr. angle in the plane perp. to the l.o.s. [rad]
               //  par[29]    alpha: integr. angle measured from the ref. l.o.s. [rad]
               //  par[30]    phi_los_peak: angle between phi_los and density peak [rad]
               //  par[31]    r_trick: radius of the point-like contribution [kpc]
               //  par[32]    l_offset: for the integr. along  l': l'=l+l_offset, dl'=dl [kpc]
               //  par[33]    is_increasing_l (true/false if integr. corresponds to increas./decreas. l)
               //  par[34]    Is this halo (rho1) the Galactic halo [true or false]
               //  par[35]    Do rho1 and rho2 have the same centre [true or false]

            */
            // define clump spatial distributions
            double par_clump_glob[12] = {
               1., gal_rs_smooth[prof_id2], gal_shape[prof_id2][0],
               gal_shape[prof_id2][1], gal_shape[prof_id2][2],
               (double)gal_card_prof[prof_id2], gMW_RMAX, gMW_RSOL, 0., 0., psi, theta
            };
            dpdv_setnormprob(par_clump_glob, gSIM_EPS);

            TF3 *f_lalphabeta = new TF3("f_lalphabeta", dpdv_lcosalphabeta, lmin, lmax, 0., gSIM_ALPHAINT, 0., 2.*PI, 12);
            f_lalphabeta->SetNpx(5000);
            f_lalphabeta->SetNpy(30);
            f_lalphabeta->SetNpz(30);
            f_lalphabeta->SetParameters(par_clump_glob);

            double par_dpdv[10] = {
               1., gal_rs_smooth[prof_id2], gal_shape[prof_id2][0],
               gal_shape[prof_id2][1], gal_shape[prof_id2][2],
               (double)gal_card_prof[prof_id2], gMW_RMAX, gMW_RSOL, 0., 0.
            };

            // To calculate <1/l^2>
            double inv_l2_mean = mean1cl_ln(par_dpdv, psi, theta, lmin, lmax, gSIM_EPS, -2);

            cout << "inv_l2_mean = " << inv_l2_mean << endl;

            // To calculate the variance of 1/l^2
            int switch_f = 5;
            double inv_l2_var = mean1cl_ln(par_dpdv, psi, theta, lmin, lmax, gSIM_EPS, -4) - inv_l2_mean * inv_l2_mean;

            int switch_pow_l = 1;
            double lbar = mean1cl_ln(par_dpdv, psi, theta, lmin, lmax, gSIM_EPS, switch_pow_l);
            double var_ana = -1; //var1cl_l(par_dpdv, psi, theta, lmin, lmax, gSIM_EPS);

            printf("Ntot          lbar_theo     lbar_tire   var_l_ana   var_l_tire    <1/l^2>  <1/l^2>theo   var(1/l^2)    var(1/l^2)theo\n\n");

            for (Ntot_cl = 10; Ntot_cl <= 1.e6; Ntot_cl *= 2) {

               double dist_sum = 0.;
               double dist_sum2 = 0.;
               double inv_dist2_sum = 0;
               double inv_dist4_sum = 0;
               int N_in_bin = 0;

               for (int i = 0; i < Ntot_cl; i++) {
                  double l, alpha, beta;
                  f_lalphabeta->GetRandom3(l, alpha, beta);
                  if (l >= lmin && l < lmax) {
                     dist_sum += l;
                     dist_sum2 += l * l;
                     inv_dist2_sum += 1. / (l * l);
                     inv_dist4_sum += 1. / (l * l * l * l);
                     N_in_bin++;
                  }
               }
               double dist_mean = dist_sum / Ntot_cl;
               double var_dist = dist_sum2 / Ntot_cl - dist_mean * dist_mean;
               double inv_dist2_mean = inv_dist2_sum / Ntot_cl;
               double inv_dist2_var = inv_dist4_sum / Ntot_cl - inv_dist2_mean * inv_dist2_mean;
               printf("%le %le %le %le %le %le  %le %le %le\n", Ntot_cl, lbar, dist_mean, var_ana, var_dist,
                      inv_dist2_mean, inv_l2_mean, inv_dist2_var, inv_l2_var);
            }

            break;
         }

      case 6: {

            double psi = 1.2;
            double theta = 0.4;
            double lmin = 9.; //kpc
            double lmax = 280;

            // define clump spatial distributions
            double par_clump_glob[12] = {
               1., gMW_SUBS_DPDV_RSCALE_TO_RS_HOST * gMW_TOT_RSCALE, gMW_SUBS_DPDV_SHAPE_PARAMS[0],
               gMW_SUBS_DPDV_SHAPE_PARAMS[1], gMW_SUBS_DPDV_SHAPE_PARAMS[2],
               (double)gMW_SUBS_DPDV_FLAG_PROFILE, gMW_RMAX, gMW_RSOL, psi, theta, 0., 0.,
            };
            dpdv_setnormprob(par_clump_glob, gSIM_EPS);

            TF3 *f_lalphabeta = new TF3("f_lalphabeta", dpdv_lcosalphabeta, lmin, lmax, 0., gSIM_ALPHAINT, 0., 2.*PI, 12);
            f_lalphabeta->SetNpx(5000);
            f_lalphabeta->SetNpy(30);
            f_lalphabeta->SetNpz(30);
            f_lalphabeta->SetParameters(par_clump_glob);

            double par_subs[24];
            gal_set_parsubs(par_subs);
            double par_dpdv[10];
            gal_set_pardpdv(par_dpdv);

            double flux_ana_bar = mean1cl_jn(par_dpdv, psi, theta, lmin, lmax, par_subs, Mmin, Mmax, 1);
            double var_flux_ana = var1cl_j(par_dpdv, psi, theta, lmin, lmax, par_subs, Mmin, Mmax);

            printf("# Nb clumps  <F>theo     <F>tire     var(F)ana  var(F)tire      RSE(F)  \n\n");

            for (Ntot_cl = 10; Ntot_cl <= 1.e7; Ntot_cl *= 1.5) {
               double fluxtot = 0.;
               double fluxtot2 = 0.; // for variance calculation

               for (int i = 0; i < Ntot_cl; i++) {
                  double log_mass = f_logmass->GetRandom();

                  double m = pow(10., log_mass);
                  double par_cl[7];
                  mdelta_to_par(par_cl, m, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, par_prof, gSIM_EPS, 0.);

                  const int n_par_tot = 21;
                  double par_tmp[n_par_tot];
                  for (int j = 0; j < 7; ++j)
                     par_tmp[j] = par_cl[j];
                  for (int j = 7; j < 11; ++j)
                     par_tmp[j] = 0.;
                  for (int j = 11; j < n_par_tot; ++j)
                     par_tmp[j] = 0.;

                  double lum = lum_singlehalo_nosubs_mix(par_tmp, gSIM_EPS);

                  double l, alpha, beta;
                  f_lalphabeta->GetRandom3(l, alpha, beta);

                  fluxtot += lum / (l * l);
                  fluxtot2 += pow(lum / (l * l), 2.);
               }

               double flux_bar_tirage = fluxtot / Ntot_cl;
               double var_flux_tirage = fluxtot2 / Ntot_cl - flux_bar_tirage * flux_bar_tirage;

               printf("%le %le %le %le %le %le %le\n",
                      Ntot_cl, flux_ana_bar, flux_bar_tirage, var_flux_ana,
                      var_flux_tirage, sqrt(var_flux_tirage) / (sqrt(Ntot_cl)*flux_bar_tirage),
                      sqrt(var_flux_ana) / (sqrt(Ntot_cl)*flux_ana_bar));
            }

            break;
         }
      case 7: {
            double psi_deg = -90.;
            double theta_deg = 0.;
            double lmin = 0.01; //kpc
            double lmax = 280;

            double psi = psi_deg * DEG_to_RAD;
            double theta = theta_deg * DEG_to_RAD;
            Mmin = 1.e0;
            Mmax = 1.e1;
            dpdm_setnormprob(par_dpdm, Mmin, Mmax, gSIM_EPS);

            double par_subs[24];
            gal_set_parsubs(par_subs);
            double par_tot[10];
            gal_set_parsubs(par_tot);
            double Ntot = nsubtot_from_nsubm1m2(par_dpdm, gMW_SUBS_M1, gMW_SUBS_M2,
                                                gMW_SUBS_N_INM1M2, gSIM_EPS);

            double flux_smooth = jsmooth(par_tot, psi, theta, gSIM_EPS);
            double par_dpdv[10] = {
               1., gal_rs_smooth[prof_id1], gal_shape[prof_id1][0],
               gal_shape[prof_id1][1], gal_shape[prof_id1][2],
               (double)gal_card_prof[prof_id1], gMW_RMAX, gMW_RSOL, 0., 0.
            };
            dpdv_setnormprob(par_dpdv, gSIM_EPS);

            for (lmin = 0.01; lmin < lmax; lmin *= 1.1) {
               double J_tot_cl1 = jsub_continuum(Ntot, par_dpdv, psi, theta, lmin, lmax, par_subs, Mmin, Mmax);

               double flux_1cl = mean1cl_jn(par_dpdv, psi, theta, lmin, lmax, par_subs, Mmin, Mmax, 1);
               double frac = frac_nsubs_in_foi(par_dpdv, psi, theta, lmin, lmax, gSIM_EPS);
               Ntot_cl = frac * Ntot;
               double J_tot_cl2 = Ntot_cl * flux_1cl;

               double var_F = var1cl_j(par_dpdv, psi, theta, lmin, lmax, par_subs, Mmin, Mmax);

               printf("%le %le %le %le %le %le\n", lmin, Ntot_cl, J_tot_cl1, J_tot_cl2, var_F,
                      sqrt(Ntot_cl * var_F) * 100 / (Ntot_cl * flux_1cl + flux_smooth));
            }


            lmin = 0.01;
            flux_smooth = jsmooth(par_tot, psi, theta, lmin, lmax, gSIM_EPS);
            double lcrit = lmin;
            double user_rse = 1.;
            double tol = 1.e-2;

            find_lcritgal_los(Ntot, par_dpdv, psi, theta, lmin, lmax, par_subs, Mmin, Mmax, user_rse, tol,
                              flux_smooth, lcrit);

            printf("\n=========> lcrit=%le  flux_sm=%le\n\n", lcrit, flux_smooth);
            break;
         }
      case 8: {

            double m = 1.e7;
            double z = 0.;

            double par_subs[24];
            gal_set_parsubs(par_subs);
            double c_mean = mdelta_to_cdelta(m, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, &par_subs[0], z);
            double par_conc[3] = {
               c_mean, gDM_LOGCDELTA_STDDEV, (double)gDM_FLAG_CDELTA_DIST
            };

            TF1 *f_c = new TF1("f_c", dpdc, 0., 10.*c_mean, 3);
            f_c->SetNpx(100);
            f_c->SetParameters(par_conc);

            TH1F *h = new TH1F("h", "", 300, 0., 10.*c_mean);
            int Ndrawn = 100000;

            for (int i = 0; i < Ndrawn; i++) {
               //double c = 35.5;
               //double lognorm = 0.;
               //dpdc(c, par_conc, lognorm);
               //cout << lognorm << endl;
               double c = f_c->GetRandom();
               //cout << "c = " << c << endl;
               h->Fill(c);
            }

            double dx = (10.*c_mean - 0.) / double(300);
            double scale = 1 / ((double)Ndrawn * dx);
            h->Scale(scale);

            h->Draw();
            f_c->Draw("LSAME");
            gSIM_ROOTAPP->Run(kTRUE);

            break;
         }
   }
}

//______________________________________________________________________________
void test_cross_product_1cl()
{

   cout << "           +++++++++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_cross_product_1cl +++" << endl;
   cout << "           +++++++++++++++++++++++++++++++" << endl;

   //load_parameters("clumpy_params.txt", false);

   FILE *fp = NULL;
   char filename[140];
   vector<string> f_error;
   vector<int> i_std;
   bool is_frac_or_rse = false;


   // Create directory "clumpy_test" to store output files
   string path = "clumpy_test/";
   mkdir(path.c_str(), S_IRWXU); // mode_t: read, write, execute/search by owner

   int opt;
   cout << "  Clump in the Galactic halo [0] or sub-clump in clump[1]? " << endl;
   cin >> opt;
   double f_dm;
   cout << " DM fraction in subs: " << endl;
   cin >> f_dm;

   double psi_los = -3.14;
   double theta_los = 0;

   double m_host;
   int n_par = 10;
   double par_tot[n_par];
   double par_dpdv[n_par];
   double par_clmean[n_par];
   //  par[0] normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1] scale radius [kpc]
   //  par[2] shape parameter #1
   //  par[3] shape parameter #2
   //  par[4] shape parameter #3
   //  par[5] card_profile [gENUM_PROFILE]
   //  par[6] radius [kpc]
   //  par[7] distance to observer [kpc]
   //  par[8] psi_center [rad]
   //  par[9] theta_center [rad]

   // Parameters to set for opt = 1
   int card_CDELTAMDELTA = kB01_VIR;
   //gal_name_prof[ngal] = {"ISO", "NFW97", "M98", "DMS04", "EINASTO_M06", "EINASTO_N04"};
   int i_halo = 1;
   double par_prof[4] = {
      gal_shape[i_halo][0], gal_shape[i_halo][1],
      gal_shape[i_halo][2], (double)gal_card_prof[i_halo]
   };
   m_host = 1.0e10;

   // If host is Galactic halo
   if (opt == 0) {
      gal_set_partot(par_tot);
      gal_set_pardpdv(par_dpdv);
      m_host = mass_singlehalo(par_tot, gSIM_EPS);
      for (int i = 0; i < n_par; ++i)
         par_clmean[i] = par_dpdv[i];
      par_clmean[0] *= f_dm * m_host;

      // Else if host far away
   } else if (opt == 1) {
      mdelta_to_par(par_tot, m_host, Delta, card_CDELTAMDELTA, par_prof, gSIM_EPS, 0.);
      par_tot[7] = 100;
      par_tot[8] = psi_los;
      par_tot[9] = theta_los;

      for (int i = 0; i < n_par; ++i)
         par_dpdv[i] = par_tot[i];
      dpdv_setnormprob(par_dpdv, gSIM_EPS);
      for (int i = 0; i < n_par; ++i)
         par_clmean[i] = par_dpdv[i];
      par_clmean[0] *= f_dm * m_host;
   }


   // Calculate jsm along the loi
   double j_sm = jsmooth_mix(m_host, par_tot, psi_los, theta_los, gSIM_EPS, f_dm, par_dpdv);

   // Set param. for sub in host
   vector<double> v_msub;
   int i = 0;
   for (double msub = 1.e-10 * m_host; msub <= 1.e-2 * m_host; msub *= 100) {
      v_msub.push_back(msub);
      int ind_cl_1 = (int)log10(m_host);
      int ind_cl_2 = (int)log10(msub);

      printf("\n");
      sprintf(filename, "crossprod_%i_%i.dat", ind_cl_1, ind_cl_2);
      string f_name = path + filename;
      f_error.push_back(f_name);
      fp = fopen(f_name.c_str(), "w");

      double par_sub[n_par];
      mdelta_to_par(par_sub, msub, Delta, card_CDELTAMDELTA, par_prof, gSIM_EPS, 0.);
      par_sub[8] = psi_los;
      par_sub[9] = theta_los;

      cout << " Testing values: " << endl;
      cout << " M(cl#1)     = " << m_host   << "    M(cl#2)     = " <<  msub << endl;
      cout << " rho_s(cl#1) = " << par_tot[0] << " rho_s(cl#2) = " << par_sub[0] << endl;
      cout << " r_s(cl#1)   = " << par_tot[1] << " r_s(cl#2)   = " << par_sub[1] << endl;
      cout << " r_vir(cl#1) = " << par_tot[6] << " r_vir(cl#2) = " << par_sub[6] << endl;

      double lmin =  1.e-3 * par_tot[6];
      if (opt == 1) {
         lmin = par_sub[6] * 1.1;
      }
      double lmax = par_tot[6];

      // Move position of sub in the host halo
      for (double l = lmin; l < lmax; l *= 1.5) {
         if (opt == 0) par_sub[7] = l;
         else if (opt == 1)
            par_sub[7] = par_tot[7] + l;

         // int(rho_sm+rho_sub)^2 = int(rho_sm^2) + int(rho_sub^2) + 2 int(rho_sm*rhosub)
         // where the last term is the cross product, rewritten as
         //  2 int(rho_sm*rhosub) = 2 (int(rho_tot*rhosub) - int(rho_clmean*rhosub))

         // Calculate jsub along the loi
         double j_sub = jsmooth(par_sub, psi_los, theta_los, gSIM_EPS);

         // Calculate cross-prod (single clump)
         const int n_par2 = 21;
         double par[n_par2];
         //  par[0]     rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
         //  par[1]     rho_1(r) scale radius [kpc]
         //  par[2]     rho_1(r) shape parameter #1
         //  par[3]     rho_1(r) shape parameter #2
         //  par[4]     rho_1(r) shape parameter #3
         //  par[5]     rho_1(r) card_profile [gENUM_PROFILE]
         //  par[6]     rho_1(r) effective radius where to stop integration [kpc]
         //  par[7]     rho_1(r) distance observer - clump centre [kpc]
         //  par[8]     rho_1(r) psi_cl to clump centre [rad]
         //  par[9]     rho_1(r) theta_cl to clump centre [rad]
         //  par[10]    switch_rho: selects which combination of rho_1 and rho_2 to use
         //               0 -> rho(r) = rho1(r)
         //               1 -> rho(r) = rho1(r)-rho2(r)
         //               2 -> rho(r) = rho1(r)*rho2(r)
         //  par[11]    rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
         //  par[12]    rho_2(r) scale radius [kpc]
         //  par[13]    rho_2(r) shape parameter #1
         //  par[14]    rho_2(r) shape parameter #2
         //  par[15]    rho_2(r) shape parameter #3
         //  par[16]    rho_2(r) card_profile [gENUM_PROFILE]
         //  par[17]    rho_2(r) effective radius where to stop integration [kpc]
         //  par[18]    rho_2(r) distance observer - clump centre [kpc]
         //  par[19]    rho_2(r) psi_cl to clump centre [rad]
         //  par[20]    rho_2(r) theta_cl to clump centre [rad]

         // Cros-prod: term 1 = int(rho_tot*rhosub)
         for (int j = 0; j < 10; ++j)
            par[j] = par_sub[j];
         par[10] = 2.;
         for (int j = 11; j < 21; ++j)
            par[j] = par_tot[j - 11];

         int switch_f = 0;
         double lsub_min = max(0., par_sub[7] - par_sub[6]);
         double lsub_max = par_sub[7] + par_sub[6];
         double crossprod1 = los_integral_mix(par, switch_f, psi_los, theta_los,
                                              lsub_min, lsub_max, gSIM_EPS);

         // Cros-prod: term 2 = int(rho_clmean*rhosub)
         for (int j = 11; j < 21; ++j)
            par[j] = par_clmean[j - 11];
         double crossprod2 = los_integral_mix(par, switch_f, psi_los, theta_los,
                                              lsub_min, lsub_max, gSIM_EPS);

         double cross_prod = 2.*(crossprod1 - crossprod2);


         printf("l_host=%le  l_cl=%le jsm=%le  j1c=%le  jcrossprod=%le (1=%le  2=%le)\n", par_tot[7], par_sub[7],
                j_sm, j_sub, cross_prod, crossprod1, crossprod2);
         double x_val;
         if (opt == 0)
            x_val = l;
         else if (opt == 1)
            x_val = l / par_tot[6];

         if (is_frac_or_rse) fprintf(fp, "%le\t%le\n", x_val, cross_prod / (j_sm + j_sub));
         else fprintf(fp, "%le\t%le\n", x_val, 100.* cross_prod / (j_sm + j_sub + cross_prod));
      }
      fclose(fp);
      i_std.push_back(i);
      i++;
   }


   //---------//
   // DISPLAY //
   //---------//

   TCanvas *error = new TCanvas("error", "error", 650, 500);
   error->SetHighLightColor(0);
   error->Range(-0.5157081, -4.594267, 1.96531, -0.1544978);
   error->SetFillColor(0);
   error->SetBorderSize(0);
   error->SetLogx();
   error->SetLogy();
   error->SetGridx(0);
   error->SetGridy(0);
   error->SetFrameFillColor(0);
   error->SetFrameFillColor(0);
   error->SetFrameFillColor(0);
   error->SetLeftMargin(0.1);
   error->SetRightMargin(0.025);
   error->SetTopMargin(0.025);
   error->SetBottomMargin(0.1);
   error->SetFrameBorderMode(0);
   error->SetBorderMode(0);

   char host[20];
   sprintf(host, "%.2le", m_host);
   string host_cl = host;
   if (opt == 0)
      host_cl = "M_{gal} = " + host_cl + " M_{#odot}";
   else if (opt == 1)
      host_cl = "M_{host} = " + host_cl + " M_{#odot}";

   TMultiGraph *multi = new TMultiGraph();
   TGraph **gr = new TGraph*[(int)i_std.size()];
   TLegend *leg = new TLegend(0.22, 0.22, 0.44, 0.56);
   leg->SetFillColor(0);
   leg->SetTextSize(0.035);
   leg->SetTextFont(132);
   for (int j = 0; j < (int)i_std.size(); ++j) {
      gr[j] = new TGraph(f_error[j].c_str(), "%lg %lg");
      gr[j]->SetLineColor(rootcolor(j));
      gr[j]->SetLineWidth(3);
      gr[j]->SetLineStyle(j);
      multi->Add(gr[j], "L");
      char nam_leg[100];
      sprintf(nam_leg, "M_{cl} = %.2le M_{#odot}",
              v_msub[j]);
      leg->AddEntry(gr[j], nam_leg, "L");
   }

   multi->Draw("A");
   if (opt == 1)
      multi->GetXaxis()->SetTitle("d_{cl-host}/R_{host}");
   else if (opt == 0)
      multi->GetXaxis()->SetTitle("d_{cl-obs} [kpc]");

   multi->GetXaxis()->SetTitleOffset(1.);
   multi->GetXaxis()->SetTitleFont(132);
   multi->GetXaxis()->SetLabelFont(132);
   multi->GetXaxis()->SetLabelSize(0.04);
   multi->GetXaxis()->SetLabelOffset(.001);
   if (opt == 1) {
      if (is_frac_or_rse) multi->GetYaxis()->SetTitle("J_{cross-prod}  /  (J_{host} + J_{cl})");
      else multi->GetYaxis()->SetTitle("J_{cross-prod}  /  (J_{host} + J_{cl} + J_{cross-prod})   [%]");
   } else if (opt == 0) {
      if (is_frac_or_rse) multi->GetYaxis()->SetTitle("J_{cross-prod}  /  (J_{gal} + J_{cl})");
      else multi->GetYaxis()->SetTitle("J_{cross-prod}  /  (J_{gal} + J_{cl} + J_{cross-prod})   [%]");
   }

   multi->GetYaxis()->SetTitleOffset(1.);
   multi->GetYaxis()->SetTitleFont(132);
   multi->GetYaxis()->SetTitleSize(0.045);
   multi->GetYaxis()->SetLabelFont(132);
   multi->GetYaxis()->SetLabelSize(0.04);
   multi->GetYaxis()->SetLabelOffset(.001);
   leg->Draw("SAME");
   gPad->SetTickx(1);
   gPad->SetTicky(1);
   TPaveText pt(0.7, 0.88, 0.9, 0.92, "NDC");
   pt.SetBorderSize(0);
   pt.SetFillColor(0);
   pt.SetTextSize(0.04);
   pt.SetTextFont(132);
   pt.AddText(host_cl.c_str());
   pt.Draw();
   error->Update();
   error->Modified();

   gSIM_ROOTAPP->Run(kTRUE);
}

//________________________________________________________________________________
void test_cross_product_continuum()
{
   // for a given l.o.s, computes the cross product coming from the interaction of the DM in clumps
   // with that of the smooth component. If f is the mass fraction of clumps and Mtot the total DM in
   // the halo, the cross product reads 2*int_lmin^lmax rho_sm * rho_cl / l^2  dV
   // where rho_cl = f * Mtot * dPdV

   cout << "           +++++++++++++++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_cross_product_continuum +++" << endl;
   cout << "           +++++++++++++++++++++++++++++++++++++" << endl;

   //load_parameters("clumpy_params.txt", false);
   double psi = 0., theta = 0.;
   double lmin = 0.;
   double lmax = gMW_RMAX;

   double par_tot[10];
   double par_dpdv[10];
   double par_subs[24];

   int prof_tot = 1;
   int prof_dpdv = 1;
   int prof_subs = 1;

   double frac;
   printf("Enter the clump mass fraction:\n");
   scanf("%le", &frac);


   // Defines the total density properties
   //  par_tot[0-6] rho_tot: norm [Msol kpc^{-3}] + rs [kpc] + shape #1,2,3 + card_profile [gENUM_PROFILE] + Rmax
   //  par_tot[7-9] Distance, longitude (psi), and latitude (theta) to halo centre [kpc,rad,rad]
   par_tot[0] = 1.;
   par_tot[1] = gal_rs_smooth[prof_tot];
   par_tot[2] = gal_shape[prof_tot][0];
   par_tot[3] = gal_shape[prof_tot][1];
   par_tot[4] = gal_shape[prof_tot][2];
   par_tot[5] = gal_card_prof[prof_tot];
   par_tot[6] = gMW_RMAX;
   par_tot[7] = gMW_RSOL;
   par_tot[8] = 0.;
   par_tot[9] = 0.;
   set_par0_given_rhoref(par_tot, gMW_RSOL, gMW_RHOSOL);
   double mtot = mass_singlehalo(par_tot, gSIM_EPS);
   double mmax = mtot * gDM_SUBS_MMAXFRAC;
   double mmin = gDM_SUBS_MMIN;

   // Defines the clump distribution properties
   //  par_dpdv[0-6] dpdv: norm [kpc^{-3}] + rs [kpc] + shape #1,2,3 + card_profile [gENUM_PROFILE] + Rmax
   //  par_dpdv[7-9] Distance, longitude (psi), and latitude (theta) to halo centre [kpc,rad,rad]
   par_dpdv[0] = 1.;
   par_dpdv[1] = gal_rs_smooth[prof_dpdv];
   par_dpdv[2] = gal_shape[prof_dpdv][0];
   par_dpdv[3] = gal_shape[prof_dpdv][1];
   par_dpdv[4] = gal_shape[prof_dpdv][2];
   par_dpdv[5] = gal_card_prof[prof_dpdv];
   par_dpdv[6] = gMW_RMAX;
   par_dpdv[7] = gMW_RSOL;
   par_dpdv[8] = 0.;
   par_dpdv[9] = 0.;
   dpdv_setnormprob(par_dpdv, gSIM_EPS); // set par_dpdv[0] to get a prob

   // Defines par_subs
   gal_set_parsubs(par_subs);
   par_subs[14] = frac;
   par_subs[16] = gal_shape[prof_subs][0];
   par_subs[17] = gal_shape[prof_subs][1];
   par_subs[18] = gal_shape[prof_subs][2];
   par_subs[19] = gal_card_prof[prof_subs];

   double mtot_subs = frac * mtot;
   double ntot_subs = nsubtot_from_msubtot(mtot_subs, mmin, mmax, par_subs, gSIM_EPS);

   // Create directory "clumpy_test" to store output files
   string path = "clumpy_test/";
   mkdir(path.c_str(), S_IRWXU); // mode_t: read, write, execute/search by owner

   FILE *fp;
   string fname = path +  "cross_prod.dat";
   fp = fopen(fname.c_str(), "w");
   printf("     psi         j_sm        j_subs     cross_prod     j_ref        j_tot       2*f(1-f) cross_prod/jref cross_prod/jtot\n");
   for (psi = 1.e-3; psi <= PI; psi *= 1.1) {
      double j_ref = jsmooth(par_tot, psi, theta, gSIM_EPS);
      double j_sm = jsmooth_mix(mtot, par_tot, psi, theta, gSIM_EPS, frac, par_dpdv);
      double j_subs = jsub_continuum(ntot_subs, par_dpdv, psi, theta, lmin, lmax,
                                     par_subs, mmin, mmax);
      double jcross_prod = jcrossprod_continuum(mtot, par_tot, psi, theta, lmin, lmax,
                           gSIM_EPS, frac, par_dpdv);
      double j_tot = j_sm + j_subs + jcross_prod;

      printf("%le %le %le %le %le %le %le %le %le\n", psi, j_sm, j_subs, jcross_prod, j_ref,
             j_tot, 2 * frac * (1 - frac), jcross_prod / j_ref, jcross_prod / j_tot);
      fprintf(fp, "%le %le %le %le %le %le %le\n", psi, j_sm, j_subs, jcross_prod, j_ref,
              j_sm + j_subs, j_tot);
   }
   fclose(fp);
}

//______________________________________________________________________________
void test_fNcl_clump()
{
   //--- Calculate, around a l.o.s. direction seen from Earth, the fraction of clump
   // in a solid angle compared to the total number of clumps in the Galaxy
   // (depends of course of the profiles, distributions of clumps, etc.

   cout << "           ++++++++++++++++++++++++" << endl;
   cout << "           ++++ test_fNcl_clump +++" << endl;
   cout << "           ++++++++++++++++++++++++" << endl;

   cout << "Given the instrument angular aperture and providing a model "
        "for the clumps, we draw the fraction of clumps expected to be "
        "found in any l.o.s. direction" << endl << endl;

   gSIM_ALPHAINT = 0.1 * DEG_to_RAD;
   double eps = 1.e-2;
   double lmin = 0.;
   double lmax = gMW_RMAX + gMW_RSOL;
   int prof_dpdv = 1;
   double par_dpdv[10];
   par_dpdv[0] = 1.;
   par_dpdv[1] = gal_rs_smooth[prof_dpdv];
   par_dpdv[2] = gal_shape[prof_dpdv][0];
   par_dpdv[3] = gal_shape[prof_dpdv][1];
   par_dpdv[4] = gal_shape[prof_dpdv][2];
   par_dpdv[5] = gal_card_prof[prof_dpdv];
   par_dpdv[6] = gMW_RMAX;
   par_dpdv[7] = gMW_RSOL;
   par_dpdv[8] = 0.;
   par_dpdv[9] = 0.;
   dpdv_setnormprob(par_dpdv, eps); // set par_dpdv[0] to get a prob


   // Create directory "clumpy_test" to store output files
   string path = "clumpy_test/";
   mkdir(path.c_str(), S_IRWXU); // mode_t: read, write, execute/search by owner

   FILE *fp;
   string fname = path + "fNcl_skymap.dat";
   fp = fopen(fname.c_str(), "w");

   // Draw Ncl and store in hist
   int n = 50;
   TH2D *h_frac = new TH2D("h_frac", "h_frac", n, -180., 180., n, -90., 90.);

   double delta_psi = 2.*PI / (double)n;
   double delta_theta = PI / (double)n;
   for (double psi = -PI; psi < PI ; psi += delta_psi) {
      printf("psi=%le  [also loop on theta]\n", psi);
      for (double theta = -PI / 2.; theta < PI / 2. ; theta += delta_theta) {
         //printf("%le  %le\n", psi, theta);
         double f = frac_nsubs_in_foi(par_dpdv, psi, theta, lmin, lmax, eps);
         fprintf(fp, "%le  %le %le\n", psi * 180. / PI, theta * 180. / PI, f);
         h_frac->Fill((psi + delta_psi / 2.) * 180. / PI, (theta + delta_theta / 2.) * 180. / PI, f);
      }
   }
   fclose(fp);

   char title[100];
   sprintf(title, "dP/dV#propto %s (#alpha_{\rm int} =%.2f^{o})", gNAMES_PROFILE[gMW_SUBS_DPDV_FLAG_PROFILE], gSIM_ALPHAINT * 180. / PI);
   h_frac->SetStats(0);
   h_frac->SetTitle("");
   h_frac->SetXTitle("#psi^{o}");
   h_frac->SetYTitle("#theta^{o}");
   h_frac->SetZTitle("f_{cl}=N_{cl}^{(#psi,#theta)_{#Delta#Omega}}/N_{cl}^{tot}");
   h_frac->GetXaxis()->SetTitleOffset(0.8);
   h_frac->GetXaxis()->SetTitleFont(132);
   h_frac->GetXaxis()->SetTitleSize(0.06);
   h_frac->GetXaxis()->SetLabelFont(132);
   h_frac->GetXaxis()->SetLabelSize(0.04);
   h_frac->GetYaxis()->SetTitleOffset(1.);
   h_frac->GetYaxis()->SetTitleFont(132);
   h_frac->GetYaxis()->SetTitleSize(0.06);
   h_frac->GetYaxis()->SetLabelFont(132);
   h_frac->GetYaxis()->SetLabelSize(0.04);
   h_frac->GetZaxis()->SetTitleOffset(.8);
   h_frac->GetZaxis()->SetTitleFont(132);
   h_frac->GetZaxis()->SetTitleSize(0.06);
   h_frac->GetZaxis()->SetLabelFont(132);
   h_frac->GetZaxis()->SetLabelSize(0.05);

   TCanvas *c_frac = new TCanvas("fNcl_skymap", "fNcl_skymap", 500, 300);
   c_frac->SetFillColor(0);
   c_frac->SetBorderSize(0);
   c_frac->GetFrame()->SetFillColor(0);
   c_frac->GetFrame()->SetBorderSize(0);
   c_frac->SetFrameBorderMode(0);
   c_frac->SetFrameFillColor(0);
   c_frac->SetLeftMargin(0.12);
   c_frac->SetRightMargin(0.05);
   c_frac->SetTopMargin(0.05);
   c_frac->SetBottomMargin(0.1);
   c_frac->SetFrameBorderMode(0);
   c_frac->SetBorderMode(0);
   c_frac->SetLogz(0);
   gStyle->SetPalette(1);
   cout << endl << endl;

   h_frac->Draw("surf3");

   TPaveText *pt = new TPaveText(0.01, 0.9208065, 0.4, 0.995, "blNDC");
   pt->SetBorderSize(2);
   pt->SetFillColor(0);
   pt->SetLineColor(1);
   pt->SetLineWidth(1);
   pt->AddText(title);
   pt->Draw();

   c_frac->Modified();
   c_frac->Update();
   gSIM_ROOTAPP->Run(kTRUE);

   // Free memory
   delete h_frac;
   delete c_frac;
}

//______________________________________________________________________________
void test_find_halo_physicalsize()
{
   //--- Tests find max of function and find point closest to a given value

   cout << endl;
   cout << "     ++++++++++++++++++++++++++++++++++++" << endl;
   cout << "     ++++ test_find_halo_physicalsize +++" << endl;
   cout << "     ++++++++++++++++++++++++++++++++++++" << endl;


   //load_parameters("clumpy_params.txt", false);

   double eps = 1.e-3;
   double par_gal[10];
   gal_set_partot(par_gal);

   double m_ref = 1.e8;
   double r_ref = 0.3;
   double rh = 1.;

   double par_size[18];
   //  par_size[0-5]     rho_sat: norm [Msol/kpc^3] + rs [kpc] + shape #(1,2,3) + card_profile [gENUM_PROFILE]
   //  par_radius[6]     UNUSED
   //  par_radius[7]     rho_host DM density normalisation [Msol/kpc^3]
   //  par_radius[8-12]  rho_host:  norm [Msol/kpc^3] + rs [kpc] + shape #(1,2,3) + card_profile [gENUM_PROFILE]
   //  par_radius[13]    d: distance from host center to satellite center [kpc]
   //  par_radius[14]    eps: relative precision sought for mass calculation
   //  par_radius[15]    n_call: must be initialised to 0!
   //  par_radius[16]    Select method used to estimate physical size
   //                         0: tidal radius condition
   //                         1: equal-density condition
   par_size[0] = 1.;
   par_size[1] = rh;
   par_size[2] = 0.2;
   par_size[3] = 0.;
   par_size[4] = 0.;
   par_size[5] = kEINASTO;
   par_size[6] = rh;
   par_size[7] = par_gal[0];
   par_size[8] = par_gal[1];
   par_size[9] = par_gal[2];
   par_size[10] = par_gal[3];
   par_size[11] = par_gal[4];
   par_size[12] = par_gal[5];
   par_size[14] = eps;
   par_size[17] = 0.; // hard fail
   set_par0_given_mref(par_size, r_ref, m_ref, eps);

   //clock_t start, end;
   //start = clock();

   // 0: rtidal; 1: equality of densities
   for (double d_halo = 0.1; d_halo < 200.; d_halo *= 1.5) {
      // 0: rtidal; 1: equality of densities
      par_size[13] = d_halo;
      par_size[15] = 0.;
      par_size[16] = kBINNEYTREMAINE87;
      double rtidal = find_halo_physicalsize(rtidal, par_size);
      par_size[13] = d_halo;
      par_size[15] = 0.;
      par_size[16] = kEQUALDENS;
      double requal = find_halo_physicalsize(requal, par_size);

      printf("  d=%.1f [kpc]  =>  r_tidal=%.2le [kpc]   r_equa=%.2le [kpc]\n", d_halo, rtidal, requal);
   }
   //end = clock();
   //printf("Took %ld ticks\n", end-start);
   //printf("Took %f seconds\n", (double)(end-start)/CLOCKS_PER_SEC);
}


void    test_mdelta1_to_mdelta2()
{
   //--- Tests find max of function and find point closest to a given value

   cout << endl;
   cout << "     ++++++++++++++++++++++++++++++++++++" << endl;
   cout << "     ++++ test_mdelta1_to_mdelta2 +++" << endl;
   cout << "     ++++++++++++++++++++++++++++++++++++" << endl;


   //load_parameters("clumpy_params.txt", false);
   double par_prof[4];

   double mdelta1 = 1.968e15;
   double Delta1 = 500;
   int card_cdelta = kSANCHEZ14_200;
   par_prof[0] = 1.;
   par_prof[1] = 3.;
   par_prof[2] = 1.;
   par_prof[3] = kZHAO;
   double z = 0.;
   double par_tot[7];
   double r = -1;

   gSIM_EPS = 1e-2;

   cout << "\n ==> Check speed of mdelta_to_par() function with the two methods:" << endl <<  endl;

   int Nrep = 1e4;

   clock_t begin = clock();
   for (int i = 0; i < Nrep; ++i) {
      mdelta_to_par(par_tot, mdelta1, Delta1, card_cdelta, par_prof, gSIM_EPS, z);
   }
   clock_t end = clock();

   cout << "  Result with Moritz' method:" <<  endl;
   cout << "    rho_s = " << par_tot[0] << " Msol/kpc^3" <<  endl;
   cout << "      r_s = " << par_tot[1] << " kpc" <<  endl;
   cout << "  R_Delta = " << par_tot[6] << " kpc" <<  endl;

   double time_spent = (double)(end - begin) / CLOCKS_PER_SEC / Nrep;
   cout << "  Time: " << time_spent << " s." << endl << endl;

   gSIM_MDELTA_TO_PAR_METHOD = true;

   begin = clock();
   for (int i = 0; i < Nrep; ++i) {
      mdelta_to_par(par_tot, mdelta1, Delta1, card_cdelta, par_prof, gSIM_EPS, z);
   }
   end = clock();

   cout << "  Result with Celine's method:" <<  endl;
   cout << "    rho_s = " << par_tot[0] << " Msol/kpc^3" <<  endl;
   cout << "      r_s = " << par_tot[1] << " kpc" <<  endl;
   cout << "  R_Delta = " << par_tot[6] << " kpc" <<  endl;

   time_spent = (double)(end - begin) / CLOCKS_PER_SEC  / Nrep;
   cout << "  Time: " << time_spent << " s." << endl;


   cout << "\n ==> Calculate f_sub(Delta) for fixed f_sub(Delta_ref):" << endl <<  endl;

   //double Delta_Ref = 200;
   //double fsub_ref  = 0.2;

   // take Galactic subhalo shapes:
   // Set Gal. parameters
   const int npar1 = 10;
   const int npar2 = 24;
   double par_dpdv[npar1];
   double par_subs[npar2];

   gal_set_partot(par_tot);

   double mtot = mass_singlehalo(par_tot, gSIM_EPS);
   cout << "  => mtot = " << mtot << endl;
   cout << "  corresponds to Delta = " << shapeparams_to_Delta(par_tot, par_tot[6], z) << endl;
   double f_dm = 0.;
   //double ntot_subs = 0.;
   //double mmin_subs = gDM_SUBS_MMIN;
   double mmax_subs = mtot * gDM_SUBS_MMAXFRAC;

   par_dpdv[1] = 15 * gMW_TOT_RSCALE;
   par_dpdv[2] = 11;
   par_dpdv[3] = 2;
   par_dpdv[4] = 3;
   par_dpdv[5] = kDPDV_GAO04;
   par_dpdv[6] = gMW_RMAX;
   par_dpdv[7] = gMW_RSOL;
   dpdv_setnormprob(par_dpdv, gSIM_EPS);

   par_subs[0] = gMW_SUBS_SHAPE_PARAMS[0];
   par_subs[1] = gMW_SUBS_SHAPE_PARAMS[1];
   par_subs[2] = gMW_SUBS_SHAPE_PARAMS[2];
   par_subs[3] = gMW_SUBS_FLAG_PROFILE;
   par_subs[4] = gMW_RMAX;
   par_subs[5] = gSIM_EPS;
   par_subs[6] = z;
   par_subs[7] = 0.; // will only be used - and updated later - for nlevel > 1 substructures.
   par_subs[8] = 1.;
   par_subs[9] = gMW_SUBS_DPDM_SLOPE;
   par_subs[10] = gMW_SUBS_FLAG_CDELTAMDELTA;
   Delta = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, par_subs[6]);
   mdelta_to_innerslope(mtot, Delta, par_subs[10], par_subs, par_subs[6]);
   //par_subs[11] = mdelta_to_cdelta(mtot, Delta, gMW_SUBS_FLAG_CDELTAMDELTA, par_tot, par_subs[6]/*z*/);
   par_subs[11] = 0.; // will only be used - and updated later - for nlevel > 1 substructures.
   par_subs[12] = gDM_LOGCDELTA_STDDEV;
   par_subs[13] = gDM_FLAG_CDELTA_DIST;
   par_subs[14] = 0; //  f: mass fraction of substructures
   par_subs[15] = gDM_SUBS_NUMBEROFLEVELS;
   for (int i = 0; i < 7; ++i)
      par_subs[i + 16] = par_dpdv[i];
   par_subs[23] = 10.;//gMW_SUBS_DPDV_RSCALE_TO_RS_HOST;

   // Find proper normalisation for dpdm to be a probability, i.e. int_Mmin^Mmax dpdm dM = 1
   dpdm_setnormprob(&par_subs[8], gDM_SUBS_MMIN, mmax_subs, gSIM_EPS);

   // Calculate f_dm => par_subs[14]
   double ntot = nsubtot_from_nsubm1m2(&par_subs[8], gMW_SUBS_M1, gMW_SUBS_M2, gMW_SUBS_N_INM1M2, gSIM_EPS);
   cout << "  => ntot=" << ntot << endl;
   double mass_mean = mean1cl_mass(&par_subs[8], gDM_SUBS_MMIN, mmax_subs, gSIM_EPS);
   f_dm = mass_mean * ntot / mtot;
   par_subs[14] = f_dm;
   cout << "  => f_dm=" << f_dm << endl << endl;
   double rho_subs_norm = f_dm * mtot;

   // now keep normalzations, but integrate up to different Delta:
   vector <double> Delta_vec;
   double Delta_min = 1;
   double Delta_max = 1e5;
   int n_Delta = 999;
   double log_delta_Delta = (log(Delta_max) - log(Delta_min)) / (n_Delta);

   string file = gSIM_OUTPUT_DIR + "check_fsub.dat";
   ofstream myfile;
   myfile.open(file.c_str());

   for (int i = 0; i <= n_Delta; ++i) {
      Delta_vec.push_back(exp(log(Delta_min) + i * log_delta_Delta));
      double Rdelta = shapeparams_to_Rdelta(par_tot, Delta_vec[i], z);
      par_tot[6] = Rdelta;
      mtot = mass_singlehalo(par_tot, gSIM_EPS);
      par_dpdv[6] = Rdelta;
      double msubs = rho_subs_norm * mass_singlehalo(par_dpdv, gSIM_EPS);
      cout << "  => Delta = " << Delta_vec[i] << ": mtot = " << mtot  << ", msubs = " << msubs << ", fsubs = " << msubs / mtot << endl;
      myfile << Delta_vec[i] << " " << msubs / mtot << endl;
   }

   myfile.close();
   cout << "Test done. Output written in " + file << endl;
}

//______________________________________________________________________________
//______________________________________________________________________________
int main(int argc, char *argv [])
{
   if (argc <= 1) {
      printf("  usage:\n");
      printf("          ------------ base functions ---------\n");
      printf("     %s -a  => linear and logarithmic adaptive simpson integration (+ DCosmo, whatever it is)\n", argv[0]);
      printf("     %s -b  => print rho_tot, rho_sm, <rho_cl>\n", argv[0]);
      printf("     %s -c  => print/draw r2*rho2 and r2*rho for various profiles (and test r_{-2} and rsat)\n", argv[0]);
      printf("     %s -d  => plot triaxial galactic halo (for current parameter file)\n", argv[0]);
      printf("     %s -e  => Calculate rhos from a profile alpha,beta,gamma,rs and an M(r) constraint\n", argv[0]);
      printf("     %s -f  => numerical integration and precision control on mass and intrinsic luminosity\n", argv[0]);
      printf("          ----------- l.o.s. integral ---------\n");
      printf("     %s -g  => Compares lum_single_clump to lum from los_integral\n", argv[0]);
      printf("     %s -h  => 1D rho2_sm(l) seen from Earth for various l.o.s. (psi,theta)\n", argv[0]);
      printf("     %s -i  => 1D Jgal_sm(psi_los, theta_los) for various psi\n", argv[0]);
      printf("     %s -j  => 1D cumulative Jgal_sm along the l.o.s.\n", argv[0]);
      printf("     %s -k  => 2D skymap for the smooth profile\n", argv[0]);
      printf("          ------------ clump-related ----------\n");
      printf("     %s -l  => find radius where x%% of the luminosity is emitted, and test r_cl/d_cl<<1\n", argv[0]);
      printf("     %s -m  => test for drawing clump positions from dpdv_xyz(r) using different frameworks\n", argv[0]);
      printf("     %s -n  => test for drawing clump masses from dpdm(m)\n", argv[0]);
      printf("     %s -o  => test clump distributions (to get M, <M>, Ncl, etc.)\n", argv[0]);
      printf("     %s -p  => test for the fraction of clump to be drawn in gSIM_ALPHAINT from Earth\n", argv[0]);
      printf("     %s -q  => test for cross product: single clump in host\n", argv[0]);
      printf("     %s -r  => test for cross product: all clumps in the loi\n", argv[0]);
      printf("     %s -s  => test for substructures\n", argv[0]);
      printf("     %s -t  => test for tidal or density-equality radius\n", argv[0]);
      printf("     %s -u  => test for mdelta1-mdelta2 conversion\n", argv[0]);

//      printf("     %s -r  => test for limit continuum (for non-drawn clumps)\n", argv[0]);
//      printf("     %s -s  => test for distributions\n", argv[0]);
      printf("\n");
      return 1;
   }

   // --- OPEN root application
   gSIM_ROOTAPP = new TApplication("App", NULL, NULL);
   // Set CLUMPY style
   rootstyle_set2CLUMPY();

   //--- Test functions
   size_t l_arg = strlen(argv[1]);
   if (memchr(argv[1], 'a', l_arg) != NULL) {
      test_goldenmin_and_find();
      test_integr();
      test_Dcosmo();
   }
   if (memchr(argv[1], 'b', l_arg) != NULL) test_rhotot_and_sub();
   if (memchr(argv[1], 'c', l_arg) != NULL) test_profiles();
   if (memchr(argv[1], 'd', l_arg) != NULL) test_profiles_triaxiality();
   if (memchr(argv[1], 'e', l_arg) != NULL) test_profile2rhos();
   if (memchr(argv[1], 'f', l_arg) != NULL) test_mass_and_lum();


   //--- Test los integral
   if (memchr(argv[1], 'g', l_arg) != NULL) test_los_int();
   if (memchr(argv[1], 'h', l_arg) != NULL) test_rho2_smooth();
   if (memchr(argv[1], 'i', l_arg) != NULL) test_jgal_smooth();
   if (memchr(argv[1], 'j', l_arg) != NULL) test_jgal_smooth_cumul();
   if (memchr(argv[1], 'k', l_arg) != NULL) test_jgal_smooth_skymap();

   //--- Test others
   if (memchr(argv[1], 'l', l_arg) != NULL) test_R90_and_pointlikeness();
   if (memchr(argv[1], 'm', l_arg) != NULL) test_draw_positions_from_dpdv();
   if (memchr(argv[1], 'n', l_arg) != NULL) test_draw_masses_from_dpdm();
   if (memchr(argv[1], 'o', l_arg) != NULL) test_clump_distribs();
   if (memchr(argv[1], 'p', l_arg) != NULL) test_fNcl_clump();
   if (memchr(argv[1], 'q', l_arg) != NULL) test_cross_product_1cl();
   if (memchr(argv[1], 'r', l_arg) != NULL) test_cross_product_continuum();
   if (memchr(argv[1], 's', l_arg) != NULL) test_substructures();
   if (memchr(argv[1], 't', l_arg) != NULL) test_find_halo_physicalsize();
   if (memchr(argv[1], 'u', l_arg) != NULL) test_mdelta1_to_mdelta2();

   delete gSIM_ROOTAPP;
   gSIM_ROOTAPP = NULL;
   return 0;
}





/*! \file clumpy_tests.cc
  \brief \c CLUMPY test functions and plots (<i>DM clumps and J-factor calculation</i>)

   <CENTER>________________________________________</CENTER>\n

 Just type \c ./bin/clumpy_tests to get the executable usage, or look into the code.
 This is not meant to be part of the public version.

*/
