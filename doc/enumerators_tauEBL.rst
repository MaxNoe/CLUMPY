.. _rst_enumerators_tauEBL:

.. role::  raw-html(raw)
    :format: html

EBL :math:`\gamma`-ray attenuation models
--------------------------------------------------------------

Possible keyword choices for the :option:`gEXTRAGAL_FLAG_ABSORPTIONPROFILE` variable:

.. csv-table:: 
   :file: tables/gENUM_ABSORPTIONPROFILE.csv_table
   :widths: 10, 30
   :align: left

:raw-html:`<br>&nbsp;<br>`


.. seealso:: :numref:`rst_physics_definitions`, :ref:`rst_physics_definitions` and :numref:`rst_physics_cosmo`, :ref:`rst_physics_cosmo`.