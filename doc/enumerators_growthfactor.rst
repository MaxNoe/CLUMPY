.. _rst_enumerators_growthfactor:

.. role::  raw-html(raw)
    :format: html

Growth factor :math:`D(z)`
--------------------------------------------------------------

Possible keyword choices for the :option:`gSIM_EXTRAGAL_FLAG_GROWTHFACTOR` parameter:

.. csv-table::
   :file: tables/gENUM_GROWTHFACTOR.csv_table
   :widths: 10, 70
   :align: left

:raw-html:`<br>&nbsp;<br>`

.. seealso:: :numref:`rst_physics_cosmo`, :ref:`rst_physics_cosmo`.