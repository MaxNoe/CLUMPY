.. _rst_physics_cosmo:

.. sectionauthor:: Celine Combet <celine.combet@lpsc.in2p3.fr>

.. role::  raw-html(raw)
    :format: html

Cosmology (cosmo.h)
--------------------------------

Contributions from the distribution of extragalactic haloes (as introduced in :numref:`rst_physics_definitions`) require accounting for the cosmology. The module `cosmo.h <doxygen/cosmo_8h.html>`_ contains all cosmology-related functions, from the computation of the various cosmological distances to the evaluation of the halo mass function required by the intensity multiplier. A large fraction of the functions included in this module are a translation into :math:`{\tt C}` of Eiichiro Komatsu's very useful :raw-html:`<a href="https://wwwmpa.mpa-garching.mpg.de/~komatsu/crl/" target=_blank>Cosmology Routine Library in Fortran</a>`.

.. note:: In :program:`CLUMPY`, the various options described in :numref:`rst_doc_module_e` allow to quickly check and plot various intermediate and final quantities presented below (cosmological distances, intensity multiplier, EBL absorption, or differential flux).

.. note:: The extragalactic calculation requires several imbricated integrals that would be very time consuming to compute if done naively (see :numref:`rst_physics_integr_los`). To optimise the nested integration, :program:`CLUMPY` first tabulates the mass function and other cosmological quantities on a grid of masses and redshifts and then performs interpolations to obtain the results for any :math:`(M,z)` combination during integration. The range and resolution of the grid can be adjusted by the user in the main parameter file.


Cosmological distances
~~~~~~~~~~~~~~~~~~~~~~

There are several ways to define distances in cosmology, whether one is interested in:

   - the distance :math:`d_{\rm c}` between two objects along the line of sight (comoving distance),
   - the transverse distance :math:`d_{\rm trans}` between two objects at the same redshift (transverse comoving distance),
   - the physical size of an object given its observed angular size (angular diameter distance :math:`d_{\rm A}`),
   - the distance to link an observed flux to the intrinsic luminosity of a source (luminosity distance :math:`d_{\rm L}`).

These well-known definitions depend on the cosmological parameters and redshift and are not repeated here, and we refer the interested user to :raw-html:`<a href="http://cdsads.u-strasbg.fr/abs/1999astro.ph..5116H" target=_blank>Hogg (1999)</a>`.

.. note:: :program:`CLUMPY` initialises the computation of the extragalactic flux by tabulating the values of the various distance definitions on the redshift grid and the values of the mass function on the 2D :math:`(M,z)` grid. The resulting arrays are stored as global variables.


Intensity multiplier
~~~~~~~~~~~~~~~~~~~~

For DM annihilations, the quantity :math:`\langle \delta^2(z) \rangle` is computed according to the halo model approach :raw-html:`<a href="http://cdsads.u-strasbg.fr/abs/2002PhRvD..66l3502U,2013PhRvD..87l3539A,2016JCAP...08..069M" target=_blank>(Ullio et al., 2002; Ando & Komatsu, 2003; Moliné et al., 2016)</a>`. In this setup, the intensity multiplier from the inhomogeneous Universe is written as

   .. math::

      \left\langle\delta^2(z)\right\rangle = \frac{1}{\overline{\varrho}^2_{\mathrm{m,0}}}\; \int {\rm d} M \frac{{\rm d} n}{{\rm d} M}(M,z)\times \mathcal{L}(M,z) \,,

where :math:`\overline{\varrho}_{\mathrm{m,\,0}}` is today's mean total matter density, :math:`{{\rm d} n}/{\rm d} M` the halo mass function, and :math:`\mathcal{L}(M,z)` the one-halo luminosity discussed in :numref:`rst_physics_clumps_generic`.

.. note:: :program:`CLUMPY` uses the previously tabulated values of :math:`\sigma(M,z)`, :math:`P_{\rm lin}(k,z=0)`, and :math:`D(z)` to interpolate the mass function to the relevant mass and redshift and returns the corresponding intensity multiplier, but under the form :math:`d\delta^2/d\ln M`.


Halo mass function
~~~~~~~~~~~~~~~~~~

The number density of extragalactic haloes :math:`{{\rm d} n}/{{\rm d} M}` in a given mass range at redshift :math:`z` depends on the variance :math:`\sigma` of the density fluctuations and on the multiplicity function :math:`f(\sigma,\,z)` that encodes nonlinear structure formation,

   .. math::
      \frac{\mathrm{d}n}{\mathrm{d}M}(M,\,z)  = f(\sigma,z)\; \frac{\overline{\varrho}_{\rm m,0}}{M}\, \frac{\mathrm{d}\ln \sigma^{-1}}{\mathrm{d}M}\,.
      :label: eq_hmf

:program:`CLUMPY` returns a vector of tabulated values of the mass function, but written as :math:`\mathrm{d}n/\mathrm{d}\ln M` at a given redshift, for a given set of masses defined by the user in the main parameter file. The result for any mass is interpolated from these tabulated values.

.. note:: The multiplicity function :math:`f(\sigma, z)` is generally fitted to numerical simulations. :program:`CLUMPY` includes the parametrisations from a variety of simulations, considering only DM  or including baryons and relying on different cosmologies. The available parametrizations are listed in  and the user may select any one of these with a corresponding keyword (see  for details).


:math:`\sigma(M,z)` and :math:`P_{\rm lin}(k,0)`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is calculated from the linear matter power spectrum, :math:`P_{\rm lin}(k,z=0)`, according to

   .. math::

      \sigma^2(M,\,z) = \frac{D(z)^2}{2\pi^2} \int P_{\rm lin}(k, z=0)\, \widehat{W}^2(kR)\,k^2\,\mathrm{d}k\,,

with :math:`R=[M/(\gamma_{\rm f}\,\overline{\varrho}_{\rm m,0})]^{1/3}`, the comoving scale radius of a collapsing sphere containing the mass :math:`M`. Different shapes of the sphere can be selected, expressed in :math:`k`-space by

   .. math::

      \widehat{W}(kR) &= 3\,(kR)^{-3} \,[\sin(kR)  - kR \,\cos(kR)],\;\gamma_{\rm f} = \frac{4\pi}{3} \quad{\rm (top~hat)},\\
      \widehat{W}(kR) &= \exp[- (kR)^2 / 2],\;\gamma_{\rm f} = (2\pi)^{3/2} \quad{\rm (Gaussian~window)},\\
      \widehat{W}(kR) &= 1 - \theta(kR - 1),\;\gamma_{\rm f} = 6\pi^2 \quad{\rm (sharp-k~filter)}\,,

with :math:`\theta` the Heaviside step-function. 

.. note:: For a given cosmology, :program:`CLUMPY` loads the matter power spectrum from an existing file, the name of which should match a pattern obtained from the cosmological parameters. If the file does not exist, :program:`CLUMPY` is interfaced with the  :program:`CLASS` code :raw-html:`<a href="http://cdsads.u-strasbg.fr/abs/2011arXiv1104.2932L" target=_blank>(Lesgourgues, 2011)</a>` to compute :math:`P_{\rm lin}(k,z=0)` (which should have been previously installed by the user).


Growth factor :math:`D(z)`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The growth factor, :math:`D(z) = g(z)/g(z=0)` is defined from

   .. math::
      g(z) = \frac{5}{2}\times \frac{8\pi G}{3}\overline{\varrho}_{\rm m,0}\times H(z)\,\int_z^\infty \frac{1+z'}{H^3(z')}\,{\rm d} z'\,,
      :label: eq_growthfactor

and it allows to compute the variance :math:`\sigma(M,z)` at any redshift (:math:`G` is the gravitational constant).

.. note:: :program:`CLUMPY` computes the linear growth rate :math:`D(z)`, for a given redshift, using either the exact analytical solution above or the approximate (and faster to compute) formula of :raw-html:`<a href="http://cdsads.u-strasbg.fr/abs/1992ARA&A..30..499C" target=_blank>Carroll et al. (1992)</a>`, depending on the user's choice.

