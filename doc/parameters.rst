.. _rst_input_params:

.. role::  raw-html(raw)
    :format: html

Input parameters: units and definition
======================================

Cosmological parameters
~~~~~~~~~~~~~~~~~~~~~~~
.. do not change this headline, it is hardcoded linked with doxygen doc

========================================================= ========================= =====================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- -------------------------------------------------------------------------------------
:option:`gCOSMO_DELTA0`                                   :math:`\rm -`             Overdensity factor at :math:`z=0;\;-1` for original :keyword:`kBRYANNORMAN98` description
:option:`gCOSMO_FLAG_DELTA_REF`                           :math:`\rm -`             Reference density of the overdensity factor, see :ref:`rst_enumerators_delta` for possible keywords
:option:`gCOSMO_HUBBLE`                                   :math:`\rm -`             Present-day (:math:`z=0`) normalized Hubble expansion rate :math:`h = H_0/(100 {\rm km\, s^{-1} \,Mpc^{-1}})`
:option:`gCOSMO_N_S`                                      :math:`\rm -`             Scalar spectral index :math:`n_s` of primordial perturbations
:option:`gCOSMO_OMEGA0_M`                                 :math:`\rm -`             Present-day Dark and Baryonic Mass, :math:`\Omega_{\rm m,0} = \varrho_{\rm m,0}/\varrho_{\rm c,0}`
:option:`gCOSMO_OMEGA0_B`                                 :math:`\rm -`             Present-day  Baryonic Mass, :math:`\Omega_{\rm b,0}  = \varrho_{\rm b,0}/\varrho_{\rm c,0}`
:option:`gCOSMO_OMEGA0_K`                                 :math:`\rm -`             Curvature :math:`\Omega_{\rm k,0}`, only to be  used with :option:`-e0` option.
:option:`gCOSMO_SIGMA8`                                   :math:`\rm -`             Fluctuation amplitude :math:`\sigma_8` at :math:`8\,h^{-1}\,\rm{Mpc}`
:option:`gCOSMO_T0`                                       :math:`\rm K`             Present-day CMB photon temperature, :math:`T_0`
:option:`gCOSMO_TAU_REIO`                                 :math:`\rm -`             Reioization optical depth, :math:`\tau_{\rm reio}`
:option:`gCOSMO_WDE`                                      :math:`\rm -`             Dark energy equation of state exponent
========================================================= ========================= =====================================================================================


Dark Matter global parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. do not change this headline, it is hardcoded linked with doxygen doc

========================================================= ========================= =================================================================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ---------------------------------------------------------------------------------------------------------------------------------
:option:`gDM_IS_IDM`                                      :math:`\rm -`             If :keyword:`True`, switches on self-interacting DM halo mass function cut-off from :raw-html:`<a href=http://adsabs.harvard.edu/abs/2016JCAP...08..069M target="_blank">Molin&eacute; et al. (2016)</a>`.
:option:`gDM_KMAX`                                        :math:`h\;\rm Mpc^{-1}`   :math:`k_{\rm max}/h` of matter power spectrum (preferred over :math:`k_{\rm max}` from :math:`P_{\rm lin}` file for value != :keyword:`-1`)
:option:`gDM_KMAX_SIGMA_CUTOFF`                            :math:`\rm -`            Smoothness of the :math:`P_{\rm lin}(k)` log-sigmoid cut-off
:option:`gDM_LOGCDELTA_STDDEV`                            :math:`\rm -`             Width of log-normal :math:`c_\Delta(M_\Delta)` distribution (value 0 corresponds to a Dirac distribution)
:option:`gDM_RHOHALOES_TO_RHOMEAN`                        :math:`\rm -`             Fraction of :math:`\varrho_{\rm m}` bound into DM haloes
:option:`gDM_RHOSAT`                                      :math:`\rm M_\odot/kpc^3` DM saturation density, :math:`\rho_{\rm sat}\approx 3 \times 10^{18} \left(\frac{m_\chi}{100~\rm GeV}\right) \times\left( \frac{10^{-26} {\rm cm}^3~{\rm s}^{-1}}{\langle \sigma v\rangle}\right)\approx 10^{19} \rm M_\odot\,\,\rm kpc^{-3}` :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/1992PhLB..294..221B target="_blank">(Berezinskii et al., 1992)</a>`: high central density not sustainable as overcome by DM annihilations
:option:`gDM_SUBS_MMIN`                                   :math:`\rm M_\odot`       Minimal mass of DM subhaloes, :math:`m_{\rm min}`
:option:`gDM_SUBS_MMAXFRAC`                               :math:`\rm -`             Maximal mass of subhaloes in the host halo: :math:`m_{\rm max} = {\tt gDM\_SUBS\_MMAXFRAC}\times M_{\rm host}`
:option:`gDM_SUBS_NUMBEROFLEVELS`                         :math:`\rm -`             Number of multilevel substructures (no effect for DM decay)
:option:`gDM_FLAG_TIDALCUT`                               :math:`\rm -`             Select model by which subhalo outer radii are truncated by host potential. See :numref:`rst_enumerators_tidalcut` for possible keywords.
========================================================= ========================= =================================================================================================================================


Milky-Way DM total and clump parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. do not change this headline, it is hardcoded linked with doxygen doc


.. note :: Use  :option:`gMW_SUBS_FLAG_PROFILE = kHOST`, or :option:`gMW_DPDV_FLAG_PROFILE = kHOST`, or any of their shape parameters (e.g., :option:`gMW_SUBS_SHAPE_PARAMS_0 = kHOST`) to take the same value as for the Milky Way halo total DM density, :option:`gMW_TOT_FLAG_PROFILE`/:option:`gMW_TOT_SHAPE_PARAMS_0,1,2`. 


========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gMW_TOT_FLAG_PROFILE`                            :math:`\rm -`             Density profile of total Galactic DM halo. See :numref:`rst_enumerators_dm_profiles` for possible keywords
:option:`gMW_TOT_SHAPE_PARAMS_0`                          :math:`\rm -`             Shape parameter 1 of total Galactic DM halo density profile
:option:`gMW_TOT_SHAPE_PARAMS_1`                          :math:`\rm -`             Shape parameter 2 of total Galactic DM halo density profile
:option:`gMW_TOT_SHAPE_PARAMS_2`                          :math:`\rm -`             Shape parameter 3 of total Galactic DM halo density profile
:option:`gMW_TOT_RSCALE`                                  :math:`\rm kpc`           Scale radius of total Dark Matter halo density profile
:option:`gMW_RMAX`                                        :math:`\rm kpc`           Outer bound of the DM halo
:option:`gMW_RSOL`                                        :math:`\rm kpc`           Sun's distance from the Galactic center, :math:`R_\odot`
:option:`gMW_RHOSOL`                                      :math:`\rm GeV/cm^3`      Local Dark Matter density, :math:`\rho_\odot=\rho_{\rm tot}(R_\odot)`
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gMW_TRIAXIAL_IS`                                 :math:`\rm -`             Triaxial halo (:keyword:`True` or :keyword:`False`). See :ref:`rst_trixial` for details
:option:`gMW_TRIAXIAL_AXES_0`                             :math:`\rm -`             1st axis ratio :math:`a` of triaxial halo  (if no rotation angle, major along x-axis)
:option:`gMW_TRIAXIAL_AXES_1`                             :math:`\rm -`             2nd axis ratio :math:`b` of triaxial halo                                                  
:option:`gMW_TRIAXIAL_AXES_2`                             :math:`\rm -`             3rd axis ratio :math:`c` of triaxial halo  (if no rotation angle, minor along z-axis)
:option:`gMW_TRIAXIAL_ROTANGLES_0`                        :math:`\rm deg`           1st rotation angle of triaxial halo, :math:`[-180^\circ,180^\circ]`
:option:`gMW_TRIAXIAL_ROTANGLES_1`                        :math:`\rm deg`           2nd rotation angle of triaxial halo, :math:`[-90^\circ,90^\circ]`
:option:`gMW_TRIAXIAL_ROTANGLES_2`                        :math:`\rm deg`           3rd rotation angle of triaxial halo, :math:`[-180^\circ,180^\circ]`
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gMW_SUBS_FLAG_PROFILE`                           :math:`\rm -`             Halo profile of subhaloes. See :numref:`rst_enumerators_dm_profiles` for possible keywords
:option:`gMW_SUBS_SHAPE_PARAMS_0`                         :math:`\rm -`             Shape parameter 1 of subhaloes density profile
:option:`gMW_SUBS_SHAPE_PARAMS_1`                         :math:`\rm -`             Shape parameter 2 of subhaloes density profile
:option:`gMW_SUBS_SHAPE_PARAMS_2`                         :math:`\rm -`             Shape parameter 3 of subhaloes density profile
:option:`gMW_SUBS_FLAG_CDELTAMDELTA`                      :math:`\rm -`             Mass-concentration model of subhaloes. See :ref:`rst_enumerators_cdelta` for possible keywords
:option:`gMW_SUBS_DPDV_FLAG_PROFILE`                      :math:`\rm -`             :math:`{{\rm d}{\cal P}}/{{\rm d}V}` profile of subhalo distribution in host. See :numref:`rst_enumerators_dm_profiles` for possible keywords
:option:`gMW_SUBS_DPDV_SHAPE_PARAMS_0`                    :math:`\rm -`             Shape parameter 1 of  :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gMW_SUBS_DPDV_SHAPE_PARAMS_1`                    :math:`\rm -`             Shape parameter 2 of  :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gMW_SUBS_DPDV_SHAPE_PARAMS_2`                    :math:`\rm -`             Shape parameter 3 of  :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gMW_SUBS_DPDV_RSCALE_TO_RS_HOST`                 :math:`\rm -`             Ratio of scale radius of :math:`{\rm d}{\cal P}/{\rm d}V` profile to :math:`r_{\rm s}` of host density profile
:option:`gMW_SUBS_DPDM_SLOPE`                             :math:`\rm -`             Slope :math:`\alpha_m` of power-law subhalo mass spectrum :math:`{{\rm d}{\cal P}}/{{\rm d}m}\propto m^{-\alpha_m}`
:option:`gMW_SUBS_M1`                                     :math:`\rm M_\odot`       Mass :math:`m_1`
:option:`gMW_SUBS_M2`                                     :math:`\rm M_\odot`       Mass :math:`m_2`
:option:`gMW_SUBS_N_INM1M2`                               :math:`\rm -`             Number of subhaloes with masses between :math:`m_1` and :math:`m_2`
:option:`gMW_SUBS_TABULATED_IS`                           :math:`\rm -`             If true, use tabulated (evolved) sub properties
:option:`gMW_SUBS_TABULATED_CMIN_OF_R`                    :math:`\rm -`             File for minimal :math:`c(r_{\rm gal})` after tidal effects
:option:`gMW_SUBS_TABULATED_LCRIT`                        :math:`\rm -`             File for :math:`l_{\rm crit}` for evolved subhalo properties
:option:`gMW_SUBS_TABULATED_RTIDAL_TO_RS`                 :math:`\rm -`             File for :math:`r_{\rm tidal}/r_s` for evolved subhalos
========================================================= ========================= ==============================================================================================


Extragalactic DM total and clump parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. do not change this headline, it is hardcoded linked with doxygen doc

.. note :: Use  :option:`gEXTRAGAL_SUBS_DPDV_FLAG_PROFILE = kHOST`  or any of its shape parameters (e.g., :option:`gEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_0 = kHOST`) to take the same value as for the  halos' total DM density description, :option:`gEXTRAGAL_FLAG_PROFILE`/:option:`gEXTRAGAL_SHAPE_PARAMS_0,1,2`. 


========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gEXTRAGAL_FLAG_PROFILE`                          :math:`\rm -`             Dark Matter halo density profile. See :numref:`rst_enumerators_dm_profiles` for possible keywords
:option:`gEXTRAGAL_SHAPE_PARAMS_0`                        :math:`\rm -`             Shape parameter 1 of cosmic DM halo density profile
:option:`gEXTRAGAL_SHAPE_PARAMS_1`                        :math:`\rm -`             Shape parameter 2 of cosmic DM halo density profile
:option:`gEXTRAGAL_SHAPE_PARAMS_2`                        :math:`\rm -`             Shape parameter 3 of cosmic DM halo density profile
:option:`gEXTRAGAL_FLAG_CDELTAMDELTA`                     :math:`\rm -`             Mass-concentration model of cosmic DM field halos. See :ref:`rst_enumerators_cdelta` for possible keywords
:option:`gEXTRAGAL_FLAG_CDELTAMDELTA_LIST`                :math:`\rm -`             List of mass-concentration models. See :ref:`rst_enumerators_cdelta` for possible keywords to be parsed separated by commas.
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gEXTRAGAL_FLAG_MASSFUNCTION`                     :math:`\rm -`             Halo mass function multiplicity function, :math:`f(\sigma, z)`, Eq. :eq:`eq_hmf`. See :ref:`rst_enumerators_massfunction` for possible keywords
:option:`gEXTRAGAL_IDM_MHALFMODE`                         :math:`\rm M_\odot`       Interacting DM (IDM) half-mode mass of cut-off :raw-html:`<a href=http://adsabs.harvard.edu/abs/2016JCAP...08..069M target="_blank">(Molin&eacute; et al., 2016)</a>`
:option:`gEXTRAGAL_IDM_ALPHA`                             :math:`\rm -`             IDM cut-off parameter :math:`\alpha` :raw-html:`<a href=http://adsabs.harvard.edu/abs/2016JCAP...08..069M target="_blank">(Molin&eacute; et al., 2016)</a>`
:option:`gEXTRAGAL_IDM_BETA`                              :math:`\rm -`             IDM cut-off parameter :math:`\beta` :raw-html:`<a href=http://adsabs.harvard.edu/abs/2016JCAP...08..069M target="_blank">(Molin&eacute; et al., 2016)</a>`
:option:`gEXTRAGAL_IDM_GAMMA`                             :math:`\rm -`             IDM cut-off parameter :math:`\gamma` :raw-html:`<a href=http://adsabs.harvard.edu/abs/2016JCAP...08..069M target="_blank">(Molin&eacute; et al., 2016)</a>`
:option:`gEXTRAGAL_IDM_DELTA`                             :math:`\rm -`             IDM cut-off parameter :math:`\delta` :raw-html:`<a href=http://adsabs.harvard.edu/abs/2016JCAP...08..069M target="_blank">(Molin&eacute; et al., 2016)</a>`
:option:`gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE`             :math:`\rm -`             Force halo mass function slope to be steeper than value :math:`\alpha_M` (:math:`>0`, option switched off for value :keyword:`-1`)
:option:`gEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM`              :math:`\rm M_\odot`       Set mass below which halo mass function slope :math:`\alpha_M` is enforced
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gEXTRAGAL_SUBS_FLAG_CDELTAMDELTA`                :math:`\rm -`             Mass-concentration model of subhalos inside field halos. See :ref:`rst_enumerators_cdelta` for possible keywords
:option:`gEXTRAGAL_SUBS_DPDV_FLAG_PROFILE`                :math:`\rm -`             :math:`{{\rm d}{\cal P}}/{{\rm d}V}` profile of subhalo distribution in host. See :numref:`rst_enumerators_dm_profiles` for possible keywords
:option:`gEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_0`              :math:`\rm -`             Shape parameter 1 of :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_1`              :math:`\rm -`             Shape parameter 2 of :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gEXTRAGAL_SUBS_DPDV_SHAPE_PARAMS_2`              :math:`\rm -`             Shape parameter 3 of :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gEXTRAGAL_SUBS_DPDV_RSCALE_TO_RS_HOST`           :math:`\rm -`             Ratio of scale radius of :math:`{\rm d}{\cal P}/{\rm d}V` profile to :math:`r_{\rm s}` of host density profile
:option:`gEXTRAGAL_SUBS_DPDM_SLOPE`                       :math:`\rm -`             Slope :math:`\alpha_m` of power-law subhalo mass spectrum :math:`{{\rm d}{\cal P}}/{{\rm d}m}\propto m^{-\alpha_m}`
:option:`gEXTRAGAL_SUBS_DPDM_SLOPE_LIST`                  :math:`\rm -`             Comma-separated list of slopes of subhalo mass spectrum :math:`{{\rm d}{\cal P}}/{{\rm d}m}`
:option:`gEXTRAGAL_SUBS_MASSFRACTION`                     :math:`\rm -`             Fraction of host halo mass bound in subhaloes
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
:option:`gEXTRAGAL_FLAG_ABSORPTIONPROFILE`                :math:`\rm -`             EBL absorption profile. See :ref:`rst_enumerators_tauEBL` for possible keywords 
========================================================= ========================= ==============================================================================================


External lists with specified objects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. do not change this headline, it is hardcoded linked with doxygen doc

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gLIST_HALOES`                                    :math:`\rm -`             Text file defining halo properties for *J*-factor calculations. See :ref:`rst_halofile_format` for details
:option:`gLIST_HALOES_JEANS`                              :math:`\rm -`             Text file defining halo properties for Jeans analysis. See :ref:`rst_halofile_format` for details
:option:`gLIST_HALONAME`                                  :math:`\rm -`             Select a halo by name from above lists for analysis or drawing into 2D skymap
:option:`gLIST_HALOES_NODES`                              :math:`\rm -`             Node list of numerical halo definition(s). See :numref:`rst_kNODES` for details
:option:`gLIST_HALOES_NODES_RSCALE`                       :math:`\rm -`             Scale radius in units as of halo(s) nodes in list. If not used, scale radius is set to :math:`r_{-2}`.
========================================================= ========================= ==============================================================================================

.. _rst_universal_substructure_props:

Universal sub-clustering properties
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
separated into :keyword:`TYPE = DSPH`, :keyword:`GALAXY` (but not our Galaxy!), and :keyword:`CLUSTER`-like objects.

.. note :: Use  :option:`g[TYPE]_SUBS_FLAG_PROFILE = kHOST`, or :option:`g[TYPE]_DPDV_FLAG_PROFILE = kHOST`, or any of their shape parameters (e.g., :option:`g[TYPE]_SUBS_SHAPE_PARAMS_0 = kHOST`) to take the same value as for the host halo total density defined in :option:`gLIST_HALOES` (see :ref:`rst_halofile_format`). 

.. do not change this headline, it is hardcoded linked with doxygen doc

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gDSPH_SUBS_FLAG_PROFILE`                         :math:`\rm -`             Density profile of subhaloes. See :numref:`rst_enumerators_dm_profiles` for possible keywords
:option:`gDSPH_SUBS_SHAPE_PARAMS_0`                       :math:`\rm -`             Shape parameter 1 of subhaloes density profile
:option:`gDSPH_SUBS_SHAPE_PARAMS_1`                       :math:`\rm -`             Shape parameter 2 of subhaloes density profile
:option:`gDSPH_SUBS_SHAPE_PARAMS_2`                       :math:`\rm -`             Shape parameter 3 of subhaloes density profile
:option:`gDSPH_SUBS_FLAG_CDELTAMDELTA`                    :math:`\rm -`             Mass-concentration model of subhaloes. See :ref:`rst_enumerators_cdelta` for possible keywords
:option:`gDSPH_SUBS_DPDV_FLAG_PROFILE`                    :math:`\rm -`             :math:`{{\rm d}{\cal P}}/{{\rm d}V}` profile of subhalo distribution in host. See :numref:`rst_enumerators_dm_profiles` for possible keywords
:option:`gDSPH_SUBS_DPDV_SHAPE_PARAMS_0`                  :math:`\rm -`             Shape parameter 1 of :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gDSPH_SUBS_DPDV_SHAPE_PARAMS_1`                  :math:`\rm -`             Shape parameter 2 of :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gDSPH_SUBS_DPDV_SHAPE_PARAMS_2`                  :math:`\rm -`             Shape parameter 3 of :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gDSPH_SUBS_DPDV_RSCALE_TO_RS_HOST`               :math:`\rm -`             Ratio of scale radius of :math:`{\rm d}{\cal P}/{\rm d}V` profile to :math:`r_{\rm s}` of host density profile
:option:`gDSPH_SUBS_DPDM_SLOPE`                           :math:`\rm -`             Slope :math:`\alpha_m` of power-law subhalo mass spectrum :math:`{{\rm d}{\cal P}}/{{\rm d}m}\propto m^{-\alpha_m}`
:option:`gDSPH_SUBS_MASSFRACTION`                         :math:`\rm -`             Fraction of host halo mass bound in subhaloes
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gGALAXY_SUBS_FLAG_PROFILE`                       :math:`\rm -`             Density profile of subhaloes. See :numref:`rst_enumerators_dm_profiles` for possible keywords
:option:`gGALAXY_SUBS_SHAPE_PARAMS_0`                     :math:`\rm -`             Shape parameter 1 of subhaloes density profile
:option:`gGALAXY_SUBS_SHAPE_PARAMS_1`                     :math:`\rm -`             Shape parameter 2 of subhaloes density profile
:option:`gGALAXY_SUBS_SHAPE_PARAMS_2`                     :math:`\rm -`             Shape parameter 3 of subhaloes density profile
:option:`gGALAXY_SUBS_FLAG_CDELTAMDELTA`                  :math:`\rm -`             Mass-concentration model of subhaloes. See :ref:`rst_enumerators_cdelta` for possible keywords
:option:`gGALAXY_SUBS_DPDV_FLAG_PROFILE`                  :math:`\rm -`             :math:`{{\rm d}{\cal P}}/{{\rm d}V}`  profile of subhalo distribution in host. See :numref:`rst_enumerators_dm_profiles` for possible keywords
:option:`gGALAXY_SUBS_DPDV_SHAPE_PARAMS_0`                :math:`\rm -`             Shape parameter 1 of :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gGALAXY_SUBS_DPDV_SHAPE_PARAMS_1`                :math:`\rm -`             Shape parameter 2 of :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gGALAXY_SUBS_DPDV_SHAPE_PARAMS_2`                :math:`\rm -`             Shape parameter 3 of :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gGALAXY_SUBS_DPDV_RSCALE_TO_RS_HOST`             :math:`\rm -`             Ratio of scale radius of :math:`{\rm d}{\cal P}/{\rm d}V` profile to :math:`r_{\rm s}` of host density profile
:option:`gGALAXY_SUBS_DPDM_SLOPE`                         :math:`\rm -`             Slope :math:`\alpha_m` of power-law subhalo mass spectrum :math:`{{\rm d}{\cal P}}/{{\rm d}m}\propto m^{-\alpha_m}`
:option:`gGALAXY_SUBS_MASSFRACTION`                       :math:`\rm -`             Fraction of host halo mass bound in subhaloes
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gCLUSTER_SUBS_FLAG_PROFILE`                      :math:`\rm  -`            Density profile of subhaloes. See :numref:`rst_enumerators_dm_profiles` for possible keywords
:option:`gCLUSTER_SUBS_SHAPE_PARAMS_0`                    :math:`\rm  -`            Shape parameter 1 of subhaloes density profile
:option:`gCLUSTER_SUBS_SHAPE_PARAMS_1`                    :math:`\rm  -`            Shape parameter 2 of subhaloes density profile
:option:`gCLUSTER_SUBS_SHAPE_PARAMS_2`                    :math:`\rm  -`            Shape parameter 3 of subhaloes density profile
:option:`gCLUSTER_SUBS_FLAG_CDELTAMDELTA`                 :math:`\rm  -`            Mass-concentration model of subhaloes. See :ref:`rst_enumerators_cdelta` for possible keywords
:option:`gCLUSTER_SUBS_DPDV_FLAG_PROFILE`                 :math:`\rm  -`            :math:`{{\rm d}{\cal P}}/{{\rm d}V}`  profile of subhalo distribution in host. See :numref:`rst_enumerators_dm_profiles` for possible keywords
:option:`gCLUSTER_SUBS_DPDV_SHAPE_PARAMS_0`               :math:`\rm  -`            Shape parameter 1 of :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gCLUSTER_SUBS_DPDV_SHAPE_PARAMS_1`               :math:`\rm  -`            Shape parameter 2 of :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gCLUSTER_SUBS_DPDV_SHAPE_PARAMS_2`               :math:`\rm  -`            Shape parameter 3 of :math:`{{\rm d}{\cal P}}/{{\rm d}V}` distribution profile
:option:`gCLUSTER_SUBS_DPDV_RSCALE_TO_RS_HOST`            :math:`\rm  -`            Ratio of scale radius of :math:`{\rm d}{\cal P}/{\rm d}V` profile to :math:`r_{\rm s}` of host density profile
:option:`gCLUSTER_SUBS_DPDM_SLOPE`                        :math:`\rm  -`            Slope :math:`\alpha_m` of power-law subhalo mass spectrum :math:`{{\rm d}{\cal P}}/{{\rm d}m}\propto m^{-\alpha_m}`
:option:`gCLUSTER_SUBS_MASSFRACTION`                      :math:`\rm  -`            Fraction of host halo mass bound in subhaloes
========================================================= ========================= ==============================================================================================


Particle physics parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. do not change this headline, it is hardcoded linked with doxygen doc

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gPP_BR`                                          :math:`\rm -`             List of branching channel ratios, see :numref:`rst_branching_ratios`
:option:`gPP_FLAG_SPECTRUMMODEL`                          :math:`\rm -`             Spectrum model. See :ref:`rst_enumerators_spectra` for possible keywords
:option:`gPP_DM_ANNIHIL_DELTA`                            :math:`\rm -`             :math:`\delta=2` for a Majorana, :math:`\delta=4` for a Dirac particle
:option:`gPP_DM_ANNIHIL_SIGMAV_CM3PERS`                   :math:`\rm cm^3/s`        Velocity averaged annihilation cross section
:option:`gPP_DM_DECAY_LIFETIME_S`                         :math:`\rm s`             Decay lifetime
:option:`gPP_DM_IS_ANNIHIL_OR_DECAY`                      :math:`\rm -`             DM model (:option:`False`: Decay, :option:`True`: Annihilation)
:option:`gPP_DM_MASS_GEV`                                 :math:`\rm GeV`           DM particle mass
:option:`gPP_NUMIXING_THETA12_DEG`                        :math:`\rm deg`           Neutrino mixing angle 1, :math:`\theta_{12}`
:option:`gPP_NUMIXING_THETA13_DEG`                        :math:`\rm deg`           Neutrino mixing angle 2, :math:`\theta_{13}`
:option:`gPP_NUMIXING_THETA23_DEG`                        :math:`\rm deg`           Neutrino mixing angle 3, :math:`\theta_{23}`
========================================================= ========================= ==============================================================================================


Statistical analysis of single halo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. do not change this headline, it is hardcoded linked with doxygen doc

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gSTAT_CL`                                        :math:`\rm -`             Confidence level percentage for which to search the min/max
:option:`gSTAT_CL_LIST`                                   :math:`\rm -`             List of x% confidence levels to be drawn
:option:`gSTAT_DATAFILES`                                 :math:`\rm -`             Data on which to superimpose the median + confidence levels
:option:`gSTAT_FILES`                                     :math:`\rm -`             Statistical analysis file or file containing filenames
:option:`gSTAT_ID_LIST`                                   :math:`\rm -`             Identifiers of parameters to plot
:option:`gSTAT_IS_LOGL_OR_CHI2`                           :math:`\rm -`             Analysis file contains log-likelihood (:keyword:`True`) or :math:`\chi^2`-test (:keyword:`False`)
:option:`gSTAT_MODE`                                      :math:`\rm -`             Mode: 0: PDF, 1: :math:`\chi^2`-test, 2: both, 3: mean & dispersion
:option:`gSTAT_N_REALIZATIONS`                            :math:`\rm -`             Number of samples on which mean value is based
:option:`gSTAT_RKPC_FOR_MR`                               :math:`\rm -`             If 1: plot M(rkpc_for_mr) instead of 1st ID parameter
:option:`gSTAT_IS_COMBINE_CHAINS`                         :math:`\rm -`             If true, analyze list of chains together
========================================================= ========================= ==============================================================================================


Simulation parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. do not change this headline, it is hardcoded linked with doxygen doc

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gSIM_ALPHAINT_DEG`                               :math:`\rm deg`           Spatial *J*-factor integration angle :math:`\alpha_{\rm int}` corresponding to the solid angle :math:`\Delta\Omega = 2\pi[1-\cos(\alpha_{\rm int})]`
:option:`gSIM_ALPHAINT_MIN_DEG`                           :math:`\rm deg`           Minimum :math:`\alpha_{\rm int}` value for 1D calculations
:option:`gSIM_ALPHAINT_MAX_DEG`                           :math:`\rm deg`           Maximum :math:`\alpha_{\rm int}` value for 1D calculations
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gSIM_IS_WRITE_FLUXMAPS`                          :math:`\rm -`             If :keyword:`True`, calculate gamma-ray and/or neutrino fluxes. Option for both 1D and 2D calculations
:option:`gSIM_IS_CALC_JVARIANCE`                          :math:`\rm -`             If :keyword:`True`, calculate the J-factor pixel variance. Option only for 2D calculations
:option:`gSIM_FLUX_IS_INTEG_OR_DIFF`                      :math:`\rm -`             Flux either differential (:keyword:`False`) or integrated (:keyword:`True`)
:option:`gSIM_FLUX_AT_E_GEV`                              :math:`\rm GeV`           Energy of differential flux
:option:`gSIM_FLUX_EMIN_GEV`                              :math:`\rm GeV`           Lower limit for integrated flux
:option:`gSIM_FLUX_EMAX_GEV`                              :math:`\rm GeV`           Upper limit for integrated flux
:option:`gSIM_FLUX_FLAG_NUFLAVOUR`                        :math:`\rm -`             Considered neutrino flavour. See :ref:`rst_enumerators_nuflavours` for possible keywords
:option:`gSIM_FLUX_FLAG_FINALSTATE`                       :math:`\rm -`             Considered final state particle.  See :ref:`rst_enumerators_finalstates` for possible keywords
:option:`gSIM_GAUSSBEAM_GAMMA_FWHM_DEG`                   :math:`\rm deg`           FWHM of Gaussian beam of gamma-ray telescope. Value :keyword:`-1` disables smoothing. The smoothed *J*-factor and/or flux maps are saved in their own third :program:`FITS` extension
:option:`gSIM_GAUSSBEAM_NEUTRINO_FWHM_DEG`                :math:`\rm deg`           FWHM of Gaussian beam of neutrino telescope, same as above for a second angular resolution
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gSIM_HEALPIX_FITS_DATATYPE`                      :math:`\rm -`             :program:`HEALPix` datatype (:keyword:`FLOAT32` or :keyword:`FLOAT64`)
:option:`gSIM_HEALPIX_ITER`                               :math:`\rm -`             Number of iterations for :math:`a_{\ell m}` computation. See :program:`HEALPix` documentation for further details.
:option:`gSIM_HEALPIX_NLMAX_FAC`                          :math:`\rm -`             Factor (:math:`\times N_{\rm side}`) = maximum :math:`\ell` of :math:`a_{\ell m}` computation for smoothing. This values must be :math:`\leq 4`. Smaller values will speed up the calculation but introduce artefacts (i.e., result in lower accuracy)
:option:`gSIM_HEALPIX_NSIDE`                              :math:`\rm -`             2D skymaps :program:`HEALPix` resolution :math:`N_{\rm side}`
:option:`gSIM_HEALPIX_RING_WEIGHTS_DIR`                   :math:`\rm -`             Relative or absolute path to the directory containing :program:`HEALPix` ring weights files for smoothing. Set to value :keyword:`-1` to not use any ring weights
:option:`gSIM_HEALPIX_SCHEME`                             :math:`\rm -`             :program:`HEALPix` scheme (:keyword:`RING` or :keyword:`NESTED`)
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gSIM_IS_XLOG`                                    :math:`\rm -`             If :keyword:`True`, evaluate and plot points in x-axis log-scale
:option:`gSIM_NX`                                         :math:`\rm -`             Number of points at which 1D calculation is done
:option:`gSIM_XPOWER`                                     :math:`\rm -`             Multiply dependent variable with power of x-axis value. This can be used, e.g., to draw a spectral energy distribution :math:`E^2\,{\rm d}\Phi/{\rm d}E`
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gSIM_IS_ASTRO_OR_PP_UNITS`                       :math:`\rm -`             Output units :math:`\rm  M_\odot` & :math:`\rm kpc`  (:keyword:`True`) or :math:`\rm  GeV` & :math:`\rm cm`  (:keyword:`False`)
:option:`gSIM_JFACTOR`                                    variable                  Input *J*/*D*-factor for plotting flux spectrum. Unit depends on choice for :option:`gSIM_IS_ASTRO_OR_PP_UNITS`
:option:`gSIM_JFRACTION`                                  :math:`\rm -`             Fraction of total spatially extended *J*-factor
:option:`gSIM_PHI_CUT_DEG`                                :math:`\rm deg`           Do not show/print haloes with :math:`\phi` (w.r.t. Galactic centre) < :option:`gSIM_PHI_CUT_DEG` (if list of haloes analysed)
:option:`gSIM_R_MIN`                                      :math:`\rm kpc`           Minimum radial coordinate for 1D profile
:option:`gSIM_R_MAX`                                      :math:`\rm kpc`           Maximum radial coordinate for 1D profile
:option:`gSIM_REDSHIFT`                                   :math:`\rm -`             Redshift :math:`z` of object(s) in the calculation
:option:`gSIM_SORT_CONTRAST_THRESH`                       :math:`\rm -`             Only print/sort haloes with fraction J_halo/J_Gal > :option:`gSIM_SORT_CONTRAST_THRESH` (if list of haloes analysed)
:option:`gSIM_THETA_MIN_DEG`                              :math:`\rm deg`           Minimum :math:`\theta` value for 1D :math:`{\rm d}J/{\rm \Omega}` or ntensity profiles
:option:`gSIM_THETA_MAX_DEG`                              :math:`\rm deg`           Maximum :math:`\theta` value for 1D :math:`{\rm d}J/{\rm \Omega}` or intensity profiles
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gSIM_PSI_OBS_DEG`                                :math:`\rm deg`           Galactic longitude coordinate of 2D field of view center
:option:`gSIM_THETA_OBS_DEG`                              :math:`\rm deg`           Galactic latitude coordinate of 2D field of view center
:option:`gSIM_THETA_ORTH_SIZE_DEG`                        :math:`\rm deg`           2D field of view diameter in direction orthogonal to the latitudinal extension of the field of view
:option:`gSIM_THETA_SIZE_DEG`                             :math:`\rm deg`           2D field of view diameter in latitudinal direction
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gSIM_EXTRAGAL_EBL_UNCERTAINTY`                   :math:`\rm -`             Systematic uncertainty on EBL optical depth :math:`\tau`
:option:`gSIM_EXTRAGAL_FLAG_GROWTHFACTOR`                 :math:`\rm -`             Method to compute perturbation growth factor :math:`g(z)`, Eq. :eq:`eq_growthfactor`. See :ref:`rst_enumerators_growthfactor` for possible keywords
:option:`gSIM_EXTRAGAL_FLAG_WINDOWFUNC`                   :math:`\rm -`             Spherical collapse model window function. See :ref:`rst_enumerators_windowfunc` for possible keywords
:option:`gSIM_EXTRAGAL_KMAX_PRECOMP`                      :math:`h\;\rm Mpc^{-1}`   :math:`k_{\rm max}/h` of computed CDM power spectrum
:option:`gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE`         :math:`\rm -`             If :keyword:`True`, translate halo mass function between different overdensity factors :math:`\Delta` using a defined halo profile shape (Einasto, NFW,...)
:option:`gSIM_EXTRAGAL_MF_SIGMA_CUTOFF`                   :math:`\rm -`             Smoothness of the mass function cut-off. Smoothing is done by a log-sigmoid window
:option:`gSIM_EXTRAGAL_MMIN`                              :math:`\rm M_\odot`       Minimum mass for extragalactic mass function
:option:`gSIM_EXTRAGAL_MMAX`                              :math:`\rm M_\odot`       Maximum mass for extragalactic mass function
:option:`gSIM_EXTRAGAL_NM`                                :math:`\rm -`             Number of mass points to be evaluated for 2D analyses in :math:`(M, z)` plane
:option:`gSIM_EXTRAGAL_IS_MLOG`                           :math:`\rm -`             If :keyword:`True`, select logarithmic mass grid for 2D analyses in :math:`(M, z)` plane
:option:`gSIM_EXTRAGAL_NM_PRECOMP`                        :math:`\rm -`             Resolution of logarithmic mass nodes for precalculated integration grid (number :math:`n_m` of nodes)
:option:`gSIM_EXTRAGAL_ZMIN`                              :math:`\rm -`             Minimum redshift for grid for 2D analyses in :math:`(M, z)` plane
:option:`gSIM_EXTRAGAL_ZMAX`                              :math:`\rm -`             Maximum redshift for grid for 2D analyses in :math:`(M, z)` plane
:option:`gSIM_EXTRAGAL_NZ`                                :math:`\rm -`             Number of redshift points in grid for 2D analyses in :math:`(M, z)` plane
:option:`gSIM_EXTRAGAL_IS_ZLOG`                           :math:`\rm -`             If :keyword:`True`, select logarithmic redshift grid for 2D analyses in :math:`(M, z)` plane
:option:`gSIM_EXTRAGAL_DELTAZ_PRECOMP`                    :math:`\rm -`             Resolution of linear redshift nodes for precalculated integration grid (:math:`\Delta z` between nodes)
========================================================= ========================= ==============================================================================================

========================================================= ========================= ==============================================================================================
**Parameter**                                             **Unit**                  **Comment**
--------------------------------------------------------- ------------------------- ----------------------------------------------------------------------------------------------
:option:`gSIM_EPS`                                        :math:`\rm -`             Numeric precision of the simulation
:option:`gSIM_EPS_DRAWN`                                  :math:`\rm -`             Numeric precision of the drawn clumps (can be decreased separately, to significantly speed up calculations)
:option:`gSIM_IS_WRITE_GALPOWERSPECTRUM`                  :math:`\rm -`             If :keyword:`True`, write angular power spectrum of DM skymap. Find :download:`here <DocData/plotClumpyAPS.ipynb>` an example Jupyter-notebook of how to display the APS output.
:option:`gSIM_IS_WRITE_ROOTFILES`                         :math:`\rm -`             If :keyword:`True`, write output additionally in :program:`ROOT` format
:option:`gSIM_OUTPUT_DIR`                                 :math:`\rm -`             Relative or absolute path where the output files are stored. For the value :keyword:`-1`, files will be stored in the current directory
:option:`gSIM_SEED`                                       :math:`\rm -`             Random generator seed. If set to zero, seed is chosen from computer clock 
:option:`gSIM_USER_RSE`                                   :math:`\rm -`             Contrast level (in percent) above which substructures are resolved in 2D calculations. Key quantity of :program:`CLUMPY`, see the `first CLUMPY paper <http://cdsads.u-strasbg.fr/abs//2012CoPhC.183..656C>`__ and :ref:`rst_subsec_average`
========================================================= ========================= ==============================================================================================
