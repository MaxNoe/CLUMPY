.. _rst_physics:

Physics and equations
=====================

Please select one of the following sub-topics:

.. toctree::
   :maxdepth: 1

   physics_definitions
   physics_geometry
   physics_integr_los
   physics_spectra
   physics_profiles
   physics_clumps_generic
   physics_cosmo
   physics_jeans
   physics_2Dmaps
   physics_instruments
   physics_1Dtoflux
