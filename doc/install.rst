.. _rst_install:

.. role::  raw-html(raw)
    :format: html

Download and Installation
==========================

The :program:`CLUMPY` package is written in C/C++ (but no classes) for UNIX systems (Linux, Mac OSX) and is interfaced with several third-party softwares. Some are mandatory, other optional to be installed before proceeding to the :program:`CLUMPY` installation.

.. warning ::

        Currently, :program:`CLUMPY` does not compile on newest Mac OSX systems with M1/M2 chips. We are working on fixing this issue.

Third-party softwares
---------------------

* To install **(mandatory)**

   1. `GSL (Gnu scientific library) <http://www.gnu.org/software/gsl>`__: we recommend installing the package for your distribution with a package manager (or, if needed, install it locally). 
   
    .. note :: You have to install the development files ``gsl-devel`` for the header files to include.

   2. `CFITSIO (handle FITS files) <http://heasarc.gsfc.nasa.gov/fitsio/>`__:  we recommend installing the package for your distribution with a package manager. If it does not exist for your system, `download it <http://heasarc.gsfc.nasa.gov/fitsio/>`__ and install it by hand (see the :ref:`rst_troubleshooting` section if you face problems with a manual :program:`CFITSIO` installation). 
   
    .. note :: You can use a local :program:`CFITSIO` installation outside the common system include and library directories. In this case, you have to set the environmental variables :envvar:`CFITSIO_LIB_DIR` and :envvar:`CFITSIO_INC_DIR` pointing to your :program:`CFITSIO` library/include file directories to make :program:`cmake` finding it. Also, you may add :envvar:`CFITSIO_LIB_DIR` to your :envvar:`LD_LIBRARY_PATH`: 

      .. code-block:: console

        $ export LD_LIBRARY_PATH=$CFITSIO_LIB_DIR:$LD_LIBRARY_PATH
        $ export DYLD_LIBRARY_PATH=$CFITSIO_LIB_DIR:$DYLD_LIBRARY_PATH  *[Mac OSX only]*

* Shipped with the code **(no need to install!)**

   * `HEALPix (Hierarchical Equal Area isoLatitude Pixelisation) <https://healpix.jpl.nasa.gov/>`__: a frozen version of this library (version 3.31) is shipped with the code, and internally built at compilation, so you no longer have to worry about how to install it.

* Optional packages

   1. `ROOT CERN library <https://root.cern.ch/>`__ for displays and random drawing clumps from multivariate distributions. The easiest way to install it on your system is via your package manager (*apt-get*, *dnf*, or *brew*, etc.), and do not forget the development (:option:`-devel`) packages. Alternatively, you may want to compile locally :program:`ROOT`. To do so, `download a version <https://root.cern.ch/releases>`_ and follow the configuration/compilation instructions to `build ROOT <https://root.cern.ch/building-root>`_. In addition, in the latter case, do not forget to define the :envvar:`ROOTSYS` environment variable, e.g., in your :file:`~/.bashrc`).

   2. `GreAT (Grenoble Analysis Toolkit) <https://gitlab.in2p3.fr/derome/GreAT>`__: if you want to run MCMC/Jeans analyses, currenty, you have to install (compile) the GreAT software, to be downloaded from the `GreAT repository on GitLab <https://gitlab.in2p3.fr/derome/GreAT>`__. 

      .. warning :: GreAT is not actively developed anymore. Even if you follow installation instructions in the GreAT repository, compilation may not be possible on newer systems. To compile GreAT with c++17 compilers, please see the :ref:`rst_troubleshooting` section.

      After having compiled GreAT, before configuring :program:`CLUMPY` with :program:`cmake`, you have to set the environmental variable :envvar:`GREAT` and do (e.g., in your :file:`~/.bashrc`):

      .. code-block:: console

        $ export LD_LIBRARY_PATH=$GREAT/lib:$LD_LIBRARY_PATH
        $ export DYLD_LIBRARY_PATH=$GREAT/lib:$DYLD_LIBRARY_PATH  *[Mac OSX only]*

      .. note :: :program:`GreAT` iself depends on :program:`ROOT`. By this, linking :program:`CLUMPY` against :program:`GreAT` makes the :program:`CLUMPY` compilation also dependent on :program:`ROOT`. 

   3. `Doxygen <http://www.stack.nl/~dimitri/doxygen/>`__: useful for developers if you want to generate locally the code documentation. Otherwise, we already provide it `online <doxygen/index.html#http://>`__.

   4. `CLASS <http://www.class-code.net>`__ (*version 3 onwards required*): needed for the computation of the linear matter power spectrum. In the directory ``data/pk_precomp``, we already ship a variety of power spectra for different cosmological parameters and a large range in :math:`k`. For further user-demands, the environmental variable :envvar:`CLASS` must point to main directory of a local :program:`CLASS` installation, and :program:`CLASS` is run from inside :program:`CLUMPY`. Computed power spectra are saved in ``$CLUMPY/data/pk_precomp``.

.. note:: For :program:`Mac OSX`, we recommend to install :program:`GSL`, :program:`CFITSIO`, and optionally :program:`ROOT` via :raw-html:`<a href=http://brew.sh/ target=_blank>homebrew</a>`. To define the environment variable :envvar:`ROOTSYS`,  you need to source, e.g., in your configuration file :file:`~/.bashrc`,  :file:`thisroot.sh`/:file:`thisroot.csh`. To know where it is, type, e.g. for :program:`ROOT 6`, ``brew info root6``.

Installation
------------

   1. Clone from git repository (if you seek for an old code release, see :ref:`rst_release_history`):

      .. code-block:: console

         $ git clone https://gitlab.com/clumpy/CLUMPY.git --depth=1 --branch release3.1

      or see the `GitLab repository <https://gitlab.com/clumpy/CLUMPY>`__ for alternative ways to download the code. :raw-html:`<br><br>`

   2. The compilation relies on `cmake <https://cmake.org/>`_ (file :file:`CmakeLists.txt`):

      .. code-block:: console

        $ cd CLUMPY
        $ mkdir build; cd build
        $ cmake ../
        $ make -jN   *[using N=2, 3,... cores]*

      .. note:: :program:`CLUMPY` requires :program:`cmake` :math:`\geq` 3.2. If you need to install :program:`cmake` itself and do not have access to a C++2011 compiler, :raw-html:`<a href=http://gitlab.kitware.com/cmake/cmake/tree/v3.2.3 target=_blank>cmake 3.2.3</a>` does the job.

   3. Define the :program:`CLUMPY` environment variables (e.g., in your :file:`~/.bashrc`):

      .. code-block:: console

        $ export CLUMPY=absolute_path_to_local_installation
        $ export PATH=$CLUMPY/bin:$PATH
        $ export LD_LIBRARY_PATH=$CLUMPY/lib:$LD_LIBRARY_PATH
        $ export DYLD_LIBRARY_PATH=$CLUMPY/lib:$DYLD_LIBRARY_PATH  *[Mac OSX only]*

      .. note:: Do not forget to ``$ source ~/.bashrc`` (or whereever your variables are defined) to ensure that the environment variables are set in your current xterm (``$ echo $CLUMPY`` should point to the directory where you installed :program:`CLUMPY`).

      You should now be able to run the code from anywhere just typing

      .. code-block:: console

        $ clumpy


   4. Test installation: we now provide an automated test suite to check the proper output of all modules after the installation. After having set up correctly all environmental variables, just type

      .. code-block:: console

        $ clumpy_tests

      .. note:: To pass all the tests, the environmental variable :envvar:`CLUMPY` must be set.


   This verson was succesfully installed and tested on:
      
            - Ubuntu 14.04 (LTS, gcc 4.8.4) and 17.10 (c++ 7.2)
            - MacOS X High Sierra (10.13.4, clang-902.0.39.2) and Capitan
            - Scientific Linux 6.9 (Carbon, gcc 4.4.7)
            - Fedora 25 (c++ 6.4.1)

   Alternatively, you can also use the preinstalled code version in the :program:`Docker` image `hub.docker.com/r/zimmerst85/clumpy <https://hub.docker.com/r/zimmerst85/clumpy>`__, kindly provided and maintained by Stephan Zimmer (University of Innsbruck).

Files and directories
---------------------

**Files in** ``$CLUMPY/``

+----------------------------------------------+--------------------------------------------------------------------------------------------------------+
| :file:`CmakeLists.txt`                       | File used by cmake for compilation                                                                     |
+----------------------------------------------+--------------------------------------------------------------------------------------------------------+
| :file:`FindROOT.cmake`                       | Called by CmakeLists.txt to find path to :program:`ROOT` installation                                  |
+----------------------------------------------+--------------------------------------------------------------------------------------------------------+
| :file:`README.md`                            | README file (for GitLab website)                                                                       |
+----------------------------------------------+--------------------------------------------------------------------------------------------------------+
| :file:`LICENSE`                              | A copy of the GNU General Public License v2 under which :program:`CLUMPY` is licensed                  |
+----------------------------------------------+--------------------------------------------------------------------------------------------------------+

**Directories in** ``$CLUMPY/``

.. <http://warp.povusers.org/FunctionParser/fparser.html>`__

+---------------------------+-------------------------------------------------------------+
| ``data/``                 | Tabulated files (EBL, :math:`P_{\rm lin}(k)`, CLUMPY files) |
+---------------------------+-------------------------------------------------------------+
| ``doc/``                  | This documentation                                          |
+---------------------------+-------------------------------------------------------------+
| ``Healpix3.31ForClumpy/`` | Frozen version of Healpix3.31                               |
+---------------------------+-------------------------------------------------------------+
| ``include/*.h``           | :program:`CLUMPY` headers                                   |
+---------------------------+-------------------------------------------------------------+
| ``python_helperscripts``  | Auxiliary :program:`Python` scripts                         |
+---------------------------+-------------------------------------------------------------+
| ``src/*.cc``              | :program:`CLUMPY` sources                                   |
+---------------------------+-------------------------------------------------------------+

**…and created at compilation time:** 

+---------------------------+-------------------------------------------+
| ``bin/``                  | :program:`CLUMPY` executables             |
+---------------------------+-------------------------------------------+
| ``lib/``                  | :program:`CLUMPY` library                 |
+---------------------------+-------------------------------------------+

**Example data files in** ``$CLUMPY/data``

+------------------------------------+----------------------------------------------------------+
| ``data/list_generic.txt``          | List of generic spherical DM haloes (option :option:`-h`)|
+------------------------------------+----------------------------------------------------------+
| ``data/list_generic_triaxial.txt`` | List of generic triaxial DM haloes (option :option:`-h`) |
+------------------------------------+----------------------------------------------------------+
| ``data/stat_example.dat``          | Example of statistical-like file (option :option:`-s`)   |
+------------------------------------+----------------------------------------------------------+

+------------------------------------+-------------------------------------------------+
| ``data/list_generic_jeans.txt``    | List of DM haloes for Jeans-related quantities  |
+------------------------------------+-------------------------------------------------+
| ``data/params_jeans.txt``          | User-defined parameters for Jeans analysis      |
+------------------------------------+-------------------------------------------------+
| ``data/data_light.txt``            | Sample of simulated surface brightness data     |
+------------------------------------+-------------------------------------------------+
| ``data/data_sigmap.txt``           | Sample of simulated velocity dispersion data    |
+------------------------------------+-------------------------------------------------+
| ``data/data_vel.txt``              | Sample of simulated line-of-sight velocities    |
+------------------------------------+-------------------------------------------------+

**Auxiliary data files in** ``$CLUMPY/data`` **(do not change)** [#f1]_

+------------------------------------+-------------------------------------------------------------------------+
| ``data/EBL/``                      | Tabulated optical depths :math:`\tau`  (option :option:`-e`)            |
+------------------------------------+-------------------------------------------------------------------------+
| ``data/healpix/``                  | Pixel windows and ring weights for spherical harmonic transformation    |
+------------------------------------+-------------------------------------------------------------------------+
| ``data/pk_precomp/``               | Pre-computed :math:`P_{\rm lin}(k)` for cosmology (option :option:`-e`) |
+------------------------------------+-------------------------------------------------------------------------+
| ``data/PPPC4DMID-spectra/``        | Marco Cirelli's particle physics spectra                                |
+------------------------------------+-------------------------------------------------------------------------+

+------------------------------------+-------------------------------------------------------------------------+
| ``data/clumpy_tests/``             | Reference files (for :math:`\tt clumpy\_tests`)                         |
+------------------------------------+-------------------------------------------------------------------------+

.. [#f1] You may add your own files to ``data/pk_precomp/``, though.

.. _rst_troubleshooting:

Troubleshooting
---------------------

- For a clean recompilation of :program:`CLUMPY`, do

      .. code-block:: console

        $ cd CLUMPY/Healpix3.31ForClumpy/
        $ make clean

  This is in particular necessary if you link :program:`CLUMPY` against another :program:`CFITSIO` instance, and if you consequently obtain a warning of mismatching :program:`CFITSIO` versions at run time.
  
|
  
- Some problems can occur at the execution of the code with Mac OSX 10.8.5 if using a :program:`ROOT` version more recent than 5.34.10 (e.g., issues with TF3 with option :option:`-g7`) Please use :program:`ROOT` 5.34.10 or try to update your OS.
  
|

- If you experience the error

  .. code-block:: console
  
     ../lib/libCLPY.so: undefined reference to `curl_global_init'
     ../lib/libCLPY.so: undefined reference to `curl_easy_cleanup'
     ../lib/libCLPY.so: undefined reference to `curl_global_cleanup'
     
     ...
     
  this might be due to the fact that you use a manual installation of :program:`CFITSIO` version > 3.42 and do not have :program:`libcurl` installed on your system. Either (i) use a  :program:`CFITSIO` version <= 3.42 (see `here <https://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/>`__), (ii) install the :program:`libcurl` library (see `here <https://curl.haxx.se/download.html>`__) or (iii) install :program:`CFITSIO` via a package manager, which we recommend and which takes care of all dependencies.

- To compile GreAT with c++17 compilers, please replace in the GreAT ``CMakeList.txt`` (not in :program:`CLUMPY`'s one!) the following lines

      .. code-block:: console

        # Check for C++14 support
        include(CheckCXXCompilerFlag)
        check_cxx_compiler_flag("-std=c++14" COMPILER_SUPPORTS_CXX14)
        if(COMPILER_SUPPORTS_CXX14)
           set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
        else()
           message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++14 support needed for ROOT 6. Please use a different C++ compiler or an older ROOT version.")

  by
  
      .. code-block:: console

        # Check for C++17 support
        include(CheckCXXCompilerFlag)
        check_cxx_compiler_flag("-std=c++17" COMPILER_SUPPORTS_CXX17)
        if(COMPILER_SUPPORTS_CXX17)
           set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")
        else()
           message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++17 support needed for ROOT 6. Please use a different C++ compiler or an older ROOT version.")

