.. _rst_doc_modules:

clumpy executable: options and plots
-------------------------------------------------

The main executable :math:`\tt clumpy` contains the seven modules:

.. toctree::
   :maxdepth: 1

   doc_module_g
   doc_module_h
   doc_module_e
   doc_module_s
   doc_module_o
   doc_module_z
   doc_module_f

They are executed by specifying the module + index right after the executable, and then parsing any input parameters afterwards. Input parameters can be gathered in a parameter file called by :option:`-i` or :option:`--infile`; a default parameter file is generated with the :option:`-D` flag. Input parameters provided in the command line are preferred over the same parameter specified in a file. E.g.

.. code-block:: console

       $ clumpy -g1 -i params.txt --gMW_SUBS_DPDM_SLOPE=2.0


The output of these commands are interactive :program:`ROOT` displays (:program:`ROOT` canvas and objects can be straightforwardly saved in many formats, e.g., eps, jpeg, etc.), ASCII, :program:`ROOT`, or :program:`FITS` files.


.. note:: You can prevent :program:`CLUMPY` to write anything to disk with the :option:`-d` flag, e.g.,

	.. code-block:: console

         $ clumpy -g1 -d -i params.txt

    For running the code in an automated pipeline, use the :option:`-p` flag. E.g.,

	.. code-block:: console

         $ clumpy -g1 -p -i params.txt

    properly exits the code without starting the interactive ROOTAPP. If you want to avoid any :program:`ROOT` canvas to be opened (e..g, executing the code on a batch farm), you may additionally choose :option:`gSIM_IS_WRITE_ROOTFILES = False`,

	.. code-block:: console

         $ clumpy -g1 -p -i params.txt --gSIM_IS_WRITE_ROOTFILES=False

