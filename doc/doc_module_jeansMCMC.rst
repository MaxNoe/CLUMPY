.. _rst_doc_module_jeansMCMC:

executable :math:`\tt jeansMCMC`
--------------------------------

Jeans analysis with GreAT MCMC engine (only if GreAT is installed, see :ref:`rst_install`).

For example, to run MCMCs analysis on :math:`\sigma_{\rm p}` data from :file:`$CLUMPY/data/data_sigmap.txt`, execute:

.. code-block:: console

    $ clumpy_jeansMCMC -r $CLUMPY/data/data_sigmap.txt output/stat_example.root output/stat_exampleAna.root 10000 8 $CLUMPY/data/params_jeans.txt 0.05

.. note:: The :option:`-p` flag can be used as optional second option to suppress the display of the :program:`ROOT` output pop-up figure. This may be needed to run the executable on a computing cluster:

   .. code-block:: console
   
     $ clumpy_jeansMCMC -r -p ...

This analysis executes eight chains with 10000 trial points each, using a numeric accuracy of 5%. The output figure is:

.. figure:: DocImages/jeansMCMC_rD.png
   :align: center
   :figclass: align-center
   :scale: 65%


This figure can also be later visualised with the :option:`-s2` module of the :math:`\tt clumpy` executable (see :numref:`rst_doc_module_s`). 

The result of the analysis can then be further processed with the functions of the statistical module according to :numref:`rst_doc_module_s`. For example:

.. code-block:: console

   $ clumpy -s8 -D
   $ clumpy -s8 -i clumpy_params_s8.txt --gSTAT_FILES=output/stat_example.dat

.. figure:: DocImages/jeansMCMC_rD-a.png
   :align: center
   :figclass: align-center
   :scale: 54%

   MCMC results  output :program:`ROOT` figures processed with :option:`-s8` option
   
.. note:: While :math:`\tt clumpy\_jeansMCMC` allows to process several chains sequencially, it is recommended to process several chains in parallel and to afterwards combine the output :file:`.dat` files manually. This also allows for a manual sanity check of each chain using the :option:`-s2` module before combination.
