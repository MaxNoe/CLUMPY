.. _rst_doc_module_jeansChi2:

executable :math:`\tt jeansChi2`
--------------------------------

Jeans analysis with :math:`\chi^2` minimisation.


:math:`\tt -r1`: binned analysis
+++++++++++++++++++++++++++++++++



For example, to run a binned Jeans analysis on :math:`\sigma_{\rm p}` data from :file:`$CLUMPY/data/data_sigmap.txt`, execute:

.. code-block:: console

    $ clumpy_jeansChi2 -r1 $CLUMPY/data/data_sigmap.txt output/chi2_binned.dat $CLUMPY/data/params_jeans.txt 0.05

using a numeric accuracy of 5%:

.. code-block:: console

    >>>>> Load $CLUMPY/data/params_jeans.txt
    ======== Binned Jeans analysis ========
     FLOATING-POINT NUMBERS ASSUMED ACCURATE TO 1e-08
     FCN=1.12954e-07 FROM MIGRAD    STATUS=FAILED        247 CALLS         248 TOTAL
                         EDM=2.56309e-06    STRATEGY= 1      ERR MATRIX NOT POS-DEF
      EXT PARAMETER                APPROXIMATE        STEP         FIRST
      NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE
       1  log10(rho_s)   8.80950e+00   2.07851e+00   0.00000e+00  -7.50619e-03
       2  log10(r_s)  -1.18420e+00   1.48070e+00   0.00000e+00  -3.42326e-03
       3  alpha        2.13991e-01   4.80224e-01  -0.00000e+00   3.10416e-04
       4  beta         3.00000e+00     fixed
       5  gamma        1.00000e+00     fixed
       6  beta_0       5.62379e-01   1.49796e+00   0.00000e+00  -3.39263e-03
       7  beta_infinity   0.00000e+00     fixed
       8  log10(r_a)   0.00000e+00     fixed
       9  eta          2.00000e+00     fixed
      10  DM_profile   0.00000e+00     fixed
      11  Rvir         1.00000e+03     fixed
      12  rho_s*       1.00000e+00     fixed
      13  r_s*         2.10000e-02     fixed
      14  alpha*       1.00000e+00     fixed
      15  beta*        3.00000e+00     fixed
      16  gamma*       0.00000e+00     fixed
      17  Light_profile   3.00000e+00     fixed
      18  R            1.00000e+02     fixed
      19  eps          5.00000e-02     fixed
      20  Aniso_profile   0.00000e+00     fixed
    Statistical file saved in output/output.dat


The result, e.g., the best-fit value found for :math:`\sigma_{\rm p}`, can be visualised with the :option:`-s8` module of the :math:`\tt clumpy` executable (see :numref:`rst_doc_module_s`):

.. code-block:: console

   $ clumpy -s8 -D
   $ clumpy -s8 -i clumpy_params_s8.txt --gSTAT_FILES=output/chi2_binned.dat

.. figure:: DocImages/jeansChi2_r1D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :option:`-r1`  output :program:`ROOT` figure


------------

:math:`\tt -r2`: unbinned bootstrap
+++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy_jeansChi2 -r2 $CLUMPY/data/data_vel.txt output/stat_bootstrap.dat $CLUMPY/data/params_jeans.txt 0.05 100

performs an Bootstrap (100 samples) unbinned Jeans analysis of the velocity data :file:`CLUMPY/data/data_vel.txt`.

.. note :: Above example takes about 20 minutes to run.

The result can be again  visualised with the :option:`-s8` module of the :math:`\tt clumpy` executable (see :numref:`rst_doc_module_s`):

.. code-block:: console

   $ clumpy -s8 -i clumpy_params_s8.txt --gSTAT_FILES=output/stat_bootstrap.dat

.. figure:: DocImages/jeansChi2_r2D.png
   :align: center
   :figclass: align-center
   :scale: 54%

   :option:`-r2`  output :program:`ROOT` figures