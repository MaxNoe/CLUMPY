.. _rst_doc_module_f:

.. role::  raw-html(raw)
    :format: html

Add flux extensions: :math:`\tt -f`  
------------------------------------

Appends an extension containing

- a :math:`\gamma`-ray map with the flux per pixel,
- a :math:`\gamma`-ray intensity map,
- a  map with the neutrino flux per pixel,
- a neutrino intensity map,

to an existing :program:`FITS` file :file:`inputoutputfile.fits` containing a *J*-factor map:

.. code-block:: console

    $ clumpy -f -D
    $ clumpy -f -i clumpy_params_f.txt -r inputoutputfile.fits
    
.. note:: 

   - The parameters required in :file:`clumpy_params_f.txt` (or to be given via the command line) depend on the content of :file:`inputoutputfile.fits` (e.g., whether it is a *D*-factor map from decay or a *J*-factor map from annihilation, or whether it is an extragalactic halo at  :math:`z>0` for which a :math:`\gamma`-ray EBL absorption model has to be given). After loading :file:`inputoutputfile.fits`, :program:`CLUMPY` tells to the user which information it is missing. :raw-html:`<br><br>`

   - :program:`CLUMPY` does not provide a mechanism to again delete :program:`FITS` extensions once added. You can however easily use `fv (fits viewer) <http://heasarc.gsfc.nasa.gov/ftools/fv/>`__ to again remove extensions added with the :option:`-f` option.