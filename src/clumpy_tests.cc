// C++ std libraries
using namespace std;
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <iostream>
#include <vector>

// Clumpy includes
#include "../include/clumps.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/inlines.h"

// ROOT includes
#if IS_ROOT
#include <TApplication.h>
#include <TRandom.h>
#include <TFile.h>
#endif

//______________________________________________________________________________
bool stref17_tests(string test_dir, string compare_dir)
{
   //--- Tests of all functions related to Stref17 (all functions/variables start with 'stref17_')

   bool is_passed;
   string output_passed = COLOR_BGREEN "OK" COLOR_RESET;
   string output_not_passed = COLOR_BRED "not passed" COLOR_RESET;
   cout << " Testing SL17 implementation ..." << endl;

   string file_name = "stref17_tests.output";
   FILE *fp[1] = {NULL};
   string testfile = test_dir + "/" + file_name;
   fp[0] = fopen(testfile.c_str(), "w");


   // Check reading table of cmin
   string filename = gPATH_TO_CLUMPY_HOME + "data/subhalos_Stref17/cmin_nfw_eps1.dat";
   stref17_load_cmin200_vs_rgal(filename);
   // Print read values
   //for (int i=0; i< (int)stref17_c200_min.size(); ++i)
   //   printf("%lf    %lf\n", i, stref17_rgal_kpc[i], stref17_c200_min[i]);
   for (int i = 0; i < (int)gMW_SUBS_TABULATED_CMIN_OF_R_GRID.size(); i += (int)gMW_SUBS_TABULATED_CMIN_OF_R_GRID.size() - 1)
      fprintf(fp[0], "i=%d   rgal=%lf [kpc]   stref17_c200_min=%lf [-]\n", i, gMW_SUBS_TABULATED_R_OF_CMIN_GRID[i], gMW_SUBS_TABULATED_CMIN_OF_R_GRID[i]);
   // Check interpolation
   double r = 3.;
   int i_r = binary_search(gMW_SUBS_TABULATED_R_OF_CMIN_GRID, r);
   fprintf(fp[0], " Interp1D: stref17_c200_min(%f kpc)=%f/%f/%f/%f [kLINLIN,kLINLOG/kLOGLIN/kLOGLOG] -> between %f(@r=%f) and %f(@r=%f)\n\n", r,
           interp1D(r, gMW_SUBS_TABULATED_R_OF_CMIN_GRID, gMW_SUBS_TABULATED_CMIN_OF_R_GRID, kLINLIN), interp1D(r, gMW_SUBS_TABULATED_R_OF_CMIN_GRID, gMW_SUBS_TABULATED_CMIN_OF_R_GRID, kLINLOG),
           interp1D(r, gMW_SUBS_TABULATED_R_OF_CMIN_GRID, gMW_SUBS_TABULATED_CMIN_OF_R_GRID, kLOGLIN), interp1D(r, gMW_SUBS_TABULATED_R_OF_CMIN_GRID, gMW_SUBS_TABULATED_CMIN_OF_R_GRID, kLOGLOG),
           gMW_SUBS_TABULATED_CMIN_OF_R_GRID[i_r], gMW_SUBS_TABULATED_R_OF_CMIN_GRID[i_r], gMW_SUBS_TABULATED_CMIN_OF_R_GRID[i_r + 1], gMW_SUBS_TABULATED_R_OF_CMIN_GRID[i_r + 1]);


   // Check reading table of rt/rs
   filename = gPATH_TO_CLUMPY_HOME + "data/subhalos_Stref17/rt_over_rs_nfw.dat";
   stref17_load_rt2rs_vs_rgal_c200(filename);
   // Print read values
   // -> stref17_c200 values
   fprintf(fp[0], "stref17_c200 [-]: \n");
   for (int i = 0; i < (int)gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID.size(); ++i)
      fprintf(fp[0], "%lf\n", gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID[i]);
   fprintf(fp[0], "\n");
   // -> rt/rs values
   for (int j = 0; j < (int)gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID.size(); j += (int)gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID.size() - 1) {
      fprintf(fp[0], "%lf\n", gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID[j]);
      for (int i = 0; i < (int)gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID.size(); ++i)
         fprintf(fp[0], "%le\n", gMW_SUBS_TABULATED_RTIDAL_TO_RS_GRID[j][i]);
      fprintf(fp[0], "\n");
   }
   // Check 2D interpolation
   r = 3.;
   i_r = binary_search(gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID, r);
   double c = 10.;
   int i_c = binary_search(gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID, c);
   fprintf(fp[0], " Interp2D: rt/rs(r=%f kpc,c=%f)=%f [kLOGLOG] -> between %f(@r=%f,c=%f) and %f(@r=%f,c=%f)\n", r, c,
           interp2D(r, c, gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID, gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID, gMW_SUBS_TABULATED_RTIDAL_TO_RS_GRID, kLOGLOG), gMW_SUBS_TABULATED_RTIDAL_TO_RS_GRID[i_r][i_c], gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID[i_r], gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID[i_c],
           gMW_SUBS_TABULATED_RTIDAL_TO_RS_GRID[i_r + 1][i_c + 1], gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID[i_r + 1], gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID[i_c + 1]);


   // Check reading table of lcrit:
   filename = gPATH_TO_CLUMPY_HOME + "data/subhalos_Stref17/lcrit_tables/l_crit_core_eps1_rse5e-2.dat";
   vector <double> stref17_log10m1, stref17_log10m2, stref17_lcrit;
   stref17_load_lcrit(filename, stref17_log10m1, stref17_log10m2, stref17_lcrit);
   // Print read values
   fprintf(fp[0], "i    log10(m_i/1Msol)    l_crit [kpc]\n");
   for (int i = 0; i < (int)stref17_log10m1.size(); i += 1)
      fprintf(fp[0], "%d   %.1f : %.1f    %le\n", i, stref17_log10m1[i], stref17_log10m2[i], stref17_lcrit[i]);
   fclose(fp[0]);

   string command = "diff " + test_dir + "/" + file_name + " " + compare_dir + "/" + file_name;
   string diffstring = sys_command(command);

   if (diffstring == "") {
      command = "rm " + test_dir + "/" + file_name;
      sys_safe_execution(command);
      is_passed = true;
   } else {
      is_passed = false;
   }

   if (is_passed) {
      cout <<  output_passed << endl;
   } else {
      cout <<  output_not_passed << endl;
   }
   return is_passed;
}

//______________________________________________________________________________
//int main(int argc, char *argv []){
int main(int argc, char *argv [])
{

   argc = 1.;

   vector< vector<string> > filenames_test;
   vector<string>  filenames_tmp;
   string nulloutput = " > nul; ";
   string command;
   vector<string> special_commands;
   vector<string> functions_tested;
   vector<bool> functions_tested_results;


   // get location of clumpy executable:
   // N.B.: We are testing the clumpy executable located in the same directory as the executed clumpy_tests
   get_clumpy_dirs(string(argv[0]));

   string test_dir = gPATH_TO_CLUMPY_HOME + "clumpy_test_tmp/";
   string compare_dir = gPATH_TO_CLUMPY_HOME + "data/clumpy_tests/";


   // create clumpy_test_tmp directory
   command = "mkdir -p " + test_dir;
   sys_safe_execution(command);

   string testfile1_tmp, testfile2_tmp, testfile3_tmp, testfile4_tmp;

   // fill test vectors:
   functions_tested.push_back("SL17");
   filenames_tmp.push_back(" ");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back(" ");
   //---------------------------------------------------------------------------
   functions_tested.push_back("z");
   filenames_tmp.push_back("spectra_CIRELLI11_EW_GAMMA_m100.txt");
   filenames_tmp.push_back("dnde_CIRELLI11_EW_GAMMA_m100.txt");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("g0");
   filenames_tmp.push_back("gal.Mr.ecsv");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("g1");
   filenames_tmp.push_back("gal.rhor.ecsv");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("g2");
   filenames_tmp.push_back("gal.Jalphaint.ecsv");
   filenames_tmp.push_back("gal.fluxes_annihil.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999 " + nulloutput + " mv " + test_dir + "/gal.fluxes.output " + test_dir + "/gal.fluxes_annihil.output");
   //---------------------------------------------------------------------------
   functions_tested.push_back("g2");
   filenames_tmp.push_back("gal.Dalphaint.ecsv");
   filenames_tmp.push_back("gal.fluxes_decay.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gPP_DM_IS_ANNIHIL_OR_DECAY=0 --gPP_DM_DECAY_LIFETIME_S=1e+27 --gSIM_EPS=999 " + nulloutput + " mv " + test_dir + "/gal.fluxes.output " + test_dir + "/gal.fluxes_decay.output");
   //---------------------------------------------------------------------------
   functions_tested.push_back("g4");
   filenames_tmp.push_back("gal.Jtheta.ecsv");
   filenames_tmp.push_back("gal.Jthetadiff.ecsv");
   filenames_tmp.push_back("gal.intensities.output");
   filenames_tmp.push_back("list_generic_test.txt.gal.list.Jtheta.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gLIST_HALOES=" + compare_dir + "list_generic_test.txt --gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("h0");
   filenames_tmp.push_back("list_generic_test.txt.Mr.ecsv");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gLIST_HALOES=" + compare_dir + "list_generic_test.txt --gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("h1");
   filenames_tmp.push_back("list_generic_test.txt.rhor.ecsv");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gLIST_HALOES=" + compare_dir + "list_generic_test.txt --gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("h2");
   filenames_tmp.push_back("list_generic_test.txt.Jalphaint.ecsv");
   filenames_tmp.push_back("list_generic_test.txt.fluxes.ecsv");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gLIST_HALOES=" + compare_dir + "list_generic_test.txt --gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("h3");
   filenames_tmp.push_back("list_generic_test.txt.Jtheta.ecsv");
   filenames_tmp.push_back("list_generic_test.txt.intensities.ecsv");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gLIST_HALOES=" + compare_dir + "list_generic_test.txt --gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("h8");
   filenames_tmp.push_back("list_generic_jeans.txt.sigmapR.ecsv");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gLIST_HALOES_JEANS=" + compare_dir + "list_generic_jeans.txt --gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("h9");
   filenames_tmp.push_back("list_generic_jeans.txt.IR.ecsv");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gLIST_HALOES_JEANS=" + compare_dir + "list_generic_jeans.txt --gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("h10");
   filenames_tmp.push_back("list_generic_jeans.txt.Betar.ecsv");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gLIST_HALOES_JEANS=" + compare_dir + "list_generic_jeans.txt --gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("e0");
   filenames_tmp.push_back("cosmo.distances.ecsv");
   filenames_tmp.push_back("cosmo.omegas.ecsv");
   filenames_tmp.push_back("cosmo.distances_norm.ecsv");
   filenames_tmp.push_back("cosmo.hubble.ecsv");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("e1");
   filenames_tmp.push_back("cosmo.hmf_TINKER08_2D.output");
   filenames_tmp.push_back("cosmo.dndOmega_TINKER08_2D.output");
   filenames_tmp.push_back("cosmo.sigma2_2D.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("e2");
   filenames_tmp.push_back("boost_m_alpha.output");
   filenames_tmp.push_back("c_lum_m.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("e3");
   filenames_tmp.push_back("cosmo.d_intensitymultiplier_dlnM_2D.output");
   filenames_tmp.push_back("cosmo.intensitymultiplier.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("e4");
   filenames_tmp.push_back("cosmo.ebl_tau.output");
   filenames_tmp.push_back("cosmo.ebl_exp-tau.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("e5");
   filenames_tmp.push_back("cosmo.dPhidOmegadE_at10GeV_2D.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("e6");
   filenames_tmp.push_back("cosmo.dPhidOmegadE_CIRELLI11_EW_GAMMA_m100.ecsv");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------


   //---------------------------------------------------------------------------
   functions_tested.push_back("g6");
   testfile1_tmp = "test_g6a.txt";
   testfile2_tmp = "test_g6b.txt";
   testfile3_tmp = "test_g6c.txt";
   testfile4_tmp = "annihil_gal2D_LOS0_0_FOV90x45_nside1024";
   filenames_tmp.push_back(testfile1_tmp);
   filenames_tmp.push_back(testfile2_tmp);
   filenames_tmp.push_back(testfile3_tmp);
   filenames_tmp.push_back(testfile4_tmp + ".list");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999 --gLIST_HALOES=" + compare_dir + "list_generic_test.txt" + nulloutput
                              + gPATH_TO_CLUMPY_EXE + "clumpy -o1 -i "
                              + test_dir + testfile4_tmp + ".fits 1" +  nulloutput + "head -n +1500 " + test_dir
                              + testfile4_tmp + "-JFACTOR.dat > "
                              + test_dir + testfile1_tmp + ";"
                              + gPATH_TO_CLUMPY_EXE + "clumpy -o1 -i "
                              + test_dir + testfile4_tmp + ".fits 2" +  nulloutput + "head -n +1500 " + test_dir
                              + testfile4_tmp + "-JFACTOR_PER_SR.dat > "
                              + test_dir + testfile2_tmp + ";"
                              + gPATH_TO_CLUMPY_EXE + "clumpy -o1 -i "
                              + test_dir + testfile4_tmp + ".fits 3" +  nulloutput + "head -n +1500 " + test_dir
                              + testfile4_tmp + "-INTEGRATED_FLUXES.dat > "
                              + test_dir + testfile3_tmp + ";"
                             );
   //---------------------------------------------------------------------------
#if IS_ROOT
   functions_tested.push_back("g8");
   testfile1_tmp = "test_g8a.txt";
   testfile2_tmp = "test_g8b.txt";
   testfile3_tmp = "annihil_gal2D_LOS180_0_FOV4x4_nside1024";
   filenames_tmp.push_back(testfile1_tmp);
   filenames_tmp.push_back(testfile2_tmp);
   filenames_tmp.push_back(testfile3_tmp + ".list");
   filenames_tmp.push_back(testfile3_tmp + ".drawn");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999 --gLIST_HALOES=" + compare_dir + "list_generic_test.txt" + nulloutput
                              + gPATH_TO_CLUMPY_EXE + "clumpy -o1 -i "
                              + test_dir + testfile3_tmp + ".fits 1" +  nulloutput + "head -n +3000 " + test_dir
                              + testfile3_tmp + "-JFACTOR.dat > "
                              + test_dir + testfile1_tmp + ";"
                              + gPATH_TO_CLUMPY_EXE + "clumpy -o1 -i "
                              + test_dir + testfile3_tmp + ".fits 2" +  nulloutput + "head -n +3000 " + test_dir
                              + testfile3_tmp + "-JFACTOR_PER_SR.dat > "
                              + test_dir + testfile2_tmp
                              + ";");
   //---------------------------------------------------------------------------
   functions_tested.push_back("h5");
   testfile1_tmp = "test_h5a.txt";
   testfile2_tmp = "test_h5b.txt";
   testfile3_tmp = "test_h5c.txt";
   testfile4_tmp = "annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024";
   filenames_tmp.push_back(testfile1_tmp);
   filenames_tmp.push_back(testfile2_tmp);
   filenames_tmp.push_back(testfile3_tmp);
   filenames_tmp.push_back(testfile4_tmp + ".drawn");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=998 --gSIM_USER_RSE=20 --gSIM_IS_CALC_JVARIANCE=1 --gLIST_HALOES=" + compare_dir
                              + "list_generic_test.txt" + nulloutput
                              + gPATH_TO_CLUMPY_EXE + "clumpy -o1 -i "
                              + test_dir + testfile4_tmp + ".fits 1" +  nulloutput + "tail -n +500 " + test_dir
                              + testfile4_tmp + "-JFACTOR.dat > "
                              + test_dir + testfile1_tmp + ";"
                              + gPATH_TO_CLUMPY_EXE + "clumpy -o1 -i "
                              + test_dir + testfile4_tmp + ".fits 2" +  nulloutput + "tail -n +500 " + test_dir
                              + testfile4_tmp + "-JFACTOR_PER_SR.dat > "
                              + test_dir + testfile2_tmp + ";"
                              + gPATH_TO_CLUMPY_EXE + "clumpy -o1 -i "
                              + test_dir + testfile4_tmp + ".fits 3" +  nulloutput + "tail -n +500 " + test_dir
                              + testfile4_tmp + "-INTEGRATED_FLUXES.dat > "
                              + test_dir + testfile3_tmp + ";"
                             );

   //---------------------------------------------------------------------------
   functions_tested.push_back("s0");
   filenames_tmp.push_back("stat_example.dat.Mr_cls.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("s5");
   filenames_tmp.push_back("stat_example.dat.rhor_cls.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("s6");
   filenames_tmp.push_back("stat_example.dat.Jalphaint_cls.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("s8");
   filenames_tmp.push_back("stat_example.dat.sigma_p_cls.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("s9");
   filenames_tmp.push_back("stat_example.dat.vr2_cls.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("s10");
   filenames_tmp.push_back("stat_example.dat.beta_cls.output");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("--gSIM_EPS=999");
   //---------------------------------------------------------------------------
   functions_tested.push_back("r1");
   filenames_tmp.push_back("chi2_binned.dat");
   filenames_test.push_back(filenames_tmp);
   filenames_tmp.clear();
   special_commands.push_back("none");
   //---------------------------------------------------------------------------
#endif

   command = "rm " + test_dir + "* ";
   sys_safe_execution(command);
   cout << endl << "++++++++++++++++++++++++++++++++++++" << endl;
   cout <<         "+++         CLUMPY Tests         +++" << endl;
#if IS_ROOT
   cout <<         "+++ (with ROOT functionalities)  +++" << endl;
#else
   cout <<         "+++  (w/o ROOT functionalities)  +++" << endl;
#endif
   cout << "++++++++++++++++++++++++++++++++++++" << endl;

   int n1D = 22;

   cout << endl << "++++    Tests of 1D modules     ++++" << endl << endl;

   cout << endl << "[1/" << functions_tested.size() << "]";
   bool tmp = stref17_tests(test_dir, compare_dir);
   functions_tested_results.push_back(tmp);
   for (int i = 1; i < int(functions_tested.size()); ++i) {
      cout << endl << "[" << i + 1 << "/" << functions_tested.size() << "]";
      tmp = execute_test(functions_tested[i], filenames_test[i], gPATH_TO_CLUMPY_EXE,
                         test_dir, compare_dir, special_commands[i]);
      functions_tested_results.push_back(tmp);
      if (i == n1D - 1)
         cout << endl << endl << "++++   Tests of 2D/ROOT modules (please be patient...)   ++++" << endl << endl;
   }

   command = "rm " + test_dir + "clumpy_params_* "
             + test_dir + "*root "
             + test_dir + "*fits "
             + test_dir + "*R.dat "
             + test_dir + "*S.dat "
             + test_dir + "nul ";
   //+ gPATH_TO_CLUMPY_HOME + "nul";
   sys_safe_execution(command);

   // Summary:
   bool is_everything_passed = true;
   int failed_tests = 0;
   string output_passed = COLOR_BGREEN "OK" COLOR_RESET;
   string output_not_passed = COLOR_BRED "not passed" COLOR_RESET;

   cout << endl << "++++           Summary           +++" << endl << endl;
   for (int i = 0; i < int(functions_tested_results.size()); ++i) {
      string flag_passed = output_not_passed;
      if (functions_tested_results[i] == true)
         flag_passed = output_passed;
      else {
         flag_passed =  output_not_passed;
         is_everything_passed = false;
         failed_tests += 1;
      }
      ostringstream tmp1;
      tmp1 << i + 1;
      string tmp2 = " Test " + tmp1.str() + " (" + functions_tested[i] + ") ";
      cout << std::left << std::setw(17) << std::setfill('.') << tmp2 << " " << flag_passed << endl;
   }

   //command = "cd " + gPATH_TO_CLUMPY_HOME + "; rm -r " + test_dir + "; cd " + gPATH_TO_USER_EXE;

   if (is_everything_passed) {
      //sys_safe_execution(command);
      cout << endl << " All tests passed." << endl;
      cout << endl << "++++++++++++++++++++++++++++++++++++" << endl;
      return EXIT_SUCCESS;
   } else {
      cout << endl << " " <<  failed_tests << " out of " << functions_tested_results.size()  << " tests failed. Please compare the test output files in " << test_dir << " with " << compare_dir << "." << endl;
      cout << endl << "++++++++++++++++++++++++++++++++++++" << endl;
      return EXIT_FAILURE;
   }
}

/*! \file clumpy_tests.cc  \brief <b>Test suite for the main CLUMPY executables</b> (runs through all submodules and checks output) */

