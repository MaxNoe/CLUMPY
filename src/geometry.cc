/*! \file geometry.cc \brief (see geometry.h) */

// CLUMPY includes
#include "../include/geometry.h"
#include "../include/inlines.h"
#include "../include/params.h"
#include "../include/misc.h"

// C++ std libraries
//#include <math.h>
//#include <stdlib.h>

//______________________________________________________________________________
double angular_dist(double const &dec1_rad, double const &dec2_rad,
                    double const &ra1_rad, double const &ra2_rad)
{
   //--- Returns angular distance [rad] from declinations and right
   //    ascensions of two objects (in radians) using the ‘haversine’ formula.
   //
   //  dec1_rad      Declination of object 1 [rad]
   //  dec2_rad      Declination of object 2 [rad]
   //  ra1_rad       Right ascension of object 1 [rad]
   //  ra2_rad       Right ascension of object 2 [rad]

//   double phi_test = acos(MY_SIN(dec1_rad) * MY_SIN(dec2_rad)
//                  + MY_COS(dec1_rad) * MY_COS(dec2_rad)
//                  * MY_COS(ra1_rad - ra2_rad));

   double a = pow(MY_SIN((dec1_rad - dec2_rad) / 2), 2.) + MY_COS(dec1_rad) * MY_COS(dec2_rad) * pow(MY_SIN((ra1_rad - ra2_rad) / 2), 2.);
   return 2 * atan2(sqrt(a), sqrt(1 - a));


}

//______________________________________________________________________________
void check_psi(double &psi_rad)
{
   //--- Enforces angle in range -pi<psi<pi [rad].
   //
   //  psi_rad       Longitude [rad]

   if (psi_rad < -PI) psi_rad += 2 * PI;
   else if (psi_rad > PI) psi_rad -= 2 * PI;
}

//______________________________________________________________________________
double distance_from_angular_dist(double const &alpha_rad, double const &d_kpc)
{
   //--- Returns separation between two objects at the same distance d
   //    from their angular distance [kpc]
   //
   //  alpha         Angular distance between 2 objects [rad]
   //  d             Distance to objects [kpc]

   return d_kpc * tan(alpha_rad);
}

//______________________________________________________________________________
void euler_rotation(double x[3], double euler_deg[3])
{
   //--- Returns new coordinates x_new[0...2] for input point x[0...2] after
   //    roating along the three Euler angles (alpha,beta,gamma)_Euler [deg].
   //
   // INPUTS:
   //  x[3]          x,y,z in the xyz_halo or xyz_gal (gal. halo) coordinates (see geometry.h) [kpc]
   //  euler_deg[3]  Euler rotation angle (alpha=[-180,180],beta=[-90,90],gamma=[-180,180]) [deg]
   // OUTPUT:
   //  x[3]          X,Y,Z in the xyz_halo or xyz_gal (gal. halo) coordinates (see geometry.h) [kpc]


   // Check if at least one rotation angle is not 0
   if (fabs(euler_deg[0]) > 1.e-5 || fabs(euler_deg[1]) > 1.e-5
         || fabs(euler_deg[2]) > 1.e-5) {
      if (euler_deg[1] < -90. || euler_deg[1] > 90.) {
         char char_tmp[256];
         sprintf(char_tmp, "Second rotation angle (beta) is %.2lf [deg], "
                 "whereas it should be in the range [-90,90] deg", euler_deg[1]);
         print_error("geometry.cc", "euler_rotation()", string(char_tmp));

      }

      const int n_coord = 3;
      double xnew[n_coord];
      double euler[n_coord];
      double euler_cos[n_coord];
      double euler_sin[n_coord];

      // Convert input Euler_angles [deg] in [rad], and calculate cos and sin of the angles
      for (int i = 0; i < n_coord; ++i) {
         euler[i] = DEG_to_RAD * euler_deg[i];
         // Ensure alpha and gamma are in [-PI,PI]
         if (i == 0 || i == 2)
            check_psi(euler[i]);

         euler_cos[i] = MY_COS(euler[i]);
         euler_sin[i] = MY_SIN(euler[i]);
         //cout << i << " " << euler[i] << "  cos=" << euler_cos[i] << "  sin=" << euler_sin[i] << endl;
      }

      // Rotate around Euler angle
      xnew[0] = x[0] * (euler_cos[0] * euler_cos[2] - euler_sin[0] * euler_sin[2] * euler_cos[1])
                + x[1] * (euler_cos[2] * euler_sin[0] + euler_sin[2] * euler_cos[1] * euler_cos[0])
                + x[2] * (euler_sin[1] * euler_sin[2]);
      xnew[1] = x[0] * (-euler_sin[2] * euler_cos[0] - euler_sin[0] * euler_cos[1] * euler_cos[2])
                + x[1] * (-euler_sin[0] * euler_sin[2] + euler_cos[2] * euler_cos[1] * euler_cos[0])
                + x[2] * (euler_cos[2] * euler_sin[1]);
      xnew[2] = x[0] * (euler_sin[0] * euler_sin[1])
                + x[1] * (-euler_sin[1] * euler_cos[0])
                + x[2] * (euler_cos[1]);

      for (int i = 0; i < n_coord; ++i)
         x[i] = xnew[i];
   }

   return;
}

//______________________________________________________________________________
void greatcircle_intermediate_points(double const &psi1_rad, double const &theta1_rad,
                                     double const &psi2_rad, double const &theta2_rad,
                                     double const &dist_frac,
                                     double &psi_res_rad, double &theta_res_rad)
{
   //--- Returns the coordinates of a point on a great circle defined by the
   //    two points (psi1, theta1) and (psi2, theta2) at a distance dist_frac * dist,
   //    from (psi1, theta1), where dist is the distance between
   //    (psi1, theta1) and (psi2, theta2).
   //    Taken from http://www.edwilliams.org/avform.htm#Intermediate
   //
   // INPUTS:
   //  psi1_rad        longitude of point 1 [rad]
   //  theta1_rad      latitude of point 1  [rad]
   //  psi1_rad        longitude of point 2 [rad]
   //  theta1_rad      latitude of point 2  [rad]
   //  dist_frac       distance of the searched point from point 1 as a
   //                  fraction of the distance from point 1 to point 2.
   // OUTPUTS:
   //  psi_res_rad     longitude of the searched point [rad]
   //  theta_res_rad   latitude of the searched point  [rad]

   double delta = angular_dist(theta1_rad, theta2_rad, psi1_rad, psi2_rad);
   double a = MY_SIN((1 - dist_frac) * delta) / MY_SIN(delta);
   double b = MY_SIN(dist_frac  * delta) / MY_SIN(delta);
   double x = a * MY_COS(theta1_rad) * MY_COS(psi1_rad) + b * MY_COS(theta2_rad) * MY_COS(psi2_rad);
   double y = a * MY_COS(theta1_rad) * MY_SIN(psi1_rad) + b * MY_COS(theta2_rad) * MY_SIN(psi2_rad);
   double z = a * MY_SIN(theta1_rad) + b * MY_SIN(theta2_rad);
   theta_res_rad = atan2(z, sqrt(x * x + y * y));
   psi_res_rad   = atan2(y, x);
}

//______________________________________________________________________________
void lalphabeta_to_lpsitheta(double x[3], double par[2])
{
   //--- Returns (l,psi,theta) from (l,alpha,beta) [kpc,rad,rad].
   //    N.B.: l remains unchanged.
   //
   // INPUTS:
   //  par[0]        psi_centre   [rad]
   //  par[1]        theta_centre [rad]
   //  x[0]          l     [kpc]
   //  x[1]          alpha [rad]
   //  x[2]          beta  [rad]
   // OUTPUTS:
   //  x[0]          l     [kpc]
   //  x[1]          psi   [rad]
   //  x[2]          theta [rad]

   // We use "lalphabeta_to_xyz", and then "xyz_to_lpsitheta"
   lalphabeta_to_xyzgal(x, par);
   xyzgal_to_lpsitheta(x);
}

//______________________________________________________________________________
void lalphabeta_to_rpsitheta(double x[3], double par[2])
{
   //--- Returns (r,psi,theta) from (l,alpha,beta) [kpc,rad,rad].
   // INPUTS:
   //  par[0]        psi_centre   [rad]
   //  par[1]        theta_centre [rad]
   //  x[0]          l     [kpc]
   //  x[1]          alpha [rad]
   //  x[2]          beta  [rad]
   // OUTPUTS:
   //  x[0]          r     [kpc] distance from the Galactic center
   //  x[1]          psi   [rad]
   //  x[2]          theta [rad]

   // We use "lalphabeta_to_xyz", and then "xyz_to_rpsitheta"
   lalphabeta_to_xyzgal(x, par);
   xyzgal_to_rpsitheta(x);
}


//______________________________________________________________________________
void lalphabeta_to_xyzgal(double x[3], double par[2])
{
   //--- Returns (x,y,z)_gal [kpc] from (l,alpha,beta) [kpc,rad,rad].
   //
   // INPUTS:
   //  par[0]        psi   [rad]
   //  par[1]        theta [rad]
   //  x[0]          l     [kpc]
   //  x[1]          alpha [rad]
   //  x[2]          beta  [rad]
   // OUTPUTS:
   //  x[0]          x     [kpc]
   //  x[1]          y     [kpc]
   //  x[2]          z     [kpc]

   double sin_alpha = MY_SIN(x[1]);
   double x_p = x[0] * MY_COS(x[1]);
   double y_p = x[0] * sin_alpha * MY_SIN(x[2]);
   double z_p = x[0] * sin_alpha * MY_COS(x[2]);

   double sin_psi = MY_SIN(par[0]);
   double cos_psi = MY_COS(par[0]);
   double sin_theta = MY_SIN(par[1]);
   double cos_theta = MY_COS(par[1]);
   x[0] =   x_p * cos_psi * cos_theta
            - y_p * sin_psi
            - z_p * sin_theta * cos_psi - gMW_RSOL;
   x[1] =   x_p * sin_psi * cos_theta
            + y_p * cos_psi
            - z_p * sin_theta * sin_psi;
   x[2] =   x_p * sin_theta
            + z_p * cos_theta;
}

//______________________________________________________________________________
void lalphabeta_to_xyzlos(double x[3], double par[2], double const &d_kpc)
{
   //--- Returns (x,y,z)_{halo centre} [kpc] from (l,alpha,beta) [kpc,rad,rad] and
   //    observation angle (phi,theta) [rad] from the halo centre distance [kpc].
   //
   // INPUTS:
   //  par[0]        psi   [rad]
   //  par[1]        theta [rad]
   //  d_kpc         Distance to halo [kpc]
   //  x[0]          l     [kpc]
   //  x[1]          alpha [rad]
   //  x[2]          beta  [rad]
   // OUTPUTS:
   //  x[0]          x     [kpc]
   //  x[1]          y     [kpc]
   //  x[2]          z     [kpc]

   double sin_alpha = MY_SIN(x[1]);
   double x_p = x[0] * MY_COS(x[1]);
   double y_p = x[0] * sin_alpha * MY_SIN(x[2]);
   double z_p = x[0] * sin_alpha * MY_COS(x[2]);

   if (fabs(par[0]) < 1.e-10 && fabs(par[1]) < 1.e-10) {
      x[0] =   x_p - d_kpc;
      x[1] =   y_p;
      x[2] =   z_p;
   } else {
      double sin_psi = MY_SIN(par[0]);
      double cos_psi = MY_COS(par[0]);
      double sin_theta = MY_SIN(par[1]);
      double cos_theta = MY_COS(par[1]);
      x[0] =   x_p * cos_psi * cos_theta
               - y_p * sin_psi
               - z_p * sin_theta * cos_psi - d_kpc;
      x[1] =   x_p * sin_psi * cos_theta
               + y_p * cos_psi
               - z_p * sin_theta * sin_psi;
      x[2] =   x_p * sin_theta
               + z_p * cos_theta;
   }
}

//______________________________________________________________________________
void lpsitheta_to_xyzgal(double x[3])
{
   //--- Returns Gal. cart. coordinates (x,y,z) [kpc] from (l,psi,theta) [kpc,rad,rad].
   // INPUTS:
   //  x[0]          l     [kpc]
   //  x[1]          psi   [rad]
   //  x[2]          theta [rad]
   // OUTPUTS:
   //  x[0]          x [kpc]
   //  x[1]          y [kpc]
   //  x[2]          z [kpc]

   double l = x[0];
   double psi = x[1];
   double theta = x[2];

   x[0] = l * MY_COS(psi) * MY_COS(theta) - gMW_RSOL;
   x[1] = l * MY_SIN(psi) * MY_COS(theta);
   x[2] = l * MY_SIN(theta);
}

//______________________________________________________________________________
double philos_to_lmax(double const &phi_los_rad)
{
   //--- Returns boundary of the galactic DM halo [kpc] in the
   //    l.o.s direction phi_los_rad [rad].
   //
   //  phi_los_rad   l.o.s. direction [rad]

   double sin_Rsol = MY_SIN(phi_los_rad) * gMW_RSOL;
   return gMW_RSOL * MY_COS(phi_los_rad)
          + sqrt(gMW_RMAX * gMW_RMAX - sin_Rsol * sin_Rsol);
}

//______________________________________________________________________________
double psitheta_to_phi(double const &psi_rad, double const &theta_rad)
{
   //--- Returns phi [rad], the angle between the GC and the l.o.s. direction (from Earth).
   //
   //  psi_rad        Longitude of l.o.s. [rad]
   //  theta_rad      Latitude of l.o.s. [rad]

   return angular_dist(theta_rad, 0, psi_rad, 0);
}

//______________________________________________________________________________
void rpsitheta_to_xyzgal(double x[3])
{
   //--- Returns Gal. cart. coordinates (x,y,z) [kpc] from (r,psi,theta) [kpc,rad,rad].
   // INPUTS:
   //  x[0]          r     [kpc]
   //  x[1]          psi   [rad]
   //  x[2]          theta [rad]
   // OUTPUTS:
   //  x[0]          x [kpc]
   //  x[1]          y [kpc]
   //  x[2]          z [kpc]

   double r = x[0];
   double psi = x[1];
   double theta = x[2];

   x[0] = r * MY_COS(psi) * MY_COS(theta);
   x[1] = r * MY_SIN(psi) * MY_COS(theta);
   x[2] = r * MY_SIN(theta);
}

//______________________________________________________________________________
void xyzgal_to_lalphabeta(double x[3], double par[2])
{
   //--- Returns (l,alpha,beta) [kpc,rad,rad] from Gal. cart. coordinates (x,y,z) [kpc].
   //
   // INPUTS:
   //  par[0]        psi   [rad]
   //  par[1]        theta [rad]
   //  x[0]          x     [kpc]
   //  x[1]          y     [kpc]
   //  x[2]          z     [kpc]
   // OUTPUTS:
   //  x[0]          l     [kpc]
   //  x[1]          alpha [rad]
   //  x[2]          beta  [rad]

   double sin_psi = MY_SIN(par[0]);
   double cos_psi = MY_COS(par[0]);
   double sin_theta = MY_SIN(par[1]);
   double cos_theta = MY_COS(par[1]);

   x[0] += gMW_RSOL;
   double x_p =  x[0] * cos_psi * cos_theta
                 + x[1] * sin_psi * cos_theta
                 + x[2] * sin_theta;

   double y_p = -x[0] * sin_psi
                + x[1] * cos_psi;

   double z_p = -x[0] * cos_psi * sin_theta
                - x[1] * sin_psi * sin_theta
                + x[2] * cos_theta;

   // Sets output
   double tmp = x_p * x_p + y_p * y_p;
   x[0] = sqrt(tmp + z_p * z_p);
   // acos returns an angle in [0,pi] => OK as alpha is in [0,pi]
   if (x[0] != 0.) x[1] = acos(x_p / x[0]);
   else x[1] = 0.;
   // atan returns an angle in [-pi/2,pi/2], but beta is in [0,2pi]
   if (tmp > 0.) {
      x[2] = acos(z_p / sqrt(tmp));
      if (y_p < 0.)
         x[2] = 2.*PI - x[2];
   } else x[2] = 0.;
}

//______________________________________________________________________________
void xyzgal_to_lpsitheta(double x[3])
{
   //--- Returns (l,psi,theta) [kpc,rad,rad] from Gal. cart. coordinates (x,y,z) [kpc].
   //
   // INPUTS:
   //  x[0]          x     [kpc]
   //  x[1]          y     [kpc]
   //  x[2]          z     [kpc]
   // OUTPUTS:
   //  x[0]          l     [kpc]
   //  x[1]          psi   [rad]
   //  x[2]          theta [rad]

   double x0 = x[0] + gMW_RSOL;
   double y0 = x[1];
   double z0 = x[2];

   x[0] = sqrt(x0 * x0 + y0 * y0 + z0 * z0);
   // atan returns value in (-pi/2,pi/2)
   // need to be careful to be consistent with our coordinate
   // system where psi in (-pi, pi).
   if (x0 < 0. && y0 > 0.)
      x[1] = PI - atan(y0 / fabs(x0));
   else if (x0 < 0. && y0 < 0.)
      x[1] = -PI + atan(y0 / x0);
   else if (x0 > 0. && y0 < 0.)
      x[1] = -atan(fabs(y0) / x0);
   else
      x[1] = atan(y0 / x0);
   x[2] = asin(z0 / x[0]);
}

//______________________________________________________________________________
void xyzgal_to_rpsitheta(double x[3])
{
   //--- Returns (r,psi,theta) [kpc,rad,rad] from Gal. cart. coordinates (x,y,z) [kpc].
   // INPUTS:
   //  x[0]          x     [kpc]
   //  x[1]          y     [kpc]
   //  x[2]          z     [kpc]
   // OUTPUTS:
   //  x[0]          r     [kpc] distance from the Galactic center
   //  x[1]          psi   [rad] long. coordinate (but from the reference system with the GC as center!)
   //  x[2]          theta [rad] lat. coordinate (but from the reference system with the GC as center!)

   double x0 = x[0];
   double y0 = x[1];
   double z0 = x[2];

   x[0] = sqrt(x0 * x0 + y0 * y0 + z0 * z0);
   // atan returns value in (-pi/2,pi/2)
   // need to be careful to be consistent with our coordinate
   // system where psi in (-pi, pi).
   if (x0 < 0. && y0 > 0.)
      x[1] = PI - atan(y0 / fabs(x0));
   else if (x0 < 0. && y0 < 0.)
      x[1] = -PI + atan(y0 / x0);
   else if (x0 > 0. && y0 < 0.)
      x[1] = -atan(fabs(y0) / x0);
   else
      x[1] = atan(y0 / x0);
   x[2] = asin(z0 / x[0]);
}
