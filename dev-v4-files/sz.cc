
// C++ std libraries
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <iostream>
#include <vector>
#include <fftw3.h>
using namespace std;

// ROOT includes
#include <TF3.h>
#include <TH2D.h>
#include <TMath.h>
#include <TRandom.h>
#include <TMultiGraph.h>
#include <TLegendEntry.h>
#include <TLegend.h>
#include <TPaveText.h>

// HEALPix includes
#include <fitshandle.h>
#include <powspec_fitsio.h>

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/geometry.h"
#include "../include/healpix_fits.h"
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/integr_los.h"
#include "../include/janalysis.h"
#include "../include/jeans_analysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/spectra.h"
#include "../include/stat.h"
#include "../include/cosmo.h"
#include "../include/extragal.h"
#include "../include/sz.h"

// GSL includes
#include <gsl/gsl_math.h>
#define SIZEOF_ARRAY( a ) (sizeof( a ) / sizeof( a[ 0 ] ))

//______________________________________________________________________________
double sz_m500_to_r500(double &m500, double &z)
{
   // Returns R_500 [kpc] from M_500 [Msun] at redshift z

   double rho_mean = RHO_CRITperh2_MSOLperKPC3 * gCOSMO_OMEGA0_M * pow(gCOSMO_HUBBLE, 2.) * pow(1. + z, 3); //Msol kpc^-3
   double r500 = pow(3.*m500 / (500.*4 * PI * rho_mean), 1. / 3.); // kpc
   return r500;
}

//______________________________________________________________________________
void sz_m500_to_par(double &m500, double &z, double &c500, double par_prof[3], double par_tot[5])
{
   //--- Fills par_tot[4] containing pressure profile parameters given the halo mass m500,
   //    c500, and some pressure profile shape parameters.
   // INPUTS:
   //  m500          M of the halo [Msol] for Delta=500
   //  z             Redshift
   //  c500          Cluster concentration for Delta=500 (given by Arnaud et al. (2010))
   //  par_prof[0]   Pressure shape parameter #1
   //  par_prof[1]   Pressure shape parameter #2
   //  par_prof[2]   Pressure shape parameter #3
   // OUTPUTS:
   //  par_tot[0]    Pressure profile normalisation [kev cm^-3]
   //  par_tot[1]    Scale radius rs [kpc]
   //  par_tot[2]    Shape parameter #1 (=par_prof[0])
   //  par_tot[3]    Shape parameter #2 (=par_prof[1])
   //  par_tot[4]    Shape parameter #3 (=par_prof[2])


   for (int i = 0; i < 3; i++) {
      par_tot[i + 2] = par_prof[i];
   }
   par_tot[1] = sz_m500_to_r500(m500, z) / c500; // rs=R_500/c500

   double h70 = gCOSMO_HUBBLE * 100. / 70.; // H0/(70 km/s/Mpc)
   double *params = NULL;
   double p500 = 1.65e-3 * pow(m500 / 3.e14, 2. / 3.) * h70 * h70 * pow((H0_over_H(z, params)), -8. / 3.);
   double p0 = gSZ_REF_PRESSURE_NORM * pow(h70, -3. / 2.);
   par_tot[0] = p500 * p0 * pow(m500 / 3.e14, 1. / gSZ_REF_ALPHA_MYX - 5. / 3.); // keV cm^-3
}

//______________________________________________________________________________
double p_ZHAO(double &r, double par[5])
{
   //--- Returns cluster electron pressure at radius r from centre,
   //    using GNFW = ZHAO (alpha,beta,gamma) parametrisation
   //    Result is in [keV cm^-3]:
   //    => P(r)= P_s/[(r/r_s)^{gamma} [1+(r/r_s)^{alpha}]^{(beta-gamma)/alpha}].
   //    alpha, beta and gamma default values are from Arnaud+ (2010) universal profile

   //  r             Distance from the centre of the profile [kpc]
   //  par[0]        Pressure profile normalisation [keV cm^-3]
   //  par[1]        rs: scale radius [kpc]
   //  par[2]        Shape parameter #1 - alpha
   //  par[3]        Shape parameter #2 - beta
   //  par[4]        Shape parameter #3 - gamma


   // Not sure if we should have a saturation pressure to avoid divergence in integration ?

   if (par[4] > 1.e-5)
      return par[0] * pow(par[1] / r, par[4])
             / pow(1. + pow(r / par[1], par[2]), (par[3] - par[4]) / par[2]);
   else // gamma = 0
      return par[0] / pow(1. + pow(r / par[1], par[2]), par[3] / par[2]);

}
//______________________________________________________________________________
void hp_fov_init(string const &param_file, double const &psi_deg,
                 string const &theta_deg_str, string const &theta_orth_size_deg_str,
                 double const &theta_size_deg, fov_struct &hp_fov)
{

   //load_parameters(param_file, false); should be loaded in clumpy.cc MORITZ CHECK


   //--- Creates a fov_deg x fov_deg skymap around direction (psi_deg, theta_deg)
   //  param_file     Clumpy parameters
   //  psi_deg        Galactic longitude of the l.o.s [deg]
   //  theta_deg      Galactic latitude of the l.o.s [deg]
   //  theta_orth_size_deg   Dimension of the Skymap in great-circle direction orthogonal to theta [deg]
   //  theta_size_deg Dimension of the Skymap in theta-direction [deg]
   //  Returns the structure grid_struct containing all required quantities for calculation



   // Direction around which the map is built [psi_deg,theta_deg in degrees]
   hp_fov.psi = psi_deg * DEG_to_RAD ; // rad
   check_psi(hp_fov.psi);
   hp_fov.psi_deg_corr = hp_fov.psi * RAD_to_DEG ;
   //hp_fov.theta;
   //hp_fov.theta_deg; // initialise here because theta_deg_str is still a string.
   //hp_fov.theta_orth_size_deg; // initialise here because theta_orth_size_deg_str is still a string.


   hp_fov.grid_mode = "RECT";

   if (theta_deg_str == "s") {
      hp_fov.grid_mode = "STRIP";
      hp_fov.theta_orth_size_deg = atof(theta_orth_size_deg_str.c_str());
      hp_fov.theta = 0.;
      hp_fov.theta_deg = 0.;
   } else {
      hp_fov.theta_deg = atof(theta_deg_str.c_str());
      hp_fov.theta = hp_fov.theta_deg * DEG_to_RAD; // rad
   }
   if (theta_orth_size_deg_str == "d") {
      hp_fov.grid_mode = "DISK";
      hp_fov.theta_orth_size_deg = theta_size_deg;
      // theta_size_deg itself will be corrected later to real range of theta values on the sphere.
   } else {
      hp_fov.theta_orth_size_deg = atof(theta_orth_size_deg_str.c_str());
   }
   if (theta_deg_str == "s" && theta_orth_size_deg_str == "d") {
      printf("\n====> ERROR: gal_j2D() in janalysis.cc");
      printf("\n             Cannot select STRIP mode and DISK mode at the same time.");
      printf("\n             => abort()\n\n");
      abort();
   }

   // set FOV size
   hp_fov.dtheta_orth = hp_fov.theta_orth_size_deg * DEG_to_RAD; // rad
   hp_fov.dtheta = theta_size_deg * DEG_to_RAD; // rad

   //--- print information about map dimensions
   printf("            ______________________________________________________\n\n");
   if (hp_fov.grid_mode == "DISK") {
      printf("                 Skymap in direction (l,b) = (%.1f,%.1f) [deg]\n", hp_fov.psi_deg_corr, hp_fov.theta_deg);
      printf("                   circular FOV with diameter = %.1f [deg].\n", theta_size_deg);
      if (fabs(hp_fov.theta_deg) > 90.) {
         printf("\n====> ERROR: gal_j2D() in janalysis.cc");
         printf("\n             FOV center exceeding |theta|>90 [deg] makes no sense.");
         printf("\n             => abort()\n\n");
         abort();
      }
      if (fabs(theta_size_deg) > 360.) {
         printf("\n====> ERROR: gal_j2D() in janalysis.cc");
         printf("\n             Disk diameter |d|>360 [deg] makes no sense.");
         printf("\n             => abort()\n\n");
         abort();
      }
   } else if (hp_fov.grid_mode == "STRIP")  {
      printf("        Skymap around galactic plane in direction (l,b) = (%.1f,0) [deg]\n", hp_fov.psi_deg_corr);
      printf("                             FOV  = [%.1fx%.1f]\n", atof(theta_orth_size_deg_str.c_str()), theta_size_deg);
      if (fabs(hp_fov.theta_orth_size_deg) > 360. || fabs(theta_size_deg) > 180.) {
         printf("\n====> ERROR: gal_j2D() in janalysis.cc");
         printf("\n             Strip exceeding |dpsi|>360 [deg] or |dtheta|>180 [deg] causes troubles.");
         printf("\n             => abort()\n\n");
         abort();
      }
   } else if (hp_fov.grid_mode == "RECT")  {
      printf("                 Skymap in direction (l,b) = (%.1f,%.1f) [deg]\n", hp_fov.psi_deg_corr, hp_fov.theta_deg);
      printf("                          rectangular FOV = [%.1fx%.1f]\n", atof(theta_orth_size_deg_str.c_str()), theta_size_deg);
      if (fabs(hp_fov.theta_deg) > 90.) {
         printf("\n====> ERROR: gal_j2D() in janalysis.cc");
         printf("\n             FOV center exceeding |theta|>90 [deg] makes no sense.");
         printf("\n             => abort()\n\n");
         abort();
      }
      if (fabs(theta_size_deg) == 180. && fabs(hp_fov.theta_orth_size_deg) == 180.) {
         // do nothing; this is ok, it will be accounted for in geometry.cc, set_healpix_fov()
      } else if (fabs(theta_size_deg) >= 180. || fabs(hp_fov.theta_orth_size_deg) >= 180.) {
         printf("\n====> ERROR: gal_j2D() in janalysis.cc");
         printf("\n             Rectangular grid for |dtheta_orth| or |dtheta| >= 180 [deg] not supported");
         printf("\n             (the exception |dtheta_orth| = |dtheta| = 180 [deg] is possible.)");
         printf("\n             N.B.: if you want to draw a full-sky map, choose FOV dimensions either by");
         printf("\n               - RING mode:  theta_orth_size = d, theta_size = 360.");
         printf("\n               - STRIP mode: theta_obs = s, theta_orth_size = 360., theta_size = 180.");
         printf("\n             Both ways are completely equivalent. See documentation for further details.\n\n");
         printf("\n             => abort()\n\n");
         abort();
      }
   }
   printf("            ______________________________________________________\n\n");

   if (hp_fov.dtheta_orth < 0. || hp_fov.dtheta < 0.) {
      printf("\n====> WARNING: gal_j2D() in janalysis.cc");
      printf("\n               Negative FOV dimensions -> FOV is inverted (exclusion region).\n\n");
   }

   /********************** INITIALIZE HEALPIX GRID/FOV  ***********************/

   //   int n_pix;          // number of pixels on the whole sphere
   // double delta_omega; // surface area of each pixel on the sphere = J-factor integration area
   //int smooth_sz;  // will the output be smoothed by an instrumental beam
   // and if yes, by which one?

   hp_set_resolution(hp_fov.n_pix, hp_fov.delta_omega, hp_fov.smooth_sz, true);

   cout << " hp_fov.smooth_sz = " << hp_fov.smooth_sz << endl;

   // For the grid creation, we also have to know if there will be a smoothing
   // done later, because in this case, the grid for the smooth contributions
   // will be extended a bit. Smoothing of the maps will only be performed if
   //  - a beam width is provided in the input file (smooth_gamma_or_nu_or_both != 0)
   //  - substructures are drawn (is_subs_drawn = true or is_list_added = true)
   //  combine these conditions to a single one:

   if (hp_fov.smooth_sz == 0) {
      cout << "      No smoothing of map by instrumental beam is applied.\n" << endl;
   } else if (hp_fov.smooth_sz == 1) {
      cout << "      Output map will be smoothed by gSIM_GAUSSBEAM_SZ_FWHM_DEG = "
           << gSIM_GAUSSBEAM_SZ_FWHM *RAD_to_DEG << " [deg]," << endl;
      cout << "      and the result will be stored in the 3rd extension of the output fits file.\n" << endl;
   }
   // the following function now creates the FOV:

   hp_fov.rs_pix_fov = hp_set_pix_fov(hp_fov.grid_mode, hp_fov.psi,  hp_fov.theta, hp_fov.dtheta_orth,
                                      hp_fov.dtheta, gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME) ;

   //vector<int> v_pix_fov;
   hp_fov.rs_pix_fov.toVector(hp_fov.v_pix_fov);
   hp_fov.n_pix_fov = hp_fov.rs_pix_fov.nval(); // number of pixels in the FOV.
   hp_fov.hp_gridprop = new T_Healpix_Base<int>(gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME, SET_NSIDE) ;

   hp_fov.fsky = double(hp_fov.n_pix_fov) / double(hp_fov.n_pix) ; // fraction of sky covered by FOV

   cout << ">>>>> N.B.: FOV dimensions with NSIDE = " << gSIM_HEALPIX_NSIDE
        << "   =>   " << hp_fov.n_pix_fov << " pixels." << endl;
   cout << "   => surface of the FOV: " << double(hp_fov.n_pix_fov) * hp_fov.delta_omega << " sr." << endl;
   cout << "   => fraction of sky covered: " << hp_fov.fsky * 100. << "%\n" << endl;

   // if there will be a smoothing of the maps,
   // additionally create jgal_smooth/subs/cross_continuum on a bigger grid:
   // double dtheta_orth_extended ;
   // double dtheta_extended;
   // rangeset<int> rs_pix_fov_extended;
   // vector<int> v_pix_fov_extended ;
   hp_fov.n_pix_fov_extended = 0;
   hp_fov.grid_mode_extended = hp_fov.grid_mode ;

   if (hp_fov.smooth_sz != 0 && hp_fov.n_pix_fov != hp_fov.n_pix) {
      double grid_extension = 5. * gSIM_GAUSSBEAM_SZ_FWHM ;
      do_safegridextension(hp_fov.dtheta_orth, hp_fov.dtheta, grid_extension, hp_fov.grid_mode_extended,
                           hp_fov.dtheta_orth_extended, hp_fov.dtheta_extended) ;
      hp_fov.rs_pix_fov_extended = hp_set_pix_fov(hp_fov.grid_mode_extended, hp_fov.psi,  hp_fov.theta,  hp_fov.dtheta_orth_extended,  hp_fov.dtheta_extended,
                                   gSIM_HEALPIX_NSIDE, gSIM_HEALPIX_SCHEME) ;
      hp_fov.rs_pix_fov_extended.toVector(hp_fov.v_pix_fov_extended);
      hp_fov.n_pix_fov_extended = hp_fov.rs_pix_fov_extended.nval(); // number of pixels in the FOV.
      cout << "   => size of extended grid for proper smoothing: " << hp_fov.n_pix_fov_extended << " pixels.\n" << endl;
   }

   /************************* GET GRID DIMENSIONS  ****************************/

   // now, as the FOV is created, we can determine the FOV dimensions dpsi in psi direction
   // and the coordinate range dtheta_coord, which may differ from the grid size dtheta.
   // double dpsi ;
   double psi_current ;
   double psi_raw_min = 2. * PI + SMALL_NUMBER  ; // psi in [0, 2PI]
   double psi_raw_max = 0 - SMALL_NUMBER ;
   double psi_checked_min = PI + SMALL_NUMBER ; // psi in [- PI, PI]
   double psi_checked_max = - PI - SMALL_NUMBER ;
   //double dtheta_coord ;
   double theta_current ;
   hp_fov.theta_max = - PI / 2. - SMALL_NUMBER;
   hp_fov.theta_min = + PI / 2. + SMALL_NUMBER;
   //pointing ptg_i_pix ;


   for (int i = 0; i < hp_fov.n_pix_fov; ++i) {
      hp_fov.ptg_i_pix = hp_fov.hp_gridprop->pix2ang(hp_fov.v_pix_fov[i]) ;
      psi_current = hp_fov.ptg_i_pix.phi ;
      if (psi_current < psi_raw_min) psi_raw_min = psi_current ;
      if (psi_current > psi_raw_max) psi_raw_max = psi_current ;
      check_psi(psi_current) ;
      if (psi_current < psi_checked_min) psi_checked_min = psi_current ;
      if (psi_current > psi_checked_max) psi_checked_max = psi_current ;

      theta_current = PI / 2. - hp_fov.ptg_i_pix.theta ;
      if (theta_current <  hp_fov.theta_min) hp_fov.theta_min = theta_current;
      if (theta_current >  hp_fov.theta_max) hp_fov.theta_max = theta_current;
   }

   // determine delta_psi at the latitude theta as reference needed later:
   int n_pix_ring;
   int startpix ;
   bool shift ;
   pointing ptg_gridcenter = pointing(PI / 2. - hp_fov.theta, hp_fov.psi);
   int i_ring = hp_fov.hp_gridprop->pix2ring(hp_fov.hp_gridprop->ang2pix(ptg_gridcenter));
   hp_fov.hp_gridprop->get_ring_info_small(i_ring, startpix, n_pix_ring, shift);
   double delta_psi_fovcenter = 2. * PI / n_pix_ring;

   // now determine dpsi, by also checking delicate cases when FOV crosses psi = PI or psi = 0.:
   if (hp_fov.theta == 0.) {
      hp_fov.dpsi = hp_fov.dtheta_orth ;
   } else {
      if (fabs(psi_raw_max - psi_raw_min - psi_checked_max + psi_checked_min) < gSIM_EPS) {
         hp_fov.dpsi = psi_checked_max - psi_checked_min + delta_psi_fovcenter / 2;
      } else {
         if (hp_fov.theta - hp_fov.dtheta / 2. < - PI / 2 || hp_fov.theta + hp_fov.dtheta / 2. > PI / 2) {
            hp_fov.dpsi = 2. * PI ;
         } else if (psi_checked_max - psi_checked_min < 2.* PI - delta_psi_fovcenter) {
            // FOV crosses psi = 0.
            hp_fov.dpsi = psi_checked_max - psi_checked_min + delta_psi_fovcenter / 2 ;
         } else if (psi_raw_max - psi_raw_min < 2.* PI - delta_psi_fovcenter) {
            // FOV crosses psi = PI:
            hp_fov.dpsi = psi_raw_max - psi_raw_min + delta_psi_fovcenter / 2 ;
         } else {
            printf("\n====> ERROR: gal_j2D() in janalysis.cc");
            printf("\n             This is a bug...");
            printf("\n                psi_raw_min: %le [deg]", psi_raw_min * RAD_to_DEG);
            printf("\n                psi_raw_max: %le [deg]", psi_raw_max * RAD_to_DEG);
            printf("\n                psi_checked_min: %le [deg]", psi_checked_min * RAD_to_DEG);
            printf("\n                psi_checked_max: %le [deg]", psi_checked_max * RAD_to_DEG);
            printf("\n             => abort()\n\n");
            abort();
         }
      }
   }
   if (hp_fov.dpsi > 2. * PI) hp_fov.dpsi = 2. * PI;
   hp_fov.psi_size_deg = hp_fov.dpsi * RAD_to_DEG;

   hp_fov.psi_min = hp_fov.psi - hp_fov.dpsi / 2.; //rad
   hp_fov.psi_max = hp_fov.psi + hp_fov.dpsi / 2.; //rad

   // now, what about dtheta_coord, theta_min and theta_max?
   // if the FOV does not cross the poles, it's easy and overwrite theta_min, theta_max calculated before:
   // but there is an exception for the RECT mode, where dtheta_coord gets slightly bigger for large FOV:
   if (hp_fov.dtheta > 0. && hp_fov.dtheta_orth > 0. && hp_fov.theta - hp_fov.dtheta / 2. >= - PI / 2 && hp_fov.theta + hp_fov.dtheta / 2. <= PI / 2) {
      if (hp_fov.theta - hp_fov.dtheta / 2. < hp_fov.theta_min) hp_fov.theta_min = hp_fov.theta - hp_fov.dtheta / 2. ;   // rad
      if (hp_fov.theta + hp_fov.dtheta / 2. > hp_fov.theta_max) hp_fov.theta_max = hp_fov.theta + hp_fov.dtheta / 2. ;   // rad
   }
   // if not, for the DISK mode, we can get the result also easily with a simple expression:
   else if (hp_fov.dtheta > 0. && hp_fov.dtheta_orth > 0. && hp_fov.grid_mode == "DISK") {
      if (hp_fov.theta - hp_fov.dtheta / 2. >= - PI / 2) {
         hp_fov.theta_min = hp_fov.theta - hp_fov.dtheta / 2. ; // rad
      } else hp_fov.theta_min = - PI / 2. ;
      if (hp_fov.theta + hp_fov.dtheta / 2. <= PI / 2.) {
         hp_fov.theta_max = hp_fov.theta + hp_fov.dtheta / 2. ; // rad
      } else hp_fov.theta_max = + PI / 2. ;
   }

   // in all other cases (inclusive inverse mode), stay with the values theta_min, theta_max calculated
   // before and get:
   if ((hp_fov.dtheta < 0. || hp_fov.dtheta_orth < 0.) && fabs(hp_fov.dtheta_orth) < PI) hp_fov.dtheta_coord = PI ;
   else hp_fov.dtheta_coord = hp_fov.theta_max - hp_fov.theta_min ;
   hp_fov.theta_size_deg_coord = hp_fov.dtheta_coord * RAD_to_DEG ;

}

//______________________________________________________________________________
void skymap_sz(string const &param_file, double const &psi_deg, string const &theta_deg_str,
               string const &theta_orth_size_deg_str, double const &theta_size_deg,
               double const &user_rse, bool is_subs_list)
{

   fov_struct my_fov; // contains all required FoV information
   hp_fov_init(param_file, psi_deg, theta_deg_str, theta_orth_size_deg_str, theta_size_deg, my_fov);

   string out_name = "SZ2D";
   out_name = "sz_" + out_name;
   string filename_core_str ;
   char f_name[200];

   if (my_fov.grid_mode == "DISK")
      sprintf(f_name, "%s_LOS%.0f,%.0f_FOVdiameter%.1fdeg_alphaint%.2fdeg_nside%d",
              out_name.c_str(), my_fov.psi_deg_corr, my_fov.theta_deg, theta_size_deg,
              gSIM_ALPHAINT * RAD_to_DEG, gSIM_HEALPIX_NSIDE);
   else
      sprintf(f_name, "%s_LOS%.0f,%.0f_FOV%.0fx%.0f_alphaint%.2fdeg_nside%d",
              out_name.c_str(), my_fov.psi_deg_corr, my_fov.theta_deg, my_fov.theta_orth_size_deg, theta_size_deg,
              gSIM_ALPHAINT * RAD_to_DEG, gSIM_HEALPIX_NSIDE);

   filename_core_str.append(f_name) ;

   string filename_fits_str = gSIM_OUTPUT_DIR + filename_core_str + ".fits";
   string filename_root_str = gSIM_OUTPUT_DIR + filename_core_str + ".root";
   string powspecfilename_str = gSIM_OUTPUT_DIR + "powerspec_" + filename_core_str + ".fits";
   string popstudy_drawn_filename_str = gSIM_OUTPUT_DIR + "popstudy_drawn_" + filename_core_str + ".root";
   string popstudy_list_filename_str = gSIM_OUTPUT_DIR + "popstudy_list_" + filename_core_str + ".root";

   const char *filename_fits_char = filename_fits_str.c_str();
   fitshandle fh_file_out ;

   if (gSIM_IS_PRINT) {
      ifstream isfile(filename_fits_char);
      if (isfile) {
         printf("\n====> WARNING: sz_map() in sz.cc");
         printf("\n               Output file %s was already existing", filename_fits_char);
         printf("\n               => It has been overwritten...\n\n");
         remove(filename_fits_char);
      }
      fh_file_out.create(filename_fits_str);
   }


   double pixdist_max;

   //--------- define pixdist_max according to user map requirement ----------------

   // short-cut for DISK mode:
   if (my_fov.grid_mode == "DISK" && my_fov.dtheta < 2. * PI) {
      if (my_fov.dtheta > 0) pixdist_max = my_fov.dtheta / 2. ;
      else {
         //inverse mode:
         pixdist_max = PI + my_fov.dtheta / 2. ;
         my_fov.theta = - my_fov.theta;
         my_fov.psi = my_fov.psi + PI;
      }
      // short-cut for whole sky (includes DISK mode, as dtheta_orth is set to 2 * PI in this case):
   } else if (my_fov.dtheta_orth == 2 * PI || my_fov.dtheta == PI) {
      pixdist_max = PI ;
   } else {
      double pixdist ;
      vec3_t<double> vec3_FOVcenter ;
      vec3_t<double> vec3_pix;
      pointing ptg_FOVcenter ;
      vector<double> arr_FOVcenter(3, 0.);

      if (my_fov.dtheta_orth > 0 && my_fov.dtheta > 0) {
         ptg_FOVcenter.theta = PI / 2 - my_fov.theta;
         ptg_FOVcenter.phi = my_fov.psi;
      } else {
         // inverse mode:
         my_fov.theta = - my_fov.theta;
         my_fov.psi = my_fov.psi + PI;
         ptg_FOVcenter.theta = PI / 2 - my_fov.theta;
         ptg_FOVcenter.phi = my_fov.psi;
      }
      vec3_FOVcenter = ptg_FOVcenter.to_vec3();
      for (int i = 0; i < my_fov.n_pix_fov; ++i) {
         vec3_pix = my_fov.hp_gridprop->pix2vec(my_fov.v_pix_fov[i]) ;
         pixdist = hp_angular_dist(vec3_FOVcenter, vec3_pix);
         if (pixdist > pixdist_max) pixdist_max = pixdist ;
      }
   }
   // enlarge pixdist_max a bit:
   if (pixdist_max + gSIM_RESOLUTION >= PI) pixdist_max = PI ;
   else pixdist_max += gSIM_RESOLUTION ;
   //--------- pixdist_max is now defined ----------------

   //--------- initialise extragal mass function  ----------------
   double zmin_mf = 0.02;
   double zmax_mf = 2.;
   //int nz = 10;
   double Mmin_mf = 1.e14 / 0.7; //M_solar for mass function
   double Mmax_mf = 1.e15 / 0.7; //M_solar for mass function
   //int nm = 50;
   //init_extragal(zmin_mf, zmax_mf, nz, Mmin_mf, Mmax_mf, nm, kTINKER10, kTOP_HAT);

   int nz_newgrid = 100;
   int nm_newgrid = 100;
   double dz = double((zmax_mf - zmin_mf) / nz_newgrid);
   double dlnM = double((log(Mmax_mf) - log(Mmin_mf)) / nm_newgrid);

   //Initialising redshift and mass vectors on the newgrid
   vector<double> vec_z, vec_lnM;
   for (int i = 0; i <= nz_newgrid; i++) vec_z.push_back(zmin_mf + i * dz);
   for (int i = 0; i <= nm_newgrid; i++) vec_lnM.push_back(log(Mmin_mf) + i * dlnM);

   TH2D *h_dN_dlnMhdz = new TH2D("bill", "title", vec_z.size(), vec_z[0] - dz / 2., vec_z[vec_z.size() - 1] + dz / 2., vec_lnM.size(), vec_lnM[0] - dlnM / 2., vec_lnM[vec_lnM.size() - 1] + dlnM / 2.);
   for (int i = 0; i < (int)vec_z.size(); i++) {
      for (int j = 0; j < (int)vec_lnM.size(); j++) {
         // h_dN_dlnMhdz->Fill(vec_z[i], vec_lnM[j], gCOSMO_DNDVHDLNMH_Z->Interpolate(vec_z[i], vec_lnM[j]));
      }
   }

   // reduce a the range a little bit to avoid boudary problems
   //double par_ref[4] = {zmin_mf *(1. + 1.e-5), zmax_mf *(1. - 1.e-5), Mmin_mf *(1. + 1.e-5), Mmax_mf *(1. - 1.e-5)};

   // number of halo to draw in the mass and redshift range, given mass function and FoV
   double N_halo = 0.;
   //dNdOmega(par_ref, N_halo);
   double fov_area_deg2 = atof(theta_orth_size_deg_str.c_str()) * theta_size_deg; // deg^2
   double fov_area_sr = fov_area_deg2 / SR_to_DEG2;

   cout << "Fullsky = " << 4 * PI << " sr, fov_area_sr = " << fov_area_sr << endl;;
   //N_halo=int(N_halo*fov_area_sr);
   N_halo = int(N_halo * 4 * PI);

   cout << N_halo << " to be drawn..." << endl;
   N_halo = 1; //200;
   //--------- extragal mass function is now initialised  ----------------


   char fname_drawn[200];
   //char fname_list[200];

   gRandom = new TRandom(); // create new random number generator
   gRandom->SetSeed(gSIM_SEED);

   double z, lnM;

   vector<struct gStructSZHalo> list_cluster;
   int counter, counter_dec;
   vector<pair<double, int> > list_jcl;
   vector<double> y_drawn(my_fov.n_pix_fov, 1.e-40); // compton-y array map
   vector<double> y_tot(my_fov.n_pix_fov, 1.e-40);


   FILE *fp_drawn = NULL;
   if (my_fov.grid_mode == "DISK")
      sprintf(fname_drawn, "%s%s_LOS%.0f,%.0f_FOVdiameter%.1fdeg_rse%.0f_alphaint%.2fdeg_nside%d.drawn",
              gSIM_OUTPUT_DIR.c_str(), out_name.c_str(), my_fov.psi_deg_corr, my_fov.theta_deg, theta_size_deg,
              user_rse, gSIM_ALPHAINT * RAD_to_DEG, gSIM_HEALPIX_NSIDE);
   else
      sprintf(fname_drawn, "%s%s_LOS%.0f,%.0f_FOV%.0fx%.0f_rse%.0f_alphaint%.2fdeg_nside%d.drawn",
              gSIM_OUTPUT_DIR.c_str(), out_name.c_str(), my_fov.psi_deg_corr, my_fov.theta_deg, my_fov.theta_orth_size_deg, theta_size_deg,
              user_rse, gSIM_ALPHAINT * RAD_to_DEG, gSIM_HEALPIX_NSIDE);
   if (gSIM_IS_PRINT) {
      fp_drawn = fopen(fname_drawn, "w");
      fprintf(fp_drawn, "# This file lists the properties and cl(centre) for all drawn clumps\n");
      string unit = units_or_canvasname_1D(1, 2, 0);
      fprintf(fp_drawn, "#   #cluster  long     lat   |         M500            R500        dh_a       z       P_norm     | alpha  beta  gamma  profile  y_centre   Y_5R500\n");
      fprintf(fp_drawn, "#             [deg]    [deg] |        [Msol]           [kpc]      [kpc]     [-]     [keV cm^-3] |                                [-]         [-] \n");
      fprintf(fp_drawn, "#\n");
   }

   counter = 0;
   counter_dec = 0;
   double alpha_ref = gSIM_ALPHAINT;

   //need to read in file giving number of pixel to use for FFT as function of halo angular extension
   //hard-coded values for the time being
   double angsize_ref[] = {3.59791, 4.65745, 6.28731, 8.7834, 12.5746, 18.3025, 27.0223, 40.2228, 60.0389, 89.6315, 133.613, 198.589};
   int npix_fft_ref[] = {400, 400, 400, 200, 200, 200, 50, 50, 50, 50, 50, 100};
   int is_pl_ref[] = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

   //--------- Draw clusters according to random direction and mass function  -----------
   for (int i = 0; i < N_halo; i++) {

      // Choosing random direction for cluster i over the whole sky
      double u = gRandom->Uniform(0., 1.);
      double v = gRandom->Uniform(0., 1.);
      double psi_cl = 2 * PI * u;
      double theta_cl = -PI / 2. + acos(2.*v - 1.);

      theta_cl = 0.;
      psi_cl = 0.;
      check_psi(psi_cl);

      //Drawing mass and redshift of cluster i from regrided mass function
      //    g_h_dN_dlnMhdz->GetRandom2(z,lnM); // orgiginal grid
      h_dN_dlnMhdz->GetRandom2(z, lnM);
      double mass_cl = exp(lnM); //*gCOSMO_HUBBLE; // [Msun]

      mass_cl = 5.e14;
      z = 0.02;
      if (z<zmin_mf or z>zmax_mf or mass_cl<Mmin_mf or mass_cl>Mmax_mf) {
         i--;
         continue;
      }
      if (i % 1000 == 0) cout << "... progress ... N = " << i << "/" << N_halo << endl;

      // cout << theta_cl << " " << psi_cl << " " << z << " " <<  mass_cl <<endl;

      pointing ptg_cl(PI / 2 - theta_cl, psi_cl) ;
      // find the closest Pixel to the clump centre
      int i_pix_closest = my_fov.hp_gridprop->ang2pix(ptg_cl) ;

      // if cluster falls in FOV*FOV, add its contribution
      if (my_fov.rs_pix_fov.contains(i_pix_closest)) {
         ++counter;
         ++counter_dec;

         double par_prof[3] = {gSZ_REF_SHAPE_PARAMS[0],
                               gSZ_REF_SHAPE_PARAMS[1],
                               gSZ_REF_SHAPE_PARAMS[2]
                              };
         const int npar = 10;
         double par_tot[npar];


         sz_m500_to_par(mass_cl, z, gSZ_REF_C500, par_prof, par_tot);
         double r500 = sz_m500_to_r500(mass_cl, z);
         par_tot[5] = kZHAO;
         par_tot[6] = r500 * 10.; // kpc (halo size)
         par_tot[7] = spline_interp(z, gCOSMO_Z_GRID, gCOSMO_D_ANG_GRID) * 1.e3 / gCOSMO_HUBBLE; // kpc
         par_tot[8] = psi_cl;
         par_tot[9] = theta_cl;

         // Add cluster properties in list
         char tmp_n_cl[50];
         sprintf(tmp_n_cl, "cluster#%d", counter);
         gStructSZHalo s_cluster;
         s_cluster.Name = tmp_n_cl;
         s_cluster.Type = "CLUSTER_SZ";
         s_cluster.PsiDeg = psi_cl * RAD_to_DEG;
         s_cluster.ThetaDeg = theta_cl * RAD_to_DEG;;
         s_cluster.z = z;
         s_cluster.Dist_ang = par_tot[7];// kpc
         s_cluster.Dist_lum = spline_interp(z, gCOSMO_Z_GRID, gCOSMO_D_LUM_GRID) * 1.e3 / gCOSMO_HUBBLE; // kpc
         s_cluster.M500 = mass_cl;
         s_cluster.C500 = gSZ_REF_C500;
         s_cluster.Pnorm = par_tot[0];
         s_cluster.Rscale = par_tot[1]; // kpc
         s_cluster.ShapeParam1 = par_tot[2];
         s_cluster.ShapeParam2 = par_tot[3];
         s_cluster.ShapeParam3 = par_tot[4];
         list_cluster.push_back(s_cluster);

         double five_R500 = par_tot[6] / 2.; // kpc
         double alpha_5R500 = (five_R500 / par_tot[7]) * 180.*60. / PI; // arcmin
         cout << alpha_5R500 / 60. << endl;
         // Given angular size, find optimum map size fpr convolution
         int ii = 0;
         while (alpha_5R500 > angsize_ref[ii] && ii < (int)SIZEOF_ARRAY(angsize_ref) - 1) {
            ii++;
         }
         int is_sz;
         // is_sz = 0 --> J calculation, not sz
         // is_sz = N --> sz calculation, smoothing performed on N x N grid
         // is_sz = -1 --> sz calculation, smoothing done using point-like approximation
         if (is_pl_ref[ii] == 1) is_sz = -1;
         else is_sz = npix_fft_ref[ii];


         bool is_subs = false;
         double mtot = 0.;
         double f_dm = 0.;
         // double par_dpdv[10];
         // double par_subs[24];
         double ntot_subs = 0;
         double y_bkg = 0.;

         // Add all pixels in the map
         // Force to gSIM_EPS_DRAWN relative precision for subclumps, otherwise too slow
         add_halo_in_map(par_tot, user_rse, y_drawn, y_bkg, my_fov.rs_pix_fov, is_subs, mtot, f_dm, NULL, NULL, ntot_subs, is_sz);


         double y_cl_centre = sz_ysmooth(par_tot, psi_cl, theta_cl, gSIM_EPS);
         gSIM_ALPHAINT = alpha_5R500 * PI / (60.*180.);
         double l1 = max(0., par_tot[7] - par_tot[6]);
         double l2 = par_tot[7] + par_tot[6];
         int switch_f = 0;
         gSIM_IS_SZ = 0;
         double Y_5R500 = (SIGMA_THOMSON * BARN_to_CM2) * los_integral(par_tot, switch_f, psi_cl, theta_cl, l1, l2, user_rse) * pow(180.*60 / PI, 2.) / (CM_to_KPC * MASS_E); // arcmin^2
         gSIM_IS_SZ = 1;


         // //latitude move to find background value for y_bkg
         // //cannot do it in longitude because of singularities at the North and South poles
         // double theta_bkg=theta_cl+alpha_bkg;
         // double y_bkg=sz_ysmooth(par_tot, psi_cl, theta_bkg, user_rse);

         // if(y_bkg < 1.e-40) y_bkg=1.e-40;

         // //cout << "alpha_bkg = " << alpha_bkg*180./3.14159 << ", y_bkg = " << y_bkg << ", y_centre = "<<y_cl_centre << ", Y_5R500 = "<<Y_5R500<<endl;

         // printf(" %7d   %+6.1f  %+6.1f   %.2le %.2le %.2le %.2f  %.2le   %.2f   %.2f   %.2f   %s     %le\n",
         //        counter, psi_cl * RAD_to_DEG, theta_cl * RAD_to_DEG, mass_cl, par_tot[1], s_cluster.Dist_ang,
         //        z, par_tot[0], par_tot[2], par_tot[3], par_tot[4],
         //         gNAMES_PROFILE[int(par_tot[5])], y_cl_centre);

         // cout<<"\n"<<endl;

         if (gSIM_IS_PRINT) {
            fprintf(fp_drawn, " %7d    %+6.1f  %+6.1f           %.2le        %.2le    %.2le   %.2f    %.2le      %.2f   %.2f   %.2f   %s     %le    %le\n",
                    counter, psi_cl * RAD_to_DEG, theta_cl * RAD_to_DEG, mass_cl, r500, s_cluster.Dist_ang,
                    z, par_tot[0], par_tot[2], par_tot[3], par_tot[4],
                    gNAMES_PROFILE[int(par_tot[5])], y_cl_centre, Y_5R500);
         }
      }
   }
   gSIM_ALPHAINT = alpha_ref;

   if (gSIM_IS_PRINT) {
      fclose(fp_drawn);
      cout << "  ... drawn clusters [ASCII] written in: " << fname_drawn << endl;
   }


   int hp_scheme_bool = 1;
   if (gSIM_HEALPIX_SCHEME == RING) {
      hp_scheme_bool = 1 ;
   } else if (gSIM_HEALPIX_SCHEME == NEST) {
      hp_scheme_bool = 2 ;
   } ;


   // first two extensions are always written in this fixed order:
   //int ext_id_ext1 = 0 ; // extension name: y, Y/pixel area
   //int ext_id_ext2 = 1 ; // extension name: Y, integrated over pixel area

   //int filenamelength = (int)strlen(filename_fits_char);
   // create string containing all string-like input variables for parsing to
   // fits output header:
   string inputval_string = "SZ";
   //const char *inputval_char = inputval_string.c_str();
   //int inputvallength = (int)strlen(inputval_char) ;

   float *inputval_nums = new float[22];
   inputval_nums[0] = my_fov.psi_deg_corr ;
   inputval_nums[1] = my_fov.theta_deg ;
   inputval_nums[2] = my_fov.theta_orth_size_deg ;
   inputval_nums[3] = theta_size_deg ;
   inputval_nums[4] = gSIM_ALPHAINT * RAD_to_DEG ; // gSIM_ALPHAINT_DEG
   inputval_nums[5] = gSIM_GAUSSBEAM_SZ_FWHM * RAD_to_DEG ;
   inputval_nums[6] = my_fov.delta_omega ;
   inputval_nums[7] = gSIM_SEED ;
   inputval_nums[8] = user_rse ; // gSIM_EPS
   inputval_nums[9] = gSIM_HEALPIX_NSIDE;
   inputval_nums[10] = hp_scheme_bool;
   inputval_nums[11] = gSZ_REF_C500;
   inputval_nums[12] = gSZ_REF_PRESSURE_NORM;
   inputval_nums[13] = gSZ_REF_SHAPE_PARAMS[0];
   inputval_nums[14] = gSZ_REF_SHAPE_PARAMS[1] ;
   inputval_nums[15] = gSZ_REF_SHAPE_PARAMS[2];
   inputval_nums[16] = gSZ_REF_ALPHA_MYX;
   inputval_nums[17] = gSZ_FLAG_MASSFUNCTION ;
   inputval_nums[18] = gCOSMO_RHO0_C;
   inputval_nums[19] = gCOSMO_OMEGA0_M;
   inputval_nums[20] = gCOSMO_OMEGA0_LAMBDA;
   inputval_nums[21] = gSIM_GAUSSBEAM_SZ_FWHM * RAD_to_DEG ;

   //int is_astro_units;
   //if (gSIM_IS_ASTRO_OR_PP_UNITS) is_astro_units = 1;
   //else is_astro_units = 0;

   // write first extension: All values multiplied by pixel size --> Y-parameter
   vector<double> y_drawn_sr(my_fov.n_pix_fov, 1.e-40);
   double int_area = 2. * PI * (1 - cos(gSIM_ALPHAINT)) ;
   cout << int_area << endl;
   for (int i = 0; i < my_fov.n_pix_fov; ++i) {
      if (y_drawn[i] > 1.e-40)
         y_drawn_sr[i] = y_drawn[i] * int_area ;
   }

   //int is_sz = gSIM_IS_SZ;

   fits_write_clumpymaps(fh_file_out, my_fov.v_pix_fov, y_drawn) ;

   // write second extension: Compton Y - integrated over pixel solid angle
   fits_write_clumpymaps(fh_file_out, my_fov.v_pix_fov, y_drawn_sr) ;

   delete my_fov.hp_gridprop;
}


//______________________________________________________________________________
double sz_ysmooth(double par_tot[10], double const &psi_los, double const &theta_los,
                  double const &eps)
{
   //--- Returns Compton y from pressure profile P(r):
   //  [Dimensionless]
   //  par_tot[0]    P_tot host normalisation [keV cm^-3]
   //  par_tot[1]    P_tot host scale radius [kpc]
   //  par_tot[2]    P_tot host shape parameter #1
   //  par_tot[3]    P_tot host shape parameter #2
   //  par_tot[4]    P_tot host shape parameter #3
   //  par_tot[5]    P_tot host card_profile [gENUM_PROFILE]
   //  par_tot[6]    P_tot host radius [kpc]
   //  par_tot[7]    l_HC: angular diameter distance to cluster centre [kpc]
   //  par_tot[8]    psi_HC: longitude of cluster centre [rad]
   //  par_tot[9]    theta_HC: latitude of cluster centre [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  eps           Relative precision sought for integration

   // Set integration distances (to encompass the host halo)
   double l1 = max(0., par_tot[7] - par_tot[6]);
   double l2 = par_tot[7] + par_tot[6];
   double res = sz_ysmooth(par_tot, psi_los, theta_los, l1, l2, eps);
   return res;
}

//______________________________________________________________________________
double sz_ysmooth(double par_tot[10], double const &psi_los, double const &theta_los,
                  double const &l1, double const &l2, double const &eps)
{
   // //--- Returns Compton y integrated between [l1,l2] from a pressure profile P_tot(r):
   // //  [Dimensionless]
   // //  par_tot[0]    rho_tot host normalisation [Msol kpc^{-3}]
   // //  par_tot[1]    rho_tot host scale radius [kpc]
   // //  par_tot[2]    rho_tot host shape parameter #1
   // //  par_tot[3]    rho_tot host shape parameter #2
   // //  par_tot[4]    rho_tot host shape parameter #3
   // //  par_tot[5]    rho_tot host card_profile [gENUM_PROFILE]
   // //  par_tot[6]    rho_tot host radius [kpc]
   // //  par_tot[7]    l_HC: distance observer to host centre [kpc]
   // //  par_tot[8]    psi_HC: longitude of host centre [rad]
   // //  par_tot[9]    theta_HC: latitude of host centre [rad]
   // //  psi_los       psi_los: longitude for the l.o.s [rad]
   // //  theta_los     theta_los: latitude for the l.o.s [rad]
   // //  l1            Lower value for the l.o.s. integration boundary [kpc]
   // //  l2            Upper value for the l.o.s. integration boundary [kpc]
   // //  eps           Relative precision sought for integration

   // int switch_f = 0;
   // //   for(int i=0;i<10;i++) cout << par_tot[i] << endl;
   // double res=los_integral(par_tot, switch_f, psi_los, theta_los, l1, l2, eps);


   //--- fn_beta_alpha = sin(alpha) \int_{lmin}^{lmax} f(l,beta,alpha; psi,theta) dl.
   //
   // INPUTS:
   //  alpha      Integration angle measured from the ref. l.o.s. [rad]
   //  par[0]     rho_1(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[1]     rho_1(r) scale radius [kpc]
   //  par[2]     rho_1(r) shape parameter #1
   //  par[3]     rho_1(r) shape parameter #2
   //  par[4]     rho_1(r) shape parameter #3
   //  par[5]     rho_1(r) card_profile [gENUM_PROFILE]
   //  par[6]     rho_1(r) radius [kpc]
   //  par[7]     rho_1(r) distance to observer [kpc]
   //  par[8]     rho_1(r) psi_center [rad]
   //  par[9]     rho_1(r) theta_center [rad]
   //  par[10]    switch_rho: selects which combination of rho_1 and rho_2 to use
   //                0 -> rho(r) = rho1(r)
   //                1 -> rho(r) = rho1(r)-rho2(r)
   //                2 -> rho(r) = rho1(r)*rho2(r)
   //  par[11]    rho_2(r) normalisation: density [Msol/kpc^3] or proxy for dPdV [kpc^{-3}]
   //  par[12]    rho_2(r) scale radius [kpc]
   //  par[13]    rho_2(r) shape parameter #1
   //  par[14]    rho_2(r) shape parameter #2
   //  par[15]    rho_2(r) shape parameter #3
   //  par[16]    rho_2(r) card_profile [gENUM_PROFILE]
   //  par[17]    rho_2(r) radius [kpc]
   //  par[18]    rho_2(r) distance to observer [kpc]
   //  par[19]    rho_2(r) psi_center [rad]
   //  par[20]    rho_2(r) theta_center [rad]
   //  par[21]    switch_f: selects the function f to integrate
   //                0 -> rho:           for jsub_continuum (also proxy for dP/dV)
   //                1 -> rho^2[or rho]: annihilation[decay] for a single clump or DM halo
   //                2 -> l^2*rho:       for clump number/mass fraction estimates (proxy for dP/dV)
   //                3 -> l^3*rho:       to calculate <l>
   //                4 -> l^4*rho:       to calculate var(l)
   //                5 -> l^-2*rho:      to calculate var(J)
   //                6 -> l^2*rho^2:     to check with Lum direct calculation (integrand corresponds to d3Lum)
   //  par[22]    psi_los: longitude of the line of sight [rad]
   //  par[23]    theta_los: latitude of the line of sight [rad]
   //  par[24]    lmin: lower l.o.s. integration boundary [kpc]
   //  par[25]    lmax: upper l.o.s. integration boundary [kpc]
   //  par[26]    eps: relative precision sought for integrations
   //  par[27]    phi_los: direction between GC and l.o.s. (psi,theta) [rad]
   //  par[28]    beta: integr. angle in the plane perp. to the l.o.s. [rad]
   //  par[29]    alpha: integr. angle measured from the ref. l.o.s. [rad]
   //  par[30]    phi_los_peak: angle between phi_los and density peak [rad]
   //  par[31]    r_trick: radius of the point-like contribution [kpc]
   //  par[32]    l_offset: for the integr. along  l': l'=l+l_offset, dl'=dl [kpc]
   //  par[33]    is_increasing_l (true/false if integr. corresponds to increas./decreas. l)
   //  par[34]    Is this halo (rho1) the Galactic halo [true or false]
   //  par[35]    Do rho1 and rho2 have the same centre [true or false]
   // OUTPUT:
   //  res        Result of sin(alpha) \int_{lmin}^{lmax} f(l,beta,alpha; psi,theta) dl

   double res;
   double par[36];

   for (int i = 0; i < 10; i++) par[i] = par_tot[i];
   par[10] = 0;
   for (int j = 11; j < 21; j++) par[j] = 0.;
   par[21] = 0;
   par[22] = psi_los;
   par[23] = theta_los;
   par[24] = l1;
   par[25] = l2;
   par[26] = eps;
   for (int j = 27; j < 35; j++) par[j] = 0.;
   par[35] = 1;


   double alpha = 0.;
   fn_beta_alpha(alpha, par, res);
   return (SIGMA_THOMSON * BARN_to_CM2) * res / (CM_to_KPC * MASS_E);
}


//______________________________________________________________________________
void sz_1D_profiles(string const &param_file, double const &eps, double const &mass, double const &redshift)
{

   //load_parameters(param_file, false); MORITZ CHECK
   //double zmin_mf = 0.02;
   //double zmax_mf = 2.;
   //int nz = 10;
   //double Mmin_mf = 1.e14; //M_solar for mass function
   //double Mmax_mf = 2.e15; //M_solar for mass function
   //int nm = 500;

   //init_extragal(zmin_mf, zmax_mf, nz, Mmin_mf, Mmax_mf, nm, kTINKER10, kTOP_HAT);

   string file = gSIM_OUTPUT_DIR + "sz_pressure1D.dat";
   ofstream myfile;
   myfile.open(file.c_str());

   double mass_cl = mass;
   double z = redshift;
   double par_prof[3] = {gSZ_REF_SHAPE_PARAMS[0],
                         gSZ_REF_SHAPE_PARAMS[1],
                         gSZ_REF_SHAPE_PARAMS[2]
                        };
   const int npar = 10;
   double par_tot[npar];

   sz_m500_to_par(mass_cl, z, gSZ_REF_C500, par_prof, par_tot);
   double r500 = sz_m500_to_r500(mass_cl, z);
   par_tot[5] = kZHAO;
   par_tot[6] = r500 * 5.; // kpc (halo size)
   par_tot[7] = spline_interp(z, gCOSMO_Z_GRID, gCOSMO_D_ANG_GRID) * 1.e3 / gCOSMO_HUBBLE; // kpc
   par_tot[8] = 0.; //psi_cl;
   par_tot[9] = 0.; //theta_cl;
   for (int i = 0; i < 10; i++) cout << par_tot[i] << endl;


   double h70 = gCOSMO_HUBBLE * 100. / 70.; // H0/(70 km/s/Mpc)
   double *params = NULL;
   double p500 = 1.65e-3 * pow(mass_cl / 3.e14, 2. / 3.) * h70 * h70 * pow(H0_over_H(z, params), -8. / 3.);

   vector <double> x;
   double x_min = 0.001 * r500;
   double x_max = 5.*r500;
   int Npts = 1000;
   x.push_back(x_min);
   double step = pow(10., log10(x_max / x_min) / (double)(Npts - 1));



   for (int k = 1; k < Npts - 1; ++k) x.push_back(x[k - 1]*step);
   x.push_back(x_max);

   for (int i = 0; i < Npts; i++) {
      double res = p_ZHAO(x[i], par_tot);
      //    myfile << x[i]/r500 << " " << res/p500 << endl; // to be compared to fig.8 or Arnaud et al (2010)
      myfile << x[i] / r500 << " " << res / p500 << " " <<  x[i]*h70 << " " << res / sqrt(h70) << endl; // col 1-2 (resp 3-4) to be compared to fig.8 (resp. fig.1) or Arnaud et al (2010)
   }
   myfile.close();


   file = gSIM_OUTPUT_DIR + "sz_y1D.dat";
   myfile.open(file.c_str());
   for (int k = 0; k < 10; k++) cout << par_tot[k] << " " ;
   cout << "\n";

   for (int i = 0; i < Npts; i++) {
      double psi_los = 0.;
      double theta_los = x[i] / par_tot[7];
      double y_cl = sz_ysmooth(par_tot, psi_los, theta_los, eps);
      myfile << theta_los * 180.*60 / PI << " " << y_cl * 1.e6 << endl; // to be compared to fig.7 of Planck SZ maps papers
      //cout << theta_los*180.*60/PI << " " << y_cl*1.e6 << endl; // to be compared to fig.7 of Planck SZ maps papers
   }
   myfile.close();

}

//______________________________________________________________________________
void test_y_Y(string const &param_file, double const &eps)
{

   // Computes the y-parameter and the integrated Y_5R500 parameters for a set of clusters
   // distributed according to the Tinker et al (2010) mass function

   //load_parameters(param_file, false); MORITZ CHECK

   double zmin_mf = 0.02;
   double zmax_mf = 2.;
   //int nz = 10;
   double Mmin_mf = 1.e14; //M_solar for mass function
   double Mmax_mf = 1.e15; //M_solar for mass function
   //int nm = 50;
   //init_extragal(zmin_mf, zmax_mf, nz, Mmin_mf, Mmax_mf, nm, kTINKER10, kTOP_HAT);

   int nz_newgrid = 100;
   int nm_newgrid = 100;
   double dz = double((zmax_mf - zmin_mf) / nz_newgrid);
   double dlnM = double((log(Mmax_mf) - log(Mmin_mf)) / nm_newgrid);

   //Initialising redshift and mass vectors on the newgrid
   vector<double> vec_z, vec_lnM;
   for (int i = 0; i <= nz_newgrid; i++) vec_z.push_back(zmin_mf + i * dz);
   for (int i = 0; i <= nm_newgrid; i++) vec_lnM.push_back(log(Mmin_mf) + i * dlnM);

   cout << vec_z.size() << " " << vec_lnM.size() << endl;

   TH2D *h_dN_dlnMhdz = new TH2D("bill", "title", vec_z.size(), vec_z[0] - dz / 2., vec_z[vec_z.size() - 1] + dz / 2., vec_lnM.size(), vec_lnM[0] - dlnM / 2., vec_lnM[vec_lnM.size() - 1] + dlnM / 2.);
   for (int i = 0; i < (int)vec_z.size(); i++) {
      for (int j = 0; j < (int)vec_lnM.size(); j++) {
         cout << i << " " << j << " " << vec_z[i] << " " << vec_lnM[j] << endl;
         //h_dN_dlnMhdz->Fill(vec_z[i], vec_lnM[j], gCOSMO_DNDVHDLNMH_Z->Interpolate(vec_z[i], vec_lnM[j]));
         cout << h_dN_dlnMhdz->GetBinContent(i, j) << endl;
      }
   }

   vec_z.clear();
   vec_lnM.clear();

   //double par_ref[4] = {zmin_mf *(1. + 1.e-5), zmax_mf *(1. - 1.e-5), Mmin_mf *(1. + 1.e-5), Mmax_mf *(1. - 1.e-5)};

   // number of halo to draw in the mass and redshift range, given mass function and FoV
   double N_halo = 0.;
   //dNdOmega(par_ref, N_halo);


   N_halo = int(N_halo * 4 * PI);

   cout << N_halo << " to be drawn..." << endl;
   N_halo = 5.e3;

   //--------- extragal mass function is now initialised  ----------------

   //  char fname_drawn[200];
   //char fname_list[200];

   gRandom = new TRandom(); // create new random number generator
   gRandom->SetSeed(gSIM_SEED);

   double z, lnM;
   int counter, counter_dec;

   FILE *fp_drawn = NULL;
   string fname = gSIM_OUTPUT_DIR + "cluster_stat.dat";

   if (gSIM_IS_PRINT) {
      fp_drawn = fopen(fname.c_str(), "w");
      fprintf(fp_drawn, "# This file lists the properties and cl(centre) for all drawn clumps\n");
      string unit = units_or_canvasname_1D(1, 2, 0);
      fprintf(fp_drawn, "#   #cluster  long     lat   |         M500            R500        dh_a       z       P_norm     | alpha  beta  gamma  profile  y_centre       Y_5R500  \n");
      fprintf(fp_drawn, "#             [deg]    [deg] |        [Msol]           [kpc]      [kpc]     [-]     [keV cm^-3] |                                [-]         [arcmin2] \n");
      fprintf(fp_drawn, "#\n");
   }

   counter = 0;
   counter_dec = 0;
   //int percentage = 1. ;
   //int progress ;
   //  double alpha_ref=gSIM_ALPHAINT;

   //--------- Draw clusters according to random direction and mass function  -----------
   for (int i = 0; i < N_halo; i++) {

      // Choosing random direction for cluster i
      double u = gRandom->Uniform(0., 1.);
      double v = gRandom->Uniform(0., 1.);
      double psi_cl = 2 * PI * u;
      double theta_cl = -PI / 2. + acos(2.*v - 1.);

      //Drawing mass and redshift of cluster i from mass function
      // gCOSMO_DNDVHDLNMH_Z->GetRandom2(z,lnM);

      h_dN_dlnMhdz->GetRandom2(z, lnM);
      double mass_cl = exp(lnM); //*gCOSMO_HUBBLE; // [Msun]
      if (z<zmin_mf or z>zmax_mf or mass_cl<Mmin_mf or mass_cl>Mmax_mf) {
         i--;
         continue;
      }
      if (i % 1000 == 0) cout << "... progress ... N = " << i << "/" << N_halo << endl;


      ++counter;
      ++counter_dec;

      double par_prof[3] = {gSZ_REF_SHAPE_PARAMS[0],
                            gSZ_REF_SHAPE_PARAMS[1],
                            gSZ_REF_SHAPE_PARAMS[2]
                           };
      const int npar = 10;
      double par_tot[npar];


      sz_m500_to_par(mass_cl, z, gSZ_REF_C500, par_prof, par_tot);
      double r500 = sz_m500_to_r500(mass_cl, z);
      par_tot[5] = kZHAO;
      par_tot[6] = r500 * 10.; // kpc (halo size)
      par_tot[7] = spline_interp(z, gCOSMO_Z_GRID, gCOSMO_D_ANG_GRID) * 1.e3 / gCOSMO_HUBBLE; // kpc

      //theta_cl=0.;
      //psi_cl=0.;

      par_tot[8] = psi_cl;
      par_tot[9] = theta_cl;

      // Add cluster properties in list
      char tmp_n_cl[50];
      sprintf(tmp_n_cl, "cluster#%d", counter);
      gStructSZHalo s_cluster;
      s_cluster.Name = tmp_n_cl;
      s_cluster.Type = "CLUSTER_SZ";
      s_cluster.PsiDeg = psi_cl * RAD_to_DEG;
      s_cluster.ThetaDeg = theta_cl * RAD_to_DEG;;
      s_cluster.z = z;
      s_cluster.Dist_ang = par_tot[7];// kpc
      s_cluster.Dist_lum = spline_interp(z, gCOSMO_Z_GRID, gCOSMO_D_LUM_GRID) * 1.e3 / gCOSMO_HUBBLE; // kpc
      s_cluster.M500 = mass_cl;
      s_cluster.C500 = gSZ_REF_C500;
      s_cluster.Pnorm = par_tot[0];
      s_cluster.Rscale = par_tot[1]; // kpc
      s_cluster.ShapeParam1 = par_tot[2];
      s_cluster.ShapeParam2 = par_tot[3];
      s_cluster.ShapeParam3 = par_tot[4];


      double y_cl_centre = sz_ysmooth(par_tot, psi_cl, theta_cl, eps);
      double five_R500 = par_tot[6] / 2.; // kpc
      double alpha_5R500 = (five_R500 / par_tot[7]) * 180.*60. / PI; // arcmin
      gSIM_ALPHAINT = alpha_5R500 * PI / (60.*180.);
      double l1 = max(0., par_tot[7] - par_tot[6]);
      double l2 = par_tot[7] + par_tot[6];
      int switch_f = 0;
      gSIM_IS_SZ = 0;
      double Y_5R500 = (SIGMA_THOMSON * BARN_to_CM2) * los_integral(par_tot, switch_f, psi_cl, theta_cl, l1, l2, eps) * pow(180.*60 / PI, 2.) / (CM_to_KPC * MASS_E); // arcmin^2
      gSIM_IS_SZ = 1;
      fprintf(fp_drawn, " %7d    %+6.1f  %+6.1f           %.2le        %.2le    %.2le   %.2f    %.2le      %.2f   %.2f   %.2f   %s  %le   %le\n",
              counter, psi_cl * RAD_to_DEG, theta_cl * RAD_to_DEG, mass_cl, r500, s_cluster.Dist_ang,
              z, par_tot[0], par_tot[2], par_tot[3], par_tot[4],
              gNAMES_PROFILE[int(par_tot[5])], y_cl_centre, Y_5R500);
   }
   fclose(fp_drawn);
}

//______________________________________________________________________________
double *psf_image(double sizex, double sizey, int Nx, int Ny, double sigma)
{

   // Returns a 1d array of size Nx*Ny in row-major order containing the beam map
   // of a gaussian beam of sigma

   double binx = sizex / Nx;
   double biny = sizey / Ny;
   double *psf_in = new double[Nx * Ny];
   for (int i = 0; i < Nx; i++) {
      double x = -sizex / 2. + i * binx;
      for (int j = 0; j < Ny; j++) {
         int ij = i * Ny + j;
         double y = -sizey / 2. + j * biny;
         double r = sqrt(x * x + y * y); // arcmin
         double res;
         res = exp(-r * r / (2.*sigma * sigma)) / (sigma * sigma * 2.*PI);
         psf_in[ij] = res;
      }
   }

   return psf_in;
}

//______________________________________________________________________________
double *y_image(double par_tot[10], double sizex, double sizey, int Nx, int Ny)
{

   // Returns a 1d array of size Nx*Ny in row-major order containing the
   // y-map to be smoothed

   double binx = sizex / Nx; //arcmin
   double biny = sizey / Ny; //arcmin
   double *y_in = new double[Nx * Ny];
   double psi_cl = par_tot[8]; //rad
   double theta_cl = par_tot[9]; //rad
   double arcmin2rad = PI / (180.*60.);

   //cout << "y at cluster centre =  " << sz_ysmooth(par_tot,0., 0., gSIM_EPS) << endl;
   for (int i = 0; i < Nx; i++) {
      double dpsi = (-sizex / 2. + i * binx) * arcmin2rad;
      for (int j = 0; j < Ny; j++) {
         int ij = i * Ny + j;
         double dtheta = (-sizey / 2. + j * biny) * arcmin2rad;
         double res;
         //double sigma=10./2.3548; // arcmin
         res = sz_ysmooth(par_tot, psi_cl + dpsi, theta_cl + dtheta, gSIM_EPS);
         y_in[ij] = res;
      }
   }

   return y_in;
}

//______________________________________________________________________________
double *y_convolved2d(double *y_arr, double *psf_arr, int N_row, int N_col)
{
   // Performs the convolution of the y-map y_arr with the beam-map psf_arr.
   // this is done using the convolution theorem, i.e.
   // convolution in real space = multiplication in fourier space.
   // Makes use of FFTW library

   fftw_plan p_forward, p_convol;


   // Real-to-complex FFT of y-map
   fftw_complex *y_fft_out = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * N_row * (N_col / 2 + 1));


   p_forward = fftw_plan_dft_r2c_2d(N_row, N_col, y_arr, y_fft_out, FFTW_ESTIMATE);
   fftw_execute(p_forward);

   // Real-to-complex FFT of beam map
   fftw_complex *psf_fft_out = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * N_row * (N_col / 2 + 1));

   p_forward = fftw_plan_dft_r2c_2d(N_row, N_col, psf_arr, psf_fft_out, FFTW_ESTIMATE);
   fftw_execute(p_forward);

   // do complex multiplication in Fourier space
   fftw_complex *C = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * N_row * (N_col / 2 + 1));
   for (int i = 0; i < N_row; ++i)
      for (int j = 0; j < N_col / 2 + 1; ++j) {
         int ij = i * (N_col / 2 + 1) + j;
         C[ij][0] = (y_fft_out[ij][0] * psf_fft_out[ij][0]
                     - y_fft_out[ij][1] * psf_fft_out[ij][1]); // real part
         C[ij][1] = (y_fft_out[ij][0] * psf_fft_out[ij][1]
                     + y_fft_out[ij][1] * psf_fft_out[ij][0]); // complex part
      }

   // Complex-to-real FFT of complex map product
   // Note that this convolution in not properly normalised due to FFTW implementation
   // By-hand normalisation (multiplying the results by (binsize_x*binsize_y)/(N_row*N_col))
   // is performed after this function has been called

   double *y_fft_convol = new double [N_row * N_col];
   p_convol = fftw_plan_dft_c2r_2d(N_row, N_col, C, y_fft_convol, FFTW_ESTIMATE);
   fftw_execute(p_convol);

   fftw_destroy_plan(p_forward);
   fftw_destroy_plan(p_convol);

   fftw_free(C);
   fftw_free(y_fft_out);
   fftw_free(psf_fft_out);

   return y_fft_convol;

}
//______________________________________________________________________________
void write_ascii_map(string const fname, int N_row, int N_col, double *map)
{
   // Writes an ascii file Nrow*Ncol from the 1d array "map".

   ofstream myfile;
   myfile.open(fname.c_str());
   for (int i = 0; i < N_row; i++) {
      for (int j = 0; j < N_col; j++) {
         myfile << map[i * N_col + j] << " ";
      }
      myfile << "\n";
   }
   myfile.close();

   return;
}


//______________________________________________________________________________
void test_pointlike(string const &param_file, double fwhm)
{
   // fwhm in arcmin


   //load_parameters(param_file, false); MORITZ CHECK

   double arcmin_to_rad = PI / (180.*60.);
   //double zmin_mf = 0.02;
   //double zmax_mf = 2.;
   //int nz = 10;
   //double Mmin_mf = 1.e13; //M_solar for mass function
   //double Mmax_mf = 1.e15; //M_solar for mass function
   //int nm = 500;
   //init_extragal(zmin_mf, zmax_mf, nz, Mmin_mf, Mmax_mf, nm, kTINKER10, kTOP_HAT);

   double sigma_beam = fwhm / 2.3548; // arcmin
   double mass = 1.e14; // M500 in Msun

   for (double z = 1.9; z >= 0.021; z /= 1.5) {
      //loop on redshifts to put cluster at various distances, i.e. various angular sizes

      //compute cluster properties
      double par_prof[3] = {gSZ_REF_SHAPE_PARAMS[0],
                            gSZ_REF_SHAPE_PARAMS[1],
                            gSZ_REF_SHAPE_PARAMS[2]
                           };
      const int npar = 10;
      double par_tot[npar];
      sz_m500_to_par(mass, z, gSZ_REF_C500, par_prof, par_tot);
      double r500 = sz_m500_to_r500(mass, z);
      par_tot[5] = kZHAO;
      par_tot[6] = r500 * 10.; // kpc (halo size)
      par_tot[7] = spline_interp(z, gCOSMO_Z_GRID, gCOSMO_D_ANG_GRID) * 1.e3 / gCOSMO_HUBBLE; // kpc
      par_tot[8] = 0.;
      par_tot[9] = 0.;

      double five_R500 = par_tot[6] / 2.; // kpc
      double alpha_5R500 = (five_R500 / par_tot[7]) * 180.*60. / PI; // arcmin
      double size = alpha_5R500 + 10 * sigma_beam; // arcmin

      cout << "map size = " << size << " arcmin " << endl;

      int switch_f = 0;
      gSIM_IS_SZ = 0;
      double l1 = max(0., par_tot[7] - par_tot[6]);
      double l2 = par_tot[7] + par_tot[6];
      double alpha_ref = gSIM_ALPHAINT;
      gSIM_ALPHAINT = alpha_5R500 * arcmin_to_rad;
      //double DeltaOmega=2. * PI * (1 - cos(gSIM_ALPHAINT));
      double Y_5R500 = (SIGMA_THOMSON * BARN_to_CM2) * los_integral(par_tot, switch_f, par_tot[8], par_tot[9], l1, l2, gSIM_EPS) * pow(180.*60 / PI, 2.) / (CM_to_KPC * MASS_E); // arcmin^2
      gSIM_IS_SZ = 1;
      gSIM_ALPHAINT = alpha_ref;

      double y_mean = Y_5R500; ///(DeltaOmega*SR_to_ARCMIN2);
      cout << "ymean = " << y_mean << endl;
      std::ostringstream strs;
      strs << alpha_5R500;
      std::string s = strs.str();

      for (int N = 40; N < 2000; N *= 2000) {
         std::ostringstream strs2;
         strs2 << N;
         string s2 = strs2.str();

         //convolve assuming point-like contribution from cluster = Y_5R500/DeltaOmega
         string fname = "test_output/y_convol1d_N=" + s2 + "_PL_size=" + s + ".dat";
         ofstream myfile;
         myfile.open(fname.c_str());

         double bin = size / N;
         for (int i = 0; i < N / 2; i++) {
            double pos = i * bin;
            double y_convol1d_PL = y_mean * exp(-pos * pos / (2.*sigma_beam * sigma_beam)) / (sigma_beam * sigma_beam * 2.*PI);
            myfile << pos << " " << y_convol1d_PL << endl;
         }
         myfile.close();

         // do proper convolution for various number of pixels
         double scale = bin * bin / (N * N);


         fname = "test_output/y_convol1d_N=" + s2 + "_size=" + s + ".dat";
         myfile.open(fname.c_str());
         double *psf_arr = psf_image(size, size, N, N, sigma_beam);
         double *y_arr = y_image(par_tot, size, size, N, N);
         double *y_convol2d = y_convolved2d(y_arr, psf_arr, N, N);

         fname = "test_output/input_image_for_fft_N=" + s2 + "_size=" + s + ".dat"; // Note that the quadrants 1-3 and 2-4 are swapped.
         write_ascii_map(fname, N, N, y_arr);
         fname = "test_output/psf_image_for_fft_N=" + s2 + "_size=" + s + ".dat"; // Note that the quadrants 1-3 and 2-4 are swapped.
         write_ascii_map(fname, N, N, psf_arr);
         fname = "test_output/convol_image_N=" + s2 + "_size=" + s + ".dat"; // Note that the quadrants 1-3 and 2-4 are swapped.
         for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
               int ij = i * N + j;
               // y_convol2d[ij]*=scale;
               y_convol2d[ij] = y_convol2d[ij] * scale;
            }
         }

         write_ascii_map(fname, N, N, y_convol2d);

         //int i=0;
         for (int j = 0; j < N / 2; j++) {
            bin = double(size / N); // arcmin
            double pos = j * bin;
            myfile << pos << " " << y_convol2d[j] << endl;
         }
         myfile.close();
         delete[] psf_arr;
         delete[] y_arr;
         delete[] y_convol2d;
      }
   }

}

//______________________________________________________________________________
void y_convolved1d(double par_tot[10], double sigma_beam, int N, vector<vector<double> > &y1d)
{
   // Returns the Compton-y 1d profile y1d of the cluster characterised by par_tot[10] and convolved by a Gaussian beam with width=sigma_beam.
   // This is done using a N*N pixel map if N>0 or analytically using the point-like approximation if N=-1
   // y1d should be initialised before calling this function by vector<vector<double>> y1d(2, vector<double>(N_choice/2));


   double five_R500 = par_tot[6] / 2.; // kpc
   double alpha_5R500 = (five_R500 / par_tot[7]) * 180.*60. / PI; // arcmin
   double size = 2 * (alpha_5R500 + 5 * sigma_beam); // arcmin
   double arcmin_to_rad = PI / (180.*60.);

   //cout << alpha_5R500 << " " << gSIM_GAUSSBEAM_SZ_FWHM*180*60./PI<< " "<<sigma_beam << " map size = " << size << " arcmin " << endl;

   if (y1d.size() == 0)
      abort();
   else if (y1d[0].size() == 0)
      abort();

   int N_choice = y1d[0].size();
   double bin = size / (2 * N_choice);

   if (sigma_beam == 0.) { // perfect instrument --> return raw, unconvolved y values
      for (int i = 0; i < N_choice; i++) {
         double pos = i * bin * arcmin_to_rad; // rad
         y1d[0][i] = pos / arcmin_to_rad; // arcmin
         y1d[1][i] = sz_ysmooth(par_tot, 0., pos, gSIM_EPS);
         //cout << i << " " << y1d[0][i]<< " " << y1d[1][i]<< endl;
      }
   } else { //There is a convolution to perform
      if (N == -1) {
         //Use analytical point-like approximation for the cluster
         int switch_f = 0;
         gSIM_IS_SZ = 0;
         double l1 = max(0., par_tot[7] - par_tot[6]);
         double l2 = par_tot[7] + par_tot[6];
         double alpha_ref = gSIM_ALPHAINT;
         gSIM_ALPHAINT = alpha_5R500 * arcmin_to_rad;
         double Y_5R500 = (SIGMA_THOMSON * BARN_to_CM2) * los_integral(par_tot, switch_f, par_tot[8], par_tot[9], l1, l2, gSIM_EPS) * pow(180.*60 / PI, 2.) / (CM_to_KPC * MASS_E); // arcmin^2
         gSIM_IS_SZ = 1;
         gSIM_ALPHAINT = alpha_ref;

         for (int i = 0; i < N_choice; i++) {
            double r = i * bin; //arcmin
            y1d[0][i] = r;
            y1d[1][i] = Y_5R500 * exp(-r * r / (2.*sigma_beam * sigma_beam)) / (sigma_beam * sigma_beam * 2.*PI);
            //cout << i << " "<< y1d[0][i]<< " " << y1d[1][i]<< endl;
         }
      } else { //Performs numerical convolution
         double scale = bin * bin / (N * N);
         double *y_arr = y_image(par_tot, size, size, N, N);
         double *psf_arr = psf_image(size, size, N, N, sigma_beam);
         double *y_convol2d = y_convolved2d(y_arr, psf_arr, N, N);
         for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
               int ij = i * N + j;
               y_convol2d[ij] = y_convol2d[ij] * scale;
            }
         }
         for (int i = 0; i < N_choice; i++) {
            double r = i * bin; //arcmin
            y1d[0][i] = r;
            y1d[1][i] = y_convol2d[i]; // half of top row i convolution occured

         }
         delete[] psf_arr;
         delete[] y_arr;
         delete[] y_convol2d;
         psf_arr = NULL;
         y_arr = NULL;
         y_convol2d = NULL;
      }
   }
}


