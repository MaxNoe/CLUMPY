# include <stdlib.h>
# include <stdio.h>
# include <time.h>
# include <fftw3.h>

int main();
void test02(void);
double frand(void);

/******************************************************************************/

int main()

/******************************************************************************/
/*
  Purpose:
    MAIN is the main program for FFTW3_PRB.

  Discussion:
    FFTW3_PRB tests the FFTW3 library.

  Licensing:
    This code is distributed under the GNU LGPL license.

  Modified:
    05 November 2007

  Author:
    John Burkardt
*/
{
   printf("\n");
   printf("FFTW3_PRB\n");
   printf("  C version\n");
   printf("  Test the FFTW3 library.\n");

   test02();

   /*
     Terminate.
   */
   printf("\n");
   printf("FFTW3_PRB\n");
   printf("  Normal end of execution.\n");
   printf("\n");

   return 0;
}

void test02(void)

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/*
  Purpose:
    TEST02: apply FFT to real 1D data.

  Licensing:
    This code is distributed under the GNU LGPL license.

  Modified:
    23 October 2005

  Author:
    John Burkardt
*/
{
   int i;
   double *x;
   double *h;
   double *xRecovered;
   double *hRecovered;
   double *convolvedSig;
   int n = 30;
   int nIR = 3;
   int nOut = n + nIR - 1; // length of the ultimately convolved signal
   int nc;
   fftw_complex *X;
   fftw_complex *H;
   fftw_complex *fftMulti;
   fftw_plan plan_backward;
   fftw_plan plan_backwardIR;
   fftw_plan plan_backwardConv;
   fftw_plan plan_forward;
   fftw_plan plan_forwardIR;
   unsigned int seed = 12345678;

   printf("\n");
   printf("TEST02\n");
   printf("  Demonstrate FFTW3 on a single vector of real data.\n");
   printf("\n");
   printf("  Transform data to FFT coefficients.\n");
   printf("  Perform complex multiplication in the frequency domain.\n");
   printf("  Backtransform FFT coefficients to recover data.\n");
   printf("  IFFT the multiplied array to gather the convolved signal.\n");
   printf("  Compare recovered data to original data.\n");
   printf("  Evaluate the result of the convolution.\n");
   /*
     Set up an array to hold the data, and assign the data.
   */

   //Set up input arrays for input and impulse response signals
   //x = fftw_malloc ( sizeof ( double ) * n );
   //h = fftw_malloc (sizeof (double) * nIR );
   x = new double[n];
   h = new double[nIR];

   srand(seed);

   //Populate x with random elements between 0 and 1
   for (i = 0; i < n; i++) {
      x[i] = frand();
   }

   //Populate h with random elements between 0 and 1
   for (i = 0; i < nIR; i++) {
      h[i] = frand();
   }

   printf("\n");
   printf("  Input Data:            Impulse Response\n");
   printf("\n");

   for (i = 0; i < n; i++) {
      //Input audio signal
      printf("  %4d  %12f", i, x[i]);
      if (i < nIR) //assume length h(n) < length x(n)
         printf("  %4d  %12f", i, h[i]);

      printf("\n");

   }

   /*
     Set up arrays to hold the transformed data, get "plans", and execute the plans to
     transform the lowercase (time-domain) data to the uppercase (frequency-domain)
   */

   //Number of complex values nc (half convolved signal length + 1, because FFTW
   //doesn't return the redundant half of the FFT)
   nc = (nOut / 2) + 1;

   //set up output array with the number of complex elements nOut
   //X = fftw_malloc ( sizeof ( fftw_complex ) * nOut );
   //H = fftw_malloc ( sizeof (fftw_complex) * nOut );
   X = new fftw_complex[nOut];
   H = new fftw_complex[nOut];

   //set up the forward plan dfts
   plan_forward = fftw_plan_dft_r2c_1d(nOut, x, X, FFTW_ESTIMATE);
   plan_forwardIR = fftw_plan_dft_r2c_1d(nOut, h, H, FFTW_ESTIMATE);

   // PERFORM FFTs!!
   fftw_execute(plan_forward);
   fftw_execute(plan_forwardIR);

   //set up frequency-multiplied complex array
   // fftMulti = fftw_malloc(sizeof (fftw_complex) * nc);
   fftMulti = new fftw_complex[nc];

   printf("\n");
   printf("  Output FFT Coefficients:        IR FFT Coefficients:          Multiplied Coefficients:\n");
   printf("\n");

   for (i = 0; i < nc; i++) {

      //calculate real and imaginary components for the multiplied array
      fftMulti[i][0] = X[i][0] * H[i][0] - X[i][1] * H[i][1]; //real component
      fftMulti[i][1] = X[i][0] * H[i][1] + X[i][1] * H[i][0]; //imaginary component

      printf("  %4d  %10f  %10f", i, X[i][0], X[i][1]);
      printf("  %4d  %10f  %10f", i, H[i][0], H[i][1]);
      printf("  %4d  %10f  %10f", i, fftMulti[i][0], fftMulti[i][1]);
      printf("\n");
   }



   /*
     Set up arrays to hold the backtransformed (recovered) data,
     get "plans", and execute the plans to backtransform the uppercase
    (frequency-domain) data to the "Recovered" (new time-domain) data.
   */

   //set up new arrays to hold the recovered signals
   //  xRecovered = fftw_malloc ( sizeof ( double ) * nOut );
   //  hRecovered = fftw_malloc (sizeof (double) * nOut);
   xRecovered = new double[nOut];
   hRecovered = new double[nOut];

   //set up the convolved signal array
   //convolvedSig = fftw_malloc(sizeof (double) * nOut);
   convolvedSig = new double[nOut];

   plan_backwardConv = fftw_plan_dft_c2r_1d(nOut, fftMulti, convolvedSig, FFTW_ESTIMATE);
   plan_backward = fftw_plan_dft_c2r_1d(nOut, X, xRecovered, FFTW_ESTIMATE);
   plan_backwardIR = fftw_plan_dft_c2r_1d(nOut, H, hRecovered, FFTW_ESTIMATE);


   //IFFT the original signals
   fftw_execute(plan_backward);
   fftw_execute(plan_backwardIR);

   //IFFT the complex product
   fftw_execute(plan_backwardConv);

   printf("\n");
   printf("  Recovered input:          Recovered IR:       Convolved Signal:\n");
   printf("\n");

   for (i = 0; i < nOut; i++) {
      printf("  %4d  %12f", i, (i < n) ? xRecovered[i] / (double)(nOut) : 0);
      printf("     %4d  %12f", i, (i < nIR) ? hRecovered[i] / (double)(nOut) : 0);
      printf("  %4d  %12f", i, convolvedSig[i] / (double)(nOut));
      printf("\n");
   }

   /*
     Release the memory associated with the plans.
   */
   fftw_destroy_plan(plan_backward);
   fftw_destroy_plan(plan_forward);
   fftw_destroy_plan(plan_backwardIR);
   fftw_destroy_plan(plan_forwardIR);
   fftw_destroy_plan(plan_backwardConv);


   fftw_free(x);
   fftw_free(xRecovered);
   fftw_free(X);
   fftw_free(h);
   fftw_free(hRecovered);
   fftw_free(H);
   fftw_free(fftMulti);
   fftw_free(convolvedSig);

   return;
}


//*****************************************************************************/

double frand(void)

//*****************************************************************************/
/*
  Purpose:

    FRAND returns random values between 0 and 1.*/
{
   double value;

   value = ((double) rand() / (RAND_MAX));

   return value;
}
