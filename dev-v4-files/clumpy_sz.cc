
// C++ std libraries
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <iostream>
#include <vector>
using namespace std;

// ROOT includes
#include <TF3.h>
#include <TH2D.h>
#include <TMath.h>
#include <TRandom.h>

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/geometry.h"
#include "../include/healpix_fits.h"
#include "../include/inlines.h"
#include "../include/janalysis.h"
#include "../include/jeans_analysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/spectra.h"
#include "../include/stat.h"
#include "../include/cosmo.h"
#include "../include/extragal.h"
#include "../include/sz.h"



int main(int argc, char *argv [])
{

   // ./bin/clumpy_sz clumpy_params.txt 180. 0. 4. 4. 1

   gSIM_IS_PRINT = true;
   gSIM_IS_SZ = true;

   // double zmin=0.;
   // double zmax=5.;
   // int nz=10;
   // double Mmin=5.e13/gCOSMO_HUBBLE; // in units of omega_matter h^-1 M_solar
   // double Mmax=1.e15/gCOSMO_HUBBLE; // in units of omega_matter h^-1 M_solar
   // int nm=500;

   // Initialize all extragalactic-related arrays to generate tabulated mass function gCOSMO_DNDVHDLNMH_Z
   //cout << "Initialise mass function" << endl;
   //init_extragal(zmin, zmax, nz, Mmin, Mmax, nm, kTINKER08_N, kTOP_HAT);

   //printf(" ./bin/clumpy_sz clumpy_params.txt 180. 0. 4. 4. 1\n");

   if (argc < 6)
      printf("   ./bin/clumpy_sz clumpy_params.txt 180. 0. 4. 4. 1\n");
   string file = argv[1];
   double psi_obs_deg = atof(argv[2]);
   string theta_obs_deg_str = argv[3];
   string dtheta_orth_deg_str = argv[4];
   double dtheta_deg = atof(argv[5]);
   double user_rse = atof(argv[6]);
   //load_parameters(file, false);


   cout << "here" << " " << file << endl;
   //skymap_sz(file, psi_obs_deg, theta_obs_deg_str, dtheta_orth_deg_str, dtheta_deg, user_rse, false);
   //  double mass = 3.93e14;
   //  double redshift=0.61;
   //  sz_1D_profiles(file, user_rse, mass, redshift);
   //test_y_Y(file, user_rse);
   //  double fwhm=10.;
   //test_pointlike(file, fwhm);
}


