#ifndef _CLUMPY_SZ_H_
#define _CLUMPY_SZ_H_

// C++ std libraries
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <algorithm>

typedef struct {
   double psi, psi_deg_corr, theta, theta_deg, theta_orth_size_deg, dtheta_orth, dtheta_orth_extended,
          dtheta, dtheta_extended, delta_omega, fsky, dpsi, dtheta_coord, theta_max, theta_min, psi_size_deg,
          psi_min, psi_max, theta_size_deg_coord, dtheta_size_deg_coord;
   int n_pix, n_pix_fov, n_pix_fov_extended, smooth_sz;
   string grid_mode, grid_mode_extended;
   rangeset<int> rs_pix_fov, rs_pix_fov_extended;
   vector<int> v_pix_fov, v_pix_fov_extended;
   T_Healpix_Base<int> *hp_gridprop;
   pointing ptg_i_pix;
} fov_struct;

struct gStructSZHalo {
   string Name;
   string Type;
   double PsiDeg;
   double ThetaDeg;
   double z;
   double Dist_ang;
   double Dist_lum;
   int    HaloProfile;
   double M500;
   double C500;
   double Pnorm;
   double Rscale;
   double ShapeParam1;
   double ShapeParam2;
   double ShapeParam3;
   double Triaxial_a;
   double Triaxial_b;
   double Triaxial_c;
   bool   Triaxial_Is;
   double Triaxial_rotalpha;
   double Triaxial_rotbeta;
   double Triaxial_rotgamma;
};

double    p_ZHAO(double &r, double par[5]);

void      hp_fov_init(string const &param_file, double const &psi_deg, string const &theta_deg_str, string const &theta_orth_size_deg_str, double const &theta_size_deg, fov_struct &hp_fov);

void      skymap_sz(string const &param_file, double const &psi_deg, string const &theta_deg_str, string const &theta_orth_size_deg_str, double const &theta_size_deg, double const &user_rse, bool is_subs_list);
double    sz_m500_to_r500(double &m500, double &z);
void      sz_m500_to_par(double &m500, double &z, double &c500, double par_prof[3], double par_tot[5]);
double    sz_ysmooth(double par_tot[10], double const &psi_los, double const &theta_los, double const &eps);
double    sz_ysmooth(double par_tot[10], double const &psi_los, double const &theta_los, double const &l1, double const &l2, double const &eps);
void      sz_1D_profiles(string const &param_file, double const &eps, double const &mass, double const &redshift);

void      test_pointlike(string const &param_file, double fwhm);
void      test_y_Y(string const &param_file, double const &eps);

double   *psf_image(double sizex, double sizey, int Nx, int Ny, double sigma);
void      y_convolved1d(double par_tot[10], double sigma_beam, int N, vector<vector<double> > &y1d);
double   *y_convolved2d(double *y_arr, double *psf_arr, int N_row, int N_col);
double   *y_image(double par_tot[10], double sizex, double sizey, int Nx, int Ny);

void      write_ascii_map(string const fname, int N_row, int N_col, double *map);
#endif
