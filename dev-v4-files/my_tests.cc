
// C++ std libraries
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <fftw3.h>
using namespace std;

// GSL includes
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_deriv.h>
#include <gsl/gsl_chebyshev.h>
#include <gsl/gsl_dht.h>


void make_data_arr(int Npts, double *x_arr, vector <double> &data_arr)
{
   double x;
   //double alpha=2.;
   //double beta=2.;
   //double gamma=0.;


   for (int i = 0; i < Npts; i++) {
      //x=pow(10.,x_arr[i]);
      x = x_arr[i];
      double res;
      if (x < 0.5) res = 1.;
      else res = 0;

      //res=1./(pow(x/0.1,gamma)*pow((1.+x/0.1),(beta-gamma)/alpha));
      //data_arr.push_back(log10(res));
      data_arr.push_back(res);
   }
   // cout <<"my data size = " << data_arr.size()<<endl;
   // cout <<"3ieme element = " << data_arr[2]<<endl;
}


double linlin_interp(double const &x, double const &x1, double const &x2,
                     double const &y1, double const &y2)
{
   //--- Returns lin-lin simple interpolation:
   //       y_interp = y1 + (x-x1)*(y2-y1)/(x2-x1).
   //
   //  x             Position at which to interpolate
   //  x1            Lower position for which y is known
   //  x2            Upper position for which y is known
   //  y1            Value of y(x) at x1
   //  y2            Value of y(x) at x2

   return y1 + (x - x1) * (y2 - y1) / (x2 - x1);
}

double loglog_interp(double const &x, double const &x1, double const &x2,
                     double const &y1, double const &y2)
{
   //--- Returns log-log simple interpolation:
   //       y_interp = y1 * (x/x1)^(log(y2/y1)/log(x2/x1)).
   //
   //  x             Position at which to interpolate
   //  x1            Lower position for which y is known
   //  x2            Upper position for which y is known
   //  y1            Value of y(x) at x1
   //  y2            Value of y(x) at x2

   return ((y1 <= 0. || y2 <= 0.) ? 0. :
           y1 * pow(x / x1, log10(y2 / y1) / log10(x2 / x1)));
}


template<typename T, int height, int width>
std::ostream &writemap(std::ostream &os, T(&map)[height][width])
{
   for (int i = 0; i < height; ++i) {
      for (int j = 0; j < width; ++j) {
         os << map[i][j] << " ";
      }
      os << "\n";
   }
   return os;
}


void make_image(vector <double> r_arr, vector <double> data_arr, bool is_in)
{

   const int Nx = 100, Ny = 100;
   double im_in[Nx][Ny];
   double sizex = 3., sizey = 3.; // arcmin
   double binx = sizex / Nx;
   double biny = sizey / Ny;

   for (int i = 0; i < Nx; i++) {
      double x = -sizex / 2. + i * binx;
      for (int j = 0; j < Ny; j++) {
         double y = -sizey / 2. + j * biny;
         double r = sqrt(x * x + y * y);
         double val;
         if (r > 2.9) {
            val = 0.;
            continue;
         } else {
            int k = 0;
            while (r > r_arr[k]) k++;
            if (k == 0) {
               val = data_arr[0];
            } else {
               val = loglog_interp(r, r_arr[k - 1], r_arr[k],
                                   data_arr[k - 1], data_arr[k]);
            }
         }
         im_in[i][j] = val;
      }
   }
   string file;
   if (is_in) file = "../test_output/input_image.dat";
   else file = "../test_output/convolved_image.dat";
   ofstream myfile;
   myfile.open(file.c_str());
   fstream of(file.c_str());//, ios::out | ios::app);
   if (of.is_open()) {
      writemap(of, im_in);
      of.close();
   }
}


double *input_image_for_fft(double sizex, double sizey, int Nx, int Ny)
{

   //double alpha=2.;
   //double beta=2.;
   //double gamma=0.;

   double binx = sizex / Nx;
   double biny = sizey / Ny;

   double *im_in = new double[Nx * Ny];
   string fname = "../test_output/input_image_for_fft.dat";
   ofstream myfile;
   myfile.open(fname.c_str());

   for (int i = 0; i < Nx; i++) {
      double x = -sizex / 2. + i * binx;
      for (int j = 0; j < Ny; j++) {
         int ij = i * Ny + j;
         double y = -sizey / 2. + j * biny;
         double r = sqrt(x * x + y * y);
         double res;
         if (r < 0.1) {
            res = 1.;
            cout << i << " " << j << endl;
         } else res = 0.;

         //res=1./(pow(r/0.1,gamma)*pow((1.+r/0.1),(beta-gamma)/alpha));
         im_in[ij] = res;
         myfile << res << " ";
      }
      myfile << "\n";
   }
   return im_in;
}


double *psf_image_for_fft(double sizex, double sizey, int Nx, int Ny, double sigma)
{

   //psf_in = 2d array size Nx,Ny
   double binx = sizex / Nx;
   double biny = sizey / Ny;
   double *psf_in = new double[Nx * Ny];
   string fname = "../test_output/beam_for_fft.dat";
   ofstream myfile;
   myfile.open(fname.c_str());

   for (int i = 0; i < Nx; i++) {
      double x = -sizex / 2. + i * binx;
      //double x=i*binx;
      for (int j = 0; j < Ny; j++) {
         int ij = i * Ny + j;
         double y = -sizey / 2. + j * biny;
         //double y=j*biny;
         double r = sqrt(x * x + y * y);
         double res;
         res = exp(-r * r / 2. / sigma / sigma) / (sigma * sigma * 2.*3.1415926);
         //if (i==0 && j==0) res=1.; else res=0.; // do nothing kernel
         psf_in[ij] = res;
         myfile << res << " ";
      }
      myfile << "\n";
   }
   return psf_in;
}




//==========================================================
int main()
{
   // int N=100;
   // double xminl=0.;
   // double xmaxl=50; //arcmin
   // double dlx=(xmaxl-xminl)/(N);

   // double nu=0.;// order of Bessel function
   // gsl_dht *t= gsl_dht_alloc(size_t(N));
   // int tmp=gsl_dht_init(t, nu, xmaxl);

   // double x_arr[N];
   // for(int i=0;i<N;i++){
   //   x_arr[i]=gsl_dht_x_sample (t, i);
   // }

   // vector <double> data_arr;
   // make_data_arr(N, &x_arr[0], data_arr);
   // // Forward Hankel transformation
   // double fout[N];
   // int trans=gsl_dht_apply (t, &data_arr[0], &fout[0]);

   // //Backward Hankel transformation - same as forward in GSL ==> need correction factor
   // //to fall back onto original function xmax^4/j_{\nu,M}^2 (see GSL documentation)
   // //double fout2[N];
   // //trans=gsl_dht_apply (t, &fout[0], &fout2[0]);
   // double zero=gsl_dht_k_sample (t, N);
   // double corr=pow(xmaxl,4.)/pow(zero*xmaxl,2.);
   // double psf_hankel[N]; // array of modes in reciprocal space
   // double psf[N]; // array of modes in reciprocal space
   double sigma = 5.; //arcmin
   // for (int i=0;i<N;i++){
   //   double r=x_arr[i];
   //   psf[i]=exp(-r*r/2./sigma/sigma)/(sigma*sigma*2*3.1415926);
   // }

   // trans=gsl_dht_apply (t, &psf[0], &psf_hankel[0]);

   // // for (int i=0;i<N;i++){
   // //   double k=gsl_dht_k_sample (t, i);
   // //   psf_hankel[i]=exp(-k*k*sigma*sigma/2.)/sqrt(2*3.1415926);
   // //   //cout << k << " " << psf_hankel[i] << " " << psf_hankel2[i] << endl;
   // // }

   // double psf_func_prod[N]; // array of modes in reciprocal space
   // for (int i=0;i<N;i++){
   //   double k=gsl_dht_k_sample (t, i);
   //   psf_func_prod[i]=psf_hankel[i]*fout[i];
   // }
   // double convol[N];
   // trans=2*3.1415926*gsl_dht_apply (t, &psf_func_prod[0], &convol[0]);
   // for (int i=0;i<N;i++){
   //   //   cout << i<< " " <<x_arr[i]<< " " << data_arr[i] << " " << psf[i] << " " << convol[i]/corr<< endl;
   //   convol[i]/=corr;
   // }
   // vector<double> r_vec(x_arr, x_arr+N);
   // vector<double> convol_vec(convol, convol+N);
   // make_image(r_vec, data_arr, true); // save input image
   // make_image(r_vec, convol_vec, false); // save convolved image


   double sizex = 50.; //arcmin
   double sizey = 50.; //arcmin
   int N_row = 500;
   int N_col = 500;
   double scale = 1.0 / (N_row * N_col);
   fftw_plan p_forward, p_backward, p_convol;

   double *im_fft_in = input_image_for_fft(sizex, sizey, N_row, N_col); // fill in im
   fftw_complex *im_fft_out = new fftw_complex [N_row * (N_col / 2 + 1)];
   //const int rank=2;
   //int nn[2]={N_row, N_col};
   p_forward = fftw_plan_dft_r2c_2d(N_row, N_col, im_fft_in, im_fft_out, FFTW_ESTIMATE);
   fftw_execute(p_forward);

   double *psf_fft_in = psf_image_for_fft(sizex, sizey, N_row, N_col, sigma); // fill in psf
   fftw_complex *psf_fft_out = new fftw_complex [N_row * (N_col / 2 + 1)];
   p_forward = fftw_plan_dft_r2c_2d(N_row, N_col, psf_fft_in, psf_fft_out, FFTW_ESTIMATE);
   fftw_execute(p_forward);

   //for(int i=0;i<N_row;i++){
   //  for(int j=0;j<N_col/2+1;j++){
   //    int ij=i*(N_col/2+1)+j;
   //    //      cout << ij << " " << psf_fft_out[ij][0] << " " << psf_fft_out[ij][1]<< " "<< im_fft_out[ij][0] << " " << im_fft_out[ij][1] << endl;
   //  }
   //}

   // do complex multiplication in fourier space
   fftw_complex *C = new fftw_complex[N_row * (N_col / 2 + 1)];
   for (int i = 0; i < N_row; ++i)
      for (int j = 0; j < N_col / 2 + 1; ++j) {
         int ij = i * (N_col / 2 + 1) + j;
         C[ij][0] = (im_fft_out[ij][0] * psf_fft_out[ij][0]
                     - im_fft_out[ij][1] * psf_fft_out[ij][1]) ; // real part
         C[ij][1] = (im_fft_out[ij][0] * psf_fft_out[ij][1]
                     + im_fft_out[ij][1] * psf_fft_out[ij][0]); // complex part
      }

   //check that input data is recovered when performing inverse fft (complex to real)
   double *im_fft_recovered = new double [N_row * N_col];
   p_backward = fftw_plan_dft_c2r_2d(N_row, N_col, im_fft_out, im_fft_recovered, FFTW_ESTIMATE);
   fftw_execute(p_backward);
   string fname = "../test_output/recovered_image.dat";
   ofstream myfile;
   myfile.open(fname.c_str());
   for (int i = 0; i < N_row; i++) {
      for (int j = 0; j < N_col; j++) {
         myfile << im_fft_recovered[i * N_col + j]*scale << " "; //need to scale recovered data (*1/(N_row*N_col))
      }
      myfile << "\n";
   }
   myfile.close();

   //check that psf is recovered when performing inverse fft (complex to real)
   double *psf_fft_recovered = new double [N_row * N_col];
   p_backward = fftw_plan_dft_c2r_2d(N_row, N_col, psf_fft_out, psf_fft_recovered, FFTW_ESTIMATE);
   fftw_execute(p_backward);
   fname = "../test_output/recovered_psf.dat";
   myfile.open(fname.c_str());
   for (int i = 0; i < N_row; i++) {
      for (int j = 0; j < N_col; j++) {
         myfile << psf_fft_recovered[i * N_col + j]*scale << " "; //need to scale recovered data (*1/(N_row*N_col))
      }
      myfile << "\n";
   }
   myfile.close();

   cout << "here" << endl;

   double *im_fft_convol = new double [N_row * N_col];
   p_convol = fftw_plan_dft_c2r_2d(N_row, N_col, C, im_fft_convol, FFTW_ESTIMATE);
   fftw_execute(p_convol);


   fname = "../test_output/convol_image.dat"; // Note that the quadrants 1-3 and 2-4 are swapped.
   myfile.open(fname.c_str());
   for (int i = 0; i < N_row; i++) {
      for (int j = 0; j < N_col; j++) {
         myfile << im_fft_convol[i * N_col + j]*scale << " ";
      }
      myfile << "\n";
   }
   myfile.close();
   cout << "The end" << endl;

   fftw_destroy_plan(p_forward);
   fftw_destroy_plan(p_backward);
   fftw_destroy_plan(p_convol);

   return 0;
}
