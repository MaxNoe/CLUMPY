#include <stdlib.h>

#include "../include/inlines.h"
#include "../include/misc.h"
#include "../include/params.h"

#include <fstream>
#include <iomanip>
#include <iostream>
#include <stdlib.h>
#include <string>
using namespace std;

//______________________________________________________________________________
int main(int argc, char *argv [])
{

   if (argc != 6) {
      printf("  usage:\n");
      printf("   %s clumpy_params_filename  list_params_to_edit  list_new_vals  output_name switch_name\n", argv[0]);
      printf("   [exemple]: %s clumpy_params.txt gSIM_ALPHAINT_DEG,gSIM_SEED  0.2,5   _5  1\n", argv[0]);
      printf("\n");
      printf("        - clumpy_params.txt: input file of clumpy parameters\n");
      printf("        - list_params_to_edit: comma-separated list of parameter (keywords) names [case insensitive]\n");
      printf("        - list_new_vals: coma-separated list of new values [case sensitive], as many as in list_params_to_edit\n");
      printf("        - output_or_extension_name: new name of file or extansion to append to current name [case sensitive]\n");
      printf("        - switch_name:\n");
      printf("             -1: automatic name (from changed values)\n");
      printf("              0: use 'output_name' as output name\n");
      printf("              1: use 'clumpy_params_filename'_'output_name' as output name\n");
      printf("\n\n");
      return 1;
   }


   // Open clumpy parameters file
   string fname_in = argv[1];
   ifstream file(fname_in.c_str());
   if (!file) {
      printf("\n====> ERROR: in clumpy_params_edit.cc");
      printf("\n             Could not open fine %s", fname_in.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }
   cout << ">>>>> Read " << fname_in << endl;


   // Extract list of param names and new values and check same number
   string keywords = argv[2];
   string newvals = argv[3];
   vector<string> l_keywords, l_newvals;
   string2list(keywords, ",", l_keywords);
   string2list(newvals, ",", l_newvals);
   int n_new = l_newvals.size();
   if ((int)l_keywords.size() != n_new) {
      printf("\n====> ERROR: in clumpy_params_edit.cc");
      printf("\n             Number of parameter names (%d) not matching number of new vals (%d)",
             (int)l_keywords.size(), n_new);
      printf("\n             => abort()\n\n");
      abort();
   }


   // Form output name (to save later)
   string fname_out = argv[4];
   int    switch_name = atoi(argv[5]);
   if (switch_name == -1) {
      // Form name from changed parameters
      string tmp = "";
      for (int i = 0; i < n_new; ++i)
         tmp += "_" + l_keywords[i] + l_newvals[i];
      root_trimname4object(tmp);
      fname_out = fname_in + tmp + ".txt";
   } else if (switch_name == 1) {
      // Append to input file name
      fname_out = fname_in + fname_out;
   }


   // Read file and save all lines to print later (we do not want to save if the user provided wrong paraeter names!)
   //vector<string> name_par;
   //vector<string> name, value;
   //vector<bool> is_filled;

   string line;
   vector<string> lines;
   vector<bool> is_found(n_new, false);
   while (getline(file, line)) {
      if (line[0] == '#' || line.empty()) {
         lines.push_back(line);
         continue;
      }
      vector<string> params;
      string2list(line, " ", params);
      if (params.size() < 3) {
         cout << ">>>> Current line read is: " << line << endl;
         cout << ">>>> wrong format, abort()" << endl;
         abort();
      }

      // Check if current line must be changed)
      string uc_param = upper_case(params[0]);
      bool is_param_keyword = false;
      for (int i = 0; i < n_new; ++i) {
         string uc_keyword = upper_case(l_keywords[i]);
         if (uc_param == uc_keyword) {
            char tmp[2000];
            sprintf(tmp, "%-40s%-14s%s", params[0].c_str(), params[1].c_str(), l_newvals[i].c_str());
            cout << " tmp=" << tmp;
            string new_line = (string)tmp;
            lines.push_back(new_line);
            is_found[i] = true;
            is_param_keyword = true;
            continue;
         }
      }
      if (!is_param_keyword)
         lines.push_back(line);
   }
   file.close();

   // Check that all values to edit where found
   for (int i = 0; i < n_new; ++i) {
      if (!is_found[i]) {
         cout << ">>>> Parameter " << upper_case(l_keywords[i]) << " not found in " << fname_in << endl;
         cout << ">>>> abort()" << endl;
         abort();
      }
   }


   // Save in output file
   ofstream f_out(fname_out.c_str());
   for (int i = 0; i < (int)lines.size(); ++i)
      f_out << lines[i] << endl;

   f_out.close();
   cout << ">>>>> Saved (new) in " << fname_out << endl;

   return 1;
}

/*! \file clumpy_param_edit.cc
  \brief <b>Edit entry value from clumpy parameter file</b>
*/
