import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import math
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from pylab import *
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy import interpolate
import os

plt.rc('text', usetex=True)
plt.rc('font', family='sans-serif')

mypath="/archeops/combet/RECHERCHE/vincent/clumpy_dev/branches/extragal/test_output"
plot_dir=mypath+"/figs"

file_arr=np.array(os.listdir(mypath))
file_select=[]
for file in file_arr:
    a=file.find("_N")
    if a != -1:
        file_select.append(file)
N_arr=[]
size_arr=[]
        
file_select=np.array(file_select)
for file in file_select:
    tmp=file.split('=')
    N_str=tmp[1].split('_')[0]
    size_str=tmp[2].split('.dat')[0]
    N_arr.append(N_str)
    size_arr.append(size_str)

#convert list to numpy array
N_arr=np.array(N_arr)
size_arr=np.array(size_arr)

#remove multiple entries
N_arr=np.unique(N_arr)
size_arr=np.unique(size_arr)

#convert string array to int/float array
#then sort
#then convert sorted int/float array back to string array
new=np.sort(N_arr.astype(np.int))
new=new.astype(np.str)
N_arr=new

new=np.sort(size_arr.astype(np.float))
new=new.astype(np.str)
size_arr=new

namef=np.array(["input_image_for_fft_N=","psf_image_for_fft_N=","convol_image_N="])

#for i in range(0, N_arr.size):
#    for k in range(0,size_arr.size):
#        fig, ax=plt.subplots(2,3)
#        for j in range(0,namef.size):
#            print N_arr[i], size_arr[k]
#            file=namef[j]+N_arr[i]+"_size="+size_arr[k]+".dat"
#            arr_in=np.loadtxt(file)
#            if j==0:
#                orig=arr_in
#                side=int(sqrt(arr_in.size))
#                
#            x=np.arange(-side/2,side/2,1)
#            y=np.arange(-side/2,side/2,1)
#            print j, file
#            im=ax[0,j].imshow(arr_in,  extent=[x.min(),x.max(), y.min(), y.max()], interpolation='nearest', norm=Normalize(vmin=arr_in.min(), vmax=arr_in.max()))
#            divider = make_axes_locatable(ax[0,j])
#            cax = divider.append_axes("right", size="5%", pad=0.05)
#            cbar = plt.colorbar(im, cax=cax, format="%.2e")
#            ax[0,j].xaxis.set_visible(False)
#            ax[0,j].yaxis.set_visible(False)
#
#        j=0
#        ax[1,j].set_yscale('log')
#        #ax[1,j].set_xscale('log')
#        ax[1,j].plot(arr_in[0,0:side/2], color='m')
#        ax[1,j].plot(orig[side/2,side/2:side], color='c')
#        
#
#        file1="y_convol1d_N="+N_arr[i]+"_PL_size="+size_arr[k]+".dat"      
#        file2="y_convol1d_N="+N_arr[i]+"_size="+size_arr[k]+".dat";
# 
#        my_dtype=[("pos", float), ("val", float)]
#        arr_1d_pl=np.loadtxt(file1, dtype=my_dtype)
#        arr_1d=np.loadtxt(file2, dtype=my_dtype)
#        ax[1,j+1].set_yscale('log')
#        #ax[j+1].set_xscale('log')
#        #ax[j+1].set_ylim([3.5e-8,3.5e-6])
#        ax[1,j+1].plot(arr_1d_pl["pos"], arr_1d_pl["val"], color='g')
#        ax[1,j+1].plot(arr_1d["pos"], arr_1d["val"], color='m')
#        
#        tmp=(arr_1d_pl["val"]-arr_1d["val"])*100/arr_1d["val"]
#        ax[1,j+2].plot(arr_1d["pos"], tmp, color='g')
#        
#        file_fig=plot_dir+"/res_N="+N_arr[i]+"_size="+size_arr[k]+".png"
#        print file_fig
#        savefig(file_fig)
#
#        print orig.sum()
#        print arr_in.sum()
#
#
sigma=10
crit_conv=5
crit_pl=10

myfile=open('alpha_vs_fft.txt','a+')
for k in range(0,size_arr.size):
    rmax=max(float(size_arr[k]),2*sigma)
    fig=plt.figure() 
    ax=fig.add_subplot(1,1,1)
    cmap = mpl.cm.jet
    for i in range(0,N_arr.size):
        file="y_convol1d_N="+N_arr[i]+"_size="+size_arr[k]+".dat";
        my_dtype=[("pos", float), ("val", float)]
        arr=np.loadtxt(file, dtype=my_dtype)
        file1="y_convol1d_N="+N_arr[i]+"_PL_size="+size_arr[k]+".dat"     
        arr_pl=np.loadtxt(file1, dtype=my_dtype)

        val=arr["val"]
        val_pl=arr_pl["val"]
        pos=arr["pos"]
        
        idx_arr1=np.where(arr["pos"] < rmax)
        idx_arr2=np.where(val > val.max()/10)
        print "N = ", N_arr[i], "rmax = ", rmax, " --> Considering ", np.array(idx_arr1).size, " elements" 
        if i != 0:
            # check whether the new value of N is converged w.r.t N-1 that 
            # is estimated using interpolation function f
            ratio_arr=abs(f1(pos[idx_arr1])-val[idx_arr1])*100/f(pos[idx_arr1])
            print np.array(idx_arr1).size, ratio_arr
            if(np.any(ratio_arr > crit_conv)):
                print N_arr[i]+" --> Not converged"
            else:
                print size_arr[k]+" converged for N = "+N_arr[i]
                ratio_arr=abs(val_pl[idx_arr2]-val[idx_arr2])*100/val[idx_arr2]
                print np.array(idx_arr2).size, ratio_arr
                if(np.any(ratio_arr > crit_pl)):
                    myfile.write(size_arr[k]+" "+N_arr[i]+" 0 \n")
                    print 'bad'
                else:
                    myfile.write(size_arr[k]+" "+N_arr[i]+" 1 \n")
                    print 'good'
                break
                
        #create interpolation function f based on pos and val
        #linear
        f = interpolate.interp1d(pos, val,bounds_error=False, fill_value=val[val.size-1])       
        #spline
        f1 = interpolate.InterpolatedUnivariateSpline(pos, val)
        
        ax.set_yscale('log')
        ax.plot(pos, val, marker='*', linestyle='None', markerfacecolor=cmap(i/float(N_arr.size)), markeredgecolor=cmap(i/float(N_arr.size)))
        pos_interp=np.arange(pos.min(), pos.max(), step=(pos.max()-pos.min())/2000)
        #ax.plot(pos_interp, f(pos_interp), color=cmap(i/float(N_arr.size)), linestyle='--', label="N = "+N_arr[i])
        ax.plot(pos_interp, f1(pos_interp),color=cmap(i/float(N_arr.size)), linestyle='-.'  , label="N = "+N_arr[i])
        plt.legend()

    file_fig=plot_dir+"/compare_N_size="+size_arr[k]+".png"
    print file_fig
    print "\n"
    savefig(file_fig)
myfile.close()

