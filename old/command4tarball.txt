1. Create CLUMPY_v2015.06.CPC_corr2.tar.gz (for download purpose on website)
 Move to CLUMPY_v2015.06.CPC_corr2/.
    make clean
    cd ..
    tar --exclude='.svn' --exclude="download/CLUMPY*" --exclude="output/*" --exclude="write_fits_mod.mod" --exclude="latex"  --exclude="output/*" --exclude="tests" --exclude="html" --exclude="DoxyStyle" --exclude="data/MCXC*" --exclude=bugtracker.txt  --exclude=bugs2fix_desiredfeatures.txt --exclude=changes.txt --exclude=command4tarball.txt -zcvf CLUMPY_v2015.06.CPC_corr2.tar.gz CLUMPY_v2015.06.CPC_corr2
    cd -
    cp ../CLUMPY_v2015.06.CPC_coor2.tar.gz download/.

2. Create CLUMPY_v2015.06.CPCv2_librarian.tar.gz (for CPC librarian)
 Move to CLUMPY_v2015.06.CPCv2/.
    make clean
    cd ..
    tar --exclude='.svn'  --exclude="tests"  --exclude="data/MCXC*" --exclude="output/*" --exclude="write_fits_mod.mod" --exclude="download/CLUMPY*.tar.gz*" --exclude=bugtracker.txt  --exclude=bugs2fix_desiredfeatures.txt --exclude=changes.txt --exclude=command4tarball.txt -zcvf CLUMPY_v2015.06.CPCv2_librarian.tar.gz CLUMPY_v2015.06.CPCv2

3. Create CLUMPY_v2015.06_www_corr2.tar.gz (for website)
    make clean
    cd ..
    tar --exclude='.svn' --exclude="latex"  --exclude="output/*"  --exclude="tests" --exclude="html" --exclude="write_fits_mod.mod" --exclude="DoxyStyle" --exclude="data/MCXC*" --exclude=bugtracker.txt --exclude=bugs2fix_desiredfeatures.txt --exclude=changes.txt --exclude=command4tarball.txt -zcvf CLUMPY_v2015.06_www_corr2.tar.gz CLUMPY-DEV/trunk
    cd -
