import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import math
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from pylab import *
plt.rc('text', usetex=True)
plt.rc('font', family='sans-serif')

dir='output/'


#============== TEST 0 ======================================================
my_dtype=[("z", float), ("dl", float), ("da", float), ("dvdz", float)]
A=np.loadtxt(dir+'check_cosmo.dat', dtype=my_dtype)
B=np.loadtxt('hogg_low_density.txt', delimiter=',', usecols=(0,))
C=np.loadtxt('hogg_low_density.txt', delimiter=',', usecols=(1,))
fig=plt.figure()
"""ax=fig.add_subplot(1,3,1)
plt.title('$\Omega_m=0.2$, $\Omega_\Lambda=0.8$')
ax.set_xlabel("z")
ax.set_ylabel("Lum. distance $D_L/D_H$")
plt.plot(A["z"], A["dl"])"""

ax=fig.add_subplot(1,3,2)
ax.set_xlabel("z")
ax.set_ylabel("Ang. diameter distance $D_A/D_H$")
plt.title('$\Omega_m=0.2$, $\Omega_\Lambda=0.8$')
plt.plot(A["z"], A["da"])
plt.scatter(C, B)

"""ax=fig.add_subplot(1,3,3)
ax.set_xlabel("z")
ax.set_ylabel("Comoving volume $dV/dz/d\Omega \,.\,  1/D_H^3$")
plt.title('$\Omega_m=0.2$, $\Omega_\Lambda=0.8$')
plt.plot(A["z"], A["dvdz"])"""
plt.show()




"""#============== TEST 1 ======================================================
my_dtype=[("mass", float), ("z", float)]
A=np.loadtxt(dir+'MF_draw_random.dat', dtype=my_dtype)

fig=plt.figure()
ax=fig.add_subplot(1,3,1)
ax.hist(A["mass"],bins=np.linspace(np.min(A["mass"]),np.max(A["mass"]),20), normed=False)
ax.set_yscale('log')
ax.set_ybound(1., 1.e5)
ax.set_xbound(np.min(A["mass"]), np.max(A["mass"]))
ax.set_xlabel("log(Mass) [Msun]")
ax.set_ylabel("Count")

ax=fig.add_subplot(1,3,2)
ax.hist(A["z"],bins=np.linspace((np.min(A["z"])),(np.max(A["z"])),20), normed=False)
ax.set_yscale('log')
ax.set_ybound(1.e3, 2.e4)
ax.set_xbound(np.min(A["z"]), np.max(A["z"]))
ax.set_xlabel("Redshift")
ax.set_ylabel("Count")

ax=fig.add_subplot(1,3,3)
xedges, yedges = np.linspace(np.min(A["mass"]), np.max(A["mass"]),30), np.linspace(np.min(A["z"]),np.max(A["z"]),30)
hist, xedges, yedges = np.histogram2d(A["mass"], A["z"], (xedges, yedges))
xidx = np.clip(np.digitize(A["mass"], xedges), 0, hist.shape[0])
yidx = np.clip(np.digitize(A["z"], yedges), 0, hist.shape[1])
c = hist[xidx-1, yidx-1]
ax.scatter(A["mass"], A["z"], c=c)
ax.set_xlabel("log(Mass) [Msun]")
ax.set_ylabel("Redshift")

fig.show()


my_dtype=[("z", float), ("m", float), ("MF", float)]
A=np.loadtxt(dir+'MF_tab.dat', dtype=my_dtype)

fig=plt.figure()
ax=fig.add_subplot(1,1,1)
good1=np.where(A["z"]==0)
good2=np.where(A["z"]==0.2)
good3=np.where(A["z"]==0.4)
good4=np.where(A["z"]==0.8)
good5=np.where(A["z"]==1)
good6=np.where(A["z"]==0.6)
good7=np.where(A["z"]==2)

#ax.set_xscale('log')
#ax.set_yscale('log')
ax.set_ylabel('$\log_{10}((M^2/\rho_m)dN/dM)$')
ax.set_xlabel('$log(M_{200, mean}/[h^{-1} M_{sun}])$')
plt.title('Comparison with Tinker 2008 - fig 1')
plt.plot(A["m"][good1], A["MF"][good1], label='z=0')
plt.plot(A["m"][good2], A["MF"][good2], label='z=0.2')
plt.plot(A["m"][good3], A["MF"][good3], label='z=0.4')
plt.plot(A["m"][good4], A["MF"][good6], label='z=0.6')
plt.plot(A["m"][good4], A["MF"][good4], label='z=0.8')
plt.plot(A["m"][good5], A["MF"][good5], label='z=0.1')
plt.plot(A["m"][good7], A["MF"][good7], label='z=2')
plt.legend()

#============== TEST 2 ======================================================

my_dtype=[("E", float), ("dPhidE", float), ("E2dPhidE", float)]
A=np.loadtxt(dir+'diff_flux.dat', dtype=my_dtype)
fig=plt.figure()

ax=fig.add_subplot(1,2,1)
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlabel("$E$ [GeV]")
ax.set_ylabel('$d\Phi / dE$ [GeV$^{-1}$ cm$^{-2}$ s$^{-1}$ sr$^{-1}$]')
plt.plot(A["E"], A["dPhidE"],'+-', label='$m_\chi=100$ GeV')
plt.legend()

ax=fig.add_subplot(1,2,2)
ax.set_xscale('log')
ax.set_yscale('log')
plt.plot(A["E"], A["E2dPhidE"],'+-' , label='$m_\chi=100$ GeV')
plt.legend()
ax.set_xlabel("$E$ [GeV]")
ax.set_ylabel('$E^2 d\Phi / dE$ [GeV cm$^{-2}$ s$^{-1}$ sr$^{-1}$]')
fig.show()

#============== TEST 3 ======================================================

my_dtype=[("mass", float), ("z", float), ("flux", float)]
A=np.loadtxt(dir+'diff_flux_ranges.dat', dtype=my_dtype)
fig = plt.figure()
ax = fig.gca(projection='3d')
X = np.unique(np.log10(A["mass"]))
Y = np.unique(A["z"])
X, Y = np.meshgrid(X, Y, indexing='ij')
zs = A["flux"]
Z = zs.reshape(np.shape(X))
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.rainbow,linewidth=0, antialiased=True)

fig.show()

#============== TEST 4 ======================================================
my_dtype=[("mass", float), ("z", float), ("flux", float)]
A=np.loadtxt(dir+'int_flux_ranges.dat', dtype=my_dtype)
fig = plt.figure()
ax = fig.gca(projection='3d')
X = np.unique(np.log10(A["mass"]))
Y = np.unique(A["z"])
X, Y = np.meshgrid(X, Y, indexing='ij')
zs = A["flux"]
Z = zs.reshape(np.shape(X))
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.rainbow,linewidth=0, antialiased=True)

fig.show()


#============== TEST 5 ======================================================
my_dtype=[("mass", float), ("z", float), ("flux", float)]
A=np.loadtxt(dir+'int_flux_cumul.dat', dtype=my_dtype)
fig = plt.figure()
ax=fig.add_subplot(1,2,1,projection='3d')
#ax = fig.gca(projection='3d')
X = np.unique(np.log10(A["mass"]))
Y = np.unique(A["z"])
X, Y = np.meshgrid(X, Y, indexing='ij')
zs = pow(10., A["flux"])
Z = zs.reshape(np.shape(X))
surf = ax.plot_surface(X, Y, Z/np.max(Z), rstride=1, cstride=1, cmap=cm.rainbow,linewidth=0, antialiased=True)
ax.set_xlabel('$\log(M_h)$ [M$_\odot$]')
ax.set_ylabel('$z$')

ax=fig.add_subplot(1,2,2,projection='3d')
#ax = fig.gca(projection='3d')
X = np.unique(np.log10(A["mass"]))
Y = np.unique(A["z"])
X, Y = np.meshgrid(X, Y, indexing='ij')
zs = pow(10., A["flux"])
Z = zs.reshape(np.shape(X))
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.rainbow,linewidth=0, antialiased=True)
ax.set_xlabel('$\log(M_h)$ [M$_\odot$]')
ax.set_ylabel('$z$')

fig.show()


#============== TEST 6 ======================================================
my_dtype=[("theta", float), ("psi", float), ("z", float), ("mass", float)]
A=np.loadtxt(dir+'SZ_draw_random.dat', dtype=my_dtype)
fig = plt.figure()

plt.plot(A["theta"], A["psi"],'+')

fig.show()

#============== TEST 1 ======================================================
my_dtype=[("id",int), ("lon",float), ("lat",float), ("mass", float), ("r500", float), ("da", float), ("z", float), ("Pnorm", float), ("alpha",float), ("beta",float), ("gamma",float), ("prof",basestring), ("y_centre",float),("Y_5R500",float)]
file="cluster_stat.dat"
file="all_stat_sz.drawn"
A=np.loadtxt(file, dtype=my_dtype, skiprows=4)
#file="output/sz_SZ2D_LOS0,0_FOV360x180_rse2_alphaint0.12deg_nside512.drawn"
#A=np.loadtxt(file, dtype=my_dtype, skiprows=4)

print min(A["Y_5R500"]), max(A["Y_5R500"])

fig=plt.figure()
ax=fig.add_subplot(3,2,1)
ax.hist(A["mass"],bins=np.linspace(np.min(A["mass"]),np.max(A["mass"]),100), normed=False, color='c')
#ax.set_yscale('log')
ax.set_ybound(0.1, 1.e4)
ax.set_xbound(np.min(A["mass"]), np.max(A["mass"]))
ax.set_xlabel("log(Mass) [Msun]")
ax.set_ylabel("Count")

ax=fig.add_subplot(3,2,2)
ax.hist(A["z"],bins=np.linspace((np.min(A["z"])),(np.max(A["z"])),100), normed=False, color='c')
#ax.set_yscale('log')
ax.set_ybound(0.1, 1.e4)
ax.set_xbound(np.min(A["z"]), np.max(A["z"]))
ax.set_xlabel("Redshift")
ax.set_ylabel("Count")

ax=fig.add_subplot(3,2,3)
ax.hist(A["y_centre"],bins=10**np.linspace(np.log10(np.min(A["y_centre"])),np.log10(np.max(A["y_centre"])),20), color='c')
ax.set_yscale('log')
ax.set_xscale('log')
ax.set_ybound(0.1, 1.e4)
#ax.set_xbound(np.min(A["y_centre"]), np.max(A["y_centre"]))
ax.set_xlabel("y_{centre}")
ax.set_ylabel("Count")

ax=fig.add_subplot(3,2,4)
ax.hist(A["Y_5R500"],bins=10**np.linspace(np.log10(np.min(A["Y_5R500"])),np.log10(np.max(A["Y_5R500"])),20), color='c')
ax.set_yscale('log')
ax.set_xscale('log')
ax.set_ybound(0.1, 1.e4)
#ax.set_xbound(np.log10(np.min(A["Y_5R500"])), np.log10(np.max(A["Y_5R500"])))
ax.set_xlabel("Y_{5R_{500}} [arcmin^2]")
ax.set_ylabel("Count")


ax=fig.add_subplot(3,2,5)
xedges, yedges = np.linspace(np.min(A["mass"]), np.max(A["mass"]),30), np.linspace(np.min(A["z"]),np.max(A["z"]),30)
hist, xedges, yedges = np.histogram2d(A["mass"], A["z"], (xedges, yedges))
xidx = np.clip(np.digitize(A["mass"], xedges), 0, hist.shape[0])
yidx = np.clip(np.digitize(A["z"], yedges), 0, hist.shape[1])
c = hist[xidx-1, yidx-1]
ax.scatter(A["mass"], A["z"], c=c)
ax.set_xlabel("log(Mass) [Msun]")
ax.set_ylabel("Redshift")

good=np.where((A["Y_5R500"] >= 1.e-3) & (A["Y_5R500"] < 1e-1))[0]

ax=fig.add_subplot(3,2,6)
ax.set_yscale('log')
ax.set_ylim(1.e14, 3.e15)
ax.set_xlim(0, 1.5)
ax.set_ylabel("Mass [Msun]")
ax.set_xlabel("Redshift")
item1, =ax.plot(A["z"], A["mass"],'x', color='c', label='all')
item2, =ax.plot(A["z"][good], A["mass"][good],'x', color='m', label='$Y_{5R_{500}}>10^{-3}$')
ax.legend([item1, item2])


fig=plt.figure()
ax=fig.add_subplot(1,1,1)

alpha_500=(5*A["r500"]/A["da"])*180.*60./3.1416

ax.hist(alpha_500,bins=np.linspace(np.min(alpha_500),np.max(alpha_500),200), normed=False, color='c')
ax.set_xbound(np.min(alpha_500), np.max(alpha_500))
ax.set_xlabel("alpha_{500} [arcmin]")
ax.set_ylabel("Count")

fig.show()

#=================================================
file=dir+"sz_y1D.dat"
my_dtype=[("r",float), ("res",float)]
A=np.loadtxt(file, dtype=my_dtype, skiprows=4)

file="/Users/combet/Desktop/PSZ1G045.txt"
my_dtype=[("r",float), ("res",float)]
B=np.loadtxt(file, dtype=my_dtype, skiprows=4)


fig=plt.figure()
ax=fig.add_subplot(1,1,1)
ax.set_ylabel("y x 10^6")
ax.set_xlabel("r [arcmin]")
#ax.set_xscale('log')
#ax.set_yscale('log')
ax.set_ylim(0.1, 1000)
ax.set_xlim(0.01, 100)
ax.plot(A["r"], A["res"],'-', color='c', label="CL1227-like")
ax.legend()
fig.show()
savefig('y_prof.png')


file="sz_pressure1D.dat"
my_dtype=[("r1",float), ("res1",float),("r2",float), ("res2",float)]
A=np.loadtxt(file, dtype=my_dtype, skiprows=4)

fig=plt.figure()
ax=fig.add_subplot(1,2,1)
ax.set_ylabel("P/P$_{500}$")
ax.set_xlabel("r/R$_{500}")
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_ylim(4.e-4, 300)
ax.set_xlim(0.007, 5)
ax.plot(A["r1"], A["res1"],'-', color='c', label="$")
ax.legend()

ax=fig.add_subplot(1,2,2)
ax.set_ylabel("P [$h_{70}^{1/2}$ keV cm$^{-3}$]")
ax.set_xlabel("r [$h_{70}^{-1}$ kpc]")
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_ylim(2.e-5, 0.6)
ax.set_xlim(6, 2000)
ax.plot(A["r2"], A["res2"],'-', color='c', label="$")
ax.legend()


fig.show()
savefig('p_prof.png')


#=================================================

file="input_image_for_fft.dat"
arr_in=np.loadtxt(file)
x=np.arange(-250,250,1)
y=np.arange(-250,250,1)
fig=plt.figure()
ax=fig.add_subplot(3,2,1)
plt.imshow(arr_in, extent=[x.min(),x.max(), y.min(), y.max()], interpolation='nearest')
plt.colorbar()

file="beam_for_fft.dat"
beam_in=np.loadtxt(file)
x=np.arange(-250,250,1)
y=np.arange(-250,250,1)
ax=fig.add_subplot(3,2,2)
plt.imshow(beam_in, extent=[x.min(),x.max(), y.min(), y.max()], interpolation='nearest')
plt.colorbar()

file="recovered_image.dat"
arr_in=np.loadtxt(file)
x=np.arange(-250,250,1)
y=np.arange(-250,250,1)
ax=fig.add_subplot(3,2,3)
plt.imshow(arr_in, extent=[x.min(),x.max(), y.min(), y.max()], interpolation='nearest')
plt.colorbar()

file="recovered_psf.dat"
arr_rec=np.loadtxt(file)
x=np.arange(-250,250,1)
y=np.arange(-250,250,1)
ax=fig.add_subplot(3,2,4)
plt.imshow(arr_rec, extent=[x.min(),x.max(), y.min(), y.max()], interpolation='nearest')
plt.colorbar()



file="convol_image.dat"
arr_in=np.loadtxt(file)
x=np.arange(-250,250,1)
y=np.arange(-250,250,1)
ax=fig.add_subplot(3,2,5)
plt.imshow(arr_in, extent=[x.min(),x.max(), y.min(), y.max()], interpolation='nearest')
#plt.colorbar()


ax=fig.add_subplot(3,2,6)
plt.plot(arr_in[0,0:250], color='m', marker='+', markersize=2, linestyle='None')
plt.plot(beam_in[250,250:500], color='b', linestyle='-')
fig.show()




#==================== DR9 cluster catalogue ====================================


my_dtype=[("lon",float), ("lat",float), ("name", basestring), ("zphot", float),  ("r200",float), ("rich",float), ("N200",float)]
file="../dr9_cluster_wen12.txt"
A=np.loadtxt(file, dtype=my_dtype, skiprows=59)
logM200=-1.49+1.17*log10(A["rich"])
M200=np.power(10,logM200)*1.e14
cut1=np.where((A["zphot"]>0.05) & (A["zphot"]<0.42) & (M200>2.e14))[0]
B=A[cut1]
med=B.size

logM200=-1.54+1.14*log10(A["rich"])
M200=np.power(10,logM200)*1.e14
cut1=np.where((A["zphot"]>0.05) & (A["zphot"]<0.42) & (M200>2.e14))[0]
B=A[cut1]
min=B.size

logM200=-1.44+1.2*log10(A["rich"])
M200=np.power(10,logM200)*1.e14
cut1=np.where((A["zphot"]>0.05) & (A["zphot"]<0.42) & (M200>2.e14))[0]
B=A[cut1]
max= B.size

print min, med, max"""
