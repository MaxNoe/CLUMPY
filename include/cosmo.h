#ifndef _CLUMPY_COSMO_H_
#define _CLUMPY_COSMO_H_

// C++ std libraries
#include <vector>
#include <string>
using namespace std;


struct Mmin_params_for_rootfinding {
   double z;        //!<  redshift
   double sigma_m;  //!<  Steepness of the sigmoid transition
};

struct pk_params_for_integration {
   double rh;                        //!<
   vector<double> vec_lnk;           //!<
   vector<double> linear_lnpkz;      //!<
   int card_window;                  //!<
};

struct d_to_z_params_for_rootfinding {
   double distance;       //!<  input distance [Mpc]
   int switch_distance;   //!<  which distance? 0: d_c, 1: d_trans, 2: d_l, 3: d_a
};

void     compute_class_pk(double z);                                            //!< Calls CLASS to compute the matter power spectrum at redshift \f$z\f$.

/*! \brief Computes \f$dn/d(\ln(M))\f$ (the number density per mass bin) for each z in vec_z and mass in vec_lnM.
 * \details Result is in [\f$h^3 Mpc^{-3}\f$] is stored in dndlnMh. The mass function parametrisation is selected by card_mf = kTINKER08, kJENKINS01, kSHETHTORMEN99. Warning: the mass is in units of \f$\Omega_m^0\; h^{-1} \; M_\odot\f$
 */
vector<double>  compute_massfunction(const double z,                            //!< [in] redshift \f$z\f$
                                     const vector<double> &Mh_grid,             //!< [in] Mass function grid, \f$[\Omega_{m0} h^{-1} M_{\odot}]\f$
                                     const int card_mf,                         //!< [in] mass function keyword
                                     const vector<double>  &linear_lnk_vec,     //!< [in] \f$log(k/h)\f$ vector  \f$[h \rm Mpc^{-1}]\f$
                                     const vector<double>  &linear_lnp_vec,     //!< [in] \f$log(P_{\rm lin})\f$ vector  \f$[h^{-3} \rm Mpc^3 ]\f$
                                     const int card_window,                     //!< [in] window function keyword
                                     const int card_growth_mthd                 //!< [in] selects the method to obtain \f$P(k,z)\f$: either \f$P(k,z)\f$ is directly given (kPKZ_FROMFILE), <br> or \f$P(k,0)\f$ is given, and \f$\sigma^2(z)\f$ is calculated from a growth factor model.
                                    );

double   d_to_z(const double &d, const int switch_d);                           //!< Computes the redshift from a given comoving distance.
double   dh_c(const double &z);                                                 //!< Computes the comoving distance by integration.
double   dh_trans(const double &z);                                             //!< Computes the transverse comoving distance by integration.
double   dh_a(const double &z);                                                 //!< Angular diameter distance at redshift \f$z\f$ in [\f$h^{-1} \; Mpc \f$].
double   dh_l(const double &z);                                                 //!< Luminosity distance at redshift \f$z\f$ in [\f$h^{-1} \; Mpc \f$].

double   dNdVhdlnMh_interpol(const double &z, const double &Mh);                //!<
void     dNdVhdlnMh_integrand_Mh(double &M, double par_z[1], double &res);      //!<
void     dNdVhdlnMh_integrand_logMh(double &logMh, double par_z[1],
                                    double &res);                               //!<
void     dNdVhdlnMh_sigmoid(double &Mh, double par[3], double &res);            //!<
double   dNdVhdlnMh_literature(const double &z, const double &Mh,
                               const int card_mf,
                               vector<string> &label_literature);               //!<

double   dNdOmega(double &zmin, double &zmax, double const &Mmin,
                  double const &Mmax);                                          //!<
void     dNdOmegadlnMh(double &lnMh, double par_z[2], double &res);             //!<
void     dNdOmegadz(double &z, double par_Mh[2], double &res);                  //!<
void     dNdOmegadlnMhdz(double &z, double par_Mh[1], double &res);             //!<

double   ds2dlnk(double lnk, void *p);                                          //!< Returns \f$ d(\sigma^2)/dk \propto k^3\;p(k)\; W(kr)\f$,  where r is the scale of interest and \f$ W(kr)\f$ is the Fourier transform of the 3D top-hat function.
double   dVhdzdOmega(double z);                                                 //!< Differential comoving volume element in [\f$h^{-3} \; Mpc^3 \; sr^{-1}\f$].
string   legend_for_delta();
double   lnsigma2(double lnR, void *p);                                         //!< Returns \f$\ln(\sigma^2)\f$ for the scale \f$ R \f$ passed as \f$\ln(R)\f$
double   gamma_f(const int card_window);                                        //!<

void     get_pk(const double &z, vector<double>  &lnk_vec,
                vector<double>  &lnp_vec,
                const bool is_nonlinear = false,
                const bool is_force_recompute = false);                         //!< Loads the matter power spectrum from a file. Fills in vec_lnk with mode number \f$\ln(k)\f$, \f$ k \f$ in [\f$ h \;Mpc^{-1}\f$]. Fills in linear_lnpkz with \f$\ln(p_{\rm matter}(k,z))\f$ [\f$ Mpc^3\;h^{-3}\f$]. If the p_matter(k,z) file is not present, calls CLASS to compute it.
double   growth_factor(const double &z, const int card_growth_mthd);            //!< Computes the linear growth rate factor.
double   growth_integrand(const double z, void *params = NULL);                 //!<

double   mf(int card_mf, double const &nu, double const &z,
            double const &Delta_c);                                             //!<
double   mf_bolshoi_planck(double const &nu, double const &z,
                           double const &Delta_c);                              //!<
double   mf_magneticum(double const &nu, double const &z, double const &Delta_c,
                       const bool is_hydro);                                    //!<
double   mf_press_schechter(double const &nu, double const &z,
                            double const &Delta_c);                             //!<
double   mf_sheth_tormen(double const &nu, double const &z,
                         double const &Delta_c);                                //!<
double   mf_tinker(double const &nu, double const &z, double const &Delta_c);   //!<
double   mf_tinker10(double const &nu, double const &z, double const &Delta_c); //!<
double   mf_tinker_norm(double const &nu, double const &z,
                        double const &Delta_c);                                 //!<
double   mf_jenkins(double const &nu, double const &z, double const &Delta_c);  //!<
vector< vector<double> > nu_andderiv(const double z,
                                     const vector<double> &Mh_vec,
                                     const vector<double>  &linear_lnk_vec,
                                     const vector<double>  &linear_lnp_vec,
                                     const int card_window,
                                     const int card_growth_mthd);               //!<
double   H2_over_H02(const double &z);                                          //!<
double   H_over_H0(const double &z);                                            //!< \f$H(z)/H_0\f$ where \f$H(z)\f$ is the Hubble rate at redshift \f$z\f$.
double   H0_over_H(const double z, void *params = NULL);                        //!< \f$1/h(z) = H_0/H(z)\f$ where \f$H(z)\f$ is the Hubble rate at redshift \f$z\f$.
double   Omega_r(const double &z);                                              //!<
double   Omega_m(const double &z);                                              //!<
double   Omega_cdm(const double &z);                                            //!<
double   Omega_b(const double &z);                                              //!<
double   Omega_lambda(const double &z);                                         //!<
double   Omega_k(const double &z);                                              //!<
double   rho_crit(const double &z);                                             //!<
vector< vector<double> > sigma2_andderiv(const double z,
      const vector<double> &Mh_vec, const vector<double>  &linear_lnk_vec,
      const vector<double>  &linear_lnp_vec, const int card_window,
      const int card_growth_mthd);                                              //!<
double   sigma8(vector<double>  &lnk_vec, vector<double>  &lnp_vec,
                const int card_window);                                         //!<
double   solve_Mhmin(double Mhmin, void *p);                                    //!<
double   solve_d_to_z(double d, void *p);                                       //!<
double   window_fnct(const double &x, const int card_window);                   //!<
#endif

/*! \file cosmo.h
  \brief Cosmology base functions: distances, Omega parameters, and mass functions

*/
