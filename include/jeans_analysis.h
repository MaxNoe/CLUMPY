#ifndef _CLUMPY_JEANSANALYSIS_H_
#define _CLUMPY_JEANSANALYSIS_H_

double beta_anisotropy(double &r, double par_ani[5]);
double beta_anisotropy_fr(double &s, double par_ani[5], double const &eps);
double beta_anisotropy_kernel(double &r, double par_ani[5], double const &R);
double beta_anisotropy_BAES(double &r, double par[4]);
double beta_anisotropy_CONSTANT(double &r, double par[1]);
double beta_anisotropy_OSIPKOV(double &r, double par[1]);
string beta_anisotropy_legend(double par_ani[5], bool is_with_formula = false);

void   jeans_isigmap2_integrand(double &y_or_r, double par_jeans[21], double &res);
void   jeans_isigmap2_integrand_log(double &y_or_r, double par_jeans[21], double &res);
void   jeans_isigmap2_integrand_numerical(double &y, double par_jeans[20], double &res);
void   jeans_isigmap2_integrand_withkernel(double &y, double par_jeans[20], double &res);
double jeans_nuvr2(double &r, double par_jeans[20]);
void   jeans_nuvr2_integrand(double &s, double par_jeans[20], double &res);
void   jeans_nuvr2_integrand_log(double &s, double par_jeans[20], double &res);
double jeans_sigmap2(double &R, double par_jeans[20], bool is_use_kernel = true);

double light_i(double &R, double par_light[8]);
void   light_i_integrand(double &y, double par_light[8], double &res);
double light_i_norm(double par_light[8]);
void   light_i_norm_integrand(double &R, double par_light[8], double &res);
double light_nu(double &r, double par_light[8]);
void   light_nu_integrand(double &Y, double par_light[8], double &res);
double light_profile(double &rR_or_yY, double par_light[8], int switch_qty, double rmax_integr = -1.);
string light_profile_legend(double par_light[6], bool is_with_formula = false);
double light_profile_EXP2D(double &R_or_r, double par[2], bool is_deproject);
double light_profile_EXP3D(double &r_or_R, double par[2], bool is_project);
double light_profile_KING2D(double &R_or_r, double par[3], bool is_deproject);
double light_profile_PLUMMER2D(double &R_or_r, double par[2], bool is_deproject);
double light_profile_SERSIC2D(double &R_or_r, double par[3], bool is_deproject, double const &rmax_integr = 1.e10, double const &eps = 1.e-2);
double light_profile_ZHAO3D(double &r_or_R, double par[5], bool is_project, double const &rmax_integr = 1.e10, double const &eps = 1.e-2);

void   load_jeansanalysis_parameters(string file_name, struct gStructJeansAnalysis &jeans_analysis);
double log_likelihood_jeans_binned(double par_jeans[20], gStructHalo stat_halo, int type_analysis);
double log_likelihood_jeans_unbinned(double par_jeans[20], gStructHalo stat_halo, double vmean);

void   print_parjeans(double par_jeans[20]);
void   print_parlight(double par_light[8]);


#endif

/*! \file jeans_analysis.h
  \brief Jeans analysis to get DM profiles from stars (light profile and velocity dispersion)


<b><A NAME="jeans_func"> \anchor jeans_func  Implementation in the code and functions</A></b>\n

 1. <b>Light profile quantities</b>
    - <i>Native 2D profiles (surface brightness)</i>: \c light_profile_EXP2D(), \c light_profile_KING2D(),
      \c light_profile_PLUMMER2D(), and \c light_profile_SERSIC2D().
       \param[in]    R_or_r                Projected distance (\f$R\f$) or distance (\f$r\f$) from halo centre [kpc]
       \param[in]    par[0-2]              Light: \f$I(R)\f$ +  scale radius [kpc] + shape #1 (SERSIC2D) or second scale radius (KING)
       \param[in]    is_deproject          Whether to return projected (native) or deprojected \f$\nu(r)\f$
       \param[in]    rmax                  Integration boundary [used only if SERSIC2D and is_deproject==true]
       \param[in]    eps                   Integration precision [used only if SERSIC2D and is_deproject==true]
       \returns      \f$I^{\rm XXX}(R)\f$ in [\f$L_\odot\f$] or \f$\nu^{\rm XXX}(r)\f$ in [\f$kpc^{-3}\f$], where \f${\rm XXX} =\f$ EXP2D, KING2D, PLUMMER2D,
                     or SERSIC2D
    \n\n\n
    - <i>Native 3D profiles (density profile)</i>: \c light_profile_EXP3D(), and \c light_profile_ZHAO3D().
       \param[in]    r_or_R                Distance (\f$r\f$) or projected distance (\f$R\f$) from halo centre [kpc]
       \param[in]    par[0-4]              Light: \f$\nu(r)\f$ + scale radius [kpc] + shape #(1,2,3) [only for ZHAO3D]
       \param[in]    is_project            Whether to return deprojected (native) or projected \f$I(R)\f$
       \param[in]    rmax                  Integration boundary [used only if ZHAO3D and is_project==true]
       \param[in]    eps                   Integration precision [used only if ZHAO3D and is_project==true]
       \returns      \f$\nu^{\rm XXX}(r)\f$ in [\f$kpc^{-3}\f$] or \f$I^{\rm XXX}(R)\f$ in [\f$L_\odot\f$], where \f${\rm XXX} =\f$ EXP3D, or ZHAO3D
    \n\n\n
    - light_profile() returns, depending on a switch, surface brightness \f$I(R)\f$
       (or integrand to calculate it), or density profile \f$f\nu(r)\f$ (or integrand to calculate it),
       for any light profile available in \c gENUM_LIGHTPROFILE (see params.h).
       \param[in]    rR_or_yY              Distance (depends on \c par_light[7]) if 3D: \f$r\f$ or \f$y=\sqrt{r^2-R^2}\f$; if 2D: \f$R\f$ or \f$Y=\sqrt{R^2-r^2}\f$  [kpc]
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    par_light[6]          Radius \f$R\f$ (resp. \f$r\f$) [kpc] considered for projection (resp. deprojection)
       \param[in]    par_light[7]          Switch quantity to calculate:
                                              - if 1, surface brightness \f$I(R)=\Sigma(R)\f$ [\f$L_\odot\f$]
                                              - if 2, density profile \f$\nu(r)=\rho(r)\f$ [\f$kpc^{-3}\f$]
                                              - if 3, integrand to calculate \f$I(R)\f$ from \f$\nu(r)\f$, i.e. \f$2 \nu(y)\f$ with \f$y = \sqrt{r^2-R^2}\f$]
                                              - if 4, integrand to calculate \f$\nu(r)\f$ from \f$I(R)\f$, i.e.  \f$-\frac{1}{\pi R} \frac{dI(R)}{dR}\f$
       \param[in]    par_light[8]          Maximum radius for integration (if \c par_light[7]==3 or 4) [kpc]
       \param[in]    par_light[9]          Relative precision sought for integration (if \c par_light[7]==3 or 4)
       \returns      \f$I(R)\f$ in [\f$L_\odot\f$], or \f$\nu(r)\f$ in [\f$kpc^{-3}\f$], or \f$2\nu(y)\f$ in [\f$kpc^{-3}\f$],
                      or \f$-\frac{1}{\pi R}\frac{dI(R)}{dR}\f$ in [\f$L_\odot kpc^{-2}\f$]
    \n\n\n
    - light_i() returns the surface density brightness \f$I(R)\f$ calling light_profile().
       \param[in]    R                     Projected radius [kpc]
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    rmax                  Maximum radius for integration (if \c par_light[7]==3 or 4) [kpc]
       \param[in]    eps                   If not analytical, relative precision sought for the integration
       \returns      Surface density brightness \f$I(R)\f$ in [\f$L_\odot\f$]
    \n\n\n
    - light_i_integrand() calculates integrand of surface density brightness calling light_profile().
       \param[in]    y                     Variable on which integration is performed (at projected radius \f$R\f$): \f$y = \sqrt{r^2-R^2}\f$ [kpc]
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    par_light[6]   Radius \f$R\f$ [kpc] considered for projection
       \param[out]   res                   Integrand \f$2\nu(y)\f$ of \f$I(R) = 2 \int_0^\infty nu(y) dy\f$ in [\f$kpc^{-3}\f$]
    \n\n\n
    - light_i_norm() returns \f$\int_0^{r_{\rm max}} 2\pi \, R \, I(R) dR\f$, i.e. normalisation of surface brightness
                     (useful, e.g., to ensure \f$I(R)\f$ is a probability).
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    rmax                  Maximum radius for integration [kpc]
       \param[in]    eps                   Relative precision sought for the integration
       \returns     \f$\int_0^{r_{\rm max}} 2\pi \, R \, I(R) dR\f$ in [\f$L_\odot kpc^2\f$]
    \n\n\n
    - light_i_norm_integrand() calculates \f$2\pi R I(R)\f$, i.e. integrand used in light_i_norm().
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    par_light[6]   Radius \f$R\f$ [kpc] considered for projection
       \param[in]    par_light[7]   Maximum radius for integration [kpc]
       \param[out]   res                   Calculates \f$2\pi R I(R)\f$ in [\f$L_\odot kpc^2\f$]
    \n\n\n
    - light_nu() returns the 3D density profile \f$\nu(r)\f$ calling light_profile().
       \param[in]    r                     Radius [kpc]
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    rmax                  If not analytical, maximum radius for integration [kpc]
       \param[in]    eps                   If not analytical, relative precision sought for the integration
       \returns      Stellar density \f$\nu(r)\f$ in [\f$kpc^{-3}\f$]
    \n\n\n
    - light_nu_integrand() calculates integrand of 3D density profile calling light_profile().
       \param[in]    Y                     Variable on which integration is performed (at radius \f$r\f$): \f$y = \sqrt{r^2-R^2}\f$ [kpc]
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    par_light[6]   Radius \f$r\f$ [kpc] considered for deprojection
       \param[out]   res                   Integrand \f$-\frac{1}{\pi R} \frac{dI(R)}{dR}\f$ of \f$\nu(r) = -\frac{1}{\pi}\int_0^\infty \frac{dI(R)}{dR}\frac{dY}{R}\f$
                                           in [\f$L_\odot kpc^{-2}\f$]
    \n\n\n\n

 2. <b>Anisotropy \f$\beta_{\rm ani}(r)\f$ (no unit)</b>

    Anisotropy-related functions are of two types, which are listed and detailed below:\n
    -# <i>Anisotropy parametrisations</i> (see \c gENUM_ANISOTROPYPROFILE in \c params.h)\n
       - \c beta_anisotropy_BAES(): \f$\displaystyle\beta_{\rm ani}^{\rm BAES}(r)=\displaystyle\frac{\beta_0 + \beta_\infty (r/r_a)^\eta}{1+(r/r_a)^\eta}\f$
          \param[in]    r                     Distance from halo centre [kpc]
          \param[in]    par[0]                Anisotropy \f$\beta_0\f$ at \f$r=0\f$
          \param[in]    par[1]                Anisotropy \f$\beta_\infty\f$ at \f$r=\infty\f$
          \param[in]    par[2]                Anisotropy scale radius \f$r_a\f$ [kpc]
          \param[in]    par[3]                Anisotropy shape parameter \f$\eta\f$
       \n\n
       - \c beta_anisotropy_CONSTANT(): \f$\displaystyle\beta_{\rm ani}^{\rm CONSTANT}(r)=\beta_0\f$
          \param[in]    r                     Distance from halo centre [kpc]
          \param[in]    par[0]                Anisotropy \f$\beta_0\f$
       \n\n
       - \c beta_anisotropy_OSIPKOV(): \f$\displaystyle\beta_{\rm ani}^{\rm OSIPKOV}(r)=\displaystyle\frac{r^2}{r^2+r_a^2}\f$
          \param[in]    r                     Distance from halo centre [kpc]
          \param[in]    par[0]                Anisotropy scale radius \f$r_a\f$ [kpc]
    \n\n\n
    -# <i>Functions depending on the anisotropy profile</i>: they rely on a generic call with 5
    following anisotropy parameters, where the fifth one is the keyword selecting the parametrisation to consider.
       \param[in]    par_ani[0]            Anisotropy \f$\beta_0\f$      [unused for OSIPKOV]
       \param[in]    par_ani[1]            Anisotropy \f$\beta_\infty\f$ [unused for CONSTANT and OSIPKOV]
       \param[in]    par_ani[2]            Anisotropy scale radius \f$r_a\f$ [kpc]  [unused for CONSTANT]
       \param[in]    par_ani[3]            Anisotropy shape parameter \f$\eta\f$    [unused for CONSTANT and OSIPKOV]
       \param[in]    par_ani[5]            Anisotropy card_profile (see \c gENUM_ANISOTROPYPROFILE in \c params.h)

      \n\n

       - beta_anisotropy() for a generic call to any of the above anisotropy profiles \c beta_anisotropy_XXX().
          \param[in]    r                     Distance from halo centre [kpc]
          \param[in]    par_ani[0..5]         Anisotropy \f$\beta_0\f$      [unused for OSIPKOV]
          \returns      \f$\beta_{\rm ani}^{\rm XXX}(r)\f$ with \f${\rm XXX}\f$ any anistotropy profile
                                            from \c gENUM_ANISOTROPYPROFILE (see params.h)
       \n\n\n
       - beta_anisotropy_fr() calculates Eq.(2) above, i.e. \f$f(r)\f$ -- used in the calculation of
          \f$\nu(r)\, \bar{v_r^2}(r)\f$ --, for any of the above anisotropy profile.
          \param[in]    r                     Distance from halo centre [kpc]
          \param[in]    par_ani[0..5]  Anisotropy parameters
          \param[in]    eps                   Relative precision sought for the integration
          \returns    \f$f(r)= f_{r_1} \exp\left[\int_{r_1}^r \frac{2}{t}\beta_{\rm ani}(t)\, dt \right]\f$,
                             which is required for the calculation of \f$\nu(r)\, \bar{v_r^2}(r)\f$,
                             for any anistotropy profile from \c gENUM_ANISOTROPYPROFILE (see params.h).
       \n\n\n
       - beta_anisotropy_kernel() calculates the kernel \f${\cal K}(u,u_a)\f$ used for the calculation of
            \f$\sigma_p^2(R)\f$, see Eq.(4). Note that the Kernel does not exist for all anisotropy profiles
            (see, e.g., Appendix of Mamon & Lokas, MNRAS 363, 705 (2005)).
          \param[in]    r                     Radius from halo centre [kpc]
          \param[in]    par_ani[0..5]         Anisotropy parameters
          \param[in]    R                     Projected radius from halo centre [kpc]
          \returns    \f${\cal K}(u,u_a)\f$ (no unit) for anisotropy profiles for which it exists, otherwise abort()
       \n\n\n


 3. <b>Solution of the 'un'-projected and projected Jeans equation \f$\nu(r)\bar{v_r^2}(r)\f$ and \f$\sigma_p^2(R)\f$</b>\n
     All the function related to the solution of the Jeans equation rely on the same number of parameters (namely 20, related
     to the DM profile, the light profile, and the anisotropy profile). They are gathered below, using
     \f$f(r)=\f$beta_anisotropy_fr(), \f$f(r)=\f$beta_anisotropy_kernel(), light_nu() in [\f$kpc^{-3}\f$],
     and the DM halo mass \f$M(s)\f$ in [\f$M_\odot\f$]:
    \n\n
    - jeans_nuvr2() returns \f$\displaystyle \frac{\nu(r)\bar{v_r^2}(r)}{G} = \frac{1}{f(r)} \int_r^\infty f(s) \nu(s) \frac{M(s)}{s^2}\, ds\f$
      in [\f$M_\odot kpc^{-4}\f$];
    \n
    - jeans_nuvr2_integrand() calculates \f$f(s)\,\nu(s)\,M(s) s^{-2}\f$ in [\f$M_\odot kpc^{-5}\f$],
     the integrand used in jeans_nuvr2() to get \f$\displaystyle\frac{\nu(r)\bar{v_r^2}(r)}{G}\f$;
    \n
    - jeans_nuvr2_integrand_log() calculates \f$s\times\f$ jeans_nuvr2_integrand() in [\f$M_\odot kpc^{-4}\f$].
       It is used to find radii where to cut the integrations of jeans_nuvr2_integrand() in different sub-parts
       (integrations are in log-scale).

       \param[in]    r_or_s              Radius from halo centre [kpc]
       \param[in]    par_jeans[0]        Dark matter profile normalisation \f$[M_\odot\,\,kpc^{-3}]\f$
       \param[in]    par_jeans[1]        Dark matter profile scale radius [kpc]
       \param[in]    par_jeans[2]        Dark matter shape parameter #1
       \param[in]    par_jeans[3]        Dark matter shape parameter #2
       \param[in]    par_jeans[4]        Dark matter shape parameter #3
       \param[in]    par_jeans[5]        Dark matter card_profile (see \c enum gENUM_PROFILE in \c params.h)
       \param[in]    par_jeans[6]        Maximum radius for integration [kpc]
       \param[in]    par_jeans[7]        Light profile normalisation
       \param[in]    par_jeans[8]        Light profile scale radius [kpc]
       \param[in]    par_jeans[9]        Light profile shape parameter #1
       \param[in]    par_jeans[10]       Light profile shape parameter #2
       \param[in]    par_jeans[11]       Light profile shape parameter #3
       \param[in]    par_jeans[12]       Light card_profile (see \c enum gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    par_jeans[13]       Projected radius \f$R\f$ considered for calculations of projected quantities [kpc]
       \param[in]    par_jeans[14]       Relative precision sought for the integration
       \param[in]    par_jeans[15]       Anisotropy parameter #1
       \param[in]    par_jeans[16]       Anisotropy parameter #2
       \param[in]    par_jeans[17]       Anisotropy parameter #3
       \param[in]    par_jeans[18]       Anisotropy parameter #4
       \param[in]    par_jeans[19]       Anisotropy card_profile (see \c gENUM_ANISOTROPYPROFILE in \c params.h)
       \param[out]   res                 See description in the above defined functions


\n


\remark It is sometimes useful to print for checks the content of the parameters used
        in the various functions, and we have here print_pardpdv(), print_parhost(),
        print_parmix(), print_parsubs(), and print_partot().



 4. <b>Log-likelihood for minimization Relevant functions in \c CLUMPY</b> \n

    - load_jeansanalysis_parameters(): fills from a file the parameters to use in a Jeans
      analysis (free and fixed parameters, priors, etc.). An example file is
      <A href="../data/params_jeans.txt" target="_blank">data/params_jeans.txt</A>.
       \param[in]   file_name     Name of the file frow which to load parameters
       \param[in]   analysis      Structure in which to store Jeans analysis parameters
    \n\n
    - log_likelihood_jeans_binned() and log_likelihood_jeans_unbinned()
       \param[in]   par_jeans[20] Jeans parameters
       \param[in]   stat_halo     Structure in which to store Jeans analysis parameters

*/

