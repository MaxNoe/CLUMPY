#ifndef _CLUMPY_PROFILES_H_
#define _CLUMPY_PROFILES_H_

void   dlog_rho_dlog_r(double &r, double par[6], double &res);
double dlog_rho_dlog_r_BURKERT(double &r, double par[2]);
double dlog_rho_dlog_r_EINASTO(double &r, double par[3]);
double dlog_rho_dlog_r_EINASTO_N(double &r, double par[3]);
double dlog_rho_dlog_r_DPDV_GAO04(double &r, double par[5]);
double dlog_rho_dlog_r_DPDV_SIGMOID_EINASTO(double &r, double par[5]);
double dlog_rho_dlog_r_DPDV_SPRINGEL08_ANTIBIASED(double &r, double par[3]);
double dlog_rho_dlog_r_DPDV_SPRINGEL08_FIT(double &r, double par[5]);
double dlog_rho_dlog_r_NODES(double &r, double par[3]);
double dlog_rho_dlog_r_ZHAO(double &r, double par[5]);

double get_ratio_r_2_rscale(double par_prof[4]);
double get_riso_triaxial(double x[3], double axes[3], double euler_deg[3]);
double get_rsat(double par[6]);
double get_rscale_from_r_2(double const &r_2, double par_prof[4]);
double get_rvir(double par[6], double const &rho_vir);
double get_r_2_from_rscale(double const &rs, double par_prof[4]);

string legend_for_profile(double par_prof[4], bool is_with_formula = false);

double rho_BURKERT(double &r, double par[2]);
double rho_DPDV_GAO04(double &r, double par[5]);
double rho_DPDV_SIGMOID_EINASTO(double &r, double par[5]);
double rho_DPDV_SPRINGEL08_FIT(double &r, double par[5]);
double rho_DPDV_SPRINGEL08_ANTIBIASED(double &r, double par[3]);
double rho_EINASTO(double &r, double par[3]);
double rho_EINASTO_N(double &r, double par[3]);
double rho_NODES(double &r, double par[3]);
double rho_NODES_log(double r, void *p);
double rho_NODES_find_r_2(double r, void *p);
double rho_NODES_find_rsat(double r, void *p);
double rho_ZHAO(double &r, double par[5]);

void   rho(double &r, double par[6], double &res);
void   rho2(double &r, double par[6], double &res);
void   r2rho(double &r, double par[6], double &res);
void   r2rho2(double &r, double par[6], double &res);
void   r3rho(double &r, double par[6], double &res);
void   rho_mix(double &r, double par[21], double &res);
void   rho2_mix(double &r, double par[21], double &res);
void   r2rho_mix(double &r, double par[21], double &res);
void   r2rho2_mix(double &r, double par[21], double &res);

void   set_par0_given_mref(double par[6], double const &r_ref, double const &m_ref, double const &eps);
void   set_par0_given_rhoref(double par[6], double &r_ref, double const &rho_ref);
void   set_rhonodes(const vector<double> &r_vec, const vector<double> &rho_vec,
                    const int i_halo, const double r0 = 1, int card_interpol_type = -1,
                    bool is_verbose = true);
void   set_rhonodes_fromfile();
#endif

/*! \file profiles.h
  \brief Profile \f$\rho(r)\f$ \f$[M_\odot\,\,kpc^{-3}]\f$ and related functions (applicable for halos, sub-halos...)
*/
