\title{Gao et al. (2004) subclumps distribution profile in Clumpy}
\maketitle
\section{Some analytic calculations}

\begin{align}
\frac{N(< x)}{N_{200}} = \frac{(1 + ac)\,x^{\beta}}{1 + ac\,x^{\alpha}},\qquad
x=\frac{r}{r_{200}},\quad c=\frac{r_{200}}{r_s},\quad\beta>\alpha \\[0.3cm]
n(x) :=\frac{d}{dx}\frac{N(< x)}{N_{200}}=\frac{(1+ac) \,x^{\beta -1}
\left(\beta +a c (\beta - \alpha)\, x^{\alpha }\right)}{\left(1+a c\,
x^{\alpha }\right)^2}
\label{eq:nx}
\end{align}
According to the construction of $n(x)$ as deviation of the function $N(< x)$
and with $x_{vir}:= \frac{r_{vir}}{r_{200}}$:
\begin{align}
\int\limits_0^1 n(x)\, dx &= 1\\[0.2cm]
\int\limits_0^{x_{vir}} n(x)\, dx &= \frac{(1 + ac)\,x_{vir}^{\beta}}{1 +
ac\,x_{vir}^{\alpha}}
\end{align}
Furthermore,
\begin{align}
N_{tot} = N_{200}\, \int_0^{x_{vir}} n(x)\, dx.
\label{eq:N200}
\end{align}
Knowing $N_{tot}$, one can use \autoref{eq:N200} to substitute $N_{200}$ and
get the corresponding expression for  $\frac{N(< \tilde{x})}{N_{tot}}$ with
$\tilde{x}= \frac{r}{r_{vir}}$:
\begin{align}
\frac{N(< \tilde{x})}{N_{tot}} = \frac{\int \limits_0^{\tilde{x}} n(x)\,
dx}{\int\limits_0^{x_{vir}} n(x)\, dx}
\end{align}
To get a probability density, \autoref{eq:nx} is divided by $4\pi\,x^2$
\begin{align}
\frac{dP}{dV}(x) = \frac{(1+ac) \,x^{\beta -3}
\left(\beta +a c (\beta - \alpha)\, x^{\alpha }\right)}{4\pi\,\left(1+a c\,
x^{\alpha }\right)^2},
\label{eq:dPdx}
\end{align}
and
\begin{align}
\frac{dP}{dV}(\tilde{x}) = \frac{\int \limits_0^{\tilde{x}}
\int\limits_{S^2}\, \frac{dP}{dV}(x)\, dV}{\int \limits_0^{x_{vir}}
\int\limits_{S^2}\, \frac{dP}{dV}(x)\, dV}.
\end{align}
A meaningful profile is defined for all $x\geq 0$ under the conditions
\begin{itemize}
\item $ac\geq0$, $\alpha\geq0$, $\beta\geq0$,
\item $\beta \geq \alpha$ (otherwise negative values),
\item $\beta \leq 3 +\alpha$ (otherwise profile increasing with $x$, positive slope).
\end{itemize}

The value $r_{-2}$, where
\begin{align}
\frac{d}{dx}\left(x^2\,\frac{dP}{dV}(x)\right)&=0\\
\Leftrightarrow \frac{d^2n}{dx^2}&=0,
\end{align}
computes (besides the solution $r_{-2} = 0$) to the real solution for the case
$0<\beta-\alpha<1$
\begin{align}
\frac{r_{-2}}{r_{200}} = \left(\frac{\alpha ^2+\alpha  \left(2 \beta -1-\sqrt{4 (\alpha +1) 
\beta +(\alpha -1)^2-4 \beta ^2}\right)-2
(\beta -1) \beta } {2a c  (\beta -\alpha) (\beta-\alpha - 1)}\right)^{1/\alpha
},
\label{eq:factorr2torscale1}
\end{align}
to the solution for the special case $\beta-\alpha=1$, $\alpha > 1$
\begin{align}
\frac{r_{-2}}{r_{200}} =\left(\frac{\alpha+1}{ac\; (\alpha-1)}\right)^{{1}/{\alpha}}\,,
\label{eq:factorr2torscale2}
\end{align}
and to the solution for the  special case $\beta=\alpha$, $\alpha\geq 1$
\begin{align}
\frac{r_{-2}}{r_{200}} = \left(\frac{\alpha-1}{ac\,(\alpha+1)}\right)^{{1}/{\alpha}}\,.
\label{eq:factorr2torscale3}
\end{align}


For $\beta-\alpha>1$, $\beta<1$,  and $\alpha=0$ there are no solutions for $r_{-2}$. 

The logarithmic slope reads
\begin{align}
\frac{d \log\left(\frac{dP}{dV}(x)\right)}{d \log(x)} = -3 + \beta + 
 \alpha \left( \frac{2}{1 + ac\; x^\alpha} - \frac{\beta}{
    \beta + ac (\beta - \alpha ) x^\alpha} - 1\right)\,.
\label{eq:logslope}
\end{align}
The inner logarithmic slope approaches for $x\rightarrow 0$:
\begin{align}
\frac{d \log\left(\frac{dP}{dV}(x)\right)}{d \log(x)} \overset{x\rightarrow 0}{\rightarrow} -3 + \beta \,.
\label{eq:logslope_inner}
\end{align}
The outer logarithmic slope non-steadily changes for $\beta\rightarrow \alpha$, as a term "zero times infinity" occurs in the denominator. For $\beta\neq \alpha$,
\begin{align}
\frac{d \log\left(\frac{dP}{dV}(x)\right)}{d \log(x)} \overset{x\rightarrow \infty}{\rightarrow} -3 + \beta - \alpha\,.
\label{eq:logslope_outer1}
\end{align}
However, for fixed $\beta=\alpha$
\begin{align}
\frac{d \log\left(\frac{dP}{dV}(x)\right)}{d \log(x)} \overset{x\rightarrow \infty}{\rightarrow} - 3 - \alpha\,.
\label{eq:logslope_outer2}
\end{align}

\newpage
\section{Implementation in Clumpy}

Following \autoref{eq:nx}, I constructed a density according to
\begin{align}
\rho(r) = \rho_s\, \frac{(1+ac) \,\left(\frac{r}{r_{200}}\right)^{\beta -3}
\left(\beta +a c (\beta - \alpha)\, \left(\frac{r}{r_{200}}\right)^{\alpha }\right)}{\left(1+a c\,
\left(\frac{r}{r_{200}}\right)^{\alpha }\right)^2}
\label{eq:rhoGAO}
\end{align}
which I included in \texttt{profiles.cc}. The input parameters are:
\begin{itemize}
 \item \texttt{par[0]}    $\;\Rightarrow\;\rho_s$: density normalisation
 $[\unit{M_{\odot}/kpc^3}]$
  \item \texttt{par[1] = gXXX\_YYY\_RSCALE}    $\;\Rightarrow\;r_{200}$ as
  scale radius $[\unit{kpc}]$
  \item \texttt{par[2] = gXXX\_YYY\_SHAPE\_PARAMS[0]} $\;\Rightarrow\;ac$:
  product of parameter $a$ and concentration parameter $c$ of host halo.
  \item \texttt{par[3] = gXXX\_YYY\_SHAPE\_PARAMS[0]}   
  $\;\Rightarrow\;\alpha$
  \item \texttt{par[4] = gXXX\_YYY\_SHAPE\_PARAMS[0]} $\;\Rightarrow\;\beta$
\end{itemize}
This way, the parameter $r_{200}$ has to be plugged in manually as an
input parameter despite the fact that it follows from the host profile (by
the $r_{200}$ value, the GAO $dp/dV$ profile is coupled to the host halo!).
But this way, I could easily include the profile into Clumpy without making big changes
to the code.

For the function \texttt{factor\_r\_2\_to\_rscale} in \texttt{profiles.cc}, I
included the analytic expressions from \autoref{eq:factorr2torscale1} and
\autoref{eq:factorr2torscale2}.