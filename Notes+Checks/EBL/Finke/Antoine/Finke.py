import numpy as np
import matplotlib.pyplot as pl
import math
from scipy.optimize import curve_fit
import scipy.integrate as integrate
from scipy import interpolate
from operator import add,sub

h=6.67e-34
c=3.e8
sigma_thomson=6.65e-29
m_e=9.1e-31
h_hubble=0.7
z_liste=[0.003,0.01,0.03,0.1,0.3,0.5,1,1.5,2,2.5,3,4]
"z_liste=[0.003]"
omega_m=0.3
omega_lambda=0.7
re=2.8e-15
mec2=511.e3


def cross_section(E_gamma, e,z):
	so=E_gamma*e*(1+z)
	beta=1-1/so
	w0=(1+beta)/(1-beta)
	L, err3 =integrate.quad(L_inte, 1, w0)
	"print E_gamma,'         ', e, '         ', so,'          ', beta,'          ', w0,'          ', L"
	res=((1+beta*beta)*np.log(w0)/(1-beta*beta) - beta*beta*np.log(w0) - 4*beta/(1-beta*beta) +2*beta + 4*np.log(w0)*np.log(1+w0)-4*L)
	"print res"
	return res

def L_inte(w0):
	return np.log(1+w0)/w0

"""def troisieme_inte(e, E_gamma, z, coeffs):
	sigma=cross_section(E_gamma,e,z)
	for i in range(0, len(z_connus)-1):
		if(z==z_connus[i]):
			poly=np.poly1d(coeffs["%s" %z_connus[i]])
			EBL=10.**poly(np.log10(e*mec2))
		elif(z==z_connus[i+1]):
			poly=np.poly1d(coeffs["%s" %z_connus[i+1]])
			EBL=10.**poly(np.log10(e*mec2))
		elif((z>z_connus[i])&(z<z_connus[i+1])):
			poly_max=np.poly1d(coeffs["%s" %z_connus[i+1]])
			EBL_max=10.**poly_max(np.log10(e*mec2))
			poly_min=np.poly1d(coeffs["%s" %z_connus[i]])
			EBL_min=10.**poly_min(np.log10(e*mec2))
			EBL=EBL_min+(EBL_max-EBL_min)*(z-z_connus[i])/(z_connus[i+1]-z_connus[i])
			"print z, '     ', z_connus[i], '      ',z_connus[i+1],'       ', EBL_min,'       ', EBL_max,'       ', EBL"
	dn_over_de=EBL/(e**4)
	print dn_over_de, '     ',sigma,'     ', e, '      ',z,'       ', EBL
	print sigma*dn_over_de
	return sigma*dn_over_de
"""

def troisieme_inte_log(e, E_gamma, z, coeffs):
	sigma=cross_section(E_gamma,10**e,z)
	e_reel=mec2*10**e
	if ((e_reel<2e-3) or (e_reel>10)):
		EBL=0.
		print plop
	else:
		for i in range(0, len(z_connus)-1):
			if(z==z_connus[i]):
				poly=np.poly1d(coeffs["%s" %z_connus[i]])
				EBL=10.**poly(np.log10(e_reel))
			elif(z==z_connus[i+1]):
				poly=np.poly1d(coeffs["%s" %z_connus[i+1]])
				EBL=10.**poly(np.log10(e_reel))
			elif((z>z_connus[i])&(z<z_connus[i+1])):
				poly_max=np.poly1d(coeffs["%s" %z_connus[i+1]])
				EBL_max=10.**poly_max(np.log10(e_reel))
				poly_min=np.poly1d(coeffs["%s" %z_connus[i]])
				EBL_min=10.**poly_min(np.log10(e_reel))
				EBL=EBL_min+(EBL_max-EBL_min)*(z-z_connus[i])/(z_connus[i+1]-z_connus[i])
				"print EBL,'         ', mec2*10**(e)"
				"print z, '     ', z_connus[i], '      ',z_connus[i+1],'       ', EBL_min,'       ', EBL_max,'       ', EBL"
	dn_over_de=EBL/((10**e)**3)
	"""print dn_over_de, '     ',sigma,'     ', e, '      ',z,'       ', EBL
	print sigma*dn_over_de*(10**e)"""
	return sigma*dn_over_de

def troisieme_inte_lin(e, E_gamma, z, coeffs):
	sigma=cross_section(E_gamma,e,z)
	e_reel=mec2*e
	if ((e_reel<2e-3) or (e_reel>10)):
		EBL=0.
		print plop
	else:
		for i in range(0, len(z_connus)-1):
			if(z==z_connus[i]):
				poly=np.poly1d(coeffs["%s" %z_connus[i]])
				EBL=10.**poly(np.log10(e_reel))
			elif(z==z_connus[i+1]):
				poly=np.poly1d(coeffs["%s" %z_connus[i+1]])
				EBL=10.**poly(np.log10(e_reel))
			elif((z>z_connus[i])&(z<z_connus[i+1])):
				poly_max=np.poly1d(coeffs["%s" %z_connus[i+1]])
				EBL_max=10.**poly_max(np.log10(e_reel))
				poly_min=np.poly1d(coeffs["%s" %z_connus[i]])
				EBL_min=10.**poly_min(np.log10(e_reel))
				EBL=EBL_min+(EBL_max-EBL_min)*(z-z_connus[i])/(z_connus[i+1]-z_connus[i])
				"print EBL,'         ', mec2*10**(e)"
				"print z, '     ', z_connus[i], '      ',z_connus[i+1],'       ', EBL_min,'       ', EBL_max,'       ', EBL"
	dn_over_de=EBL/(e**4)
	"""print dn_over_de, '     ',sigma,'     ', e, '      ',z,'       ', EBL
	print sigma*dn_over_de*(10**e)"""
	return sigma*dn_over_de

def premiere_inte(z, E_gamma, coeffs):
	e_th=1/(E_gamma*(1+z))
	if e_th> 2.e-3/mec2:
		lim_inf=e_th
	else:
		lim_inf= 2.e-3/mec2
	if z<0.9:
		res, err=integrate.quad(troisieme_inte_lin, lim_inf,10./mec2, args=(E_gamma, z, coeffs))
	else:
		res, err=integrate.quad(troisieme_inte_log, np.log10(lim_inf), np.log10(10./mec2), args=(E_gamma, z, coeffs))
	"print res, '          ', z"
	return res*np.sqrt((((1.+z)**3.)*omega_m+omega_lambda))/(100.*h_hubble*(1.+z)*(1.+z)*(1.+z)*(1+z))


#finke
z_connus=[0.0,0.2,0.5,1.0,1.5,2.0,3.0,4.0]
coeffs={}
for i in z_connus:
	nom_fichier=('EBL_finke_redshifts_%s.txt' %i)
	log_nu_i=np.log10(1.e-9*4*np.pi*np.loadtxt(nom_fichier,delimiter=', ', usecols = (1,) )/(c*1.6e-19))
	log_e=np.log10(np.loadtxt(nom_fichier,delimiter=', ', usecols = (0,) ))
	pl.plot(log_e, log_nu_i)
	coeffs["{0}".format(i)] = np.polyfit(log_e, log_nu_i, 20)
	poly = np.poly1d(coeffs["%s" %i])
	x=np.log10(np.logspace(-2.8,1))
	fit=poly(x)
	pl.plot(x, fit,ls='--')
	print i
pl.show()
Tau={}
E=np.logspace(-1.5, 2, num=20, endpoint=True, base=10.0)
for z_max in z_liste:
	depth=[]
	for x in E:
		tau, err=integrate.quad(premiere_inte, 0., z_max, args=(x*1.e12/mec2, coeffs))
		e_gamma=x*1.e12/mec2
		depth.append(tau*c*np.pi*re*re*3.086e19/(e_gamma*e_gamma*mec2))
		print x,'       ', z_max
	Tau["{0}".format(z_max)]= depth
	depth=[]
	pl.loglog(E, Tau["{0}".format(z_max)], 'bo')
	"pl.loglog(E, depth, 'bo')"




"""a=[0.5,1,2,3]
energie={}
tau={}
fit={}
ener={}
t={}
coefficients={}
logfit={}
for i in a:
	nom_fichier='Finke_tau_%s.txt' %i
	energie["{0}".format(i)]=np.log10(np.loadtxt(nom_fichier,delimiter=', ', usecols = (0,) ))
	ener["{0}".format(i)]=np.loadtxt(nom_fichier,delimiter=', ', usecols = (0,) )
	tau["{0}".format(i)]=np.log10(np.loadtxt(nom_fichier,delimiter=', ', usecols = (1,) ))
	t["{0}".format(i)]=np.loadtxt(nom_fichier,delimiter=', ', usecols = (1,) )
	pl.loglog(ener["%s" %i],t["%s" %i])
pl.show()"""

a=[0.003,0.01,0.03,0.1,0.3,0.5,1,1.5,2,2.5,3,4]
energie={}
tau={}
fit={}
ener={}
t={}
coefficients={}
logfit={}
for i in a:
	nom_fichier='Simu-absorption_%s.txt' %i
	energie["{0}".format(i)]=np.log10(np.loadtxt(nom_fichier,delimiter=', ', usecols = (0,) ))
	ener["{0}".format(i)]=np.loadtxt(nom_fichier,delimiter=', ', usecols = (0,) )
	tau["{0}".format(i)]=np.log10(np.loadtxt(nom_fichier,delimiter=', ', usecols = (1,) ))
	t["{0}".format(i)]=np.loadtxt(nom_fichier,delimiter=', ', usecols = (1,) )
	pl.loglog(ener["%s" %i],t["%s" %i])
pl.show()

for z_max in z_liste:
	log_E=[np.log10(i) for i in E]
	log_Tau=[np.log10(i) for i in Tau["{0}".format(z_max)]]
	pl.plot(log_E, log_Tau)
	coeffs["{0}".format(i)] = np.polyfit(log_E, log_Tau, 10)
	poly = np.poly1d(coeffs["%s" %i])
	x=np.log10(np.logspace(-1,2))
	fit=poly(x)
	pl.plot(x, fit,ls='--')
	print coeffs["{0}".format(i)]
pl.show()
