import numpy as np
from matplotlib import pyplot as plt
import sys
import glob, os

redshifts = []
filenames = []
for file in glob.glob("data/*.dat"):
    redshifts.append(float(file[23:27]))
    filenames.append(file)
    
zvals = np.array(redshifts)

deg_max = 11

ncol = len(redshifts)
 

 
for i in range(1,ncol):
    
    data = np.loadtxt(filenames[i])
    
    energies = data[:,0]
    
    logenergies = np.log10(energies)
    logtau = np.log10(data[:,1])
    logenergies_tmp = []
    logtau_tmp = []
    
    # clean for fit:
    for j in range(len(logenergies)):
        if logtau[j] > -6:
            if (j>0 and logtau[j] <= logtau[j-1]):
                continue
            logenergies_tmp.append(logenergies[j])
            logtau_tmp.append(logtau[j])
            
    logenergies = np.array(logenergies_tmp)
    logtau = np.array(logtau_tmp)

    degrees = np.flip(np.arange(1,deg_max + 1,2),0)
 
    for degree in degrees:
        coeff = np.polyfit(logenergies, logtau, degree)
        coeffder = np.polyder(coeff)
        is_root = False
        for number in np.roots(coeffder):
            if (abs(number.imag) < 1e-8):
                is_root = True
         
        if not is_root:
            e_vals = np.linspace(logenergies[0], logenergies[-1], 100)
            plt.plot(e_vals, np.polyval(coeff, e_vals), color='r')
            #plt.plot(e_vals, np.polyval(coeffder, e_vals), color='g')
            plt.scatter(logenergies, logtau)
            break
         
    #print "degree =",degree
    while (len(coeff) < deg_max + 1):
        coeff = np.insert(coeff,0, 0.)
    
    if (degree < 7): 
        print "degree =",degree
        sys.exit()
     
    string = '%.6f, ' %(zvals[i])
    sys.stdout.write(string)
    sys.stdout.flush()
    for p in range(deg_max + 1):
        if not (p==deg_max): string = '%.8f, ' %(coeff[p])
        else: string = '%.8f' %(coeff[p])
        sys.stdout.write(string)
        sys.stdout.flush()
    sys.stdout.write('\n')
    #plt.show()

