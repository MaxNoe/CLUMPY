import numpy as np
from matplotlib import pyplot as plt
import sys


data = np.loadtxt('tau_gg_baseline.dat')
data = np.loadtxt('tau_gg_low_pop3.dat')
#data = np.loadtxt('tau_gg_up_pop3.dat')

deg_max = 33

zvals_str = '0.01 0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 4 4.1 4.2 4.3 4.4 4.5 4.6 4.7 4.8 4.9 5 5.1 5.2 5.3 5.4 5.5 5.6 5.7 5.8 5.9 6 6.1 6.2 6.3 6.4 6.5 6.6 6.7 6.8 6.9 7 7.1 7.2 7.3 7.4 7.5 7.6 7.7 7.8 7.9 8 8.1 8.2 8.3 8.4 8.5 8.6 8.7 8.8 8.9 9 9.1 9.2 9.3 9.4 9.5 9.6 9.7 9.8 9.9 10' 

zvals = zvals_str.split(' ')
zvals = [float(i) for i in zvals]

ncol = len(zvals)


energies = data[:,0]

for i in range(ncol):
    
    degrees = np.flip(np.arange(1,deg_max + 1,2),0)

    logenergies = np.log10(data[:,0])  # MeV to TeV
    logtau = np.log10(data[:,i + 1])
    

    logenergies_tmp = []
    logtau_tmp = []
    
    # clean for fit:
    for j in range(len(logenergies)):
        if logtau[j] > -6 and logtau[j] < 20:
            if (j>0 and logtau[j] <= logtau[j-1]):
                continue
            logenergies_tmp.append(logenergies[j])
            logtau_tmp.append(logtau[j])
            
    logenergies = np.array(logenergies_tmp)
    logtau = np.array(logtau_tmp)

 
    for degree in degrees:
        coeff = np.polyfit(logenergies, logtau, degree)
        coeffder = np.polyder(coeff)
        is_root = False
        for number in np.roots(coeffder):
            if (abs(number.imag) < 1e-8):
                is_root = True
        
        if not is_root:
            e_vals = np.linspace(logenergies[0], logenergies[-1], 100)
            break
        
    #print "degree =",degree
    while (len(coeff) < deg_max + 1):
        coeff = np.insert(coeff,0, 0.)
    
    if (degree < 15): 
            #print "degree =",degree
            continue
    
    plt.plot(e_vals, np.polyval(coeff, e_vals), color='r')
    plt.scatter(logenergies, logtau)

    string = '%.6f, ' %(zvals[i])
    sys.stdout.write(string)
    sys.stdout.flush()
    for p in range(deg_max + 1):
        if not (p==deg_max): string = '%.16f, ' %(coeff[p])
        else: string = '%.16f' %(coeff[p])
        sys.stdout.write(string)
        sys.stdout.flush()
    sys.stdout.write('\n')
    plt.show()

