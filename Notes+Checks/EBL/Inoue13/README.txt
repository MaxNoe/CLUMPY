Tables in this folder contains the data for the extragalactic background light flux and the optical depth of gamma-rays for the baseline model, the lower-Pop-III model, and the upper-Pop-III model presented in Inoue et al. (2012)

EBL Flux @ z=0 Data:
File names are "EBL_z_0_xxxx.dat".
1st column is the observed energy in the unit of [um].
2nd column is the EBL intensity from ALL stars in the unit of [nW/m^2/str].
3rd column is the EBL intensity from POP-III stars in the unit of [nW/m^2/str].

Proper EBL Flux Data:
File names are "EBL_proper_xxxx.dat".
1st column is the rest frame energy in the unit of [eV].
2nd-109th columns are the proper EBL flux density at the redshifts listed in the header in the unit of [1/cm^3].

Optical Depth tables:
File names are "tau_gg_xxxx.dat".
1st column is  the observed gamma-ray energy in the unit of [TeV].
2nd-109th columns are the optical depth (tau) for sources at the redshifts listed in the header.

"xxxx" corresponds to model name as follows. 
original Mitaka model -> xxxx = baseline
lower-Pop-III model -> xxxx = low_pop3
upper-Pop-III model -> xxxx = up_pop3.


If you have any queries, please feel free to ask me.

contact:
Yoshiyuki Inoue 
yinoue@slac.stanford.edu
KIPAC Stanford University
Tel: (650)926-3240
Fax: (650)926-8570
http://www.slac.stanford.edu/~yinoue

Last Update : 3/18/2013