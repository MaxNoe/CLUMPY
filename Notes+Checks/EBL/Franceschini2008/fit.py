import numpy as np
from matplotlib import pyplot as plt
import sys

deg_max = 11

data_file1 = 'Gamma-gamma-opacity-z=0-1.dat'
data_file2 = 'Gamma-gamma-opacity-z=1-2.dat'
data_file4 = 'Franceschini_papertable.txt'
data_file3 = 'Franceschini_z2.5.txt'

i_data = 0

for file in [data_file1, data_file2, data_file3, data_file4]:
    redshifts = []
    energies = []
    tau_matrix = []
    tau_current = []

    if (i_data < 2):
        with open(file) as f:
            # ignore up to the first line with "%%"
            for line in f:
                if "redshift sorgente" in line:
                    redshifts.append(float(line[23:36]))
                    if  (len(tau_current) > 0):
                        tau_matrix.append(tau_current)
                    tau_current = []
                elif "E0" in line:#
                    pass
                else:
                    line = line.strip( '\r\n' )
                    line = line.split(' ')
                    while '' in line:
                        line.remove('')
                    line = [float(i) for i in line]
                    if  (len(energies) > 0):
                        if (line[0] <= energies[-1]):
                            pass
                        else:
                            energies.append(line[0])
                    else:
                        energies.append(line[0])
                    tau_current.append(line[2])
        
        tau_matrix.append(tau_current)
        
        tau_matrix = np.array(tau_matrix)
        redshifts = np.array(redshifts)

        
    else:
        data = np.loadtxt(file)
        if (i_data == 2): 
            redshifts = np.array([2.5])
            energies = data[:,0]
            tau_matrix = np.transpose(data[:,1:])
        elif (i_data == 3):
            redshifts = np.array([3.])
            energies = data[:,0]
            tau_matrix = [np.transpose(data[:,-1])]
    
    nz = len(redshifts)
    for i in range(nz):
        
        logenergies = np.log10(energies)
        logtau = np.log10(tau_matrix[i])
        logenergies_tmp = []
        logtau_tmp = []
        
        # clean for fit:
        for j in range(len(logenergies)):
            if logtau[j] > -6:
                if (j>0 and logtau[j] <= logtau[j-1]):
                    continue
                logenergies_tmp.append(logenergies[j])
                logtau_tmp.append(logtau[j])
        logenergies = np.array(logenergies_tmp)
        logtau = np.array(logtau_tmp)
        
        degrees = np.flip(np.arange(1,deg_max + 1,2),0)
    
        for degree in degrees:
            coeff = np.polyfit(logenergies, logtau, degree)
            coeffder = np.polyder(coeff)
            is_root = False
            for number in np.roots(coeffder):
                if (abs(number.imag) < 1e-8):
                    is_root = True
            
            if not is_root:
                e_vals = np.linspace(logenergies[0], logenergies[-1], 100)
                #plt.plot(e_vals, np.polyval(coeff, e_vals), color='r')
                #plt.plot(e_vals, np.polyval(coeffder, e_vals), color='g')
                #plt.scatter(logenergies, logtau)
                break
            
        if (degree < 7): 
            print "degree =",degree
            sys.exit()
        while (len(coeff) < deg_max + 1):
            coeff = np.insert(coeff,0, 0.)
        
        string = '%.4f, ' %(redshifts[i])
        sys.stdout.write(string)
        sys.stdout.flush()
        for p in range(deg_max + 1):
            if not (p==deg_max): string = '%.8f, ' %(coeff[p])
            else: string = '%.8f' %(coeff[p])
            sys.stdout.write(string)
            sys.stdout.flush()
        sys.stdout.write('\n')
    i_data += 1
    #plt.show()

# add values from paper and table:




