import numpy as np
from matplotlib import pyplot as plt
import sys
import time

data = np.log10(np.loadtxt('tau_dominguez_finegridded/tau_dominguez11_cta.out'))
#data = np.log10(np.loadtxt('tau_dominguez_finegridded/tau_lower_uncertainties_dominguez11_cta.out'))
#data = np.log10(np.loadtxt('tau_dominguez_finegridded/tau_upper_uncertainties_dominguez11_cta.out'))
deg_max = 11

zvals = np.array([ 0.010, 0.020, 0.030, 0.040, 0.050, 0.060, 0.070, 0.080, 0.090, 0.100, 0.110, 0.120, 0.130, 0.140, 0.150, 0.160, 0.170, 0.180, 0.190, 0.200, 0.210, 0.220, 0.230, 0.240, 0.250, 0.260, 0.270, 0.280, 0.290, 0.300, 0.310, 0.320, 0.330, 0.340, 0.350, 0.360, 0.370, 0.380, 0.390, 0.400, 0.410, 0.420, 0.430, 0.440, 0.450, 0.460, 0.470, 0.480, 0.490, 0.500, 0.510, 0.520, 0.530, 0.540, 0.550, 0.560, 0.570, 0.580, 0.590, 0.600, 0.610, 0.620, 0.630, 0.640, 0.650, 0.660, 0.670, 0.680, 0.690, 0.700, 0.710, 0.720, 0.730, 0.740, 0.750, 0.760, 0.770, 0.780, 0.790, 0.800, 0.810, 0.820, 0.830, 0.840, 0.850, 0.860, 0.870, 0.880, 0.890, 0.900, 0.910, 0.920, 0.930, 0.940, 0.950, 0.960, 0.970, 0.980, 0.990, 1.000, 1.010, 1.020, 1.030, 1.040, 1.050, 1.060, 1.070, 1.080, 1.090, 1.100, 1.110, 1.120, 1.130, 1.140, 1.150, 1.160, 1.170, 1.180, 1.190, 1.200, 1.210, 1.220, 1.230, 1.240, 1.250, 1.260, 1.270, 1.280, 1.290, 1.300, 1.310, 1.320, 1.330, 1.340, 1.350, 1.360, 1.370, 1.380, 1.390, 1.400, 1.410, 1.420, 1.430, 1.440, 1.450, 1.460, 1.470, 1.480, 1.490, 1.500, 1.510, 1.520, 1.530, 1.540, 1.550, 1.560, 1.570, 1.580, 1.590, 1.600, 1.610, 1.620, 1.630, 1.640, 1.650, 1.660, 1.670, 1.680, 1.690, 1.700, 1.710, 1.720, 1.730, 1.740, 1.750, 1.760, 1.770, 1.780, 1.790, 1.800, 1.810, 1.820, 1.830, 1.840, 1.850, 1.860, 1.870, 1.880, 1.890, 1.900, 1.910, 1.920, 1.930, 1.940, 1.950, 1.960, 1.970, 1.980, 1.990, 2.000, 2.010, 2.020, 2.030, 2.040, 2.050, 2.060, 2.070, 2.080, 2.090, 2.100, 2.110, 2.120, 2.130, 2.140, 2.150, 2.160, 2.170, 2.180, 2.190, 2.200, 2.210, 2.220, 2.230, 2.240, 2.250, 2.260, 2.270, 2.280, 2.290, 2.300, 2.310, 2.320, 2.330, 2.340, 2.350, 2.360, 2.370, 2.380, 2.390, 2.400, 2.410, 2.420, 2.430, 2.440, 2.450, 2.460, 2.470, 2.480, 2.490, 2.500, 2.510, 2.520, 2.530, 2.540, 2.550, 2.560, 2.570, 2.580, 2.590, 2.600, 2.610, 2.620, 2.630, 2.640, 2.650, 2.660, 2.670, 2.680, 2.690, 2.700, 2.710, 2.720, 2.730, 2.740, 2.750, 2.760, 2.770, 2.780, 2.790, 2.800, 2.810, 2.820, 2.830, 2.840, 2.850, 2.860, 2.870, 2.880, 2.890, 2.900, 2.910, 2.920, 2.930, 2.940, 2.950, 2.960, 2.970, 2.980, 2.990, 3.000, 3.010, 3.020, 3.030, 3.040, 3.050, 3.060, 3.070, 3.080, 3.090, 3.100, 3.110, 3.120, 3.130, 3.140, 3.150, 3.160, 3.170, 3.180, 3.190, 3.200, 3.210, 3.220, 3.230, 3.240, 3.250, 3.260, 3.270, 3.280, 3.290, 3.300, 3.310, 3.320, 3.330, 3.340, 3.350, 3.360, 3.370, 3.380, 3.390, 3.400, 3.410, 3.420, 3.430, 3.440, 3.450, 3.460, 3.470, 3.480, 3.490, 3.500, 3.510, 3.520, 3.530, 3.540, 3.550, 3.560, 3.570, 3.580, 3.590, 3.600, 3.610, 3.620, 3.630, 3.640, 3.650, 3.660, 3.670, 3.680, 3.690, 3.700, 3.710, 3.720, 3.730, 3.740, 3.750, 3.760, 3.770, 3.780, 3.790, 3.800, 3.810, 3.820, 3.830, 3.840, 3.850, 3.860, 3.870, 3.880, 3.890, 3.900, 3.910, 3.920, 3.930, 3.940, 3.950, 3.960, 3.970, 3.980, 3.990])

ncol = len(zvals)

print ncol


energies = data[:,0]
nrow = len(energies)
print nrow

for i in range(nrow):

    zvals_tmp = []
    data_tmp = []
    for j in range(len(data[i,1:])):
        if data[i,j+1] != -np.inf:
            zvals_tmp.append(zvals[j])
            data_tmp.append(data[i,j+1])
    plt.scatter(zvals_tmp, data_tmp)
    plt.xlabel('z')
    plt.ylabel('log10(tau)')
    label = 'log(E/TeV) = ' + str(data[i,0])
    plt.title(label)
    plt.xlim([3.5,4])
    plt.ylim([max(data_tmp) - 1,max(data_tmp) + 0.1])
    plt.show()

sys.exit()

for i in range(ncol):
    
    degrees = np.flip(np.arange(9,deg_max + 1,2),0)

    for degree in degrees:    
        # check for zeros:
        evals_tmp = []
        data_tmp = []
        for j in range(len(data[:,i+1])):
            if data[j,i+1] != -np.inf:
                evals_tmp.append(data[j,0])
                data_tmp.append(data[j,i+1])
        coeff = np.polyfit(evals_tmp, data_tmp, degree)
        coeffder = np.polyder(coeff)
        is_root = False
        for number in np.roots(coeffder):
            if (abs(number.imag) < 1e-8):
                is_root = True
        
        if not is_root:
            print "found monotone function!"
            break
            

    e_vals = np.linspace(energies[0], energies[-1] * 1.5, 100)
    #plt.plot(e_vals, np.polyval(coeff, e_vals), color='r')
    #plt.plot(e_vals, np.polyval(coeffder, e_vals), color='g')
    plt.scatter(evals_tmp, data_tmp)
    plt.xlabel('log10(E/TeV)')
    plt.ylabel('log10(tau)')
    label = 'z = ' + str(zvals[i])
    plt.title(label)
    plt.xlim([0,2.1])
    #plt.ylim([-1,2])
    
    #print "degree =",degree
    while (len(coeff) < deg_max + 1):
        coeff = np.insert(coeff,0, 0.)
    
    
    string = '%.6f, ' %(zvals[i])
    sys.stdout.write(string)
    sys.stdout.flush()
    for p in range(deg_max + 1):
        if not (p==deg_max): string = '%.8f, ' %(coeff[p])
        else: string = '%.8f' %(coeff[p])
        sys.stdout.write(string)
        sys.stdout.flush()
    sys.stdout.write('\n')
    #plt.show()
    #time.sleep(0.5)
    #plt.close('all')

