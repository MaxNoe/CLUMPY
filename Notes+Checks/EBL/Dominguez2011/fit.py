import numpy as np
from matplotlib import pyplot as plt
import sys


data = np.log10(np.loadtxt('tau_dominguez11.out'))
data = np.log10(np.loadtxt('tau_lower_uncertainties_dominguez11.out'))
data = np.log10(np.loadtxt('tau_upper_uncertainties_dominguez11.out'))
deg_max = 11

zvals = np.array([ 0.01, 0.02526316, 0.04052632, 0.05578947, 0.07105263, 0.08631579, 0.10157895, 0.11684211, 0.13210526,  0.14736842,    0.16263158, 0.17789474,  0.19315789,  0.20842105,  0.22368421,        0.23894737, 0.25421053,  0.26947368,  0.28473684, 0.3 ,  0.35,  0.4 , 0.45,  0.5 ,  0.55,  0.6 ,  0.65,  0.7 ,  0.75,  0.8 ,  0.85,  0.9 ,   0.95,  1.,1.2,1.4,1.6,1.8,2.])

ncol = len(zvals)
print ncol

energies = data[:,0]

for i in range(ncol):
    
    degrees = np.flip(np.arange(1,deg_max + 1,2),0)

    for degree in degrees:
        coeff = np.polyfit(data[:,0], data[:,i+1], degree)
        coeffder = np.polyder(coeff)
        is_root = False
        for number in np.roots(coeffder):
            if (abs(number.imag) < 1e-8):
                is_root = True
        
        if not is_root:
            e_vals = np.linspace(energies[0], energies[-1], 100)
            plt.plot(e_vals, np.polyval(coeff, e_vals), color='r')
            #plt.plot(e_vals, np.polyval(coeffder, e_vals), color='g')
            plt.scatter(data[:,0], data[:,i+1])
            break
        
    #print "degree =",degree
    while (len(coeff) < deg_max + 1):
        coeff = np.insert(coeff,0, 0.)
    
    
    string = '%.6f, ' %(zvals[i])
    sys.stdout.write(string)
    sys.stdout.flush()
    for p in range(deg_max + 1):
        if not (p==deg_max): string = '%.8f, ' %(coeff[p])
        else: string = '%.8f' %(coeff[p])
        sys.stdout.write(string)
        sys.stdout.flush()
    sys.stdout.write('\n')
    #plt.show()

