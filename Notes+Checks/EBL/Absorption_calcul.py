import numpy as np
import matplotlib.pyplot as pl
import math
from scipy.optimize import curve_fit
import scipy.integrate as integrate
from scipy import interpolate
from operator import add,sub

h=6.67e-34   			 #Planck constant (J.s)
c=3.e8      			 #speed of light (m/s)
sigma_thomson=6.65e-29 		 #Thomson cross section (m^2)
m_e=9.1e-31  			 # electron's mass (kg)
h_hubble=0.7  			 #Hubbles'constant
z_liste=[0.003,0.01,0.03,0.1,0.3,0.5,1,1.5,2]
omega_m=0.3   			 #Normalised matter quantity in the universe
omega_lambda=0.6   		 #Normalised dark energy quantity in the universe
nanowatt_to_watt=1.0e-9
ev_to_joule=1.6e-19
angstrom_to_m=1.0e-10
Mpc_to_km=3.086e19
TeV_to_eV=1.0e12



def troisieme_inte_log(e, mu, E_gamma, z, e_th, coeffs):	#log integration over the energy spectrum of the EBL
	beta=np.sqrt(1.-(10**e_th)/(10**e))
	sigma=(3.*sigma_thomson/16.)*(1.-beta*beta)*(2.*beta*(beta*beta-2.)+(3.-beta**4.)*np.log((1.+beta)/(1.-beta))) #cross section
	if ((e<np.log10(0.28e-2)) or (e>np.log10(10))):  		#If we are out of range of the EBL, the integral is null
		EBL=0.
	else:
		for i in range(0, len(z_connus)-1):
			if(z==z_connus[i]):
				poly=np.poly1d(coeffs["%s" %z_connus[i]])
				EBL=10.**poly(e)
			elif(z==z_connus[i+1]):
				poly=np.poly1d(coeffs["%s" %z_connus[i+1]])
				EBL=10.**poly(e)
			elif((z>z_connus[i])&(z<z_connus[i+1])):	#Linear interpolation between two known values
				poly_max=np.poly1d(coeffs["%s" %z_connus[i+1]])
				EBL_max=10.**poly_max(e)
				poly_min=np.poly1d(coeffs["%s" %z_connus[i]])
				EBL_min=10.**poly_min(e)
				EBL=EBL_min+(EBL_max-EBL_min)*(z-z_connus[i])/(z_connus[i+1]-z_connus[i])
	dn_over_de=4.*np.pi*EBL/(10**e)  				#conversion of the EBl into differential photon density, the c factor does not appear as it is suppressed later
	return sigma*dn_over_de


def troisieme_inte_lin(e, mu, E_gamma, z, e_th, coeffs): 	#linear integration over the energy spectrum of the EBL
	beta=np.sqrt(1.-e_th/e)
	sigma=(3.*sigma_thomson/16.)*(1.-beta*beta)*(2.*beta*(beta*beta-2.)+(3.-beta**4.)*np.log((1.+beta)/(1.-beta))) #cross section
	if ((e<2.8e-3) or (e>10)):  				#If we are out of range of the EBL, the integral is null
		EBL=0
	else:
		for i in range(0, len(z_connus)-1):
			if(z==z_connus[i]):
				poly=np.poly1d(coeffs["%s" %z_connus[i]])
				EBL=10.**poly(np.log10(e))
			elif(z==z_connus[i+1]):
				poly=np.poly1d(coeffs["%s" %z_connus[i+1]])
				EBL=10.**poly(np.log10(e))
			elif((z>z_connus[i])&(z<z_connus[i+1])):	#Linear interpolation between two known values
				poly_max=np.poly1d(coeffs["%s" %z_connus[i+1]])
				EBL_max=10.**poly_max(np.log10(e))
				poly_min=np.poly1d(coeffs["%s" %z_connus[i]])
				EBL_min=10.**poly_min(np.log10(e))
				EBL=EBL_min+(EBL_max-EBL_min)*(z-z_connus[i])/(z_connus[i+1]-z_connus[i])
	dn_over_de=4.*np.pi*EBL/(e*e)  				#conversion of the EBl into differential photon density, the c factor does not appear as it is suppressed later
	return sigma*dn_over_de



def deuxieme_inte(mu, E_gamma, z, coeffs):		#integration over the angle of collision, mu
	e_th=2.*((511.e3)**2.)/(E_gamma*(1.-mu)*(1.+z))
	if e_th>1:  					#linear integration if the energy range is small, logarithmic otherwise
		res2, err2=integrate.quad(troisieme_inte_lin, e_th, np.inf, args=(mu, E_gamma, z, e_th, coeffs), epsrel=0.01)
	else:
		res2, err2=integrate.quad(troisieme_inte_log, np.log10(e_th), np.log10(100), args=(mu, E_gamma, z, np.log10(e_th), coeffs), epsrel=0.01)
	return res2*(1.-mu)/2.
	

def premiere_inte(z, E_gamma, poly):			#integration over z
	res, err=integrate.quad(deuxieme_inte, -1., 1., args=(E_gamma, z, coeffs), epsrel=0.01)
	return res*np.sqrt(((1.+z)**3.)*omega_m+omega_lambda)*Mpc_to_km/(100.*h_hubble*(1.+z))


#Conversion of the EBL into required units and polynomial fitting
z_connus=[0,0.2,0.6,1,1.5,2,2.5] #List of redshift for which the EBL is given
coeffs={}
for i in z_connus:
	nom_fichier=('EBL_gilmore_redshift_%s.txt' %i)
	log_EBL=np.log10(nanowatt_to_watt*np.loadtxt(nom_fichier,delimiter=', ', usecols = (1,) )/ev_to_joule)   #carefull with unit conversion, results must be in eV.m^-2.sr^-1.s^-1, and in proper units
	log_e=np.log10(h*c/(ev_to_joule*angstrom_to_m*np.loadtxt(nom_fichier,delimiter=', ', usecols = (0,) )))#carefull with unit conversion, results must be in eV, and in proper units
	pl.plot(log_e, log_EBL)
	coeffs["{0}".format(i)] = np.polyfit(log_e, log_EBL, 18) #Fitting of the EBL to get a continuous expression
	poly = np.poly1d(coeffs["%s" %i])
	x=np.log10(np.logspace(-2.8,1))
	fit=poly(x)
	pl.plot(x, fit,ls='--')
pl.show()




#Calculation of tau
E=np.logspace(-1.5, 2, num=30, endpoint=True, base=10.0) #List of observed gamma ray energies for wich tau will be calculated, in TeV
Tau={}
for z_max in z_liste:
	depth=[]
	for x in E:
		tau, err=integrate.quad(premiere_inte, 0., z_max, args=(x*TeV_to_eV, coeffs), epsrel=0.01)
		depth.append(tau)
		print x,'       ', z_max
	Tau["{0}".format(z_max)]= depth
	depth=[]
	pl.loglog(E, Tau["{0}".format(z_max)], 'bo')



#Polynomial fitting of the absorptions calculated
for z_max in z_liste:
	log_E=[np.log10(i) for i in E]
	log_Tau=[np.log10(i) for i in Tau["{0}".format(z_max)]]
	pl.plot(log_E, log_Tau)
	coeffs["{0}".format(i)] = np.polyfit(log_E, log_Tau, 10)
	poly = np.poly1d(coeffs["%s" %i])
	x=np.log10(np.logspace(-1,2))
	fit=poly(x)
	pl.plot(x, fit,ls='--')
	print coeffs["{0}".format(i)]
pl.show()
