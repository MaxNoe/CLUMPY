import numpy as np
from matplotlib import pyplot as plt
import sys


data = np.loadtxt('opdep_fiducial.dat')
#data = np.loadtxt('opdep_fixed.dat')

deg_max = 13

zvals = np.array([0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.10,0.11,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1.0,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.6,2.8,3.0,3.2,3.4,3.6,3.8,4.0,4.2,4.4,4.6,4.8,5.0,5.5,6.0,6.5,7.0,7.5,8.0,8.5,9.0])
ncol = len(zvals)


energies = data[:,0]

for i in range(ncol):
    
    degrees = np.flip(np.arange(1,deg_max + 1,2),0)

    logenergies = np.log10(data[:,0]) - 6 # MeV to TeV
    logtau = np.log10(data[:,i + 1])
        
    logenergies_tmp = []
    logtau_tmp = []
    
    # clean for fit:
    for j in range(len(logenergies)):
        if logtau[j] > -6 and logtau[j] < 20:
            if (j>0 and logtau[j] <= logtau[j-1]):
                continue
            logenergies_tmp.append(logenergies[j])
            logtau_tmp.append(logtau[j])
            
    logenergies = np.array(logenergies_tmp)
    logtau = np.array(logtau_tmp)

 
    for degree in degrees:
        coeff = np.polyfit(logenergies, logtau, degree)
        coeffder = np.polyder(coeff)
        is_root = False
        for number in np.roots(coeffder):
            if (abs(number.imag) < 1e-8):
                is_root = True
        
        if not is_root:
            e_vals = np.linspace(logenergies[0], logenergies[-1], 100)
            plt.plot(e_vals, np.polyval(coeff, e_vals), color='r')
            #plt.plot(e_vals, np.polyval(coeffder, e_vals), color='g')
            plt.scatter(logenergies, logtau)
            break
        
    #print "degree =",degree
    while (len(coeff) < deg_max + 1):
        coeff = np.insert(coeff,0, 0.)
    
    if (degree < 7): 
        print "degree =",degree
        sys.exit()
    
    
    string = '%.6f, ' %(zvals[i])
    sys.stdout.write(string)
    sys.stdout.flush()
    for p in range(deg_max + 1):
        if not (p==deg_max): string = '%.8f, ' %(coeff[p])
        else: string = '%.8f' %(coeff[p])
        sys.stdout.write(string)
        sys.stdout.flush()
    sys.stdout.write('\n')
    #plt.show()

