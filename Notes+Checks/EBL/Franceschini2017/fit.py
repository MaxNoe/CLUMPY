import numpy as np
from matplotlib import pyplot as plt
import sys


data = np.loadtxt('Franceschini_papertable.txt')


deg_max = 11

zvals = np.array([0.01, 0.03,     0.1,       0.3,       0.5,       1.0,    1.5,    2.0,    3.0])

ncol = len(zvals)


energies = data[:,0]

for i in range(ncol):
    
    degrees = np.flip(np.arange(1,deg_max + 1,2),0)

    logenergies = np.log10(data[:,0])  # MeV to TeV
    logtau = np.log10(data[:,i + 1])
    

    logenergies_tmp = []
    logtau_tmp = []
    
    # clean for fit:
    for j in range(len(logenergies)):
        if logtau[j] > -6 and logtau[j] < 20:
            if (j>0 and logtau[j] <= logtau[j-1]):
                continue
            logenergies_tmp.append(logenergies[j])
            logtau_tmp.append(logtau[j])
            
    logenergies = np.array(logenergies_tmp)
    logtau = np.array(logtau_tmp)

 
    for degree in degrees:
        coeff = np.polyfit(logenergies, logtau, degree)
        coeffder = np.polyder(coeff)
        is_root = False
        for number in np.roots(coeffder):
            if (abs(number.imag) < 1e-8):
                is_root = True
        
        if not is_root:
            e_vals = np.linspace(logenergies[0], logenergies[-1], 100)
            plt.plot(e_vals, np.polyval(coeff, e_vals), color='r')
            #plt.plot(e_vals, np.polyval(coeffder, e_vals), color='g')
            plt.scatter(logenergies, logtau)
            break
        
    #print "degree =",degree
    while (len(coeff) < deg_max + 1):
        coeff = np.insert(coeff,0, 0.)
    
    if (degree < 7): 
            print "degree =",degree
            #sys.exit()
    
    string = '%.6f, ' %(zvals[i])
    sys.stdout.write(string)
    sys.stdout.flush()
    for p in range(deg_max + 1):
        if not (p==deg_max): string = '%.8f, ' %(coeff[p])
        else: string = '%.8f' %(coeff[p])
        sys.stdout.write(string)
        sys.stdout.flush()
    sys.stdout.write('\n')
    #plt.show()

