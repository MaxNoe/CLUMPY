\section*{Calculating the structural parameters of clumps for arbitrary $M_{\Delta}$ definition}

\subsection*{Option 1}

The following input parameters are given (e.g., from kinematical Jeans-analysis of dSph galaxies):
\begin{itemize}
\item DM profile $\rho(r;\,r_s,\,\rho_s)$.
\item Scale parameter $r_s$.
\item Density normalization $\rho_s:= \rho(r_s)$.
\item Defining the outer boundary of the clump by the 
\begin{itemize}
\item overdensity factor $\Delta$ \textit{or} 
\item an explicit radius $R_{\mathrm max}$ (from which a corresponding $\Delta$, $R_{\mathrm max}=R_{\Delta}$) follows).
\end{itemize}
\end{itemize}
With these quantities given, it is straightforward to calculate $M_{\Delta}$ and $R_{\Delta}$, and all relevant clump properties are known. A conversion of $M_{\Delta}$, $R_{\Delta}$ into $M_{\Delta'}$ and $R_{\Delta'}$ is also easily possible with the given structural parameters.

\subsection*{Option 2}

The following input parameters are given (e.g., from numerical N-body simulations):
\begin{itemize}
\item DM profile $\rho(r;\,r_s,\,\rho_s)$.
\item Mass $M_{\Delta_1}$ and the corresponding $\Delta_1$.
\item A $c_{\Delta}-M_{\Delta}$ relationship for a corresponding $\Delta_2$, $c_{\Delta_2}:=\frac{R_{\Delta_2}}{r_{-2}}(M_{\Delta_2})$
\end{itemize}
From these input quantities, it is then desired to derive the structural parameters of a clump,
\begin{enumerate}
\item $R_{\Delta_1}(\text{profile},\, M_{\Delta_1},\, c_{\Delta_2})=R_{\Delta_1}(M_{\Delta_1})$.
\item $\quad r_{s}(\text{profile},\, M_{\Delta_1},\, c_{\Delta_2})$.
\item $\quad\rho_{s}(\text{profile},\, M_{\Delta_1},\, c_{\Delta_2})$.
\end{enumerate}
from which also follows $c_{\Delta_1}=\frac{R_{\Delta_1}}{r_{s}}\times\frac{r_s}{r_{-2}}(\text{profile})$.

\paragraph{Step 0.} For the subsequent calculations, the value
\begin{align}
a:= \frac{r_s}{r_{-2}} = \frac{r_s}{r_{-2}}(\text{profile})
\end{align}
is calculated. It is only a function of the DM profile (already implemented in \clumpy{}).

\paragraph{Step 1, $R_{\Delta_1}$.} The clump radius $R_{\Delta_1}$ can be directly calculated as
\begin{align}
R_{\Delta_1}(M_{\Delta_1},\,\Delta_1) = \left(\frac{3\,M_{\Delta_1}}{4\pi\,\Delta_1\,\rho_{\mathrm{crit}}}\right)^{\frac{1}{3}}
\end{align}

\paragraph{Step 1a, $M_{\Delta_1},\,M_{\Delta_2}$ conversion.} For a given $c_{\Delta_2}:=\frac{R_{\Delta_2}}{r_{-2}}(M_{\Delta_2})$ relation, it is possible to convert a given $M_{\Delta_1}$ into $M_{\Delta_2}$, and via  $M_{\Delta_2}$, also to an arbitrary  $M_{\Delta_3}$:

It is 
\begin{align}
M_{\Delta} = \frac{4\pi}{3}\,R_{\Delta}&^3\times\Delta\times \rho_{\mathrm{crit}}\\[0.1cm]
R_{\Delta} &= \frac{R_{\Delta} }{r_{-2}}\times \frac{r_{-2}}{r_s}\times r_s\\
&= \frac{c_{\Delta}\,r_s}{a}\\[0.1cm]
\Rightarrow M_{\Delta} = \frac{4\pi}{3}\,c_{\Delta}^3\times&\frac{r_s^3}{a^3}\times\Delta\times \rho_{\mathrm{crit}}
\end{align}
\begin{align}
\Rightarrow \quad\boxed{\frac{M_{\Delta_1}}{M_{\Delta_2}} = \frac{c_{\Delta_1}^3}{c_{\Delta_2}^3}\times \frac{\Delta_1}{\Delta_2}\;.
}
\label{eq:m1m2_1}
\end{align}
At the same time, it is also
\begin{align}
M_{\Delta} = 4 \pi \int\limits_0^{R_{\Delta}}r^2\,\rho(r;\,r_s,\,\rho_s)\,\dd r.
\end{align}
Here, $\rho(r;\,r_s,\,\rho_s)$ has the form of $\rho(r;\,r_s,\,\rho_s) = \rho_s\times \widetilde{\rho}(r/r_s)= \rho_s\times \widetilde{\rho}(x/a)$ with $x:= r/r_{-2}$. With this, one obtains
\begin{align}
M_{\Delta} = 4 \pi \frac{r_s^2}{a^2}\rho_s \int\limits_0^{c_{\Delta}}x^2\, \widetilde{\rho}(x/a)\,\dd x
\end{align}
and subsequently,
\begin{align}
\Rightarrow \quad\boxed{\frac{M_{\Delta_1}}{M_{\Delta_2}} = \frac{\int\limits_0^{c_{\Delta_1}}x^2\, \widetilde{\rho}(x/a)\,\dd x}{\int\limits_0^{c_{\Delta_2}}x^2\, \widetilde{\rho}(x/a)\,\dd x}\;.
}
\label{eq:m1m2_2}
\end{align}
Combining \autoref{eq:m1m2_1} and \autoref{eq:m1m2_2} results in an implicit equation for $M_{\Delta_2}$ for known  $M_{\Delta_1}$ (for $M_{\Delta_1}$ for known  $M_{\Delta_2}$) and known function $c_{\Delta_2}\left(M_{\Delta_2}\right)$ and  $\widetilde{\rho}(x/a)$:
\begin{align}
 \frac{M_{\Delta_1}}{M_{\Delta_2}} \times \frac{\int\limits_0^{c_{\Delta_2}\left(M_{\Delta_2}\right)}x^2\, \widetilde{\rho}(x/a)\,\dd x}{\int_0^{c_{\Delta_2}\left(M_{\Delta_2}\right)\times \left(\frac{M_{\Delta_1}}{M_{\Delta_2}}\times \frac{\Delta_2}{\Delta_1} \right)^{\frac{1}{3}}}x^2\, \widetilde{\rho}(x/a)\,\dd x}-1 = 0\;.
\label{eq:m1m2_3}
\end{align}

\paragraph{Step 2, $r_s$.} With known $M_{\Delta_2}$, it is straight forward to compute $r_s$:
\begin{align}
r_s = \left(\frac{3\,M_{\Delta_2}}{4\pi\,\Delta_2\,\rho_{\mathrm{crit}}}\right)^{\frac{1}{3}}\times \frac{a}{c_{\Delta_2}\left(M_{\Delta_2}\right)}
\end{align}

\paragraph{Step 3, $\rho_s$.}  With
\begin{align}
R_{\Delta_2} = \frac{r_s\times c_{\Delta_2}\left(M_{\Delta_2}\right)}{a}\,,
\end{align}
one can finally compute
\begin{align}
\rho_s  = \frac{M_{\Delta_2}}{4\pi \int\limits_0^{R_{\Delta_2}}r^2\, \widetilde{\rho}(r/r_s)\,\dd r}\,,
\end{align}




